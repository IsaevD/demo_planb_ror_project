# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160310121647) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_rights_d_access_rights", force: true do |t|
    t.integer  "system_user_role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "access_rights_d_access_rights", ["system_user_role_id"], name: "index_access_rights_d_access_rights_on_system_user_role_id", using: :btree

  create_table "access_rights_d_module_rights", force: true do |t|
    t.integer  "access_right_id"
    t.integer  "module_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "access_rights_d_module_rights", ["access_right_id"], name: "index_access_rights_d_module_rights_on_access_right_id", using: :btree
  add_index "access_rights_d_module_rights", ["module_item_id"], name: "index_access_rights_d_module_rights_on_module_item_id", using: :btree

  create_table "banner_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banners", force: true do |t|
    t.string   "name"
    t.integer  "banner_type_id"
    t.integer  "event_item_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "link"
    t.string   "position"
  end

  add_index "banners", ["banner_type_id"], name: "index_banners_on_banner_type_id", using: :btree
  add_index "banners", ["event_item_id"], name: "index_banners_on_event_item_id", using: :btree
  add_index "banners", ["system_state_id"], name: "index_banners_on_system_state_id", using: :btree

  create_table "blank_counters", force: true do |t|
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_item_id"
    t.integer  "blank_type_id"
  end

  create_table "blank_defects", force: true do |t|
    t.string   "number"
    t.integer  "ticket_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "system_user_id"
    t.integer  "blank_type_id"
    t.integer  "event_item_id"
    t.datetime "date"
    t.integer  "sale_point_item_id"
  end

  add_index "blank_defects", ["blank_type_id"], name: "index_blank_defects_on_blank_type_id", using: :btree
  add_index "blank_defects", ["event_item_id"], name: "index_blank_defects_on_event_item_id", using: :btree
  add_index "blank_defects", ["sale_point_item_id"], name: "index_blank_defects_on_sale_point_item_id", using: :btree
  add_index "blank_defects", ["system_user_id"], name: "index_blank_defects_on_system_user_id", using: :btree
  add_index "blank_defects", ["ticket_id"], name: "index_blank_defects_on_ticket_id", using: :btree

  create_table "blank_delivery_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blank_request_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blank_requests", force: true do |t|
    t.integer  "blank_delivery_type_id"
    t.integer  "blank_type_id"
    t.integer  "system_user_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "delivery_date"
    t.integer  "event_item_id"
    t.integer  "blank_request_type_id"
  end

  create_table "blank_stocks", force: true do |t|
    t.integer  "counter",        default: 0
    t.integer  "blank_type_id"
    t.integer  "event_item_id"
    t.integer  "system_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blank_stocks", ["blank_type_id"], name: "index_blank_stocks_on_blank_type_id", using: :btree
  add_index "blank_stocks", ["event_item_id"], name: "index_blank_stocks_on_event_item_id", using: :btree

  create_table "blank_transfers", force: true do |t|
    t.integer  "counter",       default: 0
    t.datetime "delivered_at"
    t.string   "state",         default: "initialized"
    t.integer  "blank_type_id"
    t.integer  "event_item_id"
    t.integer  "assignee_id"
    t.integer  "assignor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blank_transfers", ["blank_type_id"], name: "index_blank_transfers_on_blank_type_id", using: :btree
  add_index "blank_transfers", ["event_item_id"], name: "index_blank_transfers_on_event_item_id", using: :btree

  create_table "blank_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "coor_x"
    t.string   "coor_y"
  end

  add_index "cities", ["system_state_id"], name: "index_cities_on_system_state_id", using: :btree

  create_table "delivery_methods", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_images", force: true do |t|
    t.integer  "event_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_element_file_name"
    t.string   "image_element_content_type"
    t.integer  "image_element_file_size"
    t.datetime "image_element_updated_at"
  end

  add_index "event_images", ["event_item_id"], name: "index_event_images_on_event_item_id", using: :btree

  create_table "event_items", force: true do |t|
    t.string   "name"
    t.text     "message"
    t.text     "content"
    t.datetime "date"
    t.integer  "hall_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "description"
    t.datetime "published_at"
    t.string   "url"
  end

  add_index "event_items", ["hall_id"], name: "index_event_items_on_hall_id", using: :btree
  add_index "event_items", ["system_state_id"], name: "index_event_items_on_system_state_id", using: :btree

  create_table "event_tags", force: true do |t|
    t.integer  "event_item_id"
    t.integer  "event_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_tags", ["event_item_id"], name: "index_event_tags_on_event_item_id", using: :btree
  add_index "event_tags", ["event_type_id"], name: "index_event_tags_on_event_type_id", using: :btree

  create_table "event_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_types", ["system_state_id"], name: "index_event_types_on_system_state_id", using: :btree

  create_table "halls", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "schema_item_id"
  end

  add_index "halls", ["schema_item_id"], name: "index_halls_on_schema_item_id", using: :btree
  add_index "halls", ["system_state_id"], name: "index_halls_on_system_state_id", using: :btree

  create_table "kassir_counters", force: true do |t|
    t.integer  "system_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_item_id"
    t.integer  "blank_type_id"
    t.integer  "value"
  end

  create_table "modules_d_module_items", force: true do |t|
    t.string   "alias"
    t.string   "module_name"
    t.string   "name"
    t.text     "description"
    t.string   "git_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "money_stocks", force: true do |t|
    t.integer  "counter",        default: 0
    t.integer  "system_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "money_transfers", force: true do |t|
    t.integer  "counter",      default: 0
    t.datetime "delivered_at"
    t.string   "state",        default: "initialized"
    t.integer  "assignee_id"
    t.integer  "assignor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navigation_menu_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navigation_menus", force: true do |t|
    t.integer  "position"
    t.string   "name"
    t.string   "link"
    t.integer  "navigation_menu_id"
    t.integer  "navigation_menu_type_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "navigation_menus", ["navigation_menu_id"], name: "index_navigation_menus_on_navigation_menu_id", using: :btree
  add_index "navigation_menus", ["navigation_menu_type_id"], name: "index_navigation_menus_on_navigation_menu_type_id", using: :btree
  add_index "navigation_menus", ["system_state_id"], name: "index_navigation_menus_on_system_state_id", using: :btree

  create_table "news_images", force: true do |t|
    t.integer  "news_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_element_file_name"
    t.string   "image_element_content_type"
    t.integer  "image_element_file_size"
    t.datetime "image_element_updated_at"
  end

  add_index "news_images", ["news_item_id"], name: "index_news_images_on_news_item_id", using: :btree

  create_table "news_items", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "content"
    t.datetime "published_at"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "news_items", ["system_state_id"], name: "index_news_items_on_system_state_id", using: :btree

  create_table "partner_items", force: true do |t|
    t.string   "name"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "link"
  end

  add_index "partner_items", ["system_state_id"], name: "index_partner_items_on_system_state_id", using: :btree

  create_table "payment_kinds", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_methods", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_transactions", force: true do |t|
    t.integer  "status"
    t.integer  "price"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "invid"
  end

  create_table "payment_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", force: true do |t|
    t.string   "number"
    t.integer  "user_id"
    t.integer  "system_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_item_id"
    t.integer  "payment_type_id"
    t.string   "phone"
    t.string   "delivery_address"
    t.integer  "delivery_method_id"
    t.string   "fio"
    t.integer  "sale_point_item_id"
    t.integer  "payment_kind_id"
  end

  add_index "payments", ["delivery_method_id"], name: "index_payments_on_delivery_method_id", using: :btree
  add_index "payments", ["event_item_id"], name: "index_payments_on_event_item_id", using: :btree
  add_index "payments", ["payment_kind_id"], name: "index_payments_on_payment_kind_id", using: :btree
  add_index "payments", ["payment_type_id"], name: "index_payments_on_payment_type_id", using: :btree
  add_index "payments", ["sale_point_item_id"], name: "index_payments_on_sale_point_item_id", using: :btree
  add_index "payments", ["system_user_id"], name: "index_payments_on_system_user_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "promo_images", force: true do |t|
    t.integer  "promo_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_element_file_name"
    t.string   "image_element_content_type"
    t.integer  "image_element_file_size"
    t.datetime "image_element_updated_at"
  end

  add_index "promo_images", ["promo_item_id"], name: "index_promo_images_on_promo_item_id", using: :btree

  create_table "promo_items", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "content"
    t.datetime "published_at"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "beginning_at"
    t.datetime "ending_at"
  end

  add_index "promo_items", ["system_state_id"], name: "index_promo_items_on_system_state_id", using: :btree

  create_table "report_logs", force: true do |t|
    t.string   "alias"
    t.integer  "system_user_id"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "report_logs", ["system_user_id"], name: "index_report_logs_on_system_user_id", using: :btree

  create_table "request_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "requests", force: true do |t|
    t.string   "phone"
    t.text     "message"
    t.integer  "request_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  create_table "sale_point_images", force: true do |t|
    t.integer  "sale_point_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_element_file_name"
    t.string   "image_element_content_type"
    t.integer  "image_element_file_size"
    t.datetime "image_element_updated_at"
  end

  add_index "sale_point_images", ["sale_point_item_id"], name: "index_sale_point_images_on_sale_point_item_id", using: :btree

  create_table "sale_point_items", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "work_time"
    t.string   "lunch_time"
    t.string   "phone"
    t.string   "coor_x"
    t.string   "coor_y"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "city_id"
    t.boolean  "is_public"
  end

  add_index "sale_point_items", ["city_id"], name: "index_sale_point_items_on_city_id", using: :btree
  add_index "sale_point_items", ["system_state_id"], name: "index_sale_point_items_on_system_state_id", using: :btree

  create_table "schema_items", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "schema_type_id"
    t.integer  "hall_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "schema_items", ["hall_id"], name: "index_schema_items_on_hall_id", using: :btree
  add_index "schema_items", ["schema_type_id"], name: "index_schema_items_on_schema_type_id", using: :btree

  create_table "schema_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seat_item_kinds", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seat_item_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.string   "color"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seat_items", force: true do |t|
    t.integer  "row"
    t.integer  "number"
    t.integer  "sector_group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "schema_item_id"
    t.integer  "seat_item_type_id"
    t.integer  "seat_item_kind_id"
  end

  add_index "seat_items", ["schema_item_id"], name: "index_seat_items_on_schema_item_id", using: :btree
  add_index "seat_items", ["seat_item_kind_id"], name: "index_seat_items_on_seat_item_kind_id", using: :btree
  add_index "seat_items", ["seat_item_type_id"], name: "index_seat_items_on_seat_item_type_id", using: :btree
  add_index "seat_items", ["sector_group_id"], name: "index_seat_items_on_sector_group_id", using: :btree

  create_table "sector_groups", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "sector_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sectors", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "schema_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sectors", ["schema_item_id"], name: "index_sectors_on_schema_item_id", using: :btree

  create_table "settings", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "value"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["system_state_id"], name: "index_settings_on_system_state_id", using: :btree

  create_table "static_page_images", force: true do |t|
    t.integer  "static_page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_element_file_name"
    t.string   "image_element_content_type"
    t.integer  "image_element_file_size"
    t.datetime "image_element_updated_at"
  end

  add_index "static_page_images", ["static_page_id"], name: "index_static_page_images_on_static_page_id", using: :btree

  create_table "static_pages", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "content"
    t.string   "meta_keywords"
    t.text     "meta_description"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "non_removable",      default: false
  end

  add_index "static_pages", ["system_state_id"], name: "index_static_pages_on_system_state_id", using: :btree

  create_table "system_main_items_d_system_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_user_roles", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "description"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "system_main_items_d_system_user_roles", ["system_state_id"], name: "index_system_main_items_d_system_user_roles_on_system_state_id", using: :btree

  create_table "system_main_items_d_system_users", force: true do |t|
    t.string   "login"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_token"
    t.integer  "system_user_role_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "system_main_items_d_system_users", ["system_state_id"], name: "index_system_main_items_d_system_users_on_system_state_id", using: :btree
  add_index "system_main_items_d_system_users", ["system_user_role_id"], name: "index_system_main_items_d_system_users_on_system_user_role_id", using: :btree

  create_table "system_user_actions_log_d_actions_log_records", force: true do |t|
    t.integer  "system_user_id"
    t.integer  "log_record_state_id"
    t.datetime "action_date"
    t.integer  "module_item_id"
    t.integer  "entity_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_user_actions_log_d_log_record_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ticket_discounts", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "value"
    t.boolean  "is_percent"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_item_id"
    t.datetime "date"
    t.datetime "active_from"
  end

  add_index "ticket_discounts", ["event_item_id"], name: "index_ticket_discounts_on_event_item_id", using: :btree
  add_index "ticket_discounts", ["system_state_id"], name: "index_ticket_discounts_on_system_state_id", using: :btree

  create_table "ticket_reverts", force: true do |t|
    t.integer  "event_item_id"
    t.text     "text"
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ticket_reverts", ["event_item_id"], name: "index_ticket_reverts_on_event_item_id", using: :btree

  create_table "ticket_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: true do |t|
    t.integer  "price"
    t.integer  "ticket_state_id"
    t.integer  "event_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "seat_item_id"
    t.integer  "ticket_discount_id"
    t.integer  "payment_id"
    t.string   "number"
    t.integer  "blank_type_id"
    t.datetime "hold_state_expires_at"
    t.string   "reg_code"
    t.boolean  "is_registered",         default: false
  end

  add_index "tickets", ["blank_type_id"], name: "index_tickets_on_blank_type_id", using: :btree
  add_index "tickets", ["event_item_id"], name: "index_tickets_on_event_item_id", using: :btree
  add_index "tickets", ["seat_item_id"], name: "index_tickets_on_seat_item_id", using: :btree
  add_index "tickets", ["ticket_discount_id"], name: "index_tickets_on_ticket_discount_id", using: :btree
  add_index "tickets", ["ticket_state_id"], name: "index_tickets_on_ticket_state_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "discount_number"
    t.string   "password_digest"
    t.string   "user_remember_token"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_confirmation"
    t.boolean  "confirmed"
    t.string   "user_restore_password"
  end

  add_index "users", ["system_state_id"], name: "index_users_on_system_state_id", using: :btree

end
