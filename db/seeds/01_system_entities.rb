ModulesD::Engine.load_seed
SystemMainItemsD::Engine.load_seed
AccessRightsD::Engine.load_seed
SystemUserActionsLogD::Engine.load_seed

modules = {
    "index" => {
        :module_name => "admin",
        :name => "Главная страница",
        :description => "Предназначен для управления базовой страницы",
        :git_url => ""
    },
    "news_items" => {
        :module_name => "admin",
        :name => "Новости",
        :description => "Предназначен для управления новостями сайта",
        :git_url => ""
    },
    "promo_items" => {
        :module_name => "admin",
        :name => "Акции",
        :description => "Предназначен для управления акциями сайта",
        :git_url => ""
    },
    "halls" => {
        :module_name => "admin",
        :name => "Площадки",
        :description => "Предназначен для управления площадками мероприятий",
        :git_url => ""
    },
    "event_types" => {
        :module_name => "admin",
        :name => "Типы мероприятий",
        :description => "Предназначен для управления типами мероприятий",
        :git_url => ""
    },
    "event_items" => {
        :module_name => "admin",
        :name => "Мероприятия",
        :description => "Предназначен для управления мероприятиями",
        :git_url => ""
    },
    "banners" => {
        :module_name => "admin",
        :name => "Баннеры",
        :description => "Предназначен для управления баннерами",
        :git_url => ""
    },
    "sale_point_items" => {
        :module_name => "admin",
        :name => "Точки продаж",
        :description => "Предназначен для управления точками продаж",
        :git_url => ""
    },
    "cities" => {
        :module_name => "admin",
        :name => "Города",
        :description => "Предназначен для управления городами",
        :git_url => ""
    },
    "partner_items" => {
        :module_name => "admin",
        :name => "Партнеры",
        :description => "Предназначен для управления партнерами",
        :git_url => ""
    },
    "settings" => {
        :module_name => "admin",
        :name => "Настройки",
        :description => "Предназначен для управления настройками сайта",
        :git_url => ""
    },
    "requests" => {
        :module_name => "admin",
        :name => "Заявки с сайта",
        :description => "Предназначен для управления заявками на сайте",
        :git_url => ""
    },
    "static_pages" => {
        :module_name => "admin",
        :name => "Статические страницы",
        :description => "Предназначен для управления статическими страницами сайта",
        :git_url => ""
    },
    "navigation_menus" => {
        :module_name => "admin",
        :name => "Навигационное меню",
        :description => "Предназначен для управления навигационным меню сайта",
        :git_url => ""
    },
    "users" => {
        :module_name => "admin",
        :name => "Пользователи сайта",
        :description => "Предназначен для управления пользователями сайта",
        :git_url => ""
    },
    "tickets" => {
        :module_name => "admin",
        :name => "Билеты",
        :description => "Предназначен для управления билетами",
        :git_url => ""
    },
    "ticket_states" => {
        :module_name => "admin",
        :name => "Состояния билетов",
        :description => "Предназначен для управления статусами билетов",
        :git_url => ""
    },
    "ticket_discounts" => {
        :module_name => "admin",
        :name => "Скидки на билеты",
        :description => "Предназначен для управления скидками на билеты",
        :git_url => ""
    },
    "payments" => {
        :module_name => "admin",
        :name => "Заказы",
        :description => "Предназначен для управления заказы",
        :git_url => ""
    },
    "schemas" => {
        :module_name => "admin",
        :name => "Касса",
        :description => "Предназначен для покупкой/бронированием билетов в кассах",
        :git_url => ""
    },
    "advanced_schemas" => {
        :module_name => "admin",
        :name => "Управление билетами",
        :description => "Предназначен для управления состояниями билетов в кассах",
        :git_url => ""
    },
    "seat_item_types" => {
        :module_name => "admin",
        :name => "Ценовые группы",
        :description => "Предназначен для управления ценновыми группами на билеты",
        :git_url => ""
    },
    "blank_requests" => {
        :module_name => "admin",
        :name => "Движение бланков",
        :description => "Предназначен для управления движением бланков билетов",
        :git_url => ""
    },
    "blank_counters" => {
        :module_name => "admin",
        :name => "Счетчики бланков",
        :description => "Предназначен для управления счетчиками бланков",
        :git_url => ""
    },
    "blank_defects" => {
        :module_name => "admin",
        :name => "Брак",
        :description => "Предназначен для управления браками бланков",
        :git_url => ""
    },
    "blank_transfers" => {
        :module_name => 'admin',
        :name => 'Перевод бланков',
        :description => 'Предназначен для управления движением бланков билетов',
        :git_url => ''
    },
    "money_transfers" => {
        :module_name => 'admin',
        :name => 'Перевод денежных средств',
        :description => 'Предназначен для управления движением денежных средств',
        :git_url => ''
    },
    "reports" => {
        :module_name => "admin",
        :name => "Отчеты",
        :description => "Предназначен для управления отчетами",
        :git_url => ""
    },
    "ticket_reverts" => {
        :module_name => "admin",
        :name => "Возвраты",
        :description => "Предназначен для управления возвратами",
        :git_url => ""
    },
}
modules.each do |key, value|
  ModulesD::ModuleItem.where(:alias => key).first_or_create{ |mod| mod.attributes = value.merge(:alias => key) }
end
puts "Custom modules import success"
puts "*****"
# Импорт ролей пользователей
puts "--- System User Roles start import"
system_user_roles = {
    "root" => {
        :name => "Супер-пользователь",
        :description => "Имеет неограниченные права"
    },
    "administrator" => {
        :name => "Администратор",
        :description => "Управляет всеми модулями сайта по публикации контента и решения бизнес-задач"
    },
    "moderator" => {
        :name => "Модератор",
        :description => "Управляет модулями сайта по публикации контента"
    },
    "kassir" => {
        :name => "Кассир",
        :description => "Управляет модулями сайта по покупки билетов"
    },
    "kassir_major" => {
        :name => "Старший Кассир",
        :description => "Управляет модулями сайта по покупки и возврату билетов"
    },
    "kassir_administrator" => {
        :name => "Администратор кассиров",
        :description => "Управляет распределением бланков по кассирам"
    }
}
system_user_roles.each do |key, value|
  SystemMainItemsD::SystemUserRole.where(:alias => key).first_or_create do |role|
    role.attributes = value.merge(:alias => key, :system_state_id => SystemMainItemsD::SystemState.active_state.id)
  end
end
puts "+++ System User Roles import success"
puts "--- Access Rights start import"
access_rights = {
  SystemMainItemsD::SystemUserRole.find_by(:alias => "moderator").id => [
    ModulesD::ModuleItem.where(:alias => [
      'news_items', 'promo_items', 'sale_point_items', 'cities', 'banners', 'partner_items', 'static_pages', 'index'
    ]).ids
  ],
  SystemMainItemsD::SystemUserRole.find_by(:alias => "administrator").id =>
    ModulesD::ModuleItem.where(:alias => [
      'ticket_reverts', 'news_items', 'promo_items', 'sale_point_items', 'cities', 'banners', 'partner_items', 'static_pages', 'index', 'event_items', 'event_types', 'halls', 'schemas', 'advanced_schemas', 'tickets', 'ticket_states', 'ticket_discounts', 'seat_item_types', 'users', 'system_users', 'system_user_roles', 'access_rights', 'actions_log_records', 'payments', 'settings', 'requests', 'navigation_menus', 'blank_requests', 'blank_defects', 'blank_transfers', 'money_transfers'
    ]).ids,
  SystemMainItemsD::SystemUserRole.find_by(:alias => "root").id =>
    ModulesD::ModuleItem.where(:alias => [
      'ticket_reverts', 'news_items', 'promo_items', 'sale_point_items', 'cities', 'banners', 'partner_items', 'static_pages', 'index', 'event_items', 'event_types', 'halls', 'schemas', 'advanced_schemas', 'tickets', 'ticket_states', 'ticket_discounts', 'seat_item_types', 'users', 'system_users', 'system_user_roles', 'access_rights', 'actions_log_records', 'payments', 'settings', 'requests', 'navigation_menus', 'blank_requests', 'blank_counters', 'blank_transfers', 'money_transfers'
    ]).ids,
  SystemMainItemsD::SystemUserRole.find_by(:alias => "kassir").id =>
      ModulesD::ModuleItem.where(:alias => ['index', 'schemas', 'advanced_schemas', 'blank_requests', 'blank_transfers', 'reports', 'money_transfers']).ids,
  SystemMainItemsD::SystemUserRole.find_by(:alias => "kassir_major").id =>
      ModulesD::ModuleItem.where(:alias => ['index', 'schemas', 'advanced_schemas', 'blank_requests', 'blank_transfers', 'reports', 'money_transfers']).ids,
  SystemMainItemsD::SystemUserRole.find_by(:alias => "kassir_administrator").id =>
      ModulesD::ModuleItem.where(:alias => ['ticket_reverts', 'index', 'blank_requests', 'blank_counters', 'reports', 'blank_defects', 'payments', 'blank_transfers', 'money_transfers']).ids
}
access_rights.each do |key, value|
  access_right_id = AccessRightsD::AccessRight.where(:system_user_role_id => key).first_or_create.id

  value.each do |item|
    AccessRightsD::ModuleRight.where(:access_right_id  => access_right_id, :module_item_id => item).first_or_create
  end
end
puts "+++ Access Rights import success"
puts "*****"
# Импорт статичных страниц
static_pages = {
    "about_us" => {
        :name => "О нас",
        :non_removable => true,
    },
    "where_buy_tickets" => {
        :name => "Где и как купить билеты",
        :non_removable => true,
    },
    "help_to_choose_event" => {
        :name => "Помощь в выборе мероприятия",
        :non_removable => true,
    },
    "help_to_personal_page" => {
        :name => "Личный кабинет",
        :non_removable => true,
    },
    "certificate" => {
        :name => "Сертификаты",
        :non_removable => true,
    },
    "success_internet_payment" => {
        :name => "Успешное завершение оплаты через интернет",
        :non_removable => true,
    },
    "fail_internet_payment" => {
        :name => "Неуспешное завершение оплаты через интернет",
        :non_removable => true,
    },
}
static_pages.each do |key, value|
    if (StaticPage.find_by(:alias => key).nil?)
        page = StaticPage.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id })
        unless value[:non_removable].nil?
            page.non_removable = value[:non_removable]
        end
        page.save
    end
end
puts "Static pages import success"

# Импорт типов меню
menu_types = {
    "all" => {
        :name => "Везде"
    },
    "desktop" => {
        :name => "Только в полноэкранной версии"
    },
    "mobile" => {
        :name => "Только в мобильной версии"
    }
}
menu_types.each do |key, value|
    if (NavigationMenuType.find_by(:alias => key).nil?)
        NavigationMenuType.new({ :alias => key, :name => value[:name]}).save
    end
end
puts "Navigation menu type import success"
puts "--- Payment Types start import"
# Импорт городов
payment_types = {
  "purchase" => { :name => "Покупка" },
  "reservation" => { :name => "Бронирование" }
}
payment_types.each { |key, value| PaymentType.where(:alias => key, :name => value[:name]).first_or_create }

puts "+++ Payment Types import success"
puts "*****"
puts "--- Payment Methods start import"

payment_methods = {
  "spot" => { :name => "Наличный" },
  "cashless" => { :name => "Безналичный" }
}
payment_methods.each { |key, value| PaymentMethod.where(:alias => key, :name => value[:name]).first_or_create }

puts "+++ Payment Methods import success"
puts "*****"
puts "--- Delivery Methods start import"
# Импорт способов доставки
delivery_methods = {
  "courier" => { :name => "Курьер" },
  "himself" => { :name => "Самостоятельно через точку продаж" }
}
delivery_methods.each { |key, value| DeliveryMethod.where(:alias => key, :name => value[:name]).first_or_create }

puts "+++ Delivery Methods import success"
puts "*****"
puts "--- Halls start import"
halls = {
    "aksion" => {
        :name => "ДК Аксион"
    },
    "filarmonia" => {
        :name => "Филармония"
    },
    "metallurg_without_dancing" => {
        :name => "Металлург без танцпола"
    },
    "metallurg_with_dancing" => {
        :name => "Металлург с танцполом"
    },
    "cyrk" => {
        :name => "Цирк"
    },
    "izhstal" => {
        :name => "Ижсталь"
    },
    "dancing" => {
        :name => 'Танцпол'
    },
    "opera_theatre" => {
        :name => 'Театр оперы и балета'
    }
}
halls.each do |key, value|
    if (Hall.find_by(:alias => key).nil?)
        Hall.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
    end
end
puts "+++ Halls import success"
puts "*****"
puts "--- Request Type start import"
request_types = {
    "call" => {
        :name => "Запрос обратного звонка"
    },
    "certificate" => {
        :name => "Запрос сертификата"
    },
    "collective" => {
        :name => "Коллективная заявка на покупку билетов"
    }
}
request_types.each do |key, value|
    if (RequestType.find_by(:alias => key).nil?)
        RequestType.new({ :alias => key, :name => value[:name]}).save
    end
end
puts "+++ Request Type end import"
puts "*****"
puts "--- Blank counters start import"
=begin
blank_counters = {
    "normal_counter" => {
        :value => 0
    },
    "special_counter" => {
        :value => 0
    },
}
blank_counters.each do |key, value|
  if (BlankCounter.find_by(:alias => key).nil?)
    BlankCounter.new({ :alias => key, :value => 0 }).save
  end
end
=end
puts "+++ Blank counters import success"
puts "*****"
puts "--- Blank Delivery Type start import"
# delivery_method_types = {
#     "delivery" => {
#         :name => "Доставка"
#     },
#     "to_kassir" => {
#         :name => "У кассира"
#     }
# }
# delivery_method_types.each do |key, value|
#   if (BlankDeliveryType.find_by(:alias => key).nil?)
#     BlankDeliveryType.new({ :alias => key, :name => value[:name]}).save
#   end
# end
puts "*****"
puts "--- Blank Request Type start import"
# blank_request_types = {
#     "blank" => {
#         :name => "Бланки"
#     },
#     "money" => {
#         :name => "Денежные средства"
#     }
# }
# blank_request_types.each do |key, value|
#   if (BlankRequestType.find_by(:alias => key).nil?)
#     BlankRequestType.new({ :alias => key, :name => value[:name]}).save
#   end
# end
puts "*****"
puts "--- Seat Item Kind start import"
seat_item_kinds = {
    "fixed" => {
        :name => "Фиксированный"
    },
    "unfixed" => {
        :name => "Нефиксированный"
    }
}
seat_item_kinds.each do |key, value|
  if (SeatItemKind.find_by(:alias => key).nil?)
    SeatItemKind.new({ :alias => key, :name => value[:name]}).save
  end
end
puts "+++ Seat Item Kind end import"
puts "*****"
puts "--- Blank Type start import"
blank_type = {
    "normal" => {
        :name => "Общий",
    },
    "special" => {
        :name => "Фирменный",
    }
}
blank_type.each do |key, value|
  if (BlankType.find_by(:alias => key).nil?)
    BlankType.new({ :alias => key, :name => value[:name] }).save
  end
end
normalBlank = BlankType.find_by({ :alias => 'normal' })
if !normalBlank.nil?
    normalBlank.update_attribute(:name, blank_type['normal'][:name]) if normalBlank.name != "Общий"
end


puts "+++ Blank Type end import"
puts "*****"
puts "+++ Settings Import start import"
settings = {
    "phone" => {
        :name => "Контактный телефон"
    },
    "work_time" => {
        :name => "Время работы"
    },
    "vk_link" => {
        :name => "Адрес профиля ВКонтакте"
    },
    "facebook_link" => {
        :name => "Адрес профиля Facebook"
    },
    "twitter_link" => {
        :name => "Адрес профиля Twitter"
    },
    "instagram_link" => {
        :name => "Адрес профиля Instagram"
    },
    "youtube_link" => {
        :name => "Адрес профиля Youtube"
    },
    "order_call_mails" => {
        :name => "Адреса эл. почты для рассылки уведомлений о заявки на обратный звонок"
    },
    "order_cert_mails" => {
        :name => "Адреса эл. почты для рассылки уведомлений о заявки на сертификат"
    },
    "administrator_mail" => {
        :name => "Адрес электронной почты администратора"
    },
    "normal_counter_number" => {
        :name => "Счетчик номера обычного бланка",
        :value => "000000"
    },
    "normal_counter_prefix" => {
        :name => "Префикс номера обычного бланка",
        :value => "2D"
    },
    "special_counter_number" => {
        :name => "Счетчик номера фирменного бланка",
        :value => "000000"
    },
    "special_counter_prefix" => {
        :name => "Префикс номера фирменного бланка",
        :value => "1D"
    },
    "ticket_max_counter" => {
        :name => "Максимальное количество билетов",
        :value => "5"
    },
    "booking_disabled_in" => {
        :name => "Блокировать бронирование за (дней)",
        :value => "3"
    }

}
settings.each do |key, value|
  if (Setting.find_by(:alias => key).nil?)
    setting = Setting.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id })
    if !value[:value].nil?
      setting.value = value[:value]
    end
    setting.save
  end
end
puts "Settings success import"
puts "*****"
# Импорт типов мероприятий
event_types = {
    "singer" => {
        :name => "Певец/Певица",
    }
}
event_types.each do |key, value|
  if (EventType.find_by(:alias => key).nil?)
    EventType.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
  end
end
puts "Event type import success"

# Импорт типов баннеров
banner_types = {
    "main" => {
        :name => "На главной странице",
    },
    "minor" => {
        :name => "На внутренней странице"
    }
}
banner_types.each do |key, value|
  if (BannerType.find_by(:alias => key).nil?)
    BannerType.new({ :alias => key, :name => value[:name] }).save
  end
end
puts "Banner type import success"

# Импорт типов мест
ticket_states = {
    "available" => {
        :name => "Доступно"
    },
    "disabled" => {
        :name => "Недоступно"
    },
    "booked" => {
        :name => "Забронировано"
    },
    "bought" => {
        :name => "Куплено"
    },
    "hold" => {
        :name => 'Удержано'
    }
}
ticket_states.each do |key, value|
    if (TicketState.find_by(:alias => key).nil?)
        TicketState.new({ :alias => key , :name => value[:name] }).save
    end
end
puts "Ticket states import success"


# Импорт тип оплаты
payment_kinds = {
    "cash" => {
        :name => "Наличный"
    },
    "cashless" => {
        :name => "Безналичный"
    }
}
payment_kinds.each do |key, value|
  if (PaymentKind.find_by(:alias => key).nil?)
    PaymentKind.new({ :alias => key , :name => value[:name] }).save
  end
end
puts "Payment kinds import success"

# Импорт типов схем
schema_types = {
    "single_level" => {
        :name => "Одноуровневая"
    },
    "multi_level" => {
        :name => "Многоуровневая"
    }
}
schema_types.each do |key, value|
    if (SchemaType.find_by(:alias => key).nil?)
        SchemaType.new({ :alias => key , :name => value[:name] }).save
    end
end
puts "Schema types import success"
# Импорт типов мест
seat_types = {
    "from200" => {
        :name => "От 200 руб.",
        :price => 200,
        :color => "#e76aa2"
    },
    "from300" => {
        :name => "От 300 руб.",
        :price => 300,
        :color => "#df007d"
    },
    "from400" => {
        :name => "От 400 руб.",
        :price => 400,
        :color => "#f81227"
    },
    "from500" => {
        :name => "От 500 руб.",
        :price => 500,
        :color => "#e57a1a"
    },
    "from600" => {
        :name => "От 600 руб.",
        :price => 600,
        :color => "#fcc705"
    },
    "from700" => {
        :name => "От 700 руб.",
        :price => 700,
        :color => "#d9df84"
    },
    "from800" => {
        :name => "От 800 руб.",
        :price => 800,
        :color => "#adc920"
    },
    "from900" => {
        :name => "От 900 руб.",
        :price => 900,
        :color => "#019544"
    },
    "from1000" => {
        :name => "От 1000 руб.",
        :price => 1000,
        :color => "#02773b"
    },
    "from1100" => {
        :name => "От 1100 руб.",
        :price => 1100,
        :color => "#56a5b2"
    },
    "from1200" => {
        :name => "От 1200 руб.",
        :price => 1200,
        :color => "#0a99e7"
    },
    "from1300" => {
        :name => "От 1300 руб.",
        :price => 1300,
        :color => "#0b99e5"
    },
    "from1400" => {
        :name => "От 1400 руб.",
        :price => 1400,
        :color => "#006289"
    },
    "from1500" => {
        :name => "От 1500 руб.",
        :price => 1500,
        :color => "#7c70b0"
    }
}
seat_types.each do |key, value|
    if (SeatItemType.find_by(:alias => key).nil?)
        SeatItemType.new({ :alias => key , :name => value[:name], :price => value[:price], :color => value[:color] }).save
    end
end

puts "Schema types import success"
