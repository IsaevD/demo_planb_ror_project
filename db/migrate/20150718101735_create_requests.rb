class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :phone
      t.text :message
      t.integer :request_type_id
      t.timestamps
    end
  end
end
