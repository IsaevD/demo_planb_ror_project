class AddPaymentKindToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :payment_kind_id, :integer
  end
end
