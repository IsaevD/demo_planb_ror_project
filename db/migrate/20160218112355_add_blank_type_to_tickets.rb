class AddBlankTypeToTickets < ActiveRecord::Migration
  def change
    add_reference :tickets, :blank_type, index: true
  end
end
