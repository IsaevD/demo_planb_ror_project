class CreateSchemaItems < ActiveRecord::Migration
  def change
    create_table :schema_items do |t|
      t.string :alias
      t.string :name
      t.integer :schema_type_id
      t.integer :hall_id
      t.timestamps
    end
  end
end
