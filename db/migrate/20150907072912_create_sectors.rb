class CreateSectors < ActiveRecord::Migration
  def change
    create_table :sectors do |t|
      t.string :alias
      t.string :name
      t.integer :schema_item_id
      t.timestamps
    end
  end
end
