class AddBannerImage < ActiveRecord::Migration
  def up
    add_attachment :banners, :image
  end

  def down
    remove_attachment :banners, :image
  end
end
