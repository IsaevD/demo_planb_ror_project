class DeleteSpecialCounterFromKassir < ActiveRecord::Migration
  def change
    remove_column :kassirs, :special_counter, :integer
  end
end
