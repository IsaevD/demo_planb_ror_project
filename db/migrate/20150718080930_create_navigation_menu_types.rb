class CreateNavigationMenuTypes < ActiveRecord::Migration
  def change
    create_table :navigation_menu_types do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
