class AddPaymentIdToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :payment_id, :integer
  end
end
