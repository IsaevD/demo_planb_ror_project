class AddUrlToEventItem < ActiveRecord::Migration
  def change
    add_column :event_items, :url, :string
  end
end
