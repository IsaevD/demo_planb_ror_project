class CreatePaymentTransactions < ActiveRecord::Migration
  def change
    create_table :payment_transactions do |t|
      t.integer :invid
      t.integer :status
      t.integer :price
      t.text :description
      t.timestamps
    end
  end
end
