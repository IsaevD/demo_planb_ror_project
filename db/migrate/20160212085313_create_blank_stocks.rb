class CreateBlankStocks < ActiveRecord::Migration
  def change
    create_table :blank_stocks do |t|
      t.integer :counter, default: 0

      t.references :blank_type, index: true
      t.references :event_item, index: true

      t.integer :system_user_id, index: true

      t.timestamps
    end
  end
end
