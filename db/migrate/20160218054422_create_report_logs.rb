class CreateReportLogs < ActiveRecord::Migration
  def change
    create_table :report_logs do |t|
      t.string :alias
      t.references :system_user
      t.datetime :starts_at
      t.datetime :ends_at

      t.timestamps
    end
  end
end
