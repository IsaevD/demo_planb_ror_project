class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :alias
      t.string :name
      t.text :value
      t.integer :system_state_id
      t.timestamps
    end
  end
end
