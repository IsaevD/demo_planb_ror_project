class AddIsRegisteredFieldToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :is_registered, :boolean, default: false
  end
end
