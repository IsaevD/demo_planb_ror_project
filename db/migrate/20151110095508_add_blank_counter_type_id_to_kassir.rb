class AddBlankCounterTypeIdToKassir < ActiveRecord::Migration
  def change
    add_column :kassirs, :blank_counter_type_id, :integer
  end
end
