class AddBlankRequestTypeToBlankRequest < ActiveRecord::Migration
  def change
    add_column :blank_requests, :blank_request_type_id, :integer
  end
end
