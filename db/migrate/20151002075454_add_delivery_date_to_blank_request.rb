class AddDeliveryDateToBlankRequest < ActiveRecord::Migration
  def change
    add_column :blank_requests, :delivery_date, :datetime
  end
end
