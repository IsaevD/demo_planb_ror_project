class CreateMoneyTransfers < ActiveRecord::Migration
  def change
    create_table :money_transfers do |t|
      t.integer :counter, default: 0
      t.datetime :delivered_at
      t.string :state, default: 'initialized'

      t.integer :assignee_id, index: true
      t.integer :assignor_id, index: true

      t.timestamps
    end
  end
end
