class CreateSeatItemKinds < ActiveRecord::Migration
  def change
    create_table :seat_item_kinds do |t|
      t.string :alias
      t.string :name

      t.timestamps
    end
  end
end
