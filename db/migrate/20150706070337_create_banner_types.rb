class CreateBannerTypes < ActiveRecord::Migration
  def change
    create_table :banner_types do |t|
      t.string :alias
      t.string :name

      t.timestamps
    end
  end
end
