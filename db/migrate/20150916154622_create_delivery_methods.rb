class CreateDeliveryMethods < ActiveRecord::Migration
  def change
    create_table :delivery_methods do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
