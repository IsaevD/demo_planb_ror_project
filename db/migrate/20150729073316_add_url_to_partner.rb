class AddUrlToPartner < ActiveRecord::Migration
  def change
    add_column :partner_items, :link, :string
  end
end
