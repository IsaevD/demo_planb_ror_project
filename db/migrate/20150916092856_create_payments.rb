class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|

      t.string :number
      t.integer :user_id
      t.integer :system_user_id

      t.timestamps
    end
  end
end
