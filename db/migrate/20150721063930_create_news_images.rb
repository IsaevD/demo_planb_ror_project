class CreateNewsImages < ActiveRecord::Migration
  def change
    create_table :news_images do |t|
      t.integer :news_item_id
      t.timestamps
    end
  end
end
