class CreateBlankTransfers < ActiveRecord::Migration
  def change
    create_table :blank_transfers do |t|
      t.integer :counter, default: 0
      t.datetime :delivered_at
      t.string :state, default: 'initialized'

      t.references :blank_type, index: true
      t.references :event_item, index: true

      t.integer :assignee_id, index: true
      t.integer :assignor_id, index: true

      t.timestamps
    end
  end
end
