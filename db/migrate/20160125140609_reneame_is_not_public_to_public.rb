class ReneameIsNotPublicToPublic < ActiveRecord::Migration
  def change
  	rename_column :sale_point_items, :is_not_public, :is_public
  end
end
