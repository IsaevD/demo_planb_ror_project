class AddPlaceToSeat < ActiveRecord::Migration
  def change
    add_column :seats, :place, :string
  end
end
