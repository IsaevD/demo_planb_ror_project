class AddSchemaIdToSeatItems < ActiveRecord::Migration
  def change
    add_column :seat_items, :schema_item_id, :integer
  end
end
