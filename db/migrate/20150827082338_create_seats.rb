class CreateSeats < ActiveRecord::Migration
  def change
    create_table :seats do |t|
      t.integer :seat_type_id
      t.integer :seat_group_id
      t.integer :range
      t.integer :position
      t.integer :weight
      t.boolean :is_enabled
      t.integer :hall_id
      t.timestamps
    end
  end
end
