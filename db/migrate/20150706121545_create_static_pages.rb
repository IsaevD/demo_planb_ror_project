class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.string :alias
      t.string :name
      t.text :content
      t.string :meta_keywords
      t.text :meta_description
      t.integer :system_state_id
      t.timestamps
    end
  end
end
