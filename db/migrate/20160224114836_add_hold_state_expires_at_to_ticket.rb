class AddHoldStateExpiresAtToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :hold_state_expires_at, :datetime
  end
end
