class DeleteEventTypeIdFromEvents < ActiveRecord::Migration
  def change
    remove_column :event_items, :event_type_id
  end
end
