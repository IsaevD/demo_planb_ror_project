class CreateSchemaTypes < ActiveRecord::Migration
  def change
    create_table :schema_types do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
