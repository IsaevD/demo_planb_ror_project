class AddSalePointImage < ActiveRecord::Migration
  def up
    add_attachment :sale_point_items, :image
  end

  def down
    remove_attachment :sale_point_items, :image
  end
end
