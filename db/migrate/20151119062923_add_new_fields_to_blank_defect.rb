class AddNewFieldsToBlankDefect < ActiveRecord::Migration
  def change
    add_column :blank_defects, :blank_type_id, :integer
    add_column :blank_defects, :event_item_id, :integer
    add_column :blank_defects, :date, :datetime
    add_column :blank_defects, :sale_point_item_id, :integer
  end
end