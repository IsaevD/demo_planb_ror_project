class AddKassirIdToBlankDefect < ActiveRecord::Migration
  def change
    add_column :blank_defects, :system_user_id, :integer
  end
end
