class CreateBlankCounters < ActiveRecord::Migration
  def change
    create_table :blank_counters do |t|
      t.string :alias
      t.integer :value
      t.timestamps
    end
  end
end
