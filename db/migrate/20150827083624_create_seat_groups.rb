class CreateSeatGroups < ActiveRecord::Migration
  def change
    create_table :seat_groups do |t|
      t.string :alias
      t.string :name
      t.integer :position
      t.timestamps
    end
  end
end
