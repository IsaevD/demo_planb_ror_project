class DropOldSeatTypesTable < ActiveRecord::Migration
  def change
    drop_table :seat_types
  end
end
