class CreateBlankDeliveryTypes < ActiveRecord::Migration
  def change
    create_table :blank_delivery_types do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
