class RenameBlankCounterTypeIdToBlankTypeIdFromKassirCounters < ActiveRecord::Migration
  def change
    rename_column :kassir_counters, :blank_counter_type_id, :blank_type_id
  end
end
