class CreateSystemUserActionsLogDActionsLogRecords < ActiveRecord::Migration
  def change
    create_table :system_user_actions_log_d_actions_log_records do |t|
      t.integer :system_user_id
      t.integer :log_record_state_id
      t.datetime :action_date
      t.integer :module_item_id
      t.integer :entity_item_id

      t.timestamps
    end
  end
end
