class AddColumnActiveFromToTicketDiscounts < ActiveRecord::Migration
  def change
    add_column :ticket_discounts, :active_from, :datetime
  end
end
