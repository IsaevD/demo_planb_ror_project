class ChaneStaticPagesImmortalToOnRemovable < ActiveRecord::Migration
  def change
  	rename_column :static_pages, :immortal, :non_removable
  end
end
