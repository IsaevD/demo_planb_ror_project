class AddDescriptionToEventItem < ActiveRecord::Migration
  def change
    add_column :event_items, :description, :text
  end
end
