class AddEventItemToTicketDiscount < ActiveRecord::Migration
  def change
    add_column :ticket_discounts, :event_item_id, :integer
  end
end
