class CreatePartnerItems < ActiveRecord::Migration
  def change
    create_table :partner_items do |t|
      t.string :name
      t.integer :system_state_id
      t.timestamps
    end
  end
end
