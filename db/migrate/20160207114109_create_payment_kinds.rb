class CreatePaymentKinds < ActiveRecord::Migration
  def change
    create_table :payment_kinds do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
