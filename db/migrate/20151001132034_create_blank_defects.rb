class CreateBlankDefects < ActiveRecord::Migration
  def change
    create_table :blank_defects do |t|

      t.string :number
      t.integer :ticket_id

      t.timestamps
    end
  end
end
