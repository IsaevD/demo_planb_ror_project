class Change < ActiveRecord::Migration
  def change
    change_column :seat_items, :row, 'integer USING CAST(row AS integer)'
    change_column :seat_items, :number, 'integer USING CAST(number AS integer)'
  end
end
