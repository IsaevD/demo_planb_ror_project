class AddTypeToSeatItem < ActiveRecord::Migration
  def change
    add_column :seat_items, :seat_item_type_id, :integer
  end
end
