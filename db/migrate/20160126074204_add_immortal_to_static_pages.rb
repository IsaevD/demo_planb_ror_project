class AddImmortalToStaticPages < ActiveRecord::Migration
  def self.up
  	add_column :static_pages, :immortal, :boolean, :default => false
  end

  def self.down
  	change_column :static_pages, :immortal, :boolean, :default => nil
  	remove_column :static_pages, :immortal, :boolean
  end
end
