class AddCoordsToCities < ActiveRecord::Migration
  def change
    add_column :cities, :coor_x, :string
    add_column :cities, :coor_y, :string
  end
end
