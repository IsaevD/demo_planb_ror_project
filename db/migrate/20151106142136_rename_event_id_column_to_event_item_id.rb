class RenameEventIdColumnToEventItemId < ActiveRecord::Migration
  def change
    rename_column :blank_counters, :event_id, :event_item_id
  end
end
