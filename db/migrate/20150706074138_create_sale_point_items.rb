class CreateSalePointItems < ActiveRecord::Migration
  def change
    create_table :sale_point_items do |t|
      t.string :name
      t.string :address
      t.string :work_time
      t.string :lunch_time
      t.string :phone
      t.string :coor_x
      t.string :coor_y
      t.integer :system_state_id
      t.timestamps
    end
  end
end
