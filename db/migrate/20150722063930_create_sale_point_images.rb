class CreateSalePointImages < ActiveRecord::Migration
  def change
    create_table :sale_point_images do |t|
      t.integer :sale_point_item_id
      t.timestamps
    end
  end
end
