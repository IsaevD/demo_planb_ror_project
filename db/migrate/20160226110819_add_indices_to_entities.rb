class AddIndicesToEntities < ActiveRecord::Migration
  def change
    add_index :access_rights_d_access_rights, :system_user_role_id

    add_index :access_rights_d_module_rights, :access_right_id
    add_index :access_rights_d_module_rights, :module_item_id

    add_index :banners, :banner_type_id
    add_index :banners, :event_item_id
    add_index :banners, :system_state_id

    add_index :blank_defects, :ticket_id
    add_index :blank_defects, :system_user_id
    add_index :blank_defects, :blank_type_id
    add_index :blank_defects, :event_item_id
    add_index :blank_defects, :sale_point_item_id

    add_index :cities, :system_state_id

    add_index :event_images, :event_item_id

    add_index :event_items, :system_state_id
    add_index :event_items, :hall_id

    add_index :event_tags, :event_item_id
    add_index :event_tags, :event_type_id

    add_index :event_types, :system_state_id

    add_index :halls, :system_state_id
    add_index :halls, :schema_item_id

    add_index :navigation_menus, :navigation_menu_id
    add_index :navigation_menus, :navigation_menu_type_id
    add_index :navigation_menus, :system_state_id

    add_index :news_images, :news_item_id

    add_index :news_items, :system_state_id

    add_index :partner_items, :system_state_id

    add_index :payments, :user_id
    add_index :payments, :system_user_id
    add_index :payments, :event_item_id
    add_index :payments, :payment_type_id
    add_index :payments, :delivery_method_id
    add_index :payments, :sale_point_item_id
    add_index :payments, :payment_kind_id

    add_index :promo_images, :promo_item_id

    add_index :promo_items, :system_state_id

    add_index :report_logs, :system_user_id

    add_index :sale_point_images, :sale_point_item_id

    add_index :sale_point_items, :system_state_id
    add_index :sale_point_items, :city_id

    add_index :schema_items, :schema_type_id
    add_index :schema_items, :hall_id

    add_index :seat_items, :sector_group_id
    add_index :seat_items, :schema_item_id
    add_index :seat_items, :seat_item_kind_id
    add_index :seat_items, :seat_item_type_id

    add_index :sectors, :schema_item_id

    add_index :settings, :system_state_id

    add_index :static_page_images, :static_page_id

    add_index :static_pages, :system_state_id

    add_index :system_main_items_d_system_user_roles, :system_state_id

    add_index :system_main_items_d_system_users, :system_user_role_id
    add_index :system_main_items_d_system_users, :system_state_id

    add_index :ticket_discounts, :system_state_id
    add_index :ticket_discounts, :event_item_id

    add_index :ticket_reverts, :event_item_id

    add_index :tickets, :ticket_state_id
    add_index :tickets, :event_item_id
    add_index :tickets, :seat_item_id
    add_index :tickets, :ticket_discount_id

    add_index :users, :system_state_id
  end
end
