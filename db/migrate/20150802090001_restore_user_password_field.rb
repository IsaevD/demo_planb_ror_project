class RestoreUserPasswordField < ActiveRecord::Migration
  def change
    add_column :users, :user_restore_password, :string
  end
end
