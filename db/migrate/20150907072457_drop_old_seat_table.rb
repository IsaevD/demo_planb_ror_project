class DropOldSeatTable < ActiveRecord::Migration
  def change
    drop_table :seats
  end
end
