class AddCityIdFieldToSalesPoint < ActiveRecord::Migration
  def change
    add_column :sale_point_items, :city_id, :integer
  end
end
