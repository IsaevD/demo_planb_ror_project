class AddEventsImageField < ActiveRecord::Migration
  def up
    add_attachment :event_images, :image_element
  end

  def down
    remove_attachment :event_images, :image_element
  end
end
