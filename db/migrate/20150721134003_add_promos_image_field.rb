class AddPromosImageField < ActiveRecord::Migration
  def up
    add_attachment :promo_images, :image_element
  end

  def down
    remove_attachment :promo_images, :image_element
  end
end
