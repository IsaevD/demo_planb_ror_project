class DeleteAliasFromBlankCounter < ActiveRecord::Migration
  def change
    remove_column :blank_counters, :alias
  end
end
