class CreateNavigationMenus < ActiveRecord::Migration
  def change
    create_table :navigation_menus do |t|
      t.integer :position
      t.string :name
      t.string :link
      t.integer :navigation_menu_id
      t.integer :navigation_menu_type_id
      t.integer :system_state_id
      t.timestamps
    end
  end
end
