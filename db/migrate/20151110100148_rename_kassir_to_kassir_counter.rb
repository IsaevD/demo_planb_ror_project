class RenameKassirToKassirCounter < ActiveRecord::Migration
  def change
    rename_table :kassirs, :kassir_counters
  end
end
