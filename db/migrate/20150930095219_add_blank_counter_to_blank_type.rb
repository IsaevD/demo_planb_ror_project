class AddBlankCounterToBlankType < ActiveRecord::Migration
  def change
    add_column :blank_types, :blank_counter_id, :integer
  end
end
