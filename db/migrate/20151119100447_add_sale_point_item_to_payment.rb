class AddSalePointItemToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :sale_point_item_id, :integer
  end
end
