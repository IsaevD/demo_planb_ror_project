class AddEventItemIdColumnToBlankRequestEntity < ActiveRecord::Migration
  def change
    add_column :blank_requests, :event_item_id, :integer
  end
end
