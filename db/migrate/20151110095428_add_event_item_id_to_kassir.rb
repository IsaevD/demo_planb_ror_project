class AddEventItemIdToKassir < ActiveRecord::Migration
  def change
    add_column :kassirs, :event_item_id, :integer
  end
end
