class CreateSectorGroups < ActiveRecord::Migration
  def change
    create_table :sector_groups do |t|
      t.string :alias
      t.string :name
      t.integer :sector_id
      t.timestamps
    end
  end
end
