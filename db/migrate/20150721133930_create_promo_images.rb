class CreatePromoImages < ActiveRecord::Migration
  def change
    create_table :promo_images do |t|
      t.integer :promo_item_id
      t.timestamps
    end
  end
end
