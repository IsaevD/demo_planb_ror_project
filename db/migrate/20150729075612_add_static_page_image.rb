class AddStaticPageImage < ActiveRecord::Migration
  def up
    add_attachment :static_page_images, :image_element
  end

  def down
    remove_attachment :static_page_images, :image_element
  end
end
