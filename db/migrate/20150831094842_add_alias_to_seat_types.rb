class AddAliasToSeatTypes < ActiveRecord::Migration
  def change
    add_column :seat_types, :alias, :string
  end
end
