class AddImageToPartners < ActiveRecord::Migration
  def up
    add_attachment :partner_items, :image
  end

  def down
    remove_attachment :partner_items, :image
  end
end
