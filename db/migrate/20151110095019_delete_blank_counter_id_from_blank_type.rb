class DeleteBlankCounterIdFromBlankType < ActiveRecord::Migration
  def change
    remove_column :blank_types, :blank_counter_id
  end
end
