class CreateEventItems < ActiveRecord::Migration
  def change
    create_table :event_items do |t|
      t.string :name
      t.text :message
      t.text :content
      t.datetime :date
      t.integer :hall_id
      t.integer :event_type_id
      t.integer :system_state_id

      t.timestamps
    end
  end
end
