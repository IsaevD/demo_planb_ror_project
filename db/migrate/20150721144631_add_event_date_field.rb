class AddEventDateField < ActiveRecord::Migration
  def change
    add_column :event_items, :published_at, :datetime
  end
end
