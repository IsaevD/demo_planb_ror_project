class AddPublickStateToSalePoint < ActiveRecord::Migration
  def change
    add_column :sale_point_items, :is_not_public, :boolean
  end
end
