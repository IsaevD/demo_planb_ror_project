class CreateSystemMainItemsDSystemUserRoles < ActiveRecord::Migration
  def change
    create_table :system_main_items_d_system_user_roles do |t|
      t.string :alias
      t.string :name
      t.text :description
      t.integer :system_state_id
      t.timestamps
    end
  end
end
