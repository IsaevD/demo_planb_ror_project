class DropOldSeatGroupsTable < ActiveRecord::Migration
  def change
    drop_table :seat_groups
  end
end
