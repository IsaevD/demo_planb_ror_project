class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :name
      t.text :description
      t.text :content
      t.datetime :published_at
      t.integer :system_state_id
      t.timestamps
    end
  end
end
