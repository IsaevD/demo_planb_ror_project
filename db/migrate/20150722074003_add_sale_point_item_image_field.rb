class AddSalePointItemImageField < ActiveRecord::Migration
  def up
    add_attachment :sale_point_images, :image_element
  end

  def down
    remove_attachment :sale_point_images, :image_element
  end
end
