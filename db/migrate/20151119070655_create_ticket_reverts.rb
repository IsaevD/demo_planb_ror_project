class CreateTicketReverts < ActiveRecord::Migration
  def change
    create_table :ticket_reverts do |t|
      t.integer :event_item_id
      t.text :text
      t.datetime :date
      t.timestamps
    end
  end
end
