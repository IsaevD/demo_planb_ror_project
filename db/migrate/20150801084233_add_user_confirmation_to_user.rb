class AddUserConfirmationToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_confirmation, :string
  end
end
