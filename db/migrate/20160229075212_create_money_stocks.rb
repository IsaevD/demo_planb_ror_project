class CreateMoneyStocks < ActiveRecord::Migration
  def change
    create_table :money_stocks do |t|
      t.integer :counter, default: 0
      t.integer :system_user_id, index: true

      t.timestamps
    end
  end
end
