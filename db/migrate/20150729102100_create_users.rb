class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :discount_number
      t.string :password_digest
      t.string :user_remember_token
      t.integer :system_state_id

      t.timestamps
    end
  end
end
