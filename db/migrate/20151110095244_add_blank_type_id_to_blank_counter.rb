class AddBlankTypeIdToBlankCounter < ActiveRecord::Migration
  def change
    add_column :blank_counters, :blank_type_id, :integer
  end
end
