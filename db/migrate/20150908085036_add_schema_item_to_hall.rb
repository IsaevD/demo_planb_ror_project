class AddSchemaItemToHall < ActiveRecord::Migration
  def change
    add_column :halls, :schema_item_id, :integer
  end
end
