class CreateEventTypes < ActiveRecord::Migration
  def change
    create_table :event_types do |t|
      t.string :alias
      t.string :name
      t.integer :system_state_id
      t.timestamps
    end
  end
end
