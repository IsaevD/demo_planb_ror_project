class AddFieldsDateBeginAndDateEnd < ActiveRecord::Migration
  def change
    add_column :promo_items, :beginning_at, :datetime
    add_column :promo_items, :ending_at, :datetime
  end
end
