class DeleteNormalCounterFromKassir < ActiveRecord::Migration
  def change
    remove_column :kassirs, :normal_counter, :integer
  end
end
