class AddDateToDiscounts < ActiveRecord::Migration
  def change
    add_column :ticket_discounts, :date, :datetime
  end
end
