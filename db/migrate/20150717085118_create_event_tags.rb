class CreateEventTags < ActiveRecord::Migration
  def change
    create_table :event_tags do |t|
      t.integer :event_item_id
      t.integer :event_type_id
      t.timestamps
    end
  end
end
