class StaticPagesImages < ActiveRecord::Migration
  def change
    create_table :static_page_images do |t|
      t.integer :static_page_id
      t.timestamps
    end
  end
end
