class CreateSeatTypes < ActiveRecord::Migration
  def change
    create_table :seat_types do |t|
      t.string :name
      t.string :color
      t.integer :price
      t.integer :system_state_id
      t.timestamps
    end
  end
end
