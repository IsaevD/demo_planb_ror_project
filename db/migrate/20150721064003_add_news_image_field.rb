class AddNewsImageField < ActiveRecord::Migration
  def up
    add_attachment :news_images, :image_element
  end

  def down
    remove_attachment :news_images, :image_element
  end
end
