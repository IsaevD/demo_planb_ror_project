class AddValueToKassir < ActiveRecord::Migration
  def change
    add_column :kassirs, :value, :integer
  end
end
