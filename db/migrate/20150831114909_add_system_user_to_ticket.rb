class AddSystemUserToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :system_user_id, :integer
  end
end
