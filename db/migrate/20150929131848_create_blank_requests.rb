class CreateBlankRequests < ActiveRecord::Migration
  def change
    create_table :blank_requests do |t|
      t.integer :blank_delivery_type_id
      t.integer :blank_type_id
      t.integer :system_user_id
      t.integer :value
      t.timestamps
    end
  end
end
