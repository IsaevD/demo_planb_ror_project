class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :name
      t.integer :banner_type_id
      t.integer :event_item_id
      t.integer :system_state_id
      t.timestamps
    end
  end
end
