class CreateKassirs < ActiveRecord::Migration
  def change
    create_table :kassirs do |t|
      t.integer :system_user_id
      t.integer :normal_counter
      t.integer :special_counter
      t.timestamps
    end
  end
end
