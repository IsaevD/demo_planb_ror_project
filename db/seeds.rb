Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }

puts "--- System Users start import"
system_users = {
    "moderator" => {
        :first_name => "Иван",
        :middle_name => "Иванович",
        :last_name => "Иванов",
        :email => "default_moderator@test.com",
        :system_user_role_alias => "moderator"
    },
    "administrator" => {
        :first_name => "Иван",
        :middle_name => "Иванович",
        :last_name => "Иванов",
        :email => "default_administrator@test.com",
        :system_user_role_alias => "administrator"
    },
    "kassir" => {
        :first_name => "Петр",
        :middle_name => "Петрович",
        :last_name => "Петров",
        :email => "default_kassir@test.com",
        :system_user_role_alias => "kassir"
    },
    "kassir_major" => {
        :first_name => "Петр",
        :middle_name => "Петрович",
        :last_name => "Петров",
        :email => "default_kassir_major@test.com",
        :system_user_role_alias => "kassir_major"
    },
    "kassir_administrator" => {
        :first_name => "Петр",
        :middle_name => "Петрович",
        :last_name => "Петров",
        :email => "default_kassir_administrator@test.com",
        :system_user_role_alias => "kassir_administrator"
    }
}
system_users.each do |key, value|
  SystemMainItemsD::SystemUser.where(:login => key).first_or_create do |role|
    role.attributes = value.except(:system_user_role_alias).merge(
      :login => key, :system_state_id => SystemMainItemsD::SystemState.active_state.id,
      :password => '123456', :password_confirmation => '123456',
      :system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => value[:system_user_role_alias]).id
    )
  end
end


# Импорт городов
cities = {
    "votkinsk" => {
        :name   => "Воткинск",
        :coor_y => "57.03926397",
        :coor_x => "53.98284200",
    },
    "chaikovskiy" => {
        :name   => "Чайковский",
        :coor_y => "56.74402670",
        :coor_x => "54.14013700",
    },
    "izhevsk" => {
        :name => "Ижевск",
        :coor_y => "56.85577004",
        :coor_x => "53.20274950",
    }
}
cities.each do |key, value|
    # done: Неинформативное название переменных - имена в один символ недопустимы
    current_city = City.find_by(:alias => key)
    if (current_city.nil?)
        City.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
    else
      # done: Не работает. coor_x возвращает пустое значение и его на nil проверять бесполезно. Используй метод empty. Егор, неоднократно с тобой общались по поводу того, что необходимо тестировать свою разработку. Прими по этому поводу меры.
      # toDo: тестировал! у меня работает - http://joxi.ru/eAOqebohDEdEmo
      if current_city.coor_x.blank?
          current_city.coor_x = value[:coor_x]
      end
      if current_city.coor_y.blank?
          current_city.coor_y = value[:coor_y]
      end
      current_city.save
    end
end
puts "Cities import success"

@from_price_hash = {}

(2..15).to_a.map{ |i| i * 100 }.each do |i|
  @from_price_hash["from#{ i }"] = SeatItemType.find_by(:alias => "from#{ i }").id
end
# Импорт схемы "Филармония"

def generate_seats(seats_data)
  seats = {}

  seats_data.each do |data|
    seats[data[0].to_s] = data[1].map{ |seat_number| [seat_number, data[2], @from_price_hash["from#{ data[3] }"]] }
  end

  seats
end

puts "Schema Filarmonia start import"
filarmonia_schema = {
    "filarmonia" => {
        :name => "Филармония",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "filarmonia").id
    }
}
filarmonia_schema.each do |key, value|
    if (SchemaItem.find_by(:alias => key).nil?)
        schema_item = SchemaItem.create({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
        hall = Hall.find_by(:alias => "filarmonia")
        hall.schema_item_id = schema_item.id
        hall.save
    end
end
sectors = {
    "filarmonia_main" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "filarmonia").id
    }
}
sectors.each do |key, value|
    if (Sector.find_by(:alias => key).nil?)
        Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
    end
end
sector_id = Sector.find_by(:alias => "filarmonia_main").id
sector_groups = {
    "filarmonia_parter" => {
        :name => "Партер",
        :sector_id => sector_id
    },
    "filarmonia_balkon" => {
        :name => "Балкон",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
    if (SectorGroup.find_by(:alias => key).nil?)
        SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
    end
end
schema_id = SchemaItem.find_by(:alias => "filarmonia").id
# Партер
group_id = SectorGroup.find_by(:alias => "filarmonia_parter").id

seats = generate_seats([
  [1, (1..24).to_a, group_id, 200], [2, (1..24).to_a, group_id, 300], [3, (1..24).to_a, group_id, 400],
  [4, (1..24).to_a, group_id, 500], [5, (1..24).to_a, group_id, 500], [6, (1..24).to_a, group_id, 500],
  [7, (1..9).to_a+(15..24).to_a, group_id, 600], [8, (1..24).to_a, group_id, 600], [9, (1..24).to_a, group_id, 600],
  [10, (1..24).to_a, group_id, 700], [11, (1..24).to_a, group_id, 800], [12, (1..24).to_a, group_id, 800],
  [13, (1..24).to_a, group_id, 900], [14, (1..24).to_a, group_id, 900], [15, (1..24).to_a, group_id, 1000],
  [16, (1..24).to_a, group_id, 1000], [17, (1..24).to_a, group_id, 1100], [18, (1..24).to_a, group_id, 1100],
  [19, (1..24).to_a, group_id, 1200], [20, (1..24).to_a, group_id, 1200], [21, (1..24).to_a, group_id, 1300],
  [22, (1..24).to_a, group_id, 1300]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
        SeatItem.create({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] })
    end
  end
end

#Балкон
group_id = SectorGroup.find_by(:alias => "filarmonia_balkon").id

seats = generate_seats([
  [1, (1..27).to_a, group_id, 1400], [2, (1..27).to_a, group_id, 1400],
  [3, (1..27).to_a, group_id, 1400], [4, (1..27).to_a, group_id, 1500],
  [5, [1, 2, 26, 27], group_id, 1500], [6, [1, 2, 26, 27], group_id, 1500]
])

seats.each do |key, value|
    value.each do |seat|
        if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
            SeatItem.create({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] })
        end
    end
end
puts "Schema Filarmonia import success"



# Импорт схемы "Аксион"
puts "Schema Aksion start import"
aksion_schema = {
    "aksion" => {
        :name => "Аксион",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "aksion").id
    }
}
aksion_schema.each do |key, value|
    if (SchemaItem.find_by(:alias => key).nil?)
        schema_item = SchemaItem.create({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
        hall = Hall.find_by(:alias => "aksion")
        hall.schema_item_id = schema_item.id
        hall.save
    end
end
sectors = {
    "aksion_main" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "filarmonia").id
    }
}
sectors.each do |key, value|
    if (Sector.find_by(:alias => key).nil?)
        Sector.create({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] })
    end
end
sector_id = Sector.find_by(:alias => "aksion_main").id
sector_groups = {
    "aksion_parter" => {
        :name => "Партер",
        :sector_id => sector_id
    },
    "aksion_amfiteatr" => {
        :name => "Амфитеатр",
        :sector_id => sector_id
    },
    "aksion_balkon" => {
        :name => "Балкон",
        :sector_id => sector_id
    },
    "aksion_vip_1" => {
        :name => "vip-ложа 1",
        :sector_id => sector_id
    },
    "aksion_vip_2" => {
        :name => "vip-ложа 2",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
    if (SectorGroup.find_by(:alias => key).nil?)
        SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
    end
end
schema_id = SchemaItem.find_by(:alias => "aksion").id
# Партер
group_id = SectorGroup.find_by(:alias => "aksion_parter").id

seats = generate_seats([
  [1, (1..21).to_a, group_id, 200], [2, (1..23).to_a, group_id, 200], [3, (1..25).to_a, group_id, 200],
  [4, (1..27).to_a, group_id, 200], [5, (1..29).to_a, group_id, 300], [6, (1..31).to_a, group_id, 300],
  [7, (1..37).to_a, group_id, 400], [8, (1..37).to_a, group_id, 400], [9, (1..37).to_a, group_id, 500],
  [10, (1..37).to_a, group_id, 500], [11, (1..35).to_a, group_id, 500]
])

seats.each do |key, value|
    value.each do |seat|
        if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
            SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
        end
    end
end
# Амфитеатр
group_id = SectorGroup.find_by(:alias => "aksion_amfiteatr").id

seats = generate_seats([
  [12, (1..30).to_a, group_id, 600], [13, (1..30).to_a, group_id, 600], [14, (1..35).to_a, group_id, 700],
  [15, (1..37).to_a, group_id, 700], [16, (1..37).to_a, group_id, 300], [17, (1..37).to_a, group_id, 700],
  [18, (1..37).to_a, group_id, 700], [19, (1..37).to_a, group_id, 700], [20, (1..37).to_a, group_id, 700],
  [21, (1..37).to_a, group_id, 800], [22, (1..37).to_a, group_id, 900], [23, (1..35).to_a, group_id, 1000]
])

seats.each do |key, value|
    value.each do |seat|
        if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
            SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
        end
    end
end
# Балкон
group_id = SectorGroup.find_by(:alias => "aksion_balkon").id

seats = generate_seats([
  [24, (1..37).to_a, group_id, 1100], [25, (1..37).to_a, group_id, 1100], [26, (1..37).to_a, group_id, 1200],
  [27, (1..37).to_a, group_id, 1200], [28, (1..37).to_a, group_id, 1200], [29, (1..42).to_a, group_id, 1200]
])

seats.each do |key, value|
    value.each do |seat|
        if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
            SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
        end
    end
end

# VIP-зона 1
group_id = SectorGroup.find_by(:alias => "aksion_vip_1").id

seats = generate_seats([
  [1, [1, 2], group_id, 1300], [2, [1, 2], group_id, 1300], [3, [1, 2], group_id, 1300], [4, [1, 2], group_id, 1300]
])

seats.each do |key, value|
    value.each do |seat|
        if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
            SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
        end
    end
end

# VIP-зона 2
group_id = SectorGroup.find_by(:alias => "aksion_vip_2").id

seats = generate_seats([
  [1, [1, 2], group_id, 1400], [2, [1, 2], group_id, 1400], [3, [1, 2], group_id, 1400], [4, [1, 2], group_id, 1400]
])

seats.each do |key, value|
    value.each do |seat|
        if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
            SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
        end
    end
end


# Импорт схемы "Металлург"
puts "Schema Metallurg Without Dancing start import"
metallurg_without_dancing = {
    "metallurg_without_dancing" => {
        :name => "Металлург без танцопла",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "metallurg_without_dancing").id
    }
}
metallurg_without_dancing.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.create({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    hall = Hall.find_by(:alias => "metallurg_without_dancing")
    hall.schema_item_id = schema_item.id
    hall.save
    schema_item.hall_id = hall.id
    schema_item.save
  end
end
sectors = {
    "metallurg_without_dancing_main" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "metallurg_without_dancing").id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.create({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] })
  end
end
sector_id = Sector.find_by(:alias => "metallurg_without_dancing_main").id
sector_groups = {
    "metallurg_without_dancing_parter" => {
        :name => "Партер",
        :sector_id => sector_id
    },
    "metallurg_without_dancing_central_parter" => {
        :name => "Центральный партер",
        :sector_id => sector_id
    },
    "metallurg_without_dancing_amfiteatr" => {
        :name => "Амфитеатр",
        :sector_id => sector_id
    },
    "metallurg_without_dancing_balkon" => {
        :name => "Балкон",
        :sector_id => sector_id
    },
    "metallurg_without_dancing_loga_1" => {
        :name => "Ложа 1",
        :sector_id => sector_id
    },
    "metallurg_without_dancing_loga_2" => {
        :name => "Ложа 2",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
schema_id = SchemaItem.find_by(:alias => "metallurg_without_dancing").id

# Партер
group_id = SectorGroup.find_by(:alias => "metallurg_without_dancing_parter").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..24).to_a, group_id, 200], [2, (1..29).to_a, group_id, 300], [3, (1..31).to_a, group_id, 300],
  [4, (1..33).to_a, group_id, 400]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Центральный партер
group_id = SectorGroup.find_by(:alias => "metallurg_without_dancing_central_parter").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [2, (1..30).to_a, group_id, 200], [3, (1..31).to_a, group_id, 300], [4, (1..32).to_a, group_id, 300],
  [5, (1..33).to_a, group_id, 300], [6, (1..34).to_a, group_id, 300], [7, (1..35).to_a, group_id, 400],
  [8, (1..34).to_a, group_id, 500]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Амфитеатр
group_id = SectorGroup.find_by(:alias => "metallurg_without_dancing_amfiteatr").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [9, (1..32).to_a, group_id, 200], [10, (1..32).to_a, group_id, 200], [11, (1..32).to_a, group_id, 200],
  [12, (1..32).to_a, group_id, 200], [13, (1..32).to_a, group_id, 300], [14, (1..32).to_a, group_id, 200],
  [8, (1..34).to_a, group_id, 500]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Балкон
group_id = SectorGroup.find_by(:alias => "metallurg_without_dancing_balkon").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..24).to_a, group_id, 600], [2, (1..26).to_a, group_id, 600], [3, (1..26).to_a, group_id, 600],
  [4, (1..31).to_a, group_id, 600], [5, (1..31).to_a, group_id, 600], [6, (1..31).to_a, group_id, 600],
  [7, (1..31).to_a, group_id, 600], [8, (1..39).to_a, group_id, 600], [9, (1..39).to_a, group_id, 600],
  [10, (1..41).to_a, group_id, 600]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Ложа №1
group_id = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_1").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..9).to_a, group_id, 200],
  [2, (1..8).to_a, group_id, 300],
  [3, (1..7).to_a, group_id, 300],
  [4, (1..5).to_a, group_id, 400]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Ложа №2
group_id = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_2").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..9).to_a, group_id, 200],
  [2, (1..8).to_a, group_id, 300],
  [3, (1..7).to_a, group_id, 300],
  [4, (1..5).to_a, group_id, 400]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end


puts "Schema Metallurg Without Dancing import success"

# Импорт схемы "Металлург"
puts "Schema Metallurg With Dancing start import"
metallurg_with_dancing = {
    "metallurg_with_dancing" => {
        :name => "Металлург с танцполом",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "metallurg_with_dancing").id
    }
}
metallurg_with_dancing.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.new({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    schema_item.save
    hall = Hall.find_by(:alias => "metallurg_with_dancing")
    hall.schema_item_id = schema_item.id
    hall.save
    schema_item.hall_id = hall.id
    schema_item.save
  end
end
sectors = {
    "metallurg_with_dancing_main" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "metallurg_with_dancing").id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
  end
end
sector_id = Sector.find_by(:alias => "metallurg_with_dancing_main").id
sector_groups = {
    "metallurg_with_dancing_dancing_room" => {
        :name => "Танцпол",
        :sector_id => sector_id
    },
    "metallurg_with_dancing_central_parter" => {
        :name => "Центральный партер",
        :sector_id => sector_id
    },
    "metallurg_with_dancing_amfiteatr" => {
        :name => "Амфитеатр",
        :sector_id => sector_id
    },
    "metallurg_with_dancing_balkon" => {
        :name => "Балкон",
        :sector_id => sector_id
    },
    "metallurg_with_dancing_loga_1" => {
        :name => "Ложа 1",
        :sector_id => sector_id
    },
    "metallurg_with_dancing_loga_2" => {
        :name => "Ложа 2",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
schema_id = SchemaItem.find_by(:alias => "metallurg_with_dancing").id

# Танцпол
group_id = SectorGroup.find_by(:alias => "metallurg_with_dancing_dancing_room").id
seat_item_kind_id = SeatItemKind.unfixed_kind.id
seat_item_count = 4000
if SeatItem.where(:sector_group_id => group_id).count == 0
  i = 0
  while i < seat_item_count
    SeatItem.new({ :row => 0, :number => 0, :sector_group_id => group_id, :schema_item_id => schema_id, :seat_item_type_id => @from_price_hash["from200"], :seat_item_kind_id => seat_item_kind_id }).save
    i = i + 1
  end
end

# Центральный партер
group_id = SectorGroup.find_by(:alias => "metallurg_with_dancing_central_parter").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [2, (1..30).to_a, group_id, 200], [3, (1..31).to_a, group_id, 300], [4, (1..32).to_a, group_id, 300],
  [5, (1..33).to_a, group_id, 300], [6, (1..34).to_a, group_id, 300], [7, (1..35).to_a, group_id, 400],
  [8, (1..34).to_a, group_id, 500]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Амфитеатр
group_id = SectorGroup.find_by(:alias => "metallurg_with_dancing_amfiteatr").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [9, (1..32).to_a, group_id, 200], [10, (1..32).to_a, group_id, 200], [11, (1..32).to_a, group_id, 200],
  [12, (1..32).to_a, group_id, 200], [13, (1..32).to_a, group_id, 200], [14, (1..32).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Балкон
group_id = SectorGroup.find_by(:alias => "metallurg_with_dancing_balkon").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..24).to_a, group_id, 600], [2, (1..26).to_a, group_id, 600], [3, (1..26).to_a, group_id, 600],
  [4, (1..31).to_a, group_id, 600], [5, (1..31).to_a, group_id, 600], [6, (1..31).to_a, group_id, 600],
  [7, (1..31).to_a, group_id, 600], [8, (1..39).to_a, group_id, 600], [9, (1..39).to_a, group_id, 600],
  [10, (1..41).to_a, group_id, 600]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Ложа №1
group_id = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_1").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..9).to_a, group_id, 200],
  [2, (1..8).to_a, group_id, 300],
  [3, (1..7).to_a, group_id, 300],
  [4, (1..5).to_a, group_id, 400]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Ложа №2
group_id = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_2").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..9).to_a, group_id, 200],
  [2, (1..8).to_a, group_id, 300],
  [3, (1..7).to_a, group_id, 300],
  [4, (1..5).to_a, group_id, 400]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end


puts "Schema Metallurg With Dancing import success"

# Импорт схемы "Цирк"
puts "Schema Cyrk start import"
cyrk_schema = {
    "cyrk_schema" => {
        :name => "Цирк",
        :schema_type_id => SchemaType.multi_level.id,
        :hall_id => Hall.find_by(:alias => "cyrk").id
    }
}
cyrk_schema.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.new({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    schema_item.save
    hall = Hall.find_by(:alias => "cyrk")
    hall.schema_item_id = schema_item.id
    hall.save
    schema_item.hall_id = hall.id
    schema_item.save
  end
end

schema_id = SchemaItem.find_by(:alias => "cyrk_schema").id

sectors = {
    "c_amfiteatr_1" => {
        :name => "Амфитеатр 1",
        :schema_id => schema_id
    },
    "c_amfiteatr_2" => {
        :name => "Амфитеатр 2",
        :schema_id => schema_id
    },
    "c_amfiteatr_3" => {
        :name => "Амфитеатр 3",
        :schema_id => schema_id
    },
    "c_amfiteatr_4" => {
        :name => "Амфитеатр 4",
        :schema_id => schema_id
    },
    "c_amfiteatr_5" => {
        :name => "Амфитеатр 5",
        :schema_id => schema_id
    },
    "c_amfiteatr_6" => {
        :name => "Амфитеатр 6",
        :schema_id => schema_id
    },
    "c_amfiteatr_7" => {
        :name => "Амфитеатр 7",
        :schema_id => schema_id
    },
    "c_parter_1" => {
        :name => "Партер 1",
        :schema_id => schema_id
    },
    "c_parter_2" => {
        :name => "Партер 2",
        :schema_id => schema_id
    },
    "c_parter_3" => {
        :name => "Партер 3",
        :schema_id => schema_id
    },
    "c_parter_4" => {
        :name => "Партер 4",
        :schema_id => schema_id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
  end
end


# Амфитеатр 1
sector_id = Sector.find_by(:alias => "c_amfiteatr_1").id
sector_groups = {
    "c_amfiteatr_1_main" => {
        :name => "Места для амфитеатра 1",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_1_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь

seats = generate_seats([
  [1, (1..24).to_a, group_id, 200], [2, (1..24).to_a, group_id, 200], [3, (1..24).to_a, group_id, 200],
  [4, (1..24).to_a, group_id, 200], [5, (1..28).to_a, group_id, 200], [6, (1..23).to_a, group_id, 200],
  [7, (1..21).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Амфитеатр 2
sector_id = Sector.find_by(:alias => "c_amfiteatr_2").id
sector_groups = {
    "c_amfiteatr_2_main" => {
        :name => "Места для амфитеатра 2",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_2_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь

seats = generate_seats([
  [1, (1..15).to_a, group_id, 200], [2, (1..15).to_a, group_id, 200], [3, (1..17).to_a, group_id, 200],
  [4, (1..21).to_a, group_id, 200], [5, (1..21).to_a, group_id, 200], [6, (1..21).to_a, group_id, 200],
  [7, (1..21).to_a, group_id, 200], [8, (1..17).to_a, group_id, 200], [9, (1..15).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Амфитеатр 3
sector_id = Sector.find_by(:alias => "c_amfiteatr_3").id
sector_groups = {
    "c_amfiteatr_3_main" => {
        :name => "Места для амфитеатра 3",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_3_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..24).to_a, group_id, 200], [2, (1..24).to_a, group_id, 200], [3, (1..24).to_a, group_id, 200],
  [4, (1..24).to_a, group_id, 200], [5, (1..28).to_a, group_id, 200], [6, (1..23).to_a, group_id, 200],
  [7, (1..21).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Амфитеатр 4
sector_id = Sector.find_by(:alias => "c_amfiteatr_4").id
sector_groups = {
    "c_amfiteatr_4_main" => {
        :name => "Места для амфитеатра 4",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_4_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..15).to_a, group_id, 200], [2, (1..15).to_a, group_id, 200], [3, (1..17).to_a, group_id, 200],
  [4, (1..21).to_a, group_id, 200], [5, (1..21).to_a, group_id, 200], [6, (1..21).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Амфитеатр 5
sector_id = Sector.find_by(:alias => "c_amfiteatr_5").id
sector_groups = {
    "c_amfiteatr_5_main" => {
        :name => "Места для амфитеатра 5",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_5_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..24).to_a, group_id, 200], [2, (1..24).to_a, group_id, 200], [3, (1..24).to_a, group_id, 200],
  [4, (1..24).to_a, group_id, 200], [5, (1..28).to_a, group_id, 200], [6, (1..23).to_a, group_id, 200],
  [7, (1..21).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Амфитеатр 6
sector_id = Sector.find_by(:alias => "c_amfiteatr_6").id
sector_groups = {
    "c_amfiteatr_6_main" => {
        :name => "Места для амфитеатра 6",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_6_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..15).to_a, group_id, 200], [2, (1..15).to_a, group_id, 200], [3, (1..17).to_a, group_id, 200],
  [4, (1..21).to_a, group_id, 200], [5, (1..21).to_a, group_id, 200], [6, (1..21).to_a, group_id, 200],
  [7, (1..21).to_a, group_id, 200], [8, (1..17).to_a, group_id, 200], [9, (1..15).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Амфитеатр 7
sector_id = Sector.find_by(:alias => "c_amfiteatr_7").id
sector_groups = {
    "c_amfiteatr_7_main" => {
        :name => "Места для амфитеатра 7",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_amfiteatr_7_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..24).to_a, group_id, 200], [2, (1..24).to_a, group_id, 200], [3, (1..24).to_a, group_id, 200],
  [4, (1..24).to_a, group_id, 200], [5, (1..28).to_a, group_id, 200], [6, (1..23).to_a, group_id, 200],
  [7, (1..21).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Партер 1
sector_id = Sector.find_by(:alias => "c_parter_1").id
sector_groups = {
    "c_parter_1_main" => {
        :name => "Места для партера 1",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_parter_1_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..14).to_a, group_id, 200], [2, (1..16).to_a, group_id, 200], [3, (1..20).to_a, group_id, 200],
  [4, (1..22).to_a, group_id, 200], [5, (1..26).to_a, group_id, 200], [6, (1..28).to_a, group_id, 200],
  [7, (1..34).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Партер 2
sector_id = Sector.find_by(:alias => "c_parter_2").id
sector_groups = {
    "c_parter_2_main" => {
        :name => "Места для партера 2",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_parter_2_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..14).to_a, group_id, 200], [2, (1..16).to_a, group_id, 200], [3, (1..20).to_a, group_id, 200],
  [4, (1..22).to_a, group_id, 200], [5, (1..26).to_a, group_id, 200], [6, (1..28).to_a, group_id, 200],
  [7, (1..34).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Партер 3
sector_id = Sector.find_by(:alias => "c_parter_3").id
sector_groups = {
    "c_parter_3_main" => {
        :name => "Места для партера 3",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_parter_3_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..14).to_a, group_id, 200], [2, (1..16).to_a, group_id, 200], [3, (1..20).to_a, group_id, 200],
  [4, (1..22).to_a, group_id, 200], [5, (1..26).to_a, group_id, 200], [6, (1..28).to_a, group_id, 200],
  [7, (1..34).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Партер 4
sector_id = Sector.find_by(:alias => "c_parter_4").id
sector_groups = {
    "c_parter_4_main" => {
        :name => "Места для партера 4",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "c_parter_4_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..14).to_a, group_id, 200], [2, (1..16).to_a, group_id, 200], [3, (1..20).to_a, group_id, 200],
  [4, (1..22).to_a, group_id, 200], [5, (1..26).to_a, group_id, 200], [6, (1..28).to_a, group_id, 200],
  [7, (1..34).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

puts "Schema Cyrk end import"


# Импорт схемы "Ижсталь"

puts "Schema Izhstal start import"
izhstal_schema = {
    "izhstal_schema" => {
        :name => "Ижсталь",
        :schema_type_id => SchemaType.multi_level.id,
        :hall_id => Hall.find_by(:alias => "izhstal").id
    }
}
izhstal_schema.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.new({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    schema_item.save
    hall = Hall.find_by(:alias => "izhstal")
    hall.schema_item_id = schema_item.id
    hall.save
    schema_item.hall_id = hall.id
    schema_item.save
  end
end

schema_id = SchemaItem.find_by(:alias => "izhstal_schema").id

sectors = {
    "i_sector_1" => {
        :name => "Сектор 1",
        :schema_id => schema_id
    },
    "i_sector_2" => {
        :name => "Сектор 2",
        :schema_id => schema_id
    },
    "i_sector_3" => {
        :name => "Сектор 3",
        :schema_id => schema_id
    },
    "i_sector_4" => {
        :name => "Сектор 4",
        :schema_id => schema_id
    },
    "i_sector_5" => {
        :name => "Сектор 5",
        :schema_id => schema_id
    },
    "i_sector_6a" => {
        :name => "Сектор 6А",
        :schema_id => schema_id
    },
    "i_sector_6b" => {
        :name => "Сектор 6Б",
        :schema_id => schema_id
    },
    "i_sector_7" => {
        :name => "Сектор 7",
        :schema_id => schema_id
    },
    "i_sector_8" => {
        :name => "Сектор 8",
        :schema_id => schema_id
    },
    "i_sector_9" => {
        :name => "Сектор 9",
        :schema_id => schema_id
    },
    "i_sector_10" => {
        :name => "Сектор 10",
        :schema_id => schema_id
    },
    "i_sector_11" => {
        :name => "Сектор 11",
        :schema_id => schema_id
    },
    "i_dancing_room" => {
        :name => "Танцпол",
        :schema_id => schema_id
    },
    "i_fanzone" => {
        :name => "Фан-зона",
        :schema_id => schema_id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
  end
end


# Сектор 1
sector_id = Sector.find_by(:alias => "i_sector_1").id
sector_groups = {
    "i_sector_1_main" => {
        :name => "Места для сектора 1",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_1_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..19).to_a, group_id, 200], [2, (1..22).to_a, group_id, 200], [3, (1..25).to_a, group_id, 200],
  [4, (1..28).to_a, group_id, 200], [5, (1..31).to_a, group_id, 200], [6, (1..35).to_a, group_id, 200],
  [7, (1..44).to_a, group_id, 200], [8, (1..47).to_a, group_id, 200], [9, (1..50).to_a, group_id, 200],
  [10, (1..54).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Сектор 2
sector_id = Sector.find_by(:alias => "i_sector_2").id
sector_groups = {
    "i_sector_2_main" => {
        :name => "Места для сектор 2",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_2_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [2, (1..27).to_a, group_id, 200], [3, (1..27).to_a, group_id, 200],
  [4, (1..27).to_a, group_id, 200], [5, (1..27).to_a, group_id, 200], [6, (1..27).to_a, group_id, 200],
  [7, (1..40).to_a, group_id, 200], [8, (1..40).to_a, group_id, 200], [9, (1..40).to_a, group_id, 200],
  [10, (1..29).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 3
sector_id = Sector.find_by(:alias => "i_sector_3").id
sector_groups = {
    "i_sector_3_main" => {
        :name => "Места для сектора 3",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_3_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..5).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 4
sector_id = Sector.find_by(:alias => "i_sector_4").id
sector_groups = {
    "i_sector_4_main" => {
        :name => "Места для сектор 4",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_4_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..27).to_a, group_id, 200], [2, (1..27).to_a, group_id, 200], [3, (1..27).to_a, group_id, 200],
  [4, (1..27).to_a, group_id, 200], [5, (1..27).to_a, group_id, 200], [6, (1..39).to_a, group_id, 200],
  [7, (1..39).to_a, group_id, 200], [8, (1..39).to_a, group_id, 200], [9, (1..29).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 5
sector_id = Sector.find_by(:alias => "i_sector_5").id
sector_groups = {
    "i_sector_5_main" => {
        :name => "Места для сектора 5",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_5_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..18).to_a, group_id, 200], [2, (1..22).to_a, group_id, 200], [3, (1..25).to_a, group_id, 200],
  [4, (1..28).to_a, group_id, 200], [5, (1..31).to_a, group_id, 200], [6, (1..39).to_a, group_id, 200],
  [7, (1..48).to_a, group_id, 200], [8, (1..51).to_a, group_id, 200], [9, (1..54).to_a, group_id, 200],
  [10, (1..58).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 6А
sector_id = Sector.find_by(:alias => "i_sector_6a").id
sector_groups = {
    "i_sector_6a_main" => {
        :name => "Места для сектора 6А",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_6a_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..18).to_a, group_id, 200], [2, (1..18).to_a, group_id, 200], [3, (1..18).to_a, group_id, 200],
  [4, (1..18).to_a, group_id, 200], [5, (1..18).to_a, group_id, 200], [6, (1..28).to_a, group_id, 200],
  [7, (1..28).to_a, group_id, 200], [8, (1..27).to_a, group_id, 200], [9, (1..28).to_a, group_id, 200],
  [10, (1..31).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 6Б
sector_id = Sector.find_by(:alias => "i_sector_6b").id
sector_groups = {
    "i_sector_6b_main" => {
        :name => "Места для сектора 6Б",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_6b_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (19..36).to_a, group_id, 200], [2, (19..36).to_a, group_id, 200], [3, (19..36).to_a, group_id, 200],
  [4, (19..36).to_a, group_id, 200], [5, (19..36).to_a, group_id, 200], [6, (29..55).to_a, group_id, 200],
  [7, (29..55).to_a, group_id, 200], [8, (28..54).to_a, group_id, 200], [9, (29..55).to_a, group_id, 200],
  [10, (32..60).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 7
sector_id = Sector.find_by(:alias => "i_sector_7").id
sector_groups = {
    "i_sector_7_main" => {
        :name => "Места для сектора 7",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_7_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..19).to_a, group_id, 200], [2, (1..22).to_a, group_id, 200], [3, (1..25).to_a, group_id, 200],
  [4, (1..28).to_a, group_id, 200], [5, (1..32).to_a, group_id, 200], [6, (1..39).to_a, group_id, 200],
  [7, (1..48).to_a, group_id, 200], [8, (1..52).to_a, group_id, 200], [9, (1..55).to_a, group_id, 200],
  [10, (1..57).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 8
sector_id = Sector.find_by(:alias => "i_sector_8").id
sector_groups = {
    "i_sector_8_main" => {
        :name => "Места для сектора 8",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_8_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..30).to_a, group_id, 200], [2, (1..31).to_a, group_id, 200], [3, (1..31).to_a, group_id, 200],
  [4, (1..31).to_a, group_id, 200], [5, (1..31).to_a, group_id, 200], [6, (1..31).to_a, group_id, 200],
  [7, (1..42).to_a, group_id, 200], [8, (1..42).to_a, group_id, 200], [9, (1..42).to_a, group_id, 200],
  [10, (1..45).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 9
sector_id = Sector.find_by(:alias => "i_sector_9").id
sector_groups = {
    "i_sector_9_main" => {
        :name => "Места для сектора 9",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_9_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..30).to_a, group_id, 200], [2, (1..31).to_a, group_id, 200], [3, (1..31).to_a, group_id, 200],
  [4, (1..31).to_a, group_id, 200], [5, (1..31).to_a, group_id, 200], [6, (1..31).to_a, group_id, 200],
  [7, (1..42).to_a, group_id, 200], [8, (1..42).to_a, group_id, 200], [9, (1..42).to_a, group_id, 200],
  [10, (1..45).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
# Сектор 10
sector_id = Sector.find_by(:alias => "i_sector_10").id
sector_groups = {
    "i_sector_10_main" => {
        :name => "Места для сектор 10",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_10_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..30).to_a, group_id, 200], [2, (1..31).to_a, group_id, 200], [3, (1..31).to_a, group_id, 200],
  [4, (1..31).to_a, group_id, 200], [5, (1..31).to_a, group_id, 200], [6, (1..31).to_a, group_id, 200],
  [7, (1..42).to_a, group_id, 200], [8, (1..42).to_a, group_id, 200], [9, (1..42).to_a, group_id, 200],
  [10, (1..45).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end
#Сектор 11
sector_id = Sector.find_by(:alias => "i_sector_11").id
sector_groups = {
    "i_sector_11_main" => {
        :name => "Места для сектор 11",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
group_id = SectorGroup.find_by(:alias => "i_sector_11_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.fixed_kind.id
# Места здесь
seats = generate_seats([
  [1, (1..54).to_a, group_id, 200], [2, (1..50).to_a, group_id, 200], [3, (1..47).to_a, group_id, 200],
  [4, (1..44).to_a, group_id, 200], [5, (1..34).to_a, group_id, 200], [6, (1..31).to_a, group_id, 200],
  [7, (1..28).to_a, group_id, 200], [8, (1..25).to_a, group_id, 200], [9, (1..21).to_a, group_id, 200],
  [10, (1..19).to_a, group_id, 200]
])

seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2], :seat_item_kind_id => seat_item_kind_id }).save
    end
  end
end

# Танцпол
sector_id = Sector.find_by(:alias => "i_dancing_room").id
sector_groups = {
    "i_dancing_room_main" => {
        :name => "Танцпол",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end

group_id = SectorGroup.find_by(:alias => "i_dancing_room_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.unfixed_kind.id
seat_item_count = 2000
if SeatItem.where(:sector_group_id => group_id).count == 0
  i = 0
  while i < seat_item_count
    SeatItem.new({ :row => 0, :number => 0, :sector_group_id => group_id, :schema_item_id => schema_id, :seat_item_type_id => @from_price_hash["from1000"], :seat_item_kind_id => seat_item_kind_id }).save
    i = i + 1
  end
end

# Фан-зона

sector_id = Sector.find_by(:alias => "i_fanzone").id
sector_groups = {
    "i_fanzone_main" => {
        :name => "Фан-зона",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end

group_id = SectorGroup.find_by(:alias => "i_fanzone_main", :sector_id => sector_id).id
seat_item_kind_id = SeatItemKind.unfixed_kind.id
seat_item_count = 500
if SeatItem.where(:sector_group_id => group_id).count == 0
  i = 0
  while i < seat_item_count
    SeatItem.new({ :row => 0, :number => 0, :sector_group_id => group_id, :schema_item_id => schema_id, :seat_item_type_id => @from_price_hash["from1000"], :seat_item_kind_id => seat_item_kind_id }).save
    i = i + 1
  end
end

puts "Schema Izhstal end import"

# Импорт схемы "Танцпол"
puts "Schema Dancing start import"

schema_item = SchemaItem.where(
  alias: 'dancing', name: 'Танцпол', schema_type_id: SchemaType.single_level.id,
  hall_id: Hall.find_by(alias: 'dancing')
).first_or_create

hall = Hall.find_by(alias: 'dancing')
hall.update(schema_item_id: schema_item.id) if hall.present? && hall.schema_item_id.blank?

sector = Sector.where(alias: 'dancing_main', name: 'Главный', schema_item_id: schema_item.id).first_or_create

sector_group = SectorGroup.where(alias: 'dancing_dancing_room', name: 'Танцпол', sector_id: sector.id)
  .first_or_create

seat_item_kind_id = SeatItemKind.unfixed_kind.id

if SeatItem.where(sector_group_id: sector_group.id).count == 0
  4000.times do
    SeatItem.create(
      row: 0, number: 0, sector_group_id: sector_group.id, schema_item_id: schema_item.id,
      seat_item_type_id: @from_price_hash["from200"], seat_item_kind_id: seat_item_kind_id
    )
  end
end

# Импорт схемы Опера
puts "Schema Opera Theatre start import"

schema_item = SchemaItem.where(alias: 'opera_theatre').first_or_create do |si|
  si.alias =  'opera_theatre'
  si.name = 'Театр оперы и балета'
  si.schema_type_id = SchemaType.single_level.id
  si.hall_id = Hall.find_by(alias: 'opera_theatre').id
end

hall = Hall.find_by(alias: 'opera_theatre')
hall.update(schema_item_id: schema_item.id) if hall.present? && hall.schema_item_id.blank?

sector = Sector.where(alias: 'opera_theatre_main').first_or_create do |s|
  s.name = 'Главный'
  s.schema_item_id = schema_item.id
end

sector_groups = {
  "opera_theatre_parter" => { :name => "Партер" },
  "opera_theatre_balkon" => { :name => "Балкон - I ярус" },
  "opera_theatre_balkon2" => { :name => "Балкон - II ярус" },
  "opera_theatre_pandus_left" => { :name => "Левый Пандус" },
  "opera_theatre_pandus_right" => { :name => "Правый Пандус" },
}

sector_groups.each do |key, value|
  SectorGroup.where(alias: key).first_or_create do |sg|
    sg.name = value[:name]
    sg.sector_id = sector.id
  end
end

group_id = SectorGroup.find_by(:alias => "opera_theatre_parter").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (0..23).to_a, group_id, 1000],
  [2, (1..28).to_a, group_id, 1000],
  [3, (1..30).to_a, group_id, 1000],
  [4, (1..32).to_a, group_id, 1000],
  [5, (1..33).to_a, group_id, 1000],
  [6, (1..34).to_a, group_id, 1000],
  [7, (1..35).to_a, group_id, 1000],
  [8, (1..34).to_a, group_id, 1000],
  [9, (1..25).to_a, group_id, 1000],
  [10, (1..25).to_a, group_id, 1000],
  [11, (1..26).to_a, group_id, 1000]
])

seats.each do |key, value|
  value.each do |seat|
    SeatItem.where(row: key, number: seat[0], sector_group_id: seat[1]).first_or_create do |si|
      si.schema_item_id = schema_item.id
      si.seat_item_type_id = seat[2]
      si.seat_item_kind_id = seat_item_kind_id
    end
  end
end

group_id = SectorGroup.find_by(:alias => "opera_theatre_balkon").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..35).to_a, group_id, 800],
  [2, (1..28).to_a, group_id, 800],
  [3, (1..29).to_a, group_id, 800],
  [4, (1..30).to_a, group_id, 800],
  [5, (1..28).to_a, group_id, 800],
  [6, (0..28).to_a, group_id, 800],
  [7, (1..20).to_a, group_id, 800],
  [8, (1..18).to_a, group_id, 800]
])

seats.each do |key, value|
  value.each do |seat|
    SeatItem.where(row: key, number: seat[0], sector_group_id: seat[1]).first_or_create do |si|
      si.schema_item_id = schema_item.id
      si.seat_item_type_id = seat[2]
      si.seat_item_kind_id = seat_item_kind_id
    end
  end
end

group_id = SectorGroup.find_by(:alias => "opera_theatre_balkon2").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

seats = generate_seats([
  [1, (1..36).to_a, group_id, 600],
  [2, (1..37).to_a, group_id, 600],
  [3, (1..37).to_a, group_id, 600],
  [4, (0..37).to_a, group_id, 600]
])

seats.each do |key, value|
  value.each do |seat|
    SeatItem.where(row: key, number: seat[0], sector_group_id: seat[1]).first_or_create do |si|
      si.schema_item_id = schema_item.id
      si.seat_item_type_id = seat[2]
      si.seat_item_kind_id = seat_item_kind_id
    end
  end
end

pandus_left_group_id = SectorGroup.find_by(:alias => "opera_theatre_pandus_left").id
pandus_right_group_id = SectorGroup.find_by(:alias => "opera_theatre_pandus_right").id
seat_item_kind_id = SeatItemKind.fixed_kind.id

(1..6).to_a.each do |index|
  SeatItem.where(row: 1, number: index, sector_group_id: pandus_left_group_id)
  .first_or_create do |si|
    si.schema_item_id = schema_item.id
    si.seat_item_type_id = @from_price_hash["from800"]
    si.seat_item_kind_id = seat_item_kind_id
  end

  SeatItem.where(row: 1, number: index, sector_group_id: pandus_right_group_id)
  .first_or_create do |si|
    si.schema_item_id = schema_item.id
    si.seat_item_type_id = @from_price_hash["from800"]
    si.seat_item_kind_id = seat_item_kind_id
  end
end
