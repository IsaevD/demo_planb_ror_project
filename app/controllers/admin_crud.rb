module AdminCRUD
  class Railtie < Rails::Railtie
    initializer 'admincrud.action_controller' do
      ActiveSupport.on_load(:action_controller) do
        include AdminCRUD::Controller
      end
    end
  end
  module Controller
    require 'active_support/concern'
    # extend ActiveSupport::Concern
    include ActiveSupport::Concern

    # moved out @entity var usage from def
    def index
      paginator_defalut ||= Rails.configuration.paginator_per_page.freeze
      order = if params[:sort_field].present? && params[:sort_value].present?
                "#{params[:sort_field]} #{params[:sort_value]}"
              else
                { created_at: :desc }
              end
      where = params[:filter].present? ? filter_index(params[:filter]) : ''
      logging
      @items = model_name_helper.where(where)
                   .order(order)
                   .paginate(page: params[:page], per_page: paginator_defalut)
    end

    def new
      @item = model_name_helper.new
    end

    def show
      logging
      @item = model_name_helper.find(params[:id])
    end

    # not using alias for future changes only
    def edit
      logging
      @item = model_name_helper.find(params[:id])
    end

    def create
      @item = model_name_helper.new(prepared_params(@param_name, @params_array))
      entity_params = params[item_symbol_helper]
      if @item.save
        @fields.each do |field, settings|
          case settings[:type]
            when :dual_list
              if settings[:show_in_card]
                # toDO: При обратном переходе к версии r 2.3.0 необходимо вернуть старый принцип работы
                entity_params[field].each do |key, _value|
                  settings[:recipient][:entity].create(settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key)
                end if entity_params && entity_params[field].present?
              end
            when :image_list
              entity_params[field].each do |value|
                @fields[field][:image_entity].create(@fields[field][:entity_field] => @item.id, @fields[field][:image_entity_field] => value)
              end if entity_params && entity_params[field].present?
          end
        end
        logging
        redirect_to index_action_path_helper
      else
        render 'new'
      end
    end

    def update
      @item = model_name_helper.find params[:id]
      entity_params = params[item_symbol_helper]

      if @item.update_attributes(prepared_params(@param_name, @params_array))
        @fields.each do |field, settings|
          next unless entity_params
          case settings[:type]
            when :image
              params[@param_name][field] = nil if entity_params[field] == 'nil'
            when :dual_list
              if settings[:show_in_card]
                current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
                current_values.destroy_all
                entity_params[field].each do |key, _value|
                  settings[:recipient][:entity].create(settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key)
                end if entity_params && entity_params[field].present?
              end
            when :image_list
              entity_params[settings[:image_entity_field]].each do |key, value|
                @fields[field][:image_entity].find(key).destroy if value == 'nil'
              end if entity_params && entity_params[settings[:image_entity_field]].present?

              entity_params[field].each do |value|
                @fields[field][:image_entity].create(@fields[field][:entity_field] => params[:id], @fields[field][:image_entity_field] => value)
              end if entity_params && entity_params[field].present?
          end
        end
        logging
        redirect_to index_action_path_helper
      else
        render 'new'
      end
    end

    def destroy
      @item = model_name_helper.find params[:id]
      @item.destroy
      logging
      redirect_to index_action_path_helper
    end

    private

    def filter_index(data)
      where = []
      data[:text].each { |key, value| where << "#{key} like '%#{value.downcase}%'" unless value.empty? }
      data[:collection].each { |key, value| where << "#{key} = #{value.downcase}" unless value.empty? }
      data[:datetime].each do |key, value|
        where << "#{key} >= '#{Date.parse(value[:of])} 00:00:00'" if value.present? && value[:of].present?
        where << "#{key} <= '#{Date.parse(value[:to])} 23:59:59'" if value.present? && value[:of].present?
      end
      where.join(' AND ')
    end

    # earlier entyti
    def model_name_helper
      controller_name.classify.constantize
    end

    def item_symbol_helper
      controller_name.classify.underscore.to_sym
    end

    def index_action_path_helper
      Rails.application.routes.url_helpers.send "#{controller_name}_path"
    end

    def new_action_path_helper
      Rails.application.routes.url_helpers.send "new_#{controller_name}_path"
    end

    def logging
      create_action_log_record(SystemUserActionsLogD::LogRecordState.VIEW_ENTITY_CARD_ALIAS, params[:id])
    end

    # Params prepare
    def prepared_params(param_name, prepared_params)
      params.require(param_name).permit(prepared_params)
    end
  end
end
