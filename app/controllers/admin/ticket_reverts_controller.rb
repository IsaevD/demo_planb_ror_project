class Admin::TicketRevertsController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "ticket_reverts",
          :entity => TicketRevert,
          :param_name => :ticket_revert,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.ticket_reverts_path,
              :new_path => Rails.application.routes.url_helpers.new_ticket_revert_path,
              :edit_path => Rails.application.routes.url_helpers.ticket_reverts_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Возврат" => nil
          },
          :fields => {
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :text => {
                  :type => :text,
                  :label => "Описание",
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              },
              :date => {
                  :type => :datetime,
                  :label => "Дата",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
