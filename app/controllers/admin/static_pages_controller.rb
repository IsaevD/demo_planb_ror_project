class Admin::StaticPagesController < Admin::ApplicationController
  before_filter :init_variables


  private

    def init_variables

      @object = {
          :name => "static_pages",
          :entity => StaticPage,
          :param_name => :static_page,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.static_pages_path,
              :new_path => Rails.application.routes.url_helpers.new_static_page_path,
              :edit_path => Rails.application.routes.url_helpers.static_pages_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Статические страницы" => nil
          },
          :fields => {
              :alias => {
                  :type => :string,
                  :label => "URL",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Является частью будущего url-адреса по шаблону <адрес сайта>/<алиас статической страницы>"
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :content => {
                  :type => :html,
                  :label => t('form.labels.content'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :label_g => {
                  :type => :group_label,
                  :label => t('custom.labels.gallery')
              },
              :image_list => {
                  :type => :image_list,
                  :label => t('custom.labels.gallery'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :entity_field => :static_page_id,
                  :image_entity => StaticPageImage,
                  :image_entity_field => :image_element,
                  :help_message => t('custom.messages.gallery_load')
              },
              :label => {
                  :type => :group_label,
                  :label => "Seo-данные"
              },
              :meta_keywords => {
                  :type => :string,
                  :label => t('form.labels.meta_keywords'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :meta_description => {
                  :type => :text,
                  :label => t('form.labels.meta_description'),
                  :show_in_card => true,
                  :show_in_table => false
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
