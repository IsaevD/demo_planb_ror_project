class Admin::SchemasController < Admin::ApplicationController

  # include ReportsHelper

  def index
    # toDO: рефакторинг
    @event_item = EventItem.find(params[:event_id])

    case @event_item.hall.alias
    when "filarmonia"
      @parter = SectorGroup.find_by(:alias => "filarmonia_parter")
      @balkon = SectorGroup.find_by(:alias => "filarmonia_balkon")
    when "aksion"
      @parter = SectorGroup.find_by(:alias => "aksion_parter")
      @amfiteatr = SectorGroup.find_by(:alias => "aksion_amfiteatr")
      @balkon = SectorGroup.find_by(:alias => "aksion_balkon")
      @vip1 = SectorGroup.find_by(:alias => "aksion_vip_1")
      @vip2 = SectorGroup.find_by(:alias => "aksion_vip_2")
    when "metallurg_without_dancing"
      @parter = SectorGroup.find_by(:alias => "metallurg_without_dancing_parter")
      @central_parter = SectorGroup.find_by(:alias => "metallurg_without_dancing_central_parter")
      @amfiteatr = SectorGroup.find_by(:alias => "metallurg_without_dancing_amfiteatr")
      @balkon = SectorGroup.find_by(:alias => "metallurg_without_dancing_balkon")
      @loga1 = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_1")
      @loga2 = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_2")
    when "metallurg_with_dancing"
      @dancing_room = SectorGroup.find_by(:alias => "metallurg_with_dancing_dancing_room")
      @central_parter = SectorGroup.find_by(:alias => "metallurg_with_dancing_central_parter")
      @amfiteatr = SectorGroup.find_by(:alias => "metallurg_with_dancing_amfiteatr")
      @balkon = SectorGroup.find_by(:alias => "metallurg_with_dancing_balkon")
      @loga1 = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_1")
      @loga2 = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_2")
    when 'dancing'
      @dancing_room = SectorGroup.find_by(:alias => "dancing_dancing_room")
    when 'opera_theatre'
      @parter = SectorGroup.find_by(alias: 'opera_theatre_parter')
      @balkon = SectorGroup.find_by(alias: 'opera_theatre_balkon')
      @balkon2 = SectorGroup.find_by(alias: 'opera_theatre_balkon2')
      @left_pandus = SectorGroup.find_by(alias: 'opera_theatre_pandus_left')
      @right_pandus = SectorGroup.find_by(alias: 'opera_theatre_pandus_right')
    end
  end

  def only_schema
    @event_item = EventItem.find(params[:event_id])
    @schema = get_schema(@event_item)
    @tickets = Ticket.get_tickets_by_event_id(@event_item.id)
    render :layout => "easy_admin"
  end

  def print_blank
    @tickets = Ticket.find(params[:ticket_ids].split(","))
    render :layout => "blank"
  end

  # def accept_blank_request
  #   if request.post?
  #     blank_request = BlankRequest.find(params[:blank_request_id])
  #     if !blank_request.nil?
  #       blank_request.blank_delivery_type = BlankDeliveryType.to_kassir_type
  #       kassir = KassirCounter.get_kassir_by_user(current_system_user)
  #       case blank_request.blank_type.alias
  #         when BlankType.NORMAL_ALIAS
  #           kassir.normal_counter = kassir.normal_counter + blank_request.value
  #         when BlankType.SPECIAL_ALIAS
  #           kassir.special_counter = kassir.special_counter + blank_request.value
  #       end
  #       kassir.save
  #       blank_request.save
  #     end
  #     redirect_to main_page_path
  #   else
  #     @blank_requests = BlankRequest.where(:system_user_id => current_system_user.id, :blank_delivery_type => BlankDeliveryType.delivery_type.id)
  #   end
  # end


  def available_blank_counters_by_kassir
    @blank_stocks = BlankStock.where(system_user_id: current_system_user.id)
      .with_blank_type.with_event_item
  end

  # def delivery_blank_counters_by_kassir

  #   @blank_requests = BlankRequest.where(
  #       :system_user_id => current_system_user.id,
  #       :blank_delivery_type_id => BlankDeliveryType.delivery_type.id
  #   )

  # end

  # Получить платеж и билеты
  def ajax_get_reservation_payment_by_number
    number = params[:number]
    global_arr = []
    payments = Payment.where(:payment_type_id => PaymentType.reservation_state.id).where("number = '#{number}' OR phone = '#{number}'")
    if (payments.count != 0)
      payment = payments.first
      payment.tickets.each do |ticket|
        ticket_price = ticket.price
        ticket_discount = "-"
        if !ticket.ticket_discount.nil?
          if payment.updated_at <= ticket.ticket_discount.date
            ticket_price = TicketDiscount.get_price_by_discount_without_date_link(ticket)
            ticket_discount = ticket.ticket_discount.name
          else
            ticket_price = ticket.price
          end
        end
        arr = []
        seat_item = ticket.seat_item
        arr << ticket.id
        arr << seat_item.sector_group.name
        arr << seat_item.row
        arr << seat_item.number
        arr << ticket_discount
        arr << ticket_price
        global_arr << arr
      end
      render :json => [payment.id, payment.event_item.name, global_arr]
    else
      render :json => []
    end
  end

  # Купить билеты
  def ajax_buy_ticket_by_ids
    ids = params[:ticket_ids]
    payment_id = params[:payment_id]
    payment = Payment.find(payment_id)
    # Обновление билетов
    Ticket.where(:payment_id => payment_id).each do |ticket|
      ticket.payment_id = nil
      ticket.ticket_state = TicketState.available_state
      ticket.save
    end
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.payment_id = payment.id
      ticket.ticket_state = TicketState.bought_state
      ticket.save
    end
    payment.payment_type = PaymentType.purchase_state
    payment.system_user_id = current_system_user.id
    payment.save

    render json: {}, status: 200
  end

  # Сформировать платеж и оплатить билеты
  def ajax_create_payment_and_buy_ticket_by_ids
    ids = params[:ticket_ids]
    payment = Payment.create_payment(nil, current_system_user.id, params[:event_id], PaymentType.purchase_state)
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.bought_state
      ticket.payment_id = payment.id
      ticket.save
    end
    render :text => "Success"
  end

  # Сформировать платеж и забронировать билет
  def ajax_create_payment_and_book_ticket_by_ids
    ids = params[:ticket_ids]
    payment = Payment.create_payment(nil, current_system_user.id, params[:event_id], PaymentType.reservation_state)
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.booked_state
      ticket.payment_id = payment.id
      ticket.save
    end
    render :text => "Success"
  end

  # Получить информацию о билетах
  def ajax_usual_get_tickets_info
    tickets = Ticket
      .with_ticket_state
      .with_ticket_discount
      .where(event_item_id: params[:event_id])

    data = tickets.map do |ticket| 
      [
        ticket.seat_item_id,
        ticket.ticket_state.alias,
        TicketDiscount.get_price_by_discount(ticket),
        ticket.ticket_state.name,
        SeatItemType.get_color_by_price(ticket.price),
        Ticket.get_discount_name(ticket)
      ]
    end

    render :json => data
  end


  def ajax_book_ticket_by_ids
    ids = params[:ticket_ids]
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.booked_state
      ticket.system_user_id = current_system_user.id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_get_ticket_by_id
    id = params[:ticket_id]
    ticket = Ticket.find(id)
    render :json => [ticket.price]
  end

  def ajax_get_tickets
    tickets = Ticket.get_tickets_by_event_id(params[:event_id]).map {|i| [i.seat_item_id, i.ticket_state.alias, i.price] }
    render :json => tickets
  end

  # def ajax_accept_blanks
  #   blank_request = BlankRequest.find(params[:blank_request_id])
  #   if !blank_request.nil?

  #     blank_request.blank_delivery_type = BlankDeliveryType.to_kassir_type

  #     if blank_request.blank_type.alias == BlankType.NORMAL_ALIAS
  #       normal_kassir_counter = KassirCounter.get_normal_kassir_counter(current_system_user.id)
  #       normal_kassir_counter.value = normal_kassir_counter.value + blank_request.value
  #       normal_kassir_counter.save
  #     else
  #       special_kassir_counter = KassirCounter.get_special_kassir_counter(current_system_user.id, blank_request.event_item_id )
  #       special_kassir_counter.value = special_kassir_counter.value + blank_request.value
  #       special_kassir_counter.save
  #     end


  #     # kassir = KassirCounter.get_kassir_by_user(current_system_user)
  #     # case blank_request.blank_type.alias
  #     #   when BlankType.NORMAL_ALIAS
  #     #     kassir.normal_counter = kassir.normal_counter + blank_request.value
  #     #   when BlankType.SPECIAL_ALIAS
  #     #     kassir.special_counter = kassir.special_counter + blank_request.value
  #     # end
  #     # kassir.save

  #     blank_request.save
  #     render :text => "Success"
  #   else
  #     render :text => "Exist"
  #   end
  # end

end
