class Admin::AdvancedSchemasController < Admin::ApplicationController

  def index
    @event_item = EventItem.find(params[:event_id])
    @object = {
        :header_label => "#{@event_item.name} - #{@event_item.date.strftime("%d")} #{Rails.application.config.months[@event_item.date.strftime("%B")]} #{@event_item.date.strftime("%H:%M")} - #{@event_item.hall.name}"
    }
    @advanced_mode = true
    @ticket_colors = SeatItemType.all

    case @event_item.hall.alias
    when "filarmonia"
      @parter = SectorGroup.find_by(:alias => "filarmonia_parter")
      @balkon = SectorGroup.find_by(:alias => "filarmonia_balkon")
    when "aksion"
      @parter = SectorGroup.find_by(:alias => "aksion_parter")
      @amfiteatr = SectorGroup.find_by(:alias => "aksion_amfiteatr")
      @balkon = SectorGroup.find_by(:alias => "aksion_balkon")
      @vip1 = SectorGroup.find_by(:alias => "aksion_vip_1")
      @vip2 = SectorGroup.find_by(:alias => "aksion_vip_2")
    when "metallurg_without_dancing"
      @parter = SectorGroup.find_by(:alias => "metallurg_without_dancing_parter")
      @central_parter = SectorGroup.find_by(:alias => "metallurg_without_dancing_central_parter")
      @amfiteatr = SectorGroup.find_by(:alias => "metallurg_without_dancing_amfiteatr")
      @balkon = SectorGroup.find_by(:alias => "metallurg_without_dancing_balkon")
      @loga1 = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_1")
      @loga2 = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_2")
    when "metallurg_with_dancing"
      @dancing_room = SectorGroup.find_by(:alias => "metallurg_with_dancing_dancing_room")
      @central_parter = SectorGroup.find_by(:alias => "metallurg_with_dancing_central_parter")
      @amfiteatr = SectorGroup.find_by(:alias => "metallurg_with_dancing_amfiteatr")
      @balkon = SectorGroup.find_by(:alias => "metallurg_with_dancing_balkon")
      @loga1 = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_1")
      @loga2 = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_2")
    when "test_schema_with_dancing"
      @parter = SectorGroup.find_by(:alias => "test_schema_with_dancing_parter")
      @dancing_room = SectorGroup.find_by(:alias => "test_schema_with_dancing_dancing_room")
    when "test_schema"
      @sectors = Sector.where(:schema_item_id => @event_item.hall.schema_item_id)
      if !params[:sector_id].nil?
        case params[:sector_id].to_i
          when Sector.find_by(:alias => "sector_1").id
            @s1_part1 = SectorGroup.find_by(:alias => "test_dual_s1_schema_part_1")
            @s1_part2 = SectorGroup.find_by(:alias => "test_dual_s1_schema_part_2")
          when Sector.find_by(:alias => "sector_2").id
            @s2_part1 = SectorGroup.find_by(:alias => "test_dual_s2_schema_part_1")
          when Sector.find_by(:alias => "sector_3").id
        end
      end
    when "cyrk"
      @sectors = Sector.where(:schema_item_id => @event_item.hall.schema_item_id)

      if params[:sector_id].present?
        sector = Sector.find(params[:sector_id])
        @main = sector.sector_groups.first
      end
    when "izhstal"
      @sectors = Sector.where(:schema_item_id => @event_item.hall.schema_item_id)

      if params[:sector_id].present?
        sector = Sector.find(params[:sector_id])
        @main = sector.sector_groups.first
      end
    end
  end

  # Покупка билетов
  def ajax_buy_ticket
    tickets = Ticket.find(params[:ticket_ids].split(','))
    event_id = params[:event_id]

    if params[:payment_type_id].to_i == PaymentType.purchase_state.id
      Ticket.create_purchase_payment(tickets, event_id, current_system_user, true, params[:fio], params[:phone], params[:delivery_address], cookies["kassir_sale_point_item_id_for_user_#{ current_system_user.id }".to_sym])
    else
      Ticket.create_reservation_payment(tickets, event_id, current_system_user, params[:fio], params[:phone], params[:delivery_address], cookies["kassir_sale_point_item_id_for_user_#{ current_system_user.id }".to_sym])
    end

    render :text => "Success"
  end

  # Бронирование билетов
  def ajax_book_ticket
    ids = params[:ticket_ids]
    event_id = params[:event_id]
    tickets = Ticket.where("id in (#{ids})")
    Ticket.create_reservation_payment(tickets, event_id, current_system_user)
    render :text => "Success"
  end

  # Печать билета
  def ajax_print_ticket
    tickets = Ticket.find(params[:ticket_ids].split(","))
    event_item_id = params[:counter_type] == 'normal' ? nil : tickets.map(&:event_item_id).first
    blank_check_service = BlankStockCheckService.new(current_system_user.id, event_item_id)

    if blank_check_service.has_enough_blanks?(1)
      tickets.each do |ticket|
        BlankDefect.create_blank_defect(ticket, current_system_user) if ticket.number.present?

        ticket =
          if params[:counter_type] == 'normal'
            Ticket.create_ticket_normal_number(ticket, current_system_user)
          else
            Ticket.create_ticket_special_number(ticket, current_system_user, ticket.event_item)
          end

        ticket.save
      end

      render json: {}, status: 200
    else
      render json: { error: blank_check_service.error }, status: 422
    end
  end

  def ajax_cancel_printed_ticket
    Ticket.find(params[:ticket_ids].split(",")).each do |ticket|
      ticket.update(number: nil, blank_type_id: nil)

      BlankDefect.create_blank_defect(ticket, current_system_user)
    end

    render text: 'Success'
  end

  def ajax_refund_ticket
    tickets = Ticket.with_ticket_state.with_seat_item.find(params[:ticket_ids].split(','))

    TicketRevert.create(
      event_item_id: tickets.first.event_item_id, date: DateTime.now,
      text: tickets.map{ |ticket| "Ряд: #{ ticket.seat_item.row } Место: #{ ticket.seat_item.number } | "}.join(' '),
    )

    tickets.each do |ticket|
      BlankDefect.create_blank_defect(ticket, current_system_user) if ticket.ticket_state.alias == 'bought'

      ticket.update(ticket_state: TicketState.available_state, payment_id: nil, number: nil, blank_type_id: nil)
    end

    tickets.map(&:payment_id).uniq.compact.each do |payment_id|
      payment = Payment.includes(:tickets).find(payment_id)
      payment.destroy if payment.tickets.size == 0
    end

    render text: 'Success'
  end

  # Изменение статуса билетов
  def ajax_change_ticket_states
    ids = params[:ticket_ids]
    ticket_state = TicketState.find(params[:ticket_state_id])
    flag = ((ticket_state.alias == TicketState::BOUGHT_ALIAS) || (ticket_state.alias == TicketState::BOOKED_ALIAS))
    if flag
      if (ticket_state.alias == TicketState::BOOKED_ALIAS)
        payment_type = PaymentType.purchase_state
      end
      if (ticket_state.alias == TicketState::BOUGHT_ALIAS)
        payment_type = PaymentType.reservation_state
      end
      payment = Payment.create_payment(nil, current_system_user.id, params[:event_id], payment_type)
    end
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = ticket_state
      if flag
        ticket.payment_id = payment.id
      end
      if (ticket_state.alias == TicketState::BOUGHT_ALIAS)
        if ticket.number.nil?
          counter = Setting.generate_normal_counter_number
          ticket.number = "#{counter} #{Setting.get_setting(Setting.NORMAL_COUNTER_PREFIX_ALIAS)}"
          Setting.set_setting(Setting.NORMAL_COUNTER_NUMBER_ALIAS, counter)
        end
      end
      ticket.save
    end
    render :text => "Success"
  end

  # Изменение цены билетов
  def ajax_change_ticket_prices
    ids = params[:ticket_ids]
    price = params[:price].to_i
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.price = price
      ticket.save
    end
    render :text => "Success"
  end

  # Получить информацию о билетах
  def ajax_get_tickets_info
    tickets = Ticket.get_tickets_by_event_id(
        params[:event_id]).map {|i| [
          i.seat_item_id,
          i.ticket_state.alias,
          TicketDiscount.get_price_by_discount(i),
          i.ticket_state.name,
          SeatItemType.get_color_by_price(i.price),
          Ticket.get_discount_name(i),
          !i.payment.nil? ? i.payment.id : "" ,
          !i.payment.nil? ? i.payment.number : "",
          !i.payment.nil? ? (!i.payment.user.nil? ? i.payment.user.id : "" ) : "",
          !i.payment.nil? ? (!i.payment.user.nil? ? i.payment.user.email : "" ) : "",
          !i.payment.nil? ? i.payment.system_user_id : "",
          !i.payment.nil? ? (!i.payment.system_user_id.nil? ? SystemMainItemsD::SystemUser.find(i.payment.system_user_id).login : "" ) : "",
          i.number
    ]}
    render :json => tickets
  end

  # Изменение скидки на билет
  def ajax_change_ticket_discounts
    ids = params[:ticket_ids]
    ticket_discount_id = params[:ticket_discount_id].to_i
    if ticket_discount_id == 0
      ticket_discount_id = nil
    end
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_discount_id = ticket_discount_id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_buy_ticket_by_ids
    ids = params[:ticket_ids]
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.bought_state
      ticket.system_user_id = current_system_user.id
      ticket.save
    end
    Payment.create_payment_with_tickets(Ticket.where("id in (#{ids})"), "cash", nil, nil, nil)

    render json: {}, status: 200
  end

  def ajax_book_ticket_by_ids
    ids = params[:ticket_ids]
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.booked_state
      ticket.system_user_id = current_system_user.id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_get_ticket_by_id
    id = params[:ticket_id]
    ticket = Ticket.find(id)
    render :json => [ticket.price]
  end

  def ajax_get_tickets
    tickets = Ticket.get_tickets_by_event_id(params[:event_id]).map {|i| [i.seat_item_id, i.ticket_state.alias, i.price] }
    render :json => tickets
  end

  # def ajax_get_normal_kassir_counter
  #   counter = KassirCounter.get_normal_kassir_counter(current_system_user.id)
  #   render :json => counter.value
  # end

  # def ajax_get_special_kassir_counter
  #   render :json => nil
  # end

  private

    def init_variables
    end

end
