class Admin::BannersController < Admin::ApplicationController
  before_filter :init_variables


  private

    def init_variables

      @object = {
          :name => "banners",
          :entity => Banner,
          :param_name => :banner,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.banners_path,
              :new_path => Rails.application.routes.url_helpers.new_banner_path,
              :edit_path => Rails.application.routes.url_helpers.banners_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Баннеры" => nil
          },
          :fields => {
              :position => {
                  :type => :string,
                  :label => "Позиция",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => t('custom.messages.position')
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :banner_type_id =>  {
                  :type => :collection,
                  :label => "Месторасположение",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => BannerType,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :help_message => t('custom.messages.banner_place')
              },
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => {
                      "system_state_id" => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => true
                  },
                  :help_message => t('custom.messages.banner_event')
              },
              :link => {
                  :type => :string,
                  :label => "Ссылка",
                  :show_in_card => true,
                  :show_in_table => false,
                  :help_message => t('custom.messages.banner_link')
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }


      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
