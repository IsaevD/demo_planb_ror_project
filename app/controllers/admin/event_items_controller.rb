class Admin::EventItemsController < Admin::ApplicationController
  before_filter :init_variables


  def create
    super
    generate_new_tickets(@item) if @item.persisted?
  end


  private

    def init_variables

      @object = {
          :name => "event_items",
          :entity => EventItem,
          :param_name => :event_item,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.event_items_path,
              :new_path => Rails.application.routes.url_helpers.new_event_item_path,
              :edit_path => Rails.application.routes.url_helpers.event_items_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Мероприятия" => nil
          },
          :fields => {
              :url => {
                  :type => :string,
                  :label => "URL",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Является частью будущего url-адреса по шаблону <адрес сайта>/<алиас статической страницы>"
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :description => {
                  :type => :text,
                  :label => "Описание",
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :help_message => t('custom.messages.list_event_place')
              },
              :content => {
                  :type => :html,
                  :label => t('form.labels.content'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :help_message => t('custom.messages.card_event_place')
              },
              :message => {
                  :type => :text,
                  :label => "Дополнительная информация",
                  :show_in_card => true,
                  :show_in_table => false,
                  :help_message => t('custom.messages.add_event_info')
              },
              :published_at => {
                  :type => :datetime,
                  :label => "Дата публикации",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => t('custom.messages.event_published_at')
              },
              :date => {
                  :type => :datetime,
                  :label => "Дата мероприятия",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => t('custom.messages.event_archived_at')
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :hall_id =>  {
                  :type => :collection,
                  :label => "Площадка",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => Hall,
                  :where_visible_field => :name,
                  :where_statement => {
                      "system_state_id" => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :label => {
                  :type => :group_label,
                  :label => t('custom.labels.events_tag')
              },
              :event_tag => {
                  :type => :dual_list,
                  :label => "Тип мероприятия",
                  :show_in_card => true,
                  :show_in_table => false,
                  :recipient => {
                      :name => :name,
                      :label => "Типы текущего мероприятия",
                      :entity => EventTag,
                      :link_field => :event_item_id
                  },
                  :donor => {
                      :name => :name,
                      :label => "Все типы мероприятий",
                      :entity => EventType,
                      :link_field => :event_type_id
                  }
              },
              :g_label => {
                  :type => :group_label,
                  :label => t('custom.labels.gallery')
              },
              :image_list => {
                  :type => :image_list,
                  :label => t('custom.labels.gallery'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :entity_field => :event_item_id,
                  :image_entity => EventImage,
                  :image_entity_field => :image_element,
                  :help_message => t('custom.messages.gallery_load')
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
