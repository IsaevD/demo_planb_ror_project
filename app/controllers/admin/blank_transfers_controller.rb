class Admin::BlankTransfersController < Admin::ApplicationController
  before_filter :init_variables

  def new
    @blank_transfer = BlankTransfer.new
  end

  def create
    blank_transfer_service = BlankTransferCreationService.new(current_system_user, blank_transfer_params)

    if blank_transfer_service.create
      redirect_to params[:back] || main_page_path, notice: t('custom.modules.blank_transfers.success_create')
    else
      @blank_transfer = blank_transfer_service.blank_transfer
      flash.now[:error] = blank_transfer_service.error

      render :new
    end
  end

  def cancel
    BlankTransfer.find(params[:id]).cancel

    redirect_to params[:back] || main_page_path, notice: t('custom.modules.blank_transfers.success_cancel')
  end

  def finish
    BlankTransfer.find(params[:id]).finish

    redirect_to params[:back] || main_page_path, notice: t('custom.modules.blank_transfers.success_finish')
  end

  private

  def blank_transfer_params
    params.require(:blank_transfer).permit(
      :counter, :event_item_id, :blank_type_id, :assignee_id, :assignor_id
    ).tap{ |whitelisted| whitelisted[:assignor_id] = current_system_user.id }
  end

  def init_variables
    @object = {
      :name => "blank_transfers",
      :entity => BlankTransfer,
      :param_name => :blank_transfer,
      :paths => {
        :all_path => Rails.application.routes.url_helpers.blank_transfers_path,
        :new_path => Rails.application.routes.url_helpers.new_blank_transfer_path,
        :edit_path => Rails.application.routes.url_helpers.blank_transfers_path
      },
      :breadcrumbs => {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
          "Отправка бланков" => nil
      },
      :fields => {
        :state => {
          :type => :string,
          :label => "Состояние отправки",
          :show_in_table => true,
          :show_in_card => true,
          :required => true
        },
        :blank_type_id =>  {
          :type => :collection,
          :label => "Тип бланков",
          :show_in_table => false,
          :show_in_card => true,
          :where_entity => BlankType,
          :where_visible_field => :name,
          :where_statement => nil,
          :settings => {
              :include_blank => false
          }
        },
        :event_item_id =>  {
          :type => :collection,
          :label => t("custom.modules.event_items.name"),
          :show_in_table => false,
          :show_in_card => true,
          :where_entity => EventItem,
          :where_visible_field => :name,
          :where_statement => nil,
          :settings => {
            :include_blank => false
          }
        },
        :assignor_id =>  {
          :type => :collection,
          :label => "Кассир, отправивший бланки",
          :show_in_table => true,
          :show_in_card => true,
          :where_entity => SystemMainItemsD::SystemUser,
          :where_visible_field => :full_name,
          :settings => {
            :include_blank => false
          }
        },
        :assignee_id =>  {
          :type => :collection,
          :label => "Кассир, принимающий бланки",
          :show_in_table => true,
          :show_in_card => true,
          :where_entity => SystemMainItemsD::SystemUser,
          :where_visible_field => :full_name,
          :settings => {
            :include_blank => false
          }
        },
        :counter => {
          :type => :integer,
          :label => "Значение",
          :show_in_card => true,
          :show_in_table => true,
          :required => true
        },
        :delivered_at => {
          :type => :datetime,
          :label => "Дата получения",
          :show_in_card => false,
          :show_in_table => true,
          :help_message => "Дата получения бланков кассиром",
          :settings => {
            :include_blank => true
          }
        }
      }
    }

    @fields = @object[:fields]
    @visible_fields = @fields.select{ |key, value| value[:show_in_table] }.keys
    @entity = @object[:entity]
    @param_name = @object[:param_name]
    @path = @object[:paths][:all_path]
    @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
  end
end
