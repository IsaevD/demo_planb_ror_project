class Admin::UsersController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "users",
          :entity => User,
          :param_name => :user,
          :paths => {
              :all_path =>  Rails.application.routes.url_helpers.users_path,
              :new_path =>  Rails.application.routes.url_helpers.new_user_path,
              :edit_path => Rails.application.routes.url_helpers.users_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Пользователи сайта" => nil
          },
          :fields => {
              :email => {
                  :type => :string,
                  :label => t('form.labels.email'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :first_name => {
                  :type => :string,
                  :label => t('form.labels.first_name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :last_name => {
                  :type => :string,
                  :label => t('form.labels.last_name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :middle_name => {
                  :type => :string,
                  :label => t('form.labels.middle_name'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :phone => {
                  :type => :string,
                  :label => t('form.labels.phone'),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :discount_number => {
                  :type => :string,
                  :label => t("users.discount_number_field"),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :confirmed => {
                  :type => :checkbox,
                  :label => "Пользователь подтвержден",
                  :show_in_card => true,
                  :show_in_table => true,
                  :help_message => "Если пользователь неподтвержден, то он не сможет авторизоваться на сайте"
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :label => {
                  :type => :group_label,
                  :label => "Изменение пароля"
              },
              :password => {
                  :type => :string,
                  :label => t('form.labels.password'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              },
              :password_confirmation => {
                  :type => :string,
                  :label => t('form.labels.password_confirmation'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
