class Admin::NavigationMenusController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "navigation_menus",
          :entity => NavigationMenu,
          :param_name => :navigation_menu,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.navigation_menus_path,
              :new_path => Rails.application.routes.url_helpers.new_navigation_menu_path,
              :edit_path => Rails.application.routes.url_helpers.navigation_menus_path
          },
          :fields => {
              :position => {
                  :type => :string,
                  :label => "Позиция",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => t("custom.messages.position")
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :link => {
                  :type => :string,
                  :label => "Ссылка",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :navigation_menu_id =>  {
                  :type => :collection,
                  :label => "Родительское меню",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => NavigationMenu,
                  :where_visible_field => :name,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id,
                      :navigation_menu_id => nil
                  },
                  :settings => {
                      :include_blank => true
                  }
              },
              :navigation_menu_type_id =>  {
                  :type => :collection,
                  :label => "Показать",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => NavigationMenuType,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
