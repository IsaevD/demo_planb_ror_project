class Admin::RequestsController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "requests",
          :entity => Request,
          :param_name => :request,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.requests_path,
              :new_path => nil,
              :edit_path => Rails.application.routes.url_helpers.requests_path
          },
          :fields => {
              :phone => {
                  :type => :string,
                  :label => t('custom.modules.requests.phone'),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :name => {
                :type => :string,
                :label => t('custom.modules..requests.autor'),
                :show_in_table => true,
                :show_in_card => true,
              },
              :message => {
                  :type => :text,
                  :label => t('custom.modules..requests.message'),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :request_type_id =>  {
                  :type => :collection,
                  :label => t('custom.modules..requests.request_type'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => RequestType,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :created_at => {
                  :type => :datetime,
                  :label => t('custom.modules.requests.date'),
                  :show_in_card => false,
                  :show_in_table => true
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
