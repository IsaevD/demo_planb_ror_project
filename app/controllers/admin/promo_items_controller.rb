class Admin::PromoItemsController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "promo_items",
          :entity => PromoItem,
          :param_name => :promo_item,
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Акции" => nil
          },
          :paths => {
              :all_path => Rails.application.routes.url_helpers.promo_items_path,
              :new_path => Rails.application.routes.url_helpers.new_promo_item_path,
              :edit_path => Rails.application.routes.url_helpers.promo_items_path
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :description => {
                  :type => :text,
                  :label => t('form.labels.description'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :help_message => t('custom.messages.list_promo_place')
              },
              :content => {
                  :type => :html,
                  :label => t('form.labels.content'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :help_message => t('custom.messages.card_promo_place')
              },
              :published_at => {
                  :type => :datetime,
                  :label => t('form.labels.published_at'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => t('custom.messages.promo_published_at')
              },
              :beginning_at => {
                  :type => :datetime,
                  :label => "Дата начала акции",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
              },
              :ending_at => {
                  :type => :datetime,
                  :label => "Дата конца акции",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => t('custom.messages.promo_archived_at')
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :label => {
                  :type => :group_label,
                  :label => t('custom.labels.gallery')
              },
              :image_list => {
                  :type => :image_list,
                  :label => t('custom.labels.gallery'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :entity_field => :promo_item_id,
                  :image_entity => PromoImage,
                  :image_entity_field => :image_element,
                  :help_message => t('custom.messages.gallery_load')
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
