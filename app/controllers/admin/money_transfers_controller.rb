class Admin::MoneyTransfersController < Admin::ApplicationController
  before_filter :init_variables

  def new
    @money_transfer = MoneyTransfer.new
  end

  def create
    money_transfer_service = MoneyTransferCreationService.new(current_system_user, money_transfer_params)

    if money_transfer_service.create
      redirect_to params[:back] || main_page_path, notice: t('custom.modules.money_transfers.success_create')
    else
      @money_transfer = money_transfer_service.money_transfer
      flash.now[:error] = money_transfer_service.error

      render :new
    end
  end

  def cancel
    MoneyTransfer.find(params[:id]).cancel

    redirect_to params[:back] || main_page_path, notice: t('custom.modules.money_transfers.success_cancel')
  end

  def finish
    MoneyTransfer.find(params[:id]).finish

    redirect_to params[:back] || main_page_path, notice: t('custom.modules.money_transfers.success_finish')
  end

  private

  def money_transfer_params
    params.require(:money_transfer).permit(:counter, :assignee_id, :assignor_id)
      .tap{ |whitelisted| whitelisted[:assignor_id] = current_system_user.id }
  end

  def init_variables
    @object = {
      :name => "money_transfers",
      :entity => MoneyTransfer,
      :param_name => :money_transfer,
      :paths => {
        :all_path => Rails.application.routes.url_helpers.money_transfers_path,
        :new_path => Rails.application.routes.url_helpers.new_money_transfer_path,
        :edit_path => Rails.application.routes.url_helpers.money_transfers_path
      },
      :breadcrumbs => {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
          "Отправка денег" => nil
      },
      :fields => {
        :state => {
          :type => :string,
          :label => "Состояние отправки",
          :show_in_table => true,
          :show_in_card => true,
          :required => true
        },
        :assignor_id =>  {
          :type => :collection,
          :label => "Кассир, отправивший бланки",
          :show_in_table => true,
          :show_in_card => true,
          :where_entity => SystemMainItemsD::SystemUser,
          :where_visible_field => :full_name,
          :settings => {
            :include_blank => false
          }
        },
        :assignee_id =>  {
          :type => :collection,
          :label => "Кассир, принимающий бланки",
          :show_in_table => true,
          :show_in_card => true,
          :where_entity => SystemMainItemsD::SystemUser,
          :where_visible_field => :full_name,
          :settings => {
            :include_blank => false
          }
        },
        :counter => {
          :type => :integer,
          :label => "Значение",
          :show_in_card => true,
          :show_in_table => true,
          :required => true
        },
        :delivered_at => {
          :type => :datetime,
          :label => "Дата получения",
          :show_in_card => false,
          :show_in_table => true,
          :help_message => "Дата получения денег кассиром",
          :settings => {
            :include_blank => true
          }
        }
      }
    }

    @fields = @object[:fields]
    @visible_fields = @fields.select{ |key, value| value[:show_in_table] }.keys
    @entity = @object[:entity]
    @param_name = @object[:param_name]
    @path = @object[:paths][:all_path]
    @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
  end
end
