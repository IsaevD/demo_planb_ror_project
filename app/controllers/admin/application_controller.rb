class Admin::ApplicationController < ActionController::Base
  # include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include AccessRightsD::ApplicationHelper
  include SystemUserActionsLogD::ApplicationHelper
  include TicketsHelper
  include Admin::ApplicationHelper
  include AdminCRUD::Controller
  # helper_method AdminCRUD::Controller.instance_methods
  layout "admin"
  helper ObjectGeneratorD::ApplicationHelper
  helper AuthorizeD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper
  helper AccessRightsD::ApplicationHelper
  helper SystemUserActionsLogD::ApplicationHelper


  before_filter :init_app_variables, :signed_in_system?, :check_access_rights

  def signed_in_system?
    signed_in_user(system_users_authorize_d.sign_in_path, current_system_user)
  end


  private

    def init_app_variables
      items = params[:controller].split("/")
      @module_name = items[0]
      @module_item_alias = items[1]
      @module_item = ModulesD::ModuleItem.find_by(:module_name => @module_name, :alias => @module_item_alias)
      if signed_in(current_system_user)
          menus = {
            "kassir_administrator" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.strings.main_page"),
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :events => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => t("custom.modules.events.name"),
                    :value => {
                        :event_items => {
                            :type => :url,
                            :label => t("custom.modules.event_items.name"),
                            :value => Rails.application.routes.url_helpers.event_items_path
                        },
                        :event_types => {
                            :type => :url,
                            :label => t("custom.modules.event_types.name"),
                            :value => Rails.application.routes.url_helpers.event_types_path
                        },
                        :halls => {
                            :type => :url,
                            :label => t("custom.modules.halls.name"),
                            :value => Rails.application.routes.url_helpers.halls_path
                        },
                        :tickets => {
                            :type => :url,
                            :label => "Заказы",
                            :value => Rails.application.routes.url_helpers.payments_path
                        },
                        :reverts => {
                            :type => :url,
                            :label => "Возвраты",
                            :value => Rails.application.routes.url_helpers.ticket_reverts_path
                        }
                    }
                },
                :blank_transfers => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.blank_transfers.name"),
                    :value => Rails.application.routes.url_helpers.blank_transfers_path
                },
                :money_transfers => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.money_transfers.name"),
                    :value => Rails.application.routes.url_helpers.money_transfers_path
                },
                :reports => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.reports.name"),
                    :value => Rails.application.routes.url_helpers.reports_path
                }
            },
            "moderator" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.strings.main_page"),
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :news => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.news.name"),
                    :value => Rails.application.routes.url_helpers.news_items_path
                },
                :promos => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.promo_items.name"),
                    :value => Rails.application.routes.url_helpers.promo_items_path
                },
                :sale_points => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => t("custom.modules.sale_points.name"),
                    :value => {
                        :sale_point_items => {
                            :type => :url,
                            :label => t("custom.modules.sale_point_items.name"),
                            :value => Rails.application.routes.url_helpers.sale_point_items_path
                        },
                        :cities => {
                            :type => :url,
                            :label => t("custom.modules.cities.name"),
                            :value => Rails.application.routes.url_helpers.cities_path
                        }
                    }
                },
                :banners => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.banners.name"),
                    :value => Rails.application.routes.url_helpers.banners_path
                },
                :partners => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.partner_items.name"),
                    :value => Rails.application.routes.url_helpers.partner_items_path
                },
                :static_pages => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.static_pages.name"),
                    :value => Rails.application.routes.url_helpers.static_pages_path
                }
            },
            "administrator" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.strings.main_page"),
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :events => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => t("custom.modules.events.name"),
                    :value => {
                        :event_items => {
                            :type => :url,
                            :label => t("custom.modules.event_items.name"),
                            :value => Rails.application.routes.url_helpers.event_items_path
                        },
                        :event_types => {
                            :type => :url,
                            :label => t("custom.modules.event_types.name"),
                            :value => Rails.application.routes.url_helpers.event_types_path
                        },
                        :halls => {
                            :type => :url,
                            :label => t("custom.modules.halls.name"),
                            :value => Rails.application.routes.url_helpers.halls_path
                        }
                    }
                },
                :tickets => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => "Управление билетами",
                    :value => {
                        :tickets => {
                            :type => :url,
                            :label => "Заказы",
                            :value => Rails.application.routes.url_helpers.payments_path
                        },
                        :reverts => {
                            :type => :url,
                            :label => "Возвраты",
                            :value => Rails.application.routes.url_helpers.ticket_reverts_path
                        },
                        :defects => {
                            :type => :url,
                            :label => "Брак",
                            :value => Rails.application.routes.url_helpers.blank_defects_path
                        },
                        :discounts => {
                            :type => :url,
                            :label => "Скидки",
                            :value => Rails.application.routes.url_helpers.ticket_discounts_path
                        },
                        :group => {
                            :type => :url,
                            :label => "Ценовые группы",
                            :value => Rails.application.routes.url_helpers.seat_item_types_path
                        }
                    }
                },
                :news => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.news.name"),
                    :value => Rails.application.routes.url_helpers.news_items_path
                },
                :promos => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.promo_items.name"),
                    :value => Rails.application.routes.url_helpers.promo_items_path
                },
                :sale_points => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => t("custom.modules.sale_points.name"),
                    :value => {
                        :sale_point_items => {
                            :type => :url,
                            :label => t("custom.modules.sale_point_items.name"),
                            :value => Rails.application.routes.url_helpers.sale_point_items_path
                        },
                        :cities => {
                            :type => :url,
                            :label => t("custom.modules.cities.name"),
                            :value => Rails.application.routes.url_helpers.cities_path
                        }
                    }
                },
                :banners => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.banners.name"),
                    :value => Rails.application.routes.url_helpers.banners_path
                },
                :partners => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.partner_items.name"),
                    :value => Rails.application.routes.url_helpers.partner_items_path
                },
                :static_pages => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("custom.modules.static_pages.name"),
                    :value => Rails.application.routes.url_helpers.static_pages_path
                },
                :requests => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Заявки с сайта",
                    :value => Rails.application.routes.url_helpers.requests_path
                },
                :navigation_menus => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Навигационное меню",
                    :value => Rails.application.routes.url_helpers.navigation_menus_path
                },
                :users => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Пользователи сайта",
                    :value => Rails.application.routes.url_helpers.users_path
                },
                :access_rules => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => "Управление пользователями системы",
                  :value => {
                      :system_users => {
                          :type => :url,
                          :label => t("labels.menu.system_users"),
                          :value => system_main_items_d.system_users_path
                      },
                      :system_user_roles => {
                          :type => :url,
                          :label => t("labels.menu.system_user_roles"),
                          :value => system_main_items_d.system_user_roles_path
                      }
                  }
              }
            },
            "root" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.strings.main_page"),
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :news => {
                    :type => :url,
                    :icon => "icon-news",
                    :label => t("custom.modules.news.name"),
                    :value => Rails.application.routes.url_helpers.news_items_path
                },
                :promos => {
                    :type => :url,
                    :icon => "icon-star",
                    :label => t("custom.modules.promo_items.name"),
                    :value => Rails.application.routes.url_helpers.promo_items_path
                },
                :tickets => {
                    :type => :two_step_menu,
                    :icon => "icon-event1",
                    :label => "Билеты",
                    :value => {
                        :tickets => {
                            :type => :url,
                            :label => "Билеты",
                            :value => Rails.application.routes.url_helpers.tickets_path
                        },
                        :ticket_states => {
                            :type => :url,
                            :label => "Статусы билетов",
                            :value => Rails.application.routes.url_helpers.ticket_states_path
                        },
                        :ticket_discounts => {
                            :type => :url,
                            :label => "Скидки на билеты",
                            :value => Rails.application.routes.url_helpers.ticket_discounts_path
                        },
                        :reverts => {
                            :type => :url,
                            :label => "Возвраты",
                            :value => Rails.application.routes.url_helpers.ticket_reverts_path
                        }
                    }
                },
                :payments => {
                    :type => :url,
                    :icon => "icon-banners1",
                    :label => "Заказы",
                    :value => Rails.application.routes.url_helpers.payments_path
                },
                :events => {
                    :type => :two_step_menu,
                    :icon => "icon-events",
                    :label => t("custom.modules.events.name"),
                    :value => {
                        :event_items => {
                            :type => :url,
                            :label => t("custom.modules.event_items.name"),
                            :value => Rails.application.routes.url_helpers.event_items_path
                        },
                        :event_types => {
                            :type => :url,
                            :label => t("custom.modules.event_types.name"),
                            :value => Rails.application.routes.url_helpers.event_types_path
                        },
                        :halls => {
                            :type => :url,
                            :label => t("custom.modules.halls.name"),
                            :value => Rails.application.routes.url_helpers.halls_path
                        }
                    }
                },
                :banners => {
                    :type => :url,
                    :icon => "icon-banners",
                    :label => t("custom.modules.banners.name"),
                    :value => Rails.application.routes.url_helpers.banners_path
                },
                :sale_points => {
                    :type => :two_step_menu,
                    :icon => "icon-map",
                    :label => t("custom.modules.sale_points.name"),
                    :value => {
                        :sale_point_items => {
                            :type => :url,
                            :label => t("custom.modules.sale_point_items.name"),
                            :value => Rails.application.routes.url_helpers.sale_point_items_path
                        },
                        :cities => {
                            :type => :url,
                            :label => t("custom.modules.cities.name"),
                            :value => Rails.application.routes.url_helpers.cities_path
                        }
                    }
                },
                :partners => {
                    :type => :url,
                    :icon => "icon-partners",
                    :label => t("custom.modules.partner_items.name"),
                    :value => Rails.application.routes.url_helpers.partner_items_path
                },
                :static_pages => {
                    :type => :url,
                    :icon => "icon-site",
                    :label => t("custom.modules.static_pages.name"),
                    :value => Rails.application.routes.url_helpers.static_pages_path
                },
                :navigation_menus => {
                    :type => :url,
                    :icon => "icon-log",
                    :label => t("custom.modules.navigation_menus.name"),
                    :value => Rails.application.routes.url_helpers.navigation_menus_path
                },
                :requests => {
                    :type => :url,
                    :icon => "icon-pen",
                    :label => t("custom.modules.requests.name"),
                    :value => Rails.application.routes.url_helpers.requests_path
                },
                :users => {
                    :type => :url,
                    :icon => "icon-user",
                    :label => t("custom.modules.users.name"),
                    :value => Rails.application.routes.url_helpers.users_path
                },
                :settings => {
                    :type => :url,
                    :icon => "icon-log1",
                    :label => t("custom.modules.settings.name"),
                    :value => Rails.application.routes.url_helpers.settings_path
                },
                :system => {
                    :type => :two_step_menu,
                    :icon => "icon-system",
                    :label => t("labels.menu.system"),
                    :value => {
                        :system_users => {
                            :type => :url,
                            :label => t("labels.menu.system_users"),
                            :value => system_main_items_d.system_users_path
                        },
                        :system_user_roles => {
                            :type => :url,
                            :label => t("labels.menu.system_user_roles"),
                            :value => system_main_items_d.system_user_roles_path
                        },
                        :access_rights => {
                            :type => :url,
                            :label => t("labels.menu.access_rules"),
                            :value => access_rights_d.access_rights_path
                        },
                        :actions_log_records => {
                            :type => :url,
                            :label => t("labels.menu.actions_log_records"),
                            :value => system_user_actions_log_d.actions_log_records_path
                        },
                        :modules => {
                            :type => :url,
                            :label => t("labels.menu.modules"),
                            :value => modules_d.module_items_path
                        },
                        :system_states => {
                            :type => :url,
                            :label => t("labels.menu.system_states"),
                            :value => system_main_items_d.system_states_path
                        },
                        :log_record_states => {
                            :type => :url,
                            :label => t("labels.menu.log_record_states"),
                            :value => system_user_actions_log_d.log_record_states_path
                        }
                    }
                },
                :logout => {
                    :type => :link,
                    :icon => "icon-logout",
                    :label => t("labels.menu.logout"),
                    :value => generate_sign_out_link(current_system_user, system_users_authorize_d.sign_out_path)
                }
            },
        }
        @menu_items = menus[current_system_user.system_user_role.alias]
      end

    end


end
