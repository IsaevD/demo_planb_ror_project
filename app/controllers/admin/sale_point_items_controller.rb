class Admin::SalePointItemsController < Admin::ApplicationController
  before_filter :init_variables


  private

    def init_variables

      @object = {
          :name => "sale_point_items",
          :entity => SalePointItem,
          :param_name => :sale_point_item,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.sale_point_items_path,
              :new_path => Rails.application.routes.url_helpers.new_sale_point_item_path,
              :edit_path => Rails.application.routes.url_helpers.sale_point_items_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Точки продаж" => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :is_public => {
                  :type => :checkbox,
                  :label => "Отображать в публичной части",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => false
              },
              :address => {
                  :type => :string,
                  :label => "Адрес",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :work_time => {
                  :type => :string,
                  :label => "Время работы",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :lunch_time => {
                  :type => :string,
                  :label => "Время обеда",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :phone => {
                  :type => :string,
                  :label => "Телефон",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :help_message => "Отображается в списке точек продаж"
              },
              :city_id =>  {
                  :type => :collection,
                  :label => "Город",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => City,
                  :where_visible_field => :name,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :label_d => {
                  :type => :group_label,
                  :label => "Координаты"
              },
              :coor_x => {
                  :type => :string,
                  :label => "Координата по X",
                  :show_in_card => true,
                  :show_in_table => false,
                  :help_message => t('custom.messages.coor')
              },
              :coor_y => {
                  :type => :string,
                  :label => "Координата по Y",
                  :show_in_card => true,
                  :show_in_table => false
              },
              :label => {
                  :type => :group_label,
                  :label => t('custom.labels.gallery')
              },
              :image_list => {
                  :type => :image_list,
                  :label => t('custom.labels.gallery'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :entity_field => :sale_point_item_id,
                  :image_entity => SalePointImage,
                  :image_entity_field => :image_element,
                  :help_message => t('custom.messages.gallery_load')
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
