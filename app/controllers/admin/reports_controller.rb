class Admin::ReportsController < Admin::ApplicationController
  layout :determine_layout

  # include ReportsHelper

  # def blank_kassir_report
  #   @kassirs = KassirCounter.all
  # end

  # def kassir_blanks_state
  #   @kassirs = KassirCounter.all
  # end

  def index; end

  def cashbox_release
    @report_service = ReportService.new(current_system_user, 'cashbox_release')
  end

  def kassir_productivity
    system_user = SystemMainItemsD::SystemUser.find(params[:system_user_id])

    @report_service = ReportService.new(system_user, 'kassir_productivity', 
      { starts_at: params[:starts_at].first, ends_at: params[:ends_at].first }
    )

    respond_to do |format|
      format.html
      format.xls do
        response.headers['Content-Disposition'] = 'attachment; filename="kassir_productivity_report.xls"'
      end
    end
  end

  def event_items
    @report_service = ReportService.new(current_system_user, 'event_items',
      { starts_at: params[:starts_at].first, ends_at: params[:ends_at].first, event_item_id: params[:event_item_id] }
    )

    respond_to do |format|
      format.html
      format.xls do
        response.headers['Content-Disposition'] = "attachment; filename=event_#{ params[:event_item_id] }_report.xls"
      end
    end
  end

  def current_blanks_state; end

  def note_per_blanks_report
    @blank_transfers = BlankTransfer.where(id: params["ids"].split(",")).with_event_item
    @system_user = @blank_transfers.first.assignee
  end

  def determine_layout
    %w(kassir_productivity note_per_blanks_report cashbox_release event_items)
      .include?(action_name) ? 'report' : 'admin'
  end
end
