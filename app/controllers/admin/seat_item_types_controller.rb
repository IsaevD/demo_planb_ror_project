class Admin::SeatItemTypesController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "seat_item_types",
          :entity => SeatItemType,
          :param_name => :seat_item_type,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.seat_item_types_path,
              :new_path => Rails.application.routes.url_helpers.new_seat_item_type_path,
              :edit_path => Rails.application.routes.url_helpers.seat_item_types_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Билеты" => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => "Название",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :price => {
                  :type => :string,
                  :label => "Цена",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :color => {
                  :type => :string,
                  :label => "Код цвета",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Необходимо указать код цвета в формате HEX. Например, #ababab"
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
