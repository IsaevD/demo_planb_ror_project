class Admin::TicketDiscountsController < Admin::ApplicationController
  before_filter :init_variables



  private

    def init_variables

      @object = {
          :name => "ticket_discounts",
          :entity => TicketDiscount,
          :param_name => :ticket_discount,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.ticket_discounts_path,
              :new_path => Rails.application.routes.url_helpers.new_ticket_discount_path,
              :edit_path => Rails.application.routes.url_helpers.ticket_discounts_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Скидки на билеты" => nil
          },
          :fields => {
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :active_from => {
                  :type => :datetime,
                  :label => "Действует с",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Если дата меньше текущей, то скидка не активна"
              },
              :date => {
                  :type => :datetime,
                  :label => "Действует до",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Если дата станет больше текущей, то скидка автоматически отключится"
              },
              :value => {
                  :type => :string,
                  :label => "Значение",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :is_percent => {
                  :type => :checkbox,
                  :label => "В процентах",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Если не указано, то скидка будет номинальной"
              },
              :alias => {
                  :type => :string,
                  :label => "Алиас",
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              },
              :name => {
                  :type => :string,
                  :label => "Наименование",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
