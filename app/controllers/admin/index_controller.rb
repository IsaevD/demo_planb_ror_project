class Admin::IndexController < Admin::ApplicationController

  def index
    @role_alias = params[:role_alias] || 'none'
  end

end
