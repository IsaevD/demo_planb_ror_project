# class Admin::BlankRequestsController < Admin::ApplicationController
  # before_filter :init_variables



  # def create
  #   #create_item(@entity, @path, true)

  #   entity = @entity
  #   if (current_system_user.system_user_role.alias == "kassir")
  #     path = "/admin/main_page"
  #   else
  #     path = @path
  #   end
  #   is_log = true
  #   is_redirect = true

  #   @item = entity.new(prepared_params(@param_name, @params_array))
  #   if @item.save
  #     @fields.each do |field, settings|
  #       entity_params = params[@param_name]
  #       case settings[:type]
  #         when :dual_list
  #           if settings[:show_in_card]
  #             values = entity_params[field]
  #             if !values.nil?
  #               values.each do |key, value|
  #                 settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
  #               end
  #             end
  #           end
  #         when :image_list
  #           if !entity_params[field].nil?
  #             entity_params[field].each do |value|
  #               image_list = @fields[field][:image_entity].new
  #               image_list.update_attribute(@fields[field][:entity_field], @item.id)
  #               image_list.update_attribute(@fields[field][:image_entity_field], value)
  #               image_list.save
  #             end
  #           end
  #       end
  #     end
  #     if (is_log)
  #       create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
  #     end
  #     if (is_redirect)
  #       BlankCounter.change_blank_counter_by_blank_request(@item, nil, 0)
  #       redirect_to path
  #     end
  #   else
  #     render "new"
  #   end
  # end

  # def update
  #   #update_item(@entity, @path, params[:id], true)

  #   entity = @entity
  #   path = @path
  #   id = params[:id]
  #   is_log = true

  #   @item = entity.find(id)
  #   previous_blank_request = [@item.blank_delivery_type.alias, @item.value]

  #   @fields.each do |field, settings|
  #     entity_params = params[@param_name]
  #     case settings[:type]
  #       when :image
  #         if entity_params[field] == "nil"
  #           params[@param_name][field] = nil
  #         end
  #       when :dual_list
  #         if settings[:show_in_card]
  #           current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
  #           current_values.destroy_all
  #           values = entity_params[field]
  #           if !values.nil?
  #             values.each do |key, value|
  #               settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
  #             end
  #           end
  #         end
  #       when :image_list
  #         if !entity_params[settings[:image_entity_field]].nil?
  #           entity_params[settings[:image_entity_field]].each do |key, value|
  #             if value == "nil"
  #               @fields[field][:image_entity].find(key).destroy
  #             end
  #           end
  #         end
  #         if !entity_params[field].nil?
  #           entity_params[field].each do |value|
  #             image_list = @fields[field][:image_entity].new
  #             image_list.update_attribute(@fields[field][:entity_field], params[:id])
  #             image_list.update_attribute(@fields[field][:image_entity_field], value)
  #             image_list.save
  #           end
  #         end
  #     end
  #   end

  #   if @item.update_attributes(prepared_params(@param_name, @params_array))
  #     if (is_log)
  #       create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
  #     end
  #     BlankCounter.change_blank_counter_by_blank_request(@item, previous_blank_request, 1)
  #     redirect_to path
  #   else
  #     render "new"
  #   end

  # end

  # def destroy

  #   entity = @entity
  #   path = @path
  #   id = params[:id]
  #   is_log = true

  #   @item = entity.find(id)
  #   BlankCounter.change_blank_counter_by_blank_request(@item, nil, 2)
  #   @item.destroy
  #   if (is_log)
  #     create_action_log_record(SystemUserActionsLogD::LogRecordState.DELETE_ENTITY_ALIAS, id)
  #   end

  #   redirect_to path

  #   #delete_item(@entity, @path, params[:id], true)
  # end

  # private

  #   def init_variables

  #     @object = {
  #         :name => "blank_requests",
  #         :entity => BlankRequest,
  #         :param_name => :blank_request,
  #         :paths => {
  #             :all_path => Rails.application.routes.url_helpers.blank_requests_path,
  #             :new_path => Rails.application.routes.url_helpers.new_blank_request_path,
  #             :edit_path => Rails.application.routes.url_helpers.blank_requests_path
  #         },
  #         :breadcrumbs => {
  #             t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
  #             "Отправка бланков" => nil
  #         },
  #         :fields => {
  #             :blank_request_type_id =>  {
  #                 :type => :collection,
  #                 :label => "Тип отправки",
  #                 :show_in_table => true,
  #                 :show_in_card => true,
  #                 :where_entity => BlankRequestType,
  #                 :where_visible_field => :name,
  #                 :where_statement => nil,
  #                 :settings => {
  #                     :include_blank => false
  #                 }
  #             },
  #             :blank_delivery_type_id =>  {
  #                 :type => :collection,
  #                 :label => "Состояние отправки",
  #                 :show_in_table => true,
  #                 :show_in_card => true,
  #                 :where_entity => BlankDeliveryType,
  #                 :where_visible_field => :name,
  #                 :where_statement => nil,
  #                 :settings => {
  #                     :include_blank => false
  #                 }
  #             },
  #             :blank_type_id =>  {
  #                 :type => :collection,
  #                 :label => "Тип бланков",
  #                 :show_in_table => false,
  #                 :show_in_card => true,
  #                 :where_entity => BlankType,
  #                 :where_visible_field => :name,
  #                 :where_statement => nil,
  #                 :settings => {
  #                     :include_blank => false
  #                 }
  #             },
  #             :event_item_id =>  {
  #                 :type => :collection,
  #                 :label => t("custom.modules.event_items.name"),
  #                 :show_in_table => false,
  #                 :show_in_card => true,
  #                 :where_entity => EventItem,
  #                 :where_visible_field => :name,
  #                 :where_statement => nil,
  #                 :settings => {
  #                     :include_blank => false
  #                 }
  #             },
  #             :system_user_id =>  {
  #                 :type => :collection,
  #                 :label => "Кассир",
  #                 :show_in_table => true,
  #                 :show_in_card => true,
  #                 :where_entity => SystemMainItemsD::SystemUser,
  #                 :where_visible_field => :full_name,
  #                 :where_statement => {
  #                     :system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => "kassir").id
  #                 },
  #                 :settings => {
  #                     :include_blank => false
  #                 }
  #             },
  #             :value => {
  #                 :type => :string,
  #                 :label => "Значение",
  #                 :show_in_card => true,
  #                 :show_in_table => true,
  #                 :required => true
  #             },
  #             :delivery_date => {
  #                 :type => :datetime,
  #                 :label => "Дата отправки",
  #                 :show_in_card => false,
  #                 :show_in_table => true,
  #                 :required => true,
  #                 :help_message => "Дата отправки бланков кассиру"
  #             }
  #         }
  #     }
  #     @fields = @object[:fields]
  #     @visible_fields = []
  #     @fields.each do |key, value|
  #       if value[:show_in_table]
  #         @visible_fields << key
  #       end
  #     end
  #     @entity = @object[:entity]
  #     @param_name = @object[:param_name]
  #     @path = @object[:paths][:all_path]
  #     @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
  #   end

# end
