class Admin::PartnerItemsController < Admin::ApplicationController
  before_filter :init_variables


  private

    def init_variables

      @object = {
          :name => "partner_items",
          :entity => PartnerItem,
          :param_name => :partner_item,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.partner_items_path,
              :new_path => Rails.application.routes.url_helpers.new_partner_item_path,
              :edit_path => Rails.application.routes.url_helpers.partner_items_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Партнеры" => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :link => {
                  :type => :string,
                  :label => "Ссылка",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
