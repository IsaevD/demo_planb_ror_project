class Admin::PaymentsController < Admin::ApplicationController
  before_filter :init_variables


  def update

    entity = @entity
    id = params[:id]
    path = @path
    is_log = true

    @item = entity.find(id)

    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end
      # Обновление билетов
      Ticket.where(:payment_id => @item.id).each do |ticket|
        ticket.payment_id = nil
        ticket.ticket_state = TicketState.available_state
        ticket.save
      end
      if !params[@param_name][:ticket_id].nil?
        params[@param_name][:ticket_id].each do |key, value|
          ticket = Ticket.find(key)
          ticket.payment_id = @item.id
          if (@item.payment_type.alias == PaymentType::RESERVATION_ALIAS)
            ticket.ticket_state = TicketState.booked_state
          else
            if (@item.payment_type.alias == PaymentType::PURCHASE_ALIAS)
              ticket.ticket_state = TicketState.bought_state
              if ticket.number.nil?
                ticket = self.create_ticket_normal_number(ticket)
              end
            end
          end
          ticket.save
        end
      end

      redirect_to path
    else
      render "new"
    end
    #update_item(@entity, @path, params[:id], true)
  end

  def destroy
    @item = @entity.find(params[:id])

    ticket_revert_message = ""

    # Возвращение статусов билетов
    Ticket.where(:payment_id => @item.id).each do |ticket|
      ticket.payment_id = nil
      ticket_revert_message = ticket_revert_message + " Ряд: " + ticket.seat_item.row + " Место: " + ticket.seat_item.number + " | "
      ticket.ticket_state = TicketState.available_state
      ticket.save
    end

    TicketRevert.create(event_item_id: @item.event_item_id, text: ticket_revert_message, date: DateTime.now)

    @item.destroy
    redirect_to @path

    #delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "payments",
          :entity => Payment,
          :param_name => :payment,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.payments_path,
              :new_path => nil,
              :edit_path => Rails.application.routes.url_helpers.payments_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Заказы" => nil
          },
          :fields => {
              :number => {
                  :type => :string,
                  :label => "Номер заказа",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :updated_at => {
                  :type => :datetime,
                  :label => "Дата",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :payment_type_id => {
                  :type => :collection,
                  :label => "Тип",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => PaymentType,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              },
              :sale_point_item_id => {
                  :type => :collection,
                  :label => "Точка продажи",
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => SalePointItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              },
              :user_id =>  {
                  :type => :collection,
                  :label => "Пользователь сайта",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => User,
                  :where_visible_field => :email,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  },
                  :help_message => "Пользователь, который производил покупку/бронирование билета через сайт"
              },
              :system_user_id =>  {
                  :type => :collection,
                  :label => "Кассир",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  },
                  :help_message => "Кассир, который производил покупку/бронирование билета через систему"
              },
              :delivery_label => {
                  :type => :group_label,
                  :label => "Информации по доставке"
              },
              :fio =>  {
                  :type => :string,
                  :label => "ФИО",
                  :show_in_card => true,
                  :show_in_table => false
              },
              :phone =>  {
                  :type => :string,
                  :label => "Телефон",
                  :show_in_card => true,
                  :show_in_table => false
              },
              :delivery_address =>  {
                  :type => :string,
                  :label => "Адрес доставки",
                  :show_in_card => true,
                  :show_in_table => false
              },
              :delivery_method_id =>  {
                  :type => :collection,
                  :label => "Способ доставки",
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => DeliveryMethod,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              },
              :payment_kind_id =>  {
                  :type => :collection,
                  :label => "Тип оплаты",
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => PaymentKind,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              },
              :ticket_label => {
                  :type => :group_label,
                  :label => "Билеты"
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
