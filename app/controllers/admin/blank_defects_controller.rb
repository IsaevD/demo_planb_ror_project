class Admin::BlankDefectsController < Admin::ApplicationController
  before_filter :init_variables


  private

    def init_variables

      @object = {
          :name => "blank_defects",
          :entity => BlankDefect,
          :param_name => :blank_defect,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.blank_defects_path,
              :new_path => Rails.application.routes.url_helpers.new_blank_defect_path,
              :edit_path => Rails.application.routes.url_helpers.blank_defects_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Брак" => nil
          },
          :fields => {
              :number => {
                  :type => :string,
                  :label => "Номер бланка",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :system_user_id =>  {
                  :type => :collection,
                  :label => "Кассир",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => {
                      :system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => "kassir").id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :blank_type_id =>  {
                  :type => :collection,
                  :label => "Тип бланка",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => BlankType,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :date => {
                  :type => :datetime,
                  :label => "Дата",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :sale_point_item_id =>  {
                  :type => :collection,
                  :label => "Точка продажи",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SalePointItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
