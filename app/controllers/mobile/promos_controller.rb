class Mobile::PromosController < Mobile::ApplicationController

  def index
    @page_header = "Акции"
    @items = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("published_at <= now() and ending_at >= now()").order(:published_at => :desc) #all_item(Admin::NewsItem, nil, true, true)
  end

  def show
    @promo = PromoItem.find(params[:id])
    @page_header = @promo.name
    @date_header = ((@promo.published_at.nil?) ? "—" : @promo.published_at.strftime("%d.%m.%Y"))
  end

end