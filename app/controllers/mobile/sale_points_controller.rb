class Mobile::SalePointsController < Mobile::ApplicationController

	def index
		@page_header = "Точки продаж"
    # done: Убрать проверку is_public в метод get_active_sale_points
		@sale_points = SalePointItem.get_active_sale_points
		@cities = City.get_active_cities
	end

	def show
		@sale_point = SalePointItem.find(params[:id])
		if (@sale_point.system_state_id == SystemMainItemsD::SystemState.active_state.id) && @sale_point.is_public
			@page_header = @sale_point.name
		else
			redirect_to :controller => 'index', :action => 'e404'
		end
	end

end