class Mobile::SearchController < Mobile::ApplicationController

  def index
    @page_header = "Результат поиска"
    search_text = params[:search_text]
    if !search_text.nil?
      @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("name like '%#{search_text}%' OR description like '%#{search_text}%' OR content like '%#{search_text}%'")
      @items = @events.map {|i| [i.id, i.name, i.description, "events", i.date] }
      @news = NewsItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("name like '%#{search_text}%' OR description like '%#{search_text}%' OR content like '%#{search_text}%'")
      @items = @items + @news.map {|i| [i.id, i.name, i.description, "news", i.published_at] }
      @promos = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("name like '%#{search_text}%' OR description like '%#{search_text}%' OR content like '%#{search_text}%'")
      @items = @items + @promos.map {|i| [i.id, i.name, i.description, "promos", i.published_at] }
      if (params[:sort] == "date")
        @items = @items.sort_by{|e| e[4]}.reverse
      end
    end
  end

end