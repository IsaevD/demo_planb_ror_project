class Mobile::IndexController < Mobile::ApplicationController

  def index
    @events = EventItem.active.published.not_passed.sort_by_date.limit(6)
    @enable_banner = true
    @banners = Banner.get_active_banner
  end

  def archive
    @page_header = "Архив"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
    @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("date <= now()")
    @items = @events.map {|i| [i.id, i.name, i.description, "events", i.published_at] }
    @promos = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("ending_at <= now()")
    @items = @items + @promos.map {|i| [i.id, i.name, i.description, "promos", i.published_at] }
    @items = @items.sort_by{|e| e[4]}.reverse
  end

  def archive_item
    case params[:category]
      when "events"
        @entity = EventItem.find(params[:id])
        @images = @entity.event_images
      when "promos"
        @entity = PromoItem.find(params[:id])
        @images = @entity.promo_images
    end
    @page_header = @entity.name
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Архив" => Rails.application.routes.url_helpers.archive_path,
        @page_header => nil
    }
  end

  def cart
    @page_header = "Корзина"
    @is_breadcrumbs = false
  end

  def order
    @page_header = "Заказ №12345"
    @status_header = "Ожидает доставки"
    @is_breadcrumbs = false
  end

  def profile
    @page_header = "Личный кабинет"
  end

  private

  def search_params
    params.permit(:date)
  end

end
