class Mobile::UsersController < Mobile::ApplicationController

  before_filter :custom_user_signed, :only => [:profile, :save_profile, :change_password]
  before_filter :custom_user_unsigned, :only => [:register, :register_complete, :restore, :restore_complete, :set_new_password, :set_new_password_complete]

  # Регистрация пользователя
  def register
    @page_header = t("users.register")
    if request.post?
      if create_new_user(prepared_register_params)
        redirect_to mobile_register_complete_path
      end
    end
  end
  def register_complete
    @page_header = t("users.register_complete")
  end
  # Подтверждение нового пользователя
  def confirm
    if !params[:user_confirmation_token].nil?
      if confirm_user_by_token(params[:user_confirmation_token])
        redirect_to mobile_confirm_complete_path
      else
        redirect_to root_path
      end
    else
      redirect_to root_path
    end
  end
  # Успешное подтверждение нового пользователя
  def confirm_complete
    @page_header = "Аккаунт подтвержден"
  end

  # Восстановление пароля
  def restore
    @page_header = t("users.restore_password")
    if request.post?
      if restore_user_password(params[:email])
        redirect_to mobile_restore_complete_path
      end
    end
  end
  def restore_complete
    @page_header = t("users.restore_password_send")
  end


  # Запрос нового пароля
  def set_new_password
    if !params[:change_password_token].nil?
      @page_header = "Изменить пароль"
      if request.post?
        if !User.find_by(:user_restore_password => params[:change_password_token]).nil?
          if set_new_user_password_by_token(params[:change_password_token])
            redirect_to mobile_set_new_password_complete_path
          end
        else
          redirect_to root_path
        end
      end
    else
      redirect_to root_path
    end
  end
  def set_new_password_complete
    @page_header = "Пароль успешно изменен"
  end

  # Профиль пользователя
  def profile
    @page_header = "Личный кабинет"
  end
  # Обновление профиля пользователя
  def save_profile
    user = current_site_user
    if !user.update_attributes(prepared_params)
      flash[:error] = user.errors.first[1]
    else
      flash[:success] = "Данные успешно обновлены"
    end
    redirect_to params[:path]
  end
  # Обновление пароля пользователя из личного кабинета
  def change_password
    if (current_site_user.authenticate(params[:old_password]))
      if (params[:new_password] == params[:repeat_new_password])
        user = current_site_user
        user.password = params[:new_password]
        user.password_confirmation = params[:repeat_new_password]
        if user.save
          flash[:success] = "Данные успешно обновлены"
        else
          flash[:error] = user.errors.first[1]
        end
      else
        flash[:error] = "Пароли не совпали"
      end
    else
      flash[:error] = "Старый пароль некорретен"
    end
    redirect_to params[:path]
  end


  private

    def prepared_register_params
      params.permit([:last_name, :first_name, :middle_name, :phone_number, :email, :phone, :discount_number, :password, :password_confirmation])
    end

end