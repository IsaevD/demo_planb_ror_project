class Mobile::EventsController < Mobile::ApplicationController

  def index
    @page_header = "Афиша"
    @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id)
    if !params[:event_type].nil?
      if params[:event_type] != ""
        event_tags = EventTag.where(:event_type_id => params[:event_type])
        @events = @events.where(:id => event_tags.map {|i| [i.event_item_id]})
      end
    end
    if !params[:event_place].nil?
      if params[:event_place] != ""
        @events = @events.where(:hall_id => params[:event_place])
      end
    end
    if !params[:date_of].nil? && !params[:date_to].nil?
      if (params[:date_of] != "") && (params[:date_to] != "")
        @date_range = params[:date_of] + " - " + params[:date_to]
        @events = @events.where("date >= '#{Date.parse(params[:date_of])} 00:00:00' AND date <= '#{Date.parse(params[:date_to])} 23:59:59'")
      end
    end
    @events = @events.where("published_at <= now()").order(:date => :desc)
  end

  def show
    @event_item = EventItem.find_by(url: params[:url]) || not_found

    redirect_to(mobile_archive_item_path('events', @event_item)) and return if @event_item.is_passed?

    @page_header = @event_item.name
    @date_header = ((@event_item.date.nil?) ? "—" : @event_item.date.strftime("%d.%m.%Y"))
  end

  def archive
    @page_header = "Архив"
    @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.closed_state.id).order(:date => :desc)

  end

  def archive_item
    @event_item = EventItem.find(params[:id])
    @page_header = @event_item.name
    @date_header = ((@event_item.date.nil?) ? "—" : @event_item.date.strftime("%d.%m.%Y"))
  end

end
