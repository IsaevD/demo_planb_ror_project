class Mobile::NewsController < Mobile::ApplicationController

  def index
    @page_header = t("labels.menu.news")
    @items = NewsItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("published_at <= now()").order(:published_at => :desc)
  end

  def show
    @news_item = NewsItem.find(params[:id])
    @page_header = @news_item.name
    @date_header = ((@news_item.published_at.nil?) ? "—" : @news_item.published_at.strftime("%d.%m.%Y"))
  end

end