class Mobile::ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  helper ObjectGeneratorD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper
  include ApplicationHelper
  include UsersHelper
  layout "mobile"
  before_filter :init_mobile_variables

  rescue_from ActiveRecord::RecordNotFound, with: :to_404

  def not_found
    raise ActiveRecord::RecordNotFound
  end

  def to_404
    redirect_to '/404'
  end

  private

    def init_mobile_variables
      @menus = NavigationMenu.where("(navigation_menu_type_id = #{NavigationMenuType.all_type.id} OR navigation_menu_type_id = #{NavigationMenuType.mobile_type.id}) AND system_state_id = #{SystemMainItemsD::SystemState.active_state.id}")
      @current_user = current_user(User, :user_remember_token)
    end

end
