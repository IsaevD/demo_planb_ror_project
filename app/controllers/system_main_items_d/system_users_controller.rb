module SystemMainItemsD
  class SystemUsersController < ApplicationController
    before_filter :init_variables

    def index
      all_item(@entity, nil, true, true, true)
    end

    def show
      get_item_by_id(@entity, params[:id], true)
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id], true)
    end

    def create
      #create_item(@entity, @path, true)

      entity = @entity
      path = @path
      is_log = true
      is_redirect = true

      @item = entity.new(prepared_params(@param_name, @params_array))
      if @item.save
        @fields.each do |field, settings|
          entity_params = params[@param_name]
          case settings[:type]
            when :dual_list
              if settings[:show_in_card]
                values = entity_params[field]
                if !values.nil?
                  values.each do |key, value|
                    settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                  end
                end
              end
            when :image_list
              if !entity_params[field].nil?
                entity_params[field].each do |value|
                  image_list = @fields[field][:image_entity].new
                  image_list.update_attribute(@fields[field][:entity_field], @item.id)
                  image_list.update_attribute(@fields[field][:image_entity_field], value)
                  image_list.save
                end
              end
          end
        end
        if (is_log)
          create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
        end
        if (is_redirect)
          generate_kassir(@item)
          redirect_to path
        end
      else
        render "new"
      end

    end

    def update
      #update_item(@entity, @path, params[:id], true)

      id = params[:id]
      entity = @entity
      path = @path
      is_log = true

      @item = entity.find(id)
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :image
            if entity_params[field] == "nil"
              params[@param_name][field] = nil
            end
          when :dual_list
            if settings[:show_in_card]
              current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
              current_values.destroy_all
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[settings[:image_entity_field]].nil?
              entity_params[settings[:image_entity_field]].each do |key, value|
                if value == "nil"
                  @fields[field][:image_entity].find(key).destroy
                end
              end
            end
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], params[:id])
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end

      if @item.update_attributes(prepared_params(@param_name, @params_array))
        if (is_log)
          create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
        end
        generate_kassir(@item)
        redirect_to path
      else
        render "new"
      end

    end

    def destroy
      delete_item(@entity, @path, params[:id], true)
    end

    private

      def init_variables

        @object = {
            :name => "system_users",
            :entity => SystemMainItemsD::SystemUser,
            :param_name => :system_user,
            :breadcrumbs => {
                t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
                "Пользователи системы" => nil
            },
            :paths => {
                :all_path => system_main_items_d.system_users_path,
                :new_path => system_main_items_d.new_system_user_path,
                :edit_path => system_main_items_d.system_users_path
            },
            :fields => {
                :login => {
                    :type => :string,
                    :label => t('form.labels.login'),
                    :show_in_card => true,
                    :show_in_table => true,
                    :required => true,
                    :help_message => "Указывается при входе в систему"
                },
                :email => {
                    :type => :string,
                    :label => t('form.labels.email'),
                    :show_in_card => true,
                    :show_in_table => true,
                    :required => true
                },
                :system_user_role_id => {
                    :type => :collection,
                    :label => t('form.labels.system_user_role_id'),
                    :show_in_card => true,
                    :show_in_table => true,
                    :where_entity => SystemMainItemsD::SystemUserRole,
                    :where_visible_field => :name,
                    :where_statement => nil,
                    :settings => {
                        :include_blank => false
                    }
                },
                :system_state_id =>  {
                    :type => :collection,
                    :label => t('form.labels.system_state_id'),
                    :show_in_table => false,
                    :show_in_card => true,
                    :where_entity => SystemMainItemsD::SystemState,
                    :where_visible_field => :name,
                    :where_statement => nil,
                    :settings => {
                        :include_blank => false
                    }
                },
                :label_data => {
                    :type => :group_label,
                    :label => "Персональные данные"
                },
                :first_name => {
                    :type => :string,
                    :label => t('form.labels.first_name'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :middle_name => {
                    :type => :string,
                    :label => t('form.labels.middle_name'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :last_name => {
                    :type => :string,
                    :label => t('form.labels.last_name'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :avatar => {
                    :type => :image,
                    :label => "Аватар",
                    :show_in_card => true,
                    :show_in_table => false
                },
                :label_password => {
                    :type => :group_label,
                    :label => "Изменение пароля"
                },
                :password => {
                    :type => :string,
                    :label => t('form.labels.password'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :password_confirmation => {
                    :type => :string,
                    :label => t('form.labels.password_confirmation'),
                    :show_in_card => true,
                    :show_in_table => false
                },
            }
        }
        @fields = @object[:fields]
        @visible_fields = []
        @fields.each do |key, value|
          if value[:show_in_table]
            @visible_fields << key
          end
        end
        @entity = @object[:entity]
        @param_name = @object[:param_name]
        @path = @object[:paths][:all_path]
        @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
      end

  end
end
