class Desktop::UsersController < Desktop::ApplicationController

  layout :choose_layout

  before_filter :custom_user_signed, :only => [:profile, :save_profile, :change_password, :history, :payments]
  before_filter :custom_user_unsigned, :only => [:register, :register_complete, :restore, :restore_complete, :set_new_password, :set_new_password_complete]

  # Регистрация пользователя
  def register
    @page_header = t("users.register")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
    if request.post?
      if create_new_user(prepared_register_params)
        redirect_to register_complete_path
      end
    end
  end

  # форма авторизации пользователя
  def autorization
    @page_header = t("users.autorization")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
  end

  def register_complete
    @page_header = t("users.register_complete")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
  end
  # Подтверждение нового пользователя
  def confirm
    if !params[:user_confirmation_token].nil?
      if confirm_user_by_token(params[:user_confirmation_token])
        redirect_to confirm_complete_path
      else
        redirect_to root_path
      end
    else
      redirect_to root_path
    end
  end
  # Успешное подтверждение нового пользователя
  def confirm_complete
    @page_header = "Аккаунт подтвержден"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
  end

  # Авторизация пользователя
  def log_in
    path = params[:current_path]
    user_log_in(params[:email], params[:password])
    redirect_to path
  end
  def log_out
    sign_out(User, :user_remember_token)
    if mobile_device?
      redirect_to "/mobile"
    else
      redirect_to root_path
    end
  end


  # Восстановление пароля
  def restore
    @page_header = t("users.restore_password")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
    if request.post?
      if restore_user_password(params[:email])
        redirect_to restore_complete_path
      end
    end
  end
  def restore_complete
    @page_header = t("users.restore_password_send")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
  end

  # Запрос нового пароля
  def set_new_password
    if !params[:change_password_token].nil?
      @page_header = "Изменить пароль"
      @breadcrumbs = {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
          @page_header => nil
      }
      if request.post?
        if !User.find_by(:user_restore_password => params[:change_password_token]).nil?
          if set_new_user_password_by_token(params[:change_password_token])
            redirect_to set_new_password_complete_path
          end
        else
          redirect_to root_path
        end
      end
    else
      redirect_to root_path
    end
  end
  def set_new_password_complete
    @page_header = "Пароль успешно изменен"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
  end


  # Профиль пользователя
  def profile
    @page_header = "Личный кабинет"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
  end

  # История заказов
  def history
    @page_header = "История заказов"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Личный кабинет" => Rails.application.routes.url_helpers.profile_path,
        @page_header => nil
    }
    @payments = Payment.get_payments_by_user(@current_user)
  end

  # Карточка заказа
  def payments
    payment_id = params[:id]
    if (!payment_id.nil?)
      @payment = Payment.find(payment_id)
      if @payment.nil?
        redirect_to :controller => 'index', :action => 'e404'
      else
        @page_header = "Заявка №#{@payment.number}"
        @breadcrumbs = {
            t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
            "Личный кабинет" => Rails.application.routes.url_helpers.profile_path,
            "История заказов" => Rails.application.routes.url_helpers.user_history_path,
            @page_header => nil
        }
        @is_edited = (@payment.payment_type.alias == PaymentType::RESERVATION_ALIAS)
      end
    else
      redirect_to :controller => 'index', :action => 'e404'
    end
  end

  def payment_save
    if !params[:payment_id].nil?
      payment = Payment.find(params[:payment_id])
      if !payment.nil?
        if payment.payment_type.alias == PaymentType::RESERVATION_ALIAS
          # Обновление билетов
          payment.tickets.each do |ticket|
            ticket.payment_id = nil
            ticket.ticket_state = TicketState.available_state
            ticket.save
          end
          if !params[:tickets].nil?
            params[:tickets].each do |key, value|
              ticket = Ticket.find(key)
              ticket.payment_id = payment.id
              if (payment.payment_type.alias == PaymentType::RESERVATION_ALIAS)
                ticket.ticket_state = TicketState.booked_state
              else
                if (@item.payment_type.alias == PaymentType::PURCHASE_ALIAS)
                  ticket.ticket_state = TicketState.bought_state
                end
              end
              ticket.save
            end
          end
        end
      end
    end
    redirect_to user_history_path
  end

  # Обновление профиля пользователя
  def save_profile
    user = current_site_user
    if !user.update_attributes(prepared_params)
      flash[:error] = user.errors.first[1]
    else
      flash[:success] = "Данные успешно обновлены"
    end
    redirect_to params[:path]
  end
  # Обновление пароля пользователя из личного кабинета
  def change_password
    if (current_site_user.authenticate(params[:old_password]))
      if (params[:new_password] == params[:repeat_new_password])
        user = current_site_user
        user.password = params[:new_password]
        user.password_confirmation = params[:repeat_new_password]
        if user.save
          flash[:success] = "Данные успешно обновлены"
        else
          flash[:error] = user.errors.first[1]
        end
      else
        flash[:error] = "Пароли не совпали"
      end
    else
      flash[:error] = "Старый пароль некорретен"
    end
    redirect_to params[:path]
  end


  private

    def prepared_params
      params.permit([:last_name, :first_name, :middle_name, :phone_number, :email, :phone, :discount_number])
    end

    def prepared_register_params
      params.permit([:last_name, :first_name, :middle_name, :phone_number, :email, :phone, :discount_number, :password, :password_confirmation])
    end

    # done: Используй более логически понятные наименования методов. Сейчас этот метод переводится как Отметить шаблон.
    def choose_layout
      case action_name
        when "set_new_password_complete", "restore", "restore_complete", "register"
          "desktop_without_events_panel"
        else
          "desktop_without_events_panel_with_user_btns"
      end
    end

end
