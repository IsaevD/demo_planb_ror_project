class Desktop::EventsController < Desktop::ApplicationController

  layout "desktop_without_events_panel"

  before_filter :custom_user_signed, :only => [:buy_ticket]

  def index
    @page_header = t("custom.modules.events.name")
    @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id)
    if !params[:event_type].nil?
      if params[:event_type] != ""
        event_tags = EventTag.where(:event_type_id => params[:event_type])
        @events = @events.where(:id => event_tags.map {|i| [i.event_item_id]})
      end
    end
    if !params[:event_place].nil?
      if params[:event_place] != ""
        @events = @events.where(:hall_id => params[:event_place])
      end
    end
    if !params[:date_of].nil? && !params[:date_to].nil?
      if (params[:date_of] != "") && (params[:date_to] != "")
        @date_range = params[:date_of] + " - " + params[:date_to]
        @events = @events.where("date >= '#{Date.parse(params[:date_of])} 00:00:00' AND date <= '#{Date.parse(params[:date_to])} 23:59:59'")
      end
    end
    unless params[:price_of].nil? && params[:price_to].nil?
      tickets_in_price_range = Ticket.where("price >= '#{params[:price_of]}' AND price <= '#{params[:price_to]}'").map { |k,v| k.event_item_id }
      tickets_in_price_range.uniq
      @events = @events.where(id: tickets_in_price_range)
      @price_of = params[:price_of]
      @price_to = params[:price_to]
    else
      @price_of = get_events_border_price("min")
      @price_to = get_events_border_price("max")
    end

    # toDO: Необходимо пересмотреть/обсудить фильтрацию по дату публикации
    @events = @events.where("published_at <= now() AND date >= now()").order(:date => :desc)

    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.modules.events.name") => nil
    }
  end

  def show
    @event_item = EventItem.find_by(url: params[:url]) || not_found

    redirect_to(archive_item_path('events', @event_item)) and return if @event_item.is_passed?

    @page_header = @event_item.name
    @date_header = ((@event_item.date.nil?) ? "—" : @event_item.date.strftime("%d.%m.%Y"))

    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.modules.events.name") => Rails.application.routes.url_helpers.events_path,
        @event_item.name => nil
    }
  end

  def buy_ticket
    @event_item = EventItem.find(params[:id])
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.modules.events.name") => Rails.application.routes.url_helpers.events_path,
        @event_item.name => nil
    }

    case @event_item.hall.alias
      when "filarmonia"
        @parter = SectorGroup.find_by(:alias => "filarmonia_parter")
        @balkon = SectorGroup.find_by(:alias => "filarmonia_balkon")
      when "aksion"
        @parter = SectorGroup.find_by(:alias => "aksion_parter")
        @amfiteatr = SectorGroup.find_by(:alias => "aksion_amfiteatr")
        @balkon = SectorGroup.find_by(:alias => "aksion_balkon")
        @vip1 = SectorGroup.find_by(:alias => "aksion_vip_1")
        @vip2 = SectorGroup.find_by(:alias => "aksion_vip_2")
      when "metallurg_without_dancing"
        @parter = SectorGroup.find_by(:alias => "metallurg_without_dancing_parter")
        @central_parter = SectorGroup.find_by(:alias => "metallurg_without_dancing_central_parter")
        @amfiteatr = SectorGroup.find_by(:alias => "metallurg_without_dancing_amfiteatr")
        @balkon = SectorGroup.find_by(:alias => "metallurg_without_dancing_balkon")
        @loga1 = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_1")
        @loga2 = SectorGroup.find_by(:alias => "metallurg_without_dancing_loga_2")
      when "metallurg_with_dancing"
        @dancing_room = SectorGroup.find_by(:alias => "metallurg_with_dancing_dancing_room")
        @central_parter = SectorGroup.find_by(:alias => "metallurg_with_dancing_central_parter")
        @amfiteatr = SectorGroup.find_by(:alias => "metallurg_with_dancing_amfiteatr")
        @balkon = SectorGroup.find_by(:alias => "metallurg_with_dancing_balkon")
        @loga1 = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_1")
        @loga2 = SectorGroup.find_by(:alias => "metallurg_with_dancing_loga_2")
      when 'opera_theatre'
        @parter = SectorGroup.find_by(alias: 'opera_theatre_parter')
        @balkon = SectorGroup.find_by(alias: 'opera_theatre_balkon')
        @balkon2 = SectorGroup.find_by(alias: 'opera_theatre_balkon2')
        @left_pandus = SectorGroup.find_by(alias: 'opera_theatre_pandus_left')
        @right_pandus = SectorGroup.find_by(alias: 'opera_theatre_pandus_right')
      when 'dancing'
        @dancing_room = SectorGroup.find_by(:alias => "dancing_dancing_room")
      when "cyrk"
        @sectors = Sector.where(:schema_item_id => @event_item.hall.schema_item_id)

        if params[:sector_id].present?
          sector = Sector.find(params[:sector_id])
          @main = sector.sector_groups.first
        end
      when "izhstal"
        @sectors = Sector.where(:schema_item_id => @event_item.hall.schema_item_id)

        if params[:sector_id].present?
          sector = Sector.find(params[:sector_id])
          @main = sector.sector_groups.first
        end
      end

    @hall = @event_item.hall
    @schema_item = @hall.schema_item
    @seats = SeatItem.where(:schema_item_id => @schema_item.id)
    seats_groups_array = @seats.distinct.pluck('seat_item_type_id')
    @seat_types = SeatItemType.where('id in (?)',seats_groups_array).order('price')
    # maybe sometime somebody wanted to make another logic, but for now will be so
    @ticket_colors = SeatItemType.all


    #@schema = get_schema(@event_item)
    #@tickets = Ticket.get_tickets_by_event_id(@event_item.id)
  end

  # Функционал: этап Подтверждение заказа
  def confirm_order

    @page_header = t("custom.labels.confirm_order")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.labels.confirm_order") => nil
    }
    @ticket_ids = cookies[:planb_order]

    if !@ticket_ids.empty?

      flash[:booking_error] = nil
      flash[:ticket_error] = nil
      @tickets = Ticket.where("id in (?)", @ticket_ids.split(","))
      @event_item = @tickets.first.event_item
      @booking_enabled = EventItem.check_booking_disabled_in(@event_item)

      # Проверка типа запроса
      if request.post?
        # Если электронный билет
        if params[:check_buy_ticket].present?
          redirect_to internet_payment_path
        elsif params[:check_delivery_ticket].present?
          redirect_to choose_delivery_data_path
        elsif params[:check_book_ticket].present?
          redirect_to confirm_book_path
        else
          flash[:ticket_error] = "Не выбрано действие"
        end
      else

      end
    else
      redirect_to events_path
    end

    # ticketIDs = cookies[:planb_order]
    # if ticketIDs.empty?
    #   flash[:booking_error] = nil
    #   ticketIDs = ticketIDs.split(',')
    #   @tickets = Ticket.where("id in (?)", ticketIDs)
    #   @event_item = EventItem.find(@tickets.first().event_item_id)
    #
    #   if request.post?
    #     if !params[:check_book_ticket].nil?
    #
    #       if !@booking_enabled
    #         redirect_to confirm_order_path
    #       else
    #         # toDO: Сделать проверку на статус билета
    #         @payment = Payment.create_payment(@current_user.id, nil, @event_item.id, PaymentType.reservation_state)
    #         @tickets.each do |ticket|
    #           ticket.ticket_state = TicketState.booked_state
    #           ticket.payment_id = @payment.id
    #           ticket.save
    #         end
    #         cookies[:planb_order] = nil
    #         render :action => "order_result"
    #       end
    #
    #     else
    #       if !params[:check_buy_ticket].nil?
    #         unless params[:payment_id].nil?
    #           cookies[:planb_order] = Payment.find(params[:payment_id].to_i).tickets.map(&:id).join(",")
    #         end
    #         redirect_to choose_payment_methods_path
    #       end
    #     end
    #   else
    #     @page_header = "Подтверждение заказа"
    #     @breadcrumbs = {
    #         t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
    #         "Подтверждение заказа" => nil
    #     }
    #     if !@booking_enabled
    #       flash[:booking_error] = t("custom.messages.booking_disabled", :days => Setting.find_by(:alias => 'booking_disabled_in').value)
    #     end
    #     #ids = cookies[:planb_order].split(',')
    #     #@tickets = Ticket.where("id in (?)", ids)
    #   end
    # end
  end

  # Функционал: шаблон вывода успешного бронирования
  def success_book_tickets
    @page_header = t("custom.labels.success_book_ticket")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.labels.success_book_ticket") => nil
    }
  end

  # Функционал: этап Электронный билет
  def internet_payment
    @page_header = "Покупка электронного билета"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Покупка электронного билета" => nil
    }

    ticket_ids = cookies[:planb_order]

    if ticket_ids.present?
      tickets = Ticket.where(id: ticket_ids.split(","))

      total = tickets.map(&:price).reduce(:+)

      description = tickets.map do |t|
        t.event_item.name + " : Ряд - " + t.seat_item.row.to_s + " , Место - " + t.seat_item.number.to_s
      end.join('. ')

      @transaction = PaymentTransaction.create(price: total, description: description, status: PaymentTransaction::STARTED_STATUS)
    else
      redirect_to events_path
    end
  end


  # Функционал: заполнение данных для доставки курьером
  def choose_delivery_data
    @page_header = "Заполнение данных для доставки"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Заполнение данных для доставки" => nil
    }
    @ticket_ids = cookies[:planb_order]
    if !@ticket_ids.empty?
      @tickets = Ticket.where("id in (?)", @ticket_ids.split(","))
      @event_item = @tickets.first().event_item
      flash[:ticket_error] = nil
      if request.post?
        if params[:delivery_address].nil?
          flash[:ticket_error] = "Не указан адрес доставки"
        else
          payment_kind = nil
          if !params[:cash].nil?
            payment_kind = PaymentKind.cash_kind
          end
          if !params[:uncash].nil?
            payment_kind = PaymentKind.cashless_kind
          end
          payment = Payment.create_payment(@current_user.id, nil, @event_item.id, PaymentType.purchase_state, DeliveryMethod.courier_state, "#{@current_user.first_name} #{@current_user.last_name}", @current_user.phone, params[:delivery_address], nil, payment_kind.id)


          @tickets.each do |ticket|
            if (ticket.ticket_state.alias == TicketState::AVAILABLE_ALIAS)
              ticket.ticket_state = TicketState.bought_state
              ticket.payment_id = payment.id
              ticket.save
            end
          end
          cookies[:planb_order] = nil
          redirect_to order_result_path('', id: payment.id)
        end
      end
    else
      redirect_to events_path
    end
  end

  # Функционал: покупка черех кассу
  def confirm_book
    @page_header = "Подтвердите состав заказа"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Подтверждение состава заказа" => nil
    }
    @ticket_ids = cookies[:planb_order]
    if !@ticket_ids.empty?
      @tickets = Ticket.where("id in (?)", @ticket_ids.split(","))
      @event_item = @tickets.first().event_item
      flash[:ticket_error] = nil
      if request.post?
        payment = Payment.create_payment(@current_user.id, nil, @event_item.id, PaymentType.reservation_state, DeliveryMethod.himself_state, "#{@current_user.first_name} #{@current_user.last_name}", @current_user.phone, params[:delivery_address], nil, 0)
        @tickets.each do |ticket|
          if (ticket.ticket_state.alias == TicketState::AVAILABLE_ALIAS)
            ticket.ticket_state = TicketState.booked_state
            ticket.payment_id = payment.id
            ticket.save
          end
        end
        cookies[:planb_order] = nil
        redirect_to order_result_path('', id: payment.id)
      end
    else
      redirect_to events_path
    end
  end

  # Заполнение данных для доставки курьером
  def choose_payment_methods
    if request.post?
      if !params[:internet].nil?
        redirect_to internet_payment_path
        #redirect_to order_result_path, :flash => { :text => 'Билеты успешно куплено чезе интернет. !Отладочная информация! - успешно установлена связь с сервером Robokassa' }
        #redirect_to payments_path
      else
        redirect_to choose_delivery_methods_path
      end
    else
      @breadcrumbs = {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
          "Выбор способа оплаты" => nil
      }
    end
  end

  def choose_delivery_methods
    if request.post?
      empty_fields = []
      empty_fields << "Адрес доставки" if params[:delivery_address].empty?
      empty_fields << "Контактный телефон" if params[:delivery_contact].empty?
      if empty_fields.count != 0
        @breadcrumbs = {
            t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
            "Выбор способа доставки" => nil
        }
        flash[:required_fields] = "Вы не заполнини поля - " + empty_fields.join(', ')
      else
        if !params[:courier].nil?
          deliver_method = DeliveryMethod.courier_state
        else
          deliver_method = DeliveryMethod.himself_state
        end

        ids = cookies[:planb_order].split(',')
        tickets = Ticket.where("id in (?)", ids)
        event_item_id = tickets.first().event_item_id
        payment = Payment.create_payment(@current_user.id, nil, event_item_id, PaymentType.purchase_state, deliver_method, params[:delivery_contact], params[:delivery_address])
        tickets.each do |ticket|
          ticket.ticket_state = TicketState.bought_state
          ticket.payment_id = payment.id
          ticket.save
        end

        TicketsMailer.order_tickets_mail(payment, @current_user).deliver
        TicketsMailer.user_order_mail(payment, @current_user).deliver


        cookies[:planb_order] = nil
        redirect_to order_result_path('', id: payment.id), :flash => { :text => 'Билеты успешно куплены.' }
      end
    else
      @breadcrumbs = {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
          "Выбор способа доставки" => nil
      }
    end
  end

  def order_result
    @payment = Payment.find(params[:id])
    @event_item = @payment.event_item
    @tickets = @payment.tickets
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Результат" => nil
       # @event_item.name => nil
    }
  end

  def ajax_get_tickets
    tickets = Ticket.get_tickets_by_event_id(params[:event_id]).map {|i| [i.seat_item_id, i.ticket_state.alias, i.price] }
    render :json => tickets
  end

  def ajax_get_tickets_by_ids
    if !params[:ticket_ids].nil?
      render :json => Ticket.get_tickets_by_ids(params[:ticket_ids])
    else
      render :json => nil
    end
  end

  def ajax_get_ticket_price
    price = Ticket.get_ticket_price(params[:event_id], params[:seat_id])
    render :text => price
  end

  def ajax_create_collective_order
    request = Request.new
    request.phone = params[:phone]
    request.request_type_id = RequestType.collective_order_type.id
    event = EventItem.find(params[:event_id])
    request.message = "На мероприятие '#{event.name}'(#{event.id}) запрошена коллективная заявка на покупку билета. Телефон - #{params[:phone]}"
    if request.save
      MainMailer.collective_mail(request).deliver
    end
    render :text => "Success"
  end


end
