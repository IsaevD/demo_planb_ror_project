class Desktop::ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  helper ObjectGeneratorD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper

  protect_from_forgery with: :exception
  include ApplicationHelper
  include UsersHelper
  include TicketsHelper
  include EventsHelper
  layout "desktop"
  before_filter :init_desktop_variables, :check_mobile_device

  rescue_from ActiveRecord::RecordNotFound, with: :to_404

  def not_found
    raise ActiveRecord::RecordNotFound
  end

  def to_404
    redirect_to '/404'
  end

  private

    def init_desktop_variables
      @long_banner = Banner.get_active_banner(BannerType.minor_type, 1).first()
      @partners = PartnerItem.get_active_partners
      @menus = NavigationMenu.where("(navigation_menu_type_id = #{NavigationMenuType.all_type.id} OR navigation_menu_type_id = #{NavigationMenuType.desktop_type.id}) AND system_state_id = #{SystemMainItemsD::SystemState.active_state.id}")
      @current_user = current_user(User, :user_remember_token)
      @cart_info = Ticket.get_tickets_main_info(cookies[:planb_order])
      # @order_ids = nil

      if !cookies[:planb_order].nil?
        if cookies[:planb_order] != ""
          @order_ids = cookies[:planb_order].split(",")
          @cart_event = Ticket.find(@order_ids).first().event_item
        end
      end


        @order_ids = cookies[:planb_order].split(",") if cookies[:planb_order].present?


      #if !@cart_info[2].nil?
        #cookies[:planb_order] = @cart_info[2].join(",")
      #end
    end

    def check_mobile_device
      if params[:no_mobile].nil?
        if (cookies[:mobile] != "no")
          if mobile_device?
            redirect_to mobile_path
          end
        end
      else
        cookies[:mobile] = "no"
      end
    end

end
