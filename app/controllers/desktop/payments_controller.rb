class Desktop::PaymentsController < Desktop::ApplicationController
  layout "desktop_without_events_panel"

  before_filter :custom_user_signed, only: [:show, :destroy]

  def print
    payment = Payment.includes(tickets: :seat_item).find(params[:id])

    if current_site_user == payment.user
      send_data PaymentPDFService.new(payment).to_pdf, filename: 'e_ticket.pdf'
    else
      redirect_to(root_path, alert: 'Нет доступа') 
    end
  end

  def destroy
    @payment = Payment.find(params[:id])

    if current_site_user == @payment.user
      if @payment.cancelable?
        ticket_revert_message = ""

        Ticket.where(:payment_id => @payment.id).find_each do |ticket|
          ticket_revert_message = "#{ticket_revert_message} Ряд: #{ticket.seat_item.row} Место: #{ticket.seat_item.number}' | '"

          ticket.payment_id = nil
          ticket.ticket_state = TicketState.available_state
          ticket.save
        end

        ticket_revert = TicketRevert.create(
          event_item_id: @payment.event_item_id, text: ticket_revert_message, date: DateTime.now
        )

        @payment.destroy
      end

      redirect_to user_history_path
    else
      redirect_to(root_path, alert: 'Нет доступа') 
    end
  end
end
