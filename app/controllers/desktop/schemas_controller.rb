class Desktop::SchemasController < Desktop::ApplicationController

  layout "desktop_without_events_panel"

  def index

    @page_header = t("custom.modules.promo_items.name")
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Схема" => nil
    }

    @parter = SectorGroup.find_by(:alias => "filarmonia_parter")
    @no_named = SectorGroup.find_by(:alias => "filarmonia_no_name")
    @balkon = SectorGroup.find_by(:alias => "filarmonia_balkon")

  end

end