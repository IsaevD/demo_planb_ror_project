class Desktop::NewsController < Desktop::ApplicationController

  layout "desktop_without_events_panel"

  def index
    @page_header = t("custom.modules.news.name")
    @items = NewsItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("published_at <= now()").order(:published_at => :desc)
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.modules.news.name") => nil
    }
  end

  def show
    @news_item = NewsItem.find(params[:id])
    if (@news_item.published_at <= DateTime.now) && (@news_item.system_state_id == SystemMainItemsD::SystemState.active_state.id)
      @page_header = @news_item.name
      @date_header = ((@news_item.published_at.nil?) ? "—" : @news_item.published_at.strftime("%d.%m.%Y"))
      @breadcrumbs = {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
          t("labels.menu.news") => Rails.application.routes.url_helpers.news_index_path,
          @news_item.name => nil
      }
    else
      redirect_to :controller => 'index', :action => 'e404'
    end
  end

end