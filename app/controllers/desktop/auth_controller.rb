class Desktop::AuthController < Desktop::ApplicationController

  before_filter :init_variables

  def new
    if signed_in(current_user(@entity, @remember_token_name))
      redirect_to @path
    end
  end

  def create
    user = @entity.where(@login_param => params[@login_param], :system_state_id => SystemMainItemsD::SystemState.active_state.id).first()
    password = params[@password_param]
    if !user.nil?
      flash[:error] = nil
      if user.confirmed
        if authorize_user(@entity, @remember_token_name, user, password)
          redirect_to @path
        else
          flash[:error] = 'Неверное имя пользователя/пароль'
          redirect_to @path
        end
      else
        flash[:error] = 'Аккаунт неподтвержден'
        redirect_to @path
      end
    else
      flash[:error] = 'Неверное имя пользователя/пароль'
      redirect_to @path
    end
  end

  def destroy
    sign_out(@entity, @remember_token_name)
    if mobile_device?
      redirect_to "/mobile"
    else
      redirect_to root_path
    end
  end

  private

    def init_variables
      @entity = User
      @path = params[:current_path]
      @remember_token_name = :user_remember_token
      @param_name = :auth_user
      @login_param = :email
      @password_param = :password
    end


end