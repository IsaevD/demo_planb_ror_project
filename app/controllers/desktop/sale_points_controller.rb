class Desktop::SalePointsController < Desktop::ApplicationController

  layout "desktop_without_events_panel"

  def index
    @page_header = t("custom.modules.sale_point_items.all_records")
    @sale_points = SalePointItem.get_active_sale_points.where("is_public = true")
    @cities = City.get_active_cities
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.modules.sale_point_items.name") => nil
    }
  end

  def show
    @sale_point = SalePointItem.find(params[:id])
    if (@sale_point.system_state_id == SystemMainItemsD::SystemState.active_state.id) && @sale_point.is_public
      @page_header = @sale_point.name
      @breadcrumbs = {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
          t("custom.modules.sale_point_items.name") => Rails.application.routes.url_helpers.sale_points_path,
          @sale_point.name => nil
      }
    else
      redirect_to :controller => 'index', :action => 'e404'
    end
  end

  def ajax_get_sale_point_coords
    sale_point = SalePointItem.find(params[:id])
    coords = [sale_point.coor_x, sale_point.coor_y]
    render :json => coords
  end

  def ajax_get_cities
    cities = City.get_active_cities
    render :json => cities
  end

  def ajax_get_sale_points_by_city_id
    sale_points = SalePointItem.where(:city_id => params[:id])
    render :json => sale_points
  end

  def ajax_get_city
    city = City.find(params[:id])
    render :json => city
  end

end