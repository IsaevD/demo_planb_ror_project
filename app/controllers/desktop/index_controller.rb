class Desktop::IndexController < Desktop::ApplicationController

  #layout 'desktop', :only => :index
  layout 'desktop_without_events_panel', :only => [:cart, :certificate, :order, :profile, :search, :archive, :archive_item, :e404]

  def index
    @last_news_items = NewsItem.get_last_news(3)
    @events = EventItem.active.published.not_passed.sort_by_date.limit(6)
    @banners = Banner.get_active_banner
    render :layout => 'desktop'
  end

  def static_page
    @static_page = StaticPage.find_by(:alias => params[:alias])
    if !@static_page.nil?
      if (@static_page.system_state_id == SystemMainItemsD::SystemState.active_state.id)
        @page_header = @static_page.name
        @breadcrumbs = {
            t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
            @static_page.name => nil
        }
        render :layout => 'desktop_without_events_panel'
      else
        redirect_to :controller => 'index', :action => 'e404'
      end
    else
      redirect_to :controller => 'index', :action => 'e404'
    end
  end

  def search
    @page_header = "Поиск"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
    search_text = params[:search_text].downcase
    if !search_text.nil? && ( search_text.length != 0 )
      @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("name like '%#{search_text}%' OR description like '%#{search_text}%' OR content like '%#{search_text}%'")
      @items = @events.map {|i| [i.url, i.name, i.description, "events", i.date] }
      @news = NewsItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("name like '%#{search_text}%' OR description like '%#{search_text}%' OR content like '%#{search_text}%'")
      @items = @items + @news.map {|i| [i.id, i.name, i.description, "news", i.published_at] }
      @promos = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("name like '%#{search_text}%' OR description like '%#{search_text}%' OR content like '%#{search_text}%'")
      @items = @items + @promos.map {|i| [i.id, i.name, i.description, "promos", i.published_at] }
      if (params[:sort] == "date")
        @items = @items.sort_by{|e| e[4]}.reverse
      end
    end
  end

  def e404
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Ошибка 404" => nil
    }
    render status: 404
  end

  def ajax_create_call_request
    request = Request.new
    request.name    = params[:name]
    request.phone   = params[:phone]
    request.message = params[:message]
    request.request_type_id = RequestType.call_type.id
    request.message = "Запрошен обратный звонок на номер #{params[:phone]}"
    if request.save
      # Отправка уведомления на почту
      MainMailer.callback_mail(request).deliver
    end
    render :text => "Success"
  end

  def ajax_create_certificate_request
    request = Request.new
    request.phone = params[:phone]
    request.request_type_id = RequestType.certificate_type.id
    request.message = "Запрошен сертификат на сумму #{params[:value]} на номер #{params[:phone]}"
    if request.save
      MainMailer.certificate_mail(request).deliver
    end
    #if @order.save
    #  MainMailer.order_mail(@order).deliver
    #end
    render :text => "Success"
  end

  # Функционал: получить настройку Максимальное возможное количество покупки билетов
  def ajax_get_max_ticket_counter
    ticket_max_counter = Setting.get_setting(Setting.TICKET_MAX_COUNTER_ALIAS)
    render :text => ticket_max_counter
  end

  def ajax_get_events_info
    @events = EventItem.where("published_at <= now() AND date >= now()").where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).order('date asc').map {|i| [
      i.url.tr('/', ''),
      i.date.strftime("%Y"),
      i.date.strftime("%-m").to_i,
      i.date.strftime("%-d"),
      i.date.strftime("%Y-%m-%-d")
    ]}
    # @TODO have no idea why someone provided only unique by day :/
    # @events = @events.uniq { |e| e[-1] }
    render :json => @events
  end

  def archive
    @page_header = "Архив"
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        @page_header => nil
    }
    case params['filter']
      when 'events'
        @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("date <= now()")
        @items = @events.map {|i| [i.id, i.name, i.description, "events", i.published_at] }
      when 'promos'
        @promos = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("ending_at <= now()")
        @items = @promos.map {|i| [i.id, i.name, i.description, "promos", i.published_at] }
      else
        @events = EventItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("date <= now()")
        @items = @events.map {|i| [i.id, i.name, i.description, "events", i.published_at] }
        @promos = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("ending_at <= now()")
        @items = @items + @promos.map {|i| [i.id, i.name, i.description, "promos", i.published_at] }
    end
    @items = @items.sort_by{|e| e[4]}.reverse
  end

  def archive_item
    case params[:category]
      when "events"
        @entity = EventItem.find(params[:id])
        @images = @entity.event_images
      when "promos"
        @entity = PromoItem.find(params[:id])
        @images = @entity.promo_images
    end
    @page_header = @entity.name
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        "Архив" => Rails.application.routes.url_helpers.archive_path,
        @page_header => nil
    }
  end

  def cart
    @page_header = "Корзина"
    @is_breadcrumbs = false
  end

  # def certificate
  #   @page_header = "Сертификат"
  #   @breadcrumbs = {
  #       t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
  #       @page_header => nil
  #   }
  # end

  def order
    @page_header = "Заказ №12345"
    @status_header = "Ожидает доставки"
    @is_breadcrumbs = false
  end

  def profile
    @page_header = "Личный кабинет"
  end

  private

  def search_params
    params.permit(:date)
  end

end
