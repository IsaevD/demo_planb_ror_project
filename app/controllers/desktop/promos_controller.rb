class Desktop::PromosController < Desktop::ApplicationController

  layout "desktop_without_events_panel"

  def index
    @page_header = t("custom.modules.promo_items.name")
    @items = PromoItem.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).where("published_at <= now() and ending_at >= now()").order(:published_at => :desc) #all_item(Admin::NewsItem, nil, true, true)
    @breadcrumbs = {
        t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
        t("custom.modules.promo_items.name") => nil
    }
  end

  def show
    @promo_item = PromoItem.find(params[:id])

    if (@promo_item.published_at <= DateTime.now) && (@promo_item.ending_at >= DateTime.now) && (@promo_item.system_state_id == SystemMainItemsD::SystemState.active_state.id)
      @page_header = @promo_item.name
      @date_header = ((@promo_item.published_at.nil?) ? "—" : @promo_item.published_at.strftime("%d.%m.%Y"))
      @breadcrumbs = {
          t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.root_path,
          t("custom.modules.promo_items.name") => Rails.application.routes.url_helpers.promos_path,
          @promo_item.name => nil
      }
    else
      redirect_to :controller => 'index', :action => 'e404'
    end

  end

end