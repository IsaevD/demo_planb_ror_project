# == Schema Information
#
# Table name: blank_defects
#
#  id                 :integer          not null, primary key
#  number             :string(255)
#  ticket_id          :integer
#  created_at         :datetime
#  updated_at         :datetime
#  system_user_id     :integer
#  blank_type_id      :integer
#  event_item_id      :integer
#  date               :datetime
#  sale_point_item_id :integer
#

class BlankDefect < ActiveRecord::Base

  scope :with_blank_type, -> { includes(:blank_type) }

  belongs_to :event_item
  belongs_to :ticket
  belongs_to :blank_type
  belongs_to :sale_point_item
  belongs_to :system_user

  def self.create_blank_defect(ticket, user)
    BlankDefect.create(
      ticket_id: ticket.id, number: ticket.number, blank_type_id: ticket.blank_type_id,
      system_user_id: user.id
    )
  end

end
