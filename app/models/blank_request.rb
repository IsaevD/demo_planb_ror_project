# == Schema Information
#
# Table name: blank_requests
#
#  id                     :integer          not null, primary key
#  blank_delivery_type_id :integer
#  blank_type_id          :integer
#  system_user_id         :integer
#  value                  :integer
#  created_at             :datetime
#  updated_at             :datetime
#  delivery_date          :datetime
#  event_item_id          :integer
#  blank_request_type_id  :integer
#

class BlankRequest < ActiveRecord::Base

  belongs_to :blank_delivery_type
  belongs_to :blank_type
  belongs_to :event_item
  belongs_to :blank_request_type

  validates :value,
      presence: true

  validates :delivery_date,
      presence: true

end
