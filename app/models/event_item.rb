# == Schema Information
#
# Table name: event_items
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  message            :text
#  content            :text
#  date               :datetime
#  hall_id            :integer
#  system_state_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  description        :text
#  published_at       :datetime
#  url                :string(255)
#

class EventItem < ActiveRecord::Base
  VALID_ALIAS_REGEX = /[a-zA-Z_]+/

  scope :active, -> { where(:system_state_id => SystemMainItemsD::SystemState.active_state.id) }
  scope :published, -> { where('published_at <= ?', DateTime.now) }
  scope :not_passed, -> { where('date > ?', DateTime.now) }
  scope :sort_by_date, -> { order(date: :asc) }

  belongs_to :hall

  has_many :banners, dependent: :destroy

  # has_many :blank_counters, dependent: :destroy
  # has_many :blank_defects, dependent: :destroy
  # has_many :kassir_counters, dependent: :destroy

  has_many :blank_stocks, dependent: :destroy
  has_many :blank_transfers, dependent: :destroy

  has_many :event_images, dependent: :destroy
  has_many :event_tags, dependent: :destroy
  has_many :payments, dependent: :destroy
  has_many :tickets, dependent: :destroy
  has_many :ticket_discounts, dependent: :destroy

  has_attached_file :image, :styles => { :card => "662x350", :list => "480x200", :preview => "128x55" , :thumb => "100x100" }, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :name, presence: true, length: { minimum: 4, maximum: 255 }
  validates :published_at, :description, :date, :content, presence: true
  validates :url, presence: true, length: { minimum: 4, maximum: 50 }, uniqueness: true,
    format: { with: VALID_ALIAS_REGEX }

  before_save :clear_url_from_wrong_symbols

  def self.get_actual_events
    EventItem.where( "date > ?", Date.today )
  end

  def self.check_booking_disabled_in(event_item)
    booking_disabled_in = Setting.get_setting(Setting.BOOKING_DISABLED_IN)
    if !booking_disabled_in.nil?
      if (event_item.date - booking_disabled_in.to_i*60*60*24) > Time.now
        return true
      else
        # @TODO Доступ к flash есть только в контроллере
        # @TODO убрать отсюда
        # flash[:booking_error] = "Tort"
        return false
      end
    else
      # @TODO Доступ к flash есть только в контроллере
      # @TODO убрать отсюда
      # flash[:booking_error] = "Tort"
      return false
    end
  end

  def is_passed?
    date <= DateTime.now
  end

  private

    def clear_url_from_wrong_symbols
      self.url = self.url.tr('^A-Za-z0-9_', '')
    end
end
