# == Schema Information
#
# Table name: navigation_menu_types
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class NavigationMenuType < ActiveRecord::Base
  def self.ALL_TYPE_ALIAS
    return "all"
  end
  def self.DESKTOP_TYPE_ALIAS
    return "desktop"
  end
  def self.MOBILE_TYPE_ALIAS
    return "mobile"
  end

  def self.all_type
    return self.find_by(:alias => self.ALL_TYPE_ALIAS)
  end
  def self.desktop_type
    return self.find_by(:alias => self.DESKTOP_TYPE_ALIAS)
  end
  def self.mobile_type
    return self.find_by(:alias => self.MOBILE_TYPE_ALIAS)
  end
end
