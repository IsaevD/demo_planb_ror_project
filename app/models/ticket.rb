# == Schema Information
#
# Table name: tickets
#
#  id                    :integer          not null, primary key
#  price                 :integer
#  ticket_state_id       :integer
#  event_item_id         :integer
#  created_at            :datetime
#  updated_at            :datetime
#  seat_item_id          :integer
#  ticket_discount_id    :integer
#  payment_id            :integer
#  number                :string(255)
#  blank_type_id         :integer
#  hold_state_expires_at :datetime
#  reg_code              :string(255)
#

class Ticket < ActiveRecord::Base

  scope :with_seat_item, -> { includes(:seat_item) }
  scope :with_ticket_state, -> { includes(:ticket_state) }
  scope :with_ticket_discount, -> { includes(:ticket_discount) }

  belongs_to :blank_type
  belongs_to :event_item
  belongs_to :payment
  belongs_to :seat_item
  belongs_to :ticket_state
  belongs_to :ticket_discount

  has_many :blank_defects

  def check_current_state
    return unless hold_state_expires_at.present? && hold_state_expires_at < DateTime.now

    update(hold_state_expires_at: nil, ticket_state: TicketState.available_state)
  end

  # Функционал: проверка билетов на доступность
  def self.check_tickets_states(tickets)
    return false unless tickets.present?

    tickets.all?{ |ticket| ticket.ticket_state.alias == TicketState::AVAILABLE_ALIAS }
  end
  # Генерация номера обычного билета
  def self.create_ticket_normal_number(ticket, user = nil)
    counter = Setting.generate_normal_counter_number
    ticket.number = " #{ ticket.number.present? ? '5005' : '008' } #{counter} #{Setting.get_setting(Setting.NORMAL_COUNTER_PREFIX_ALIAS)}"
    ticket.blank_type = BlankType.normal_type
    Setting.set_setting(Setting.NORMAL_COUNTER_NUMBER_ALIAS, counter)

    if user.present? && ['kassir', 'kassir_major'].include?(user.system_user_role.alias)
      BlankStock.where(system_user_id: user.id, event_item_id: nil).first.withdraw(1)
    end

    return ticket
  end
  # Генерация номера фирменного билета
  def self.create_ticket_special_number(ticket, user = nil, event_item = nil)
    counter = Setting.generate_special_counter_number
    ticket.number = " #{ ticket.number.present? ? '5005' : '008' } #{counter} #{Setting.get_setting(Setting.SPECIAL_COUNTER_PREFIX_ALIAS)}"
    ticket.blank_type = BlankType.special_type
    Setting.set_setting(Setting.SPECIAL_COUNTER_NUMBER_ALIAS, counter)

    if user.present? && ['kassir', 'kassir_major'].include?(user.system_user_role.alias)
      BlankStock.where(system_user_id: user.id, event_item_id: event_item.id).first.withdraw(1)
    end

    return ticket
  end
  # Формирование платежа на покупку билетов
  def self.create_purchase_payment(tickets, event_id, user, ignore_number = true, fio = "", phone = "", delivery_address = "", sale_point_item_id = nil)
    ticket_state = TicketState.bought_state
    payment = Payment.create_payment(nil, user.id, event_id, PaymentType.purchase_state, nil, fio, phone, delivery_address, sale_point_item_id )
    tickets.each do |ticket|
      ticket.ticket_state = ticket_state
      if !ignore_number
        ticket = self.create_ticket_normal_number(ticket, user)
      else
        if ticket.number.nil?
          ticket = self.create_ticket_normal_number(ticket, user)
        end
      end
      ticket.payment_id = payment.id
      ticket.save
    end
  end
  # Формирование платежа на бронирование билетов
  def self.create_reservation_payment(tickets, event_id, user, fio = "", phone = "", delivery_address = "", sale_point_item_id = nil)
    ticket_state = TicketState.booked_state
    payment = Payment.create_payment(nil, user.id, event_id, PaymentType.reservation_state, nil, fio, phone, delivery_address, sale_point_item_id)
    tickets.each do |ticket|
      ticket.ticket_state = ticket_state
      ticket.payment_id = payment.id
      ticket.save
    end
  end

  def self.get_discount_name(ticket)
    discount = ticket.ticket_discount

    discount.present? ? discount.name : "Нет"
  end

  def self.get_actual_price(ticket)
    return TicketDiscount.get_price_by_discount(ticket)
  end

  def self.book_ticket_by_user(ticket, user)
    ticket.user = @current_user
    ticket.ticket_state = TicketState.booked_state
    ticket.save
    return nil
  end

  def self.buy_ticket_by_user(ticket, user)
    ticket.user = @current_user
    ticket.ticket_state = TicketState.bought_state
    ticket.save
  end

  def self.get_tickets_by_event_id(event_id)
    return self.where(:event_item_id => event_id)
  end

  def self.get_tickets_by_ids(ids)
    if ids.is_a? String
      ids = ids.split(",")
    end
    return Ticket.where("id in (?)", ids)
  end

  def self.get_tickets_main_info(ids)
    if !ids.nil?
      count = 0
      price = 0
      self.get_tickets_by_ids(ids).each do |ticket|
        #if !ticket.user.nil?
        #  if ticket.user.id != @current_user.id
            count = count + 1
            price = price + ticket.price
            #tickets.push(ticket.id)
        #  end
        #end
      end
      return [count, price]#, tickets]
    else
      return [0, 0, nil]
    end
  end

  def self.get_ticket_price(event_id, seat_id)
    ticket = self.where(:event_item_id => event_id, :seat_id => seat_id).first()
    if !ticket.nil?
      return ticket.price
    end
  end

  def generate_reg_code
    update(reg_code: SecureRandom.base64.gsub(/[^0-9A-Za-z]/, ''))
  end

  def registered?
    is_registered
  end

  def register
    update(is_registered: true)
  end
end
