# == Schema Information
#
# Table name: delivery_methods
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class DeliveryMethod < ActiveRecord::Base
  COURIER_ALIAS = 'courier'
  HIMSELF_ALIAS = 'himself'

  scope :courier_state, -> { find_by(alias: COURIER_ALIAS) }
  scope :himself_state, -> { find_by(alias: HIMSELF_ALIAS) }
end
