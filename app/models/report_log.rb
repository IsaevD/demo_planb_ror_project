# == Schema Information
#
# Table name: report_logs
#
#  id             :integer          not null, primary key
#  alias          :string(255)
#  system_user_id :integer
#  starts_at      :datetime
#  ends_at        :datetime
#  created_at     :datetime
#  updated_at     :datetime
#

class ReportLog < ActiveRecord::Base

  ALIASES = %w(cashbox_release)

  belongs_to :system_user, class_name: 'SystemMainItemsD::SystemUser'

  validates :system_user, :starts_at, :ends_at, presence: true
  validates :alias, presence: true, inclusion: { in: ALIASES }

end
