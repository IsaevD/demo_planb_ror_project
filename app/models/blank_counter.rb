# == Schema Information
#
# Table name: blank_counters
#
#  id            :integer          not null, primary key
#  value         :integer
#  created_at    :datetime
#  updated_at    :datetime
#  event_item_id :integer
#  blank_type_id :integer
#

# class BlankCounter < ActiveRecord::Base

#   belongs_to :event_item
#   belongs_to :blank_type

#   def self.NORMAL_COUNTER_ALIAS
#     return "normal_counter"
#   end
#   def self.SPECIAL_COUNTER_ALIAS
#     return "special_counter"
#   end


=begin
  def self.get_name_by_alias(blank_alias)
    case blank_alias
      when self.SPECIAL_COUNTER_ALIAS
        "Фирменный"
      when self.NORMAL_COUNTER_ALIAS
        "Общий"
      else
        "Тип бланка не определен"
    end
  end
=end

  # # Получить счетчик обычных бланков у кассира
  # def self.normal_counter
  #   counter = self.find_by(:blank_type_id => BlankType.normal_type.id)
  #   if counter.nil?
  #     counter = self.new({
  #       :value => 0,
  #       :blank_type_id => BlankType.normal_type.id
  #     })
  #     counter.save
  #   end
  #   return counter
  # end

  # # Получить счетчик фирменных бланков у кассира
  # def self.special_counter(event_item_id)
  #   counter = self.find_by(:blank_type_id => BlankType.special_type.id, :event_item_id => event_item_id)
  #   if counter.nil?
  #     counter = self.new({
  #       :value => 0,
  #       :blank_type_id => BlankType.special_type.id,
  #       :event_item_id => event_item_id
  #     })
  #     counter.save
  #   end
  #   return counter
  # end

  # def self.get_special_counter(event_item_id)
  #   return self.find_by(:blank_type_id => BlankType.special_type.id, :event_item_id => event_item_id)
  # end

  # # Изменить счетчик бланков по заявки на отправку
  # def self.change_blank_counter_by_blank_request(blank_request, prev_blank_request = nil, action_type = 0)

  #   # Какой тип отправки?
  #   case blank_request.blank_request_type.alias

  #     # Бланки
  #     when BlankRequestType.BLANK_ALIAS

  #       # Какой тип движения отправки?
  #       case blank_request.blank_delivery_type.alias

  #         # Доставка
  #         when BlankDeliveryType.DELIVERY_ALIAS

  #           normal_counter = BlankCounter.normal_counter
  #           special_counter = BlankCounter.special_counter(blank_request.event_item_id)
  #           # Какое действие?
  #           case action_type
  #             # Создание
  #             when 0
  #               case blank_request.blank_type.alias
  #                 when BlankType.NORMAL_ALIAS
  #                   normal_counter.value = normal_counter.value - blank_request.value
  #                   normal_counter.save

  #                 when BlankType.SPECIAL_ALIAS
  #                   special_counter.value = special_counter.value - blank_request.value
  #                   special_counter.save
  #               end

  #             # Изменение
  #             when 1
  #               case blank_request.blank_type.alias
  #                 when BlankType.NORMAL_ALIAS
  #                   normal_counter.value = normal_counter.value + prev_blank_request[1]
  #                   normal_counter.value = normal_counter.value - blank_request.value

  #                   if prev_blank_request[0] == BlankDeliveryType.TO_KASSIR_ALIAS
  #                     kassir_normal_counter = KassirCounter.get_normal_kassir_counter(blank_request.system_user_id)
  #                     kassir_normal_counter = kassir_normal_counter - prev_blank_request[1]
  #                     kassir_normal_counter.save
  #                   end

  #                   normal_counter.save

  #                 when BlankType.SPECIAL_ALIAS
  #                   special_counter.value = special_counter.value + prev_blank_request[1]
  #                   special_counter.value = special_counter.value - blank_request.value

  #                   if prev_blank_request[0] == BlankDeliveryType.TO_KASSIR_ALIAS
  #                     kassir_special_counter = KassirCounter.get_special_kassir_counter(blank_request.system_user_id, blank_request.event_item_id)
  #                     kassir_special_counter = kassir_special_counter - prev_blank_request[1]
  #                     kassir_special_counter.save
  #                   end

  #                   special_counter.save
  #               end

  #             # Удаление
  #             when 2
  #               case blank_request.blank_type.alias
  #                 when BlankType.NORMAL_ALIAS
  #                   normal_counter.value = normal_counter.value + blank_request.value
  #                   normal_counter.save
  #                 when BlankType.SPECIAL_ALIAS
  #                   special_counter.value = special_counter.value + blank_request.value
  #                   special_counter.save
  #               end


  #           end

  #         when BlankDeliveryType.TO_KASSIR_ALIAS

  #           normal_counter = BlankCounter.normal_counter
  #           special_counter = BlankCounter.special_counter(blank_request.event_item_id)
  #           kassir_normal_counter = KassirCounter.get_normal_kassir_counter(blank_request.system_user_id)
  #           kassir_special_counter = KassirCounter.get_special_kassir_counter(blank_request.system_user_id, blank_request.event_item_id)

  #           # Какое действие?
  #           case action_type
  #             # Создание
  #             when 0
  #               case blank_request.blank_type.alias
  #                 when BlankType.NORMAL_ALIAS
  #                   normal_counter.value = normal_counter.value - blank_request.value
  #                   kassir_normal_counter.value = kassir_normal_counter.value + blank_request.value
  #                   normal_counter.save
  #                   kassir_normal_counter.save

  #                 when BlankType.SPECIAL_ALIAS
  #                   special_counter.value = special_counter.value - blank_request.value
  #                   kassir_special_counter.value = kassir_special_counter.value + blank_request.value
  #                   special_counter.save
  #                   kassir_special_counter.save
  #               end

  #             # Изменение
  #             when 1
  #               case blank_request.blank_type.alias
  #                 when BlankType.NORMAL_ALIAS
  #                   #normal_counter.value = normal_counter.value + prev_blank_request[1]
  #                   #kassir_normal_counter.value = kassir_normal_counter.value - prev_blank_request[1]
  #                   if prev_blank_request[0] == BlankDeliveryType.DELIVERY_ALIAS
  #                     #normal_counter.value = normal_counter.value - blank_request.value
  #                     kassir_normal_counter.value = kassir_normal_counter.value + blank_request.value
  #                   end
  #                   normal_counter.save
  #                   kassir_normal_counter.save

  #                 when BlankType.SPECIAL_ALIAS
  #                   #special_counter.value = special_counter.value + prev_blank_request[1]
  #                   #kassir_special_counter.value = kassir_special_counter.value - prev_blank_request[1]
  #                   if prev_blank_request[0] == BlankDeliveryType.DELIVERY_ALIAS
  #                     #special_counter.value = special_counter.value - blank_request.value
  #                     kassir_special_counter.value = kassir_special_counter.value + blank_request.value
  #                   end
  #                   special_counter.save
  #                   kassir_special_counter.save

  #               end

  #             # Удаление
  #             when 2
  #               case blank_request.blank_type.alias
  #                 when BlankType.NORMAL_ALIAS
  #                   normal_counter.value = normal_counter.value + blank_request.value
  #                   kassir_normal_counter.value = kassir_normal_counter.value - blank_request.value
  #                   normal_counter.save
  #                   kassir_normal_counter.save
  #                 when BlankType.SPECIAL_ALIAS
  #                   special_counter.value = special_counter.value + blank_request.value
  #                   kassir_special_counter.value = kassir_special_counter.value - blank_request.value
  #                   special_counter.save
  #                   kassir_special_counter.save

  #               end
  #           end

  #       end

  #   end



=begin
    # Какой тип отправки?
    case blank_request.blank_request_type.alias

      #  Бланки
      when BlankRequestType.BLANK_ALIAS

        # Какой тип движения отправки
        case blank_request.blank_delivery_type.alias

          # Доставка
          when BlankDeliveryType.DELIVERY_ALIAS

            # Какое тип бланка?
            case blank_request.blank_type.alias

              ### ОБЩИЙ
              when BlankType.NORMAL_ALIAS
                counter = BlankCounter.normal_counter
                # Какое действие?
                case action_type
                  # Создание
                  when 0
                    counter.value = counter.value - blank_request.value
                  # Изменение
                  when 1

                    # Проверка на изменение типа бланка
                    if prev_blank_request.blank_type.alias == BlankType.SPECIAL_ALIAS
                      prev_counter = BlankCounter.special_counter(prev_blank_request.event_item_id)
                      prev_counter.value = prev_counter.value + prev_blank_request.value
                      prev_counter.save
                    end
                    if prev_blank_request.blank_delivery_type.alias == BlankDeliveryType.TO_KASSIR_ALIAS


                    end


                    else
                      counter.value = counter.value + prev_blank_request.value
                    end


                    counter.value = counter.value - blank_request.value


                    #counter.value = counter.value - prev_blank_request.value + blank_request.value

                    # Если изменился тип доставки
                    if prev_blank_request.blank_delivery_type.alias == BlankDeliveryType.TO_KASSIR_ALIAS
                      kassir_counter = KassirCounter.get_normal_kassir_counter(prev_blank_request.system_user_id)
                      kassir_counter.value = kassir_counter.value - prev_blank_request.value
                      kassir_counter.save
                    else
                      counter.value = counter.value - prev_blank_request.value
                    end
                    counter.value = counter.value + blank_request.value

                  # Удаление
                  when 2
                    counter.value = counter.value + blank_request.value
                end
                counter.save

              # Фирменный

              when BlankType.SPECIAL_ALIAS
                counter = BlankCounter.special_counter(blank_request.event_item_id)
                # Какое действие?
                case action_type
                  # Создание
                  when 0
                    counter.value = counter.value + blank_request.value
                  # Изменение
                  when 1

                    # Если изменился тип доставки
                    if prev_blank_request.blank_delivery_type.alias == BlankDeliveryType.TO_KASSIR_ALIAS
                      kassir_counter = KassirCounter.get_special_kassir_counter(prev_blank_request.system_user_id, prev_blank_request.event_item_id)
                      kassir_counter.value = kassir_counter.value - prev_blank_request.value
                      kassir_counter.save
                    else
                      counter.value = counter.value - prev_blank_request.value
                    end
                    prev_counter = BlankCounter.special_counter(prev_blank_request.event_item_id)
                    prev_counter.value = prev_counter.counter - prev_blank_request.value
                    counter.value = counter.value + blank_request.value

                  # Удаление
                  when 2
                    counter = BlankCounter.special_counter(blank_request.event_item_id)
                    counter.value = counter.counter - blank_request.value
                end
                counter.save
            end

          ### У КАССИРА
          when BlankDeliveryType.TO_KASSIR_ALIAS

            # Какое тип бланка?
            case blank_request.blank_type.alias

              # Общий
              when BlankType.NORMAL_ALIAS
                counter = KassirCounter.get_normal_kassir_counter(blank_request.system_user_id)
                # Какое действие?
                case action_type
                  # Создание
                  when 0
                    counter.value = counter.value + blank_request.value
                  # Изменение
                  when 1
                    counter.value = counter.value - prev_blank_request.value + blank_request.value
                  # Удаление
                  when 2
                    counter.value = counter.value - blank_request.value
                end
                counter.save
              # Фирменный
              when BlankType.SPECIAL_ALIAS
                counter = KassirCounter.get_special_kassir_counter(blank_request.system_user_id, blank_request.event_item_id)
                # Какое действие?
                case action_type
                  # Создание
                  when 0
                    counter.value = counter.value + blank_request.value
                  # Изменение
                  when 1
                    prev_counter = KassirCounter.get_special_kassir_counter(prev_blank_request.system_user_id, prev_blank_request.event_item_id)
                    prev_counter.value = prev_counter.counter - prev_blank_request.value
                    counter.value = counter.value + blank_request.value
                  # Удаление
                  when 2
                    counter = KassirCounter.get_special_kassir_counter(blank_request.system_user_id, prev_blank_request.event_item_id)
                    counter.value = counter.counter - blank_request.value
                end
                counter.save
            end


        end

      # Деньги
      when BlankRequestType.MONEY_ALIAS



    end
=end
=begin
    case blank_request.blank_delivery_type.alias
      when BlankDeliveryType.TO_KASSIR_ALIAS
        kassir_counter = KassirCounter.find_by(:system_user_id => user.id)
        if kassir_counter.nil?
          kassir_counter = KassirCounter.new
          kassir_counter.system_user_id = user.id
          kassir_counter.save
        end
        kassir_counter.event_item_id = blank_request.event_item.id
        kassir_counter.blank_type_id = blank_request.blank_type.id
        kassir_counter.value = kassir_counter.value.to_i + blank_request.value
        kassir_counter.save
    end
=end
#   end

#   # Удалить счетчик бланков при удалении заявки
#   def self.delete_blank_counter_by_blank_request(blank_request)

#     kassir_counter = KassirCounter.find_by(:system_user_id => blank_request.system_user_id)
#     blank_type_alias = blank_request.blank_type.alias
#     blank_request_value = blank_request.value.to_i
#     #normal_blank_counter = BlankCounter.normal_counter
#     #special_blank_counter = BlankCounter.special_counter

#     case blank_request.blank_delivery_type.alias
#       when BlankDeliveryType.DELIVERY_ALIAS
#         case blank_type_alias
#           when BlankType.NORMAL_ALIAS
#             #normal_blank_counter.value = BlankCounter.normal_counter.value + blank_request_value
#           when BlankType.SPECIAL_ALIAS
#             #special_blank_counter.value = BlankCounter.special_counter.value + blank_request_value
#         end
#       when BlankDeliveryType.TO_KASSIR_ALIAS
#         case blank_type_alias
#           when BlankType.NORMAL_ALIAS
#             #kassir_counter.normal_counter = kassir_counter.normal_counter - blank_request_value
#             #normal_blank_counter.value = BlankCounter.normal_counter.value + blank_request_value
#           when BlankType.SPECIAL_ALIAS
#             #kassir_counter.special_counter = kassir_counter.special_counter - blank_request_value
#             #special_blank_counter.value = BlankCounter.special_counter.value + blank_request_value
#         end
#     end

#     #kassir_counter.save
#     #normal_blank_counter.save
#     #special_blank_counter.save

#   end

#   def self.get_blank_count(blank_type, blank_delivery_type)
#     return BlankRequest.where(:blank_type_id => blank_type.id, :blank_delivery_type_id => blank_delivery_type.id).sum(:value)
#   end
# end
