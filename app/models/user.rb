# == Schema Information
#
# Table name: users
#
#  id                    :integer          not null, primary key
#  first_name            :string(255)
#  middle_name           :string(255)
#  last_name             :string(255)
#  email                 :string(255)
#  phone                 :string(255)
#  discount_number       :string(255)
#  password_digest       :string(255)
#  user_remember_token   :string(255)
#  system_state_id       :integer
#  created_at            :datetime
#  updated_at            :datetime
#  user_confirmation     :string(255)
#  confirmed             :boolean
#  user_restore_password :string(255)
#

class User < ActiveRecord::Base

  has_many :payments, dependent: :destroy

  has_secure_password

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :first_name,
      presence: true

  validates :last_name,
      presence: true

  validates :email,
      presence: true,
      length: { minimum: 5, maximum: 255},
      uniqueness: true,
      format: { with: VALID_EMAIL_REGEX }

  validates_uniqueness_of :phone, :allow_blank => true
  validates_uniqueness_of :discount_number, :allow_blank => true

end
