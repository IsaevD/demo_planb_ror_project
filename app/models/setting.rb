# == Schema Information
#
# Table name: settings
#
#  id              :integer          not null, primary key
#  alias           :string(255)
#  name            :string(255)
#  value           :text
#  system_state_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Setting < ActiveRecord::Base

  validates :name,
      presence: true,
      length: { minimum: 4, maximum: 255}

  def self.NORMAL_COUNTER_NUMBER_ALIAS
    return "normal_counter_number"
  end
  def self.NORMAL_COUNTER_PREFIX_ALIAS
    return "normal_counter_prefix"
  end
  def self.SPECIAL_COUNTER_NUMBER_ALIAS
    return "special_counter_number"
  end
  def self.SPECIAL_COUNTER_PREFIX_ALIAS
    return "special_counter_prefix"
  end
  def self.TICKET_MAX_COUNTER_ALIAS
    return "ticket_max_counter"
  end
  def self.BOOKING_DISABLED_IN
    return "booking_disabled_in"
  end

  def self.get_setting(alias_name)
    setting = self.find_by(:alias => alias_name, :system_state_id => SystemMainItemsD::SystemState.active_state.id)
    if !setting.nil?
      setting.value
    else
      nil
    end
  end

  def self.set_setting(alias_name, value)
    setting = self.find_by(:alias => alias_name)
    setting.value = value
    return setting.save
  end

  def self.generate_normal_counter_number
    normal_counter_number = self.get_setting(self.NORMAL_COUNTER_NUMBER_ALIAS)
    normal_counter_number = (normal_counter_number.to_i + 1).to_s
    i = normal_counter_number.length
    while (i < 6)
      normal_counter_number = "0" + normal_counter_number
      i = i + 1
    end
    return normal_counter_number
  end

  def self.generate_special_counter_number
    special_counter_number = self.get_setting(self.SPECIAL_COUNTER_NUMBER_ALIAS)
    special_counter_number = (special_counter_number.to_i + 1).to_s
    i = special_counter_number.length
    while (i < 6)
      special_counter_number = "0" + special_counter_number
      i = i + 1
    end
    return special_counter_number
  end

end
