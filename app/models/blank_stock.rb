# == Schema Information
#
# Table name: blank_stocks
#
#  id             :integer          not null, primary key
#  counter        :integer          default(0)
#  blank_type_id  :integer
#  event_item_id  :integer
#  system_user_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class BlankStock < ActiveRecord::Base
  scope :global, -> { where(system_user_id: nil) }
  scope :not_global, -> { where.not(system_user_id: nil) }
  scope :with_blank_type, -> { includes(:blank_type) }
  scope :with_event_item, -> { includes(:event_item) }
  scope :sort_by_system_user, -> { order(:system_user_id) }

  belongs_to :blank_type
  belongs_to :event_item
  belongs_to :system_user, class_name: 'SystemMainItemsD::SystemUser'

  validates :blank_type, presence: true
  validates :counter, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def withdraw(amount)
    update(counter: counter - amount)
  end

  def deposit(amount)
    update(counter: counter + amount)
  end
end
