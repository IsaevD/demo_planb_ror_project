# == Schema Information
#
# Table name: payment_kinds
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class PaymentKind < ActiveRecord::Base
  CASH_ALIAS = 'cash'
  CASHLESS_ALIAS = 'cashless'

  scope :cash_kind, -> { find_by(alias: CASH_ALIAS) }
  scope :cashless_kind, -> { find_by(alias: CASHLESS_ALIAS) }
end
