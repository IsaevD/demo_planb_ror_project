# == Schema Information
#
# Table name: request_types
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class RequestType < ActiveRecord::Base
  def self.CALL_ALIAS
    return "call"
  end
  def self.CERTIFICATE_ALIAS
    return "certificate"
  end
  def self.COLLECTIVE_ORDER_ALIAS
    return "collective"
  end

  def self.call_type
    return self.find_by(:alias => self.CALL_ALIAS)
  end
  def self.certificate_type
    return self.find_by(:alias => self.CERTIFICATE_ALIAS)
  end
  def self.collective_order_type
    return self.find_by(:alias => self.COLLECTIVE_ORDER_ALIAS)
  end

end
