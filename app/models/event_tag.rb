# == Schema Information
#
# Table name: event_tags
#
#  id            :integer          not null, primary key
#  event_item_id :integer
#  event_type_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class EventTag < ActiveRecord::Base

  belongs_to :event_item
  belongs_to :event_type

end
