# == Schema Information
#
# Table name: sale_point_items
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  address            :string(255)
#  work_time          :string(255)
#  lunch_time         :string(255)
#  phone              :string(255)
#  coor_x             :string(255)
#  coor_y             :string(255)
#  system_state_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  city_id            :integer
#  is_public          :boolean
#

class SalePointItem < ActiveRecord::Base

  has_many :sale_point_images, dependent: :destroy

  has_attached_file :image, :styles => { :list => "200x135", :thumb => "100x100" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 255}

  validates :address,
            presence: true

  validates :work_time,
            presence: true

  validates :phone,
            presence: true

  def self.get_active_sale_points
      return self.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id, :is_public => true).order(:updated_at => :desc)
  end

end
