# == Schema Information
#
# Table name: navigation_menus
#
#  id                      :integer          not null, primary key
#  position                :integer
#  name                    :string(255)
#  link                    :string(255)
#  navigation_menu_id      :integer
#  navigation_menu_type_id :integer
#  system_state_id         :integer
#  created_at              :datetime
#  updated_at              :datetime
#

class NavigationMenu < ActiveRecord::Base
  validates :name,
      presence: true,
      length: { minimum: 4, maximum: 255},
      uniqueness: true

  validates :link,
      presence: true

  validates :position,
      presence: true

end
