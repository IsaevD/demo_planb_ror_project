# == Schema Information
#
# Table name: requests
#
#  id              :integer          not null, primary key
#  phone           :string(255)
#  message         :text
#  request_type_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#  name            :string(255)
#

class Request < ActiveRecord::Base
end
