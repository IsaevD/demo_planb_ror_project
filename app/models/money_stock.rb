# == Schema Information
#
# Table name: money_stocks
#
#  id             :integer          not null, primary key
#  counter        :integer          default(0)
#  system_user_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class MoneyStock < ActiveRecord::Base
  belongs_to :system_user, class_name: 'SystemMainItemsD::SystemUser'

  validates :counter, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def withdraw(amount)
    update(counter: counter - amount)
  end

  def deposit(amount)
    update(counter: counter + amount)
  end
end
