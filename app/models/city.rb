# == Schema Information
#
# Table name: cities
#
#  id              :integer          not null, primary key
#  alias           :string(255)
#  name            :string(255)
#  system_state_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#  coor_x          :string(255)
#  coor_y          :string(255)
#

class City < ActiveRecord::Base

  def self.get_active_cities
    return self.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).order(:created_at => :desc)
  end

end
