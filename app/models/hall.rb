# == Schema Information
#
# Table name: halls
#
#  id              :integer          not null, primary key
#  alias           :string(255)
#  name            :string(255)
#  system_state_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#  schema_item_id  :integer
#

class Hall < ActiveRecord::Base

  belongs_to :schema_item
  has_many :event_items

  VALID_ALIAS_REGEX = /[a-zA-Z]+/

  validates :alias,
            presence: true,
            length: { minimum: 4, maximum: 50},
            uniqueness: true,
            format: { with: VALID_ALIAS_REGEX }

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 255},
            uniqueness: true

end
