# == Schema Information
#
# Table name: payment_transactions
#
#  id          :integer          not null, primary key
#  invid       :integer
#  status      :integer
#  price       :integer
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class PaymentTransaction < ActiveRecord::Base
  STARTED_STATUS = 'initialized'
  SUCCESS_STATUS = 'success'
  FAILURE_STATUS = 'failure'

  def self.get_currencies(lang = :ru)
    xml_interface(lang: lang).get_currencies_list
  end

  def self.get_payment_methods(lang = :ru)
    xml_interface(lang: lang).get_payment_methods
  end

  def self.get_rates(lang = :ru, total_sum = 1)
    xml_interface(lang: lang, total_sum: total_sum).get_rates
  end

  def self.operation_state(lang = :ru, invoice_id)
    xml_interface(lang: lang, invoice_id: invoice_id).op_state
  end

  def self.get_hash(*s)
    Digest::MD5.hexdigest(s.join(':'))
  end

  private

  def xml_interface(params)
    Rubykassa::XmlInterface.new do |interface|
      interface.invoice_id = params[:invoice_id] if params[:invoice_id].present?
      interface.total = params[:total_sum] if params[:total_sum].present?
      interface.language = params[:lang] if params[:lang].present?
    end
  end
end
