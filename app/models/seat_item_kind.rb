# == Schema Information
#
# Table name: seat_item_kinds
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class SeatItemKind < ActiveRecord::Base

  def self.FIXED_KIND_ALIAS
    return "fixed"
  end

  def self.UNFIXED_KIND_ALIAS
    return "unfixed"
  end

  def self.fixed_kind
    return self.find_by(:alias => self.FIXED_KIND_ALIAS)
  end
  def self.unfixed_kind
    return self.find_by(:alias => self.UNFIXED_KIND_ALIAS)
  end

end
