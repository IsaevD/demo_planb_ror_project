# == Schema Information
#
# Table name: partner_items
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  system_state_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  link               :string(255)
#

class PartnerItem < ActiveRecord::Base

  has_attached_file :image, :styles => { :list => "110x40" , :thumb => "100x100" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :name,
    presence: true,
    length: { minimum: 4, maximum: 255}

  def self.get_active_partners
    return self.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id).order(:created_at => :desc)
  end

end
