# == Schema Information
#
# Table name: ticket_states
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class TicketState < ActiveRecord::Base
  # @TODO have no idea why it is desktop over disabled, switched
  # mentioned that in case there is some specific situations about that
  # DISABLED_ALIAS = "desktop"
  DISABLED_ALIAS = 'disabled'
  AVAILABLE_ALIAS = 'available'
  BOOKED_ALIAS = 'booked'
  BOUGHT_ALIAS = 'bought'
  HOLD_ALIAS = 'hold'

  scope :available_state, -> { find_by(alias: AVAILABLE_ALIAS) }
  scope :disabled_state,  -> { find_by(alias: DISABLED_ALIAS)  }
  scope :booked_state,    -> { find_by(alias: BOOKED_ALIAS)    }
  scope :bought_state,    -> { find_by(alias: BOUGHT_ALIAS)    }
  scope :hold_state,      -> { find_by(alias: HOLD_ALIAS)      }
end
