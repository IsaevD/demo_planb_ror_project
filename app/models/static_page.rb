# == Schema Information
#
# Table name: static_pages
#
#  id                 :integer          not null, primary key
#  alias              :string(255)
#  name               :string(255)
#  content            :text
#  meta_keywords      :string(255)
#  meta_description   :text
#  system_state_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  non_removable      :boolean          default(FALSE)
#

class StaticPage < ActiveRecord::Base

  has_many :static_page_images, dependent: :destroy

  has_attached_file :image, :styles => { :list => "300x140" , :preview => "128x55" , :thumb => "100x100" }, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  VALID_ALIAS_REGEX = /[a-zA-Z]+/

  validates :alias,
            presence: true,
            length: { minimum: 4, maximum: 50},
            uniqueness: true,
            format: { with: VALID_ALIAS_REGEX }

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 255},
            uniqueness: true

  # toDO: наимнования таких методов пиши Капсом. Не забудь переименовать там, где они используются
  def self.about_us
    return 'about_us'
  end

  def self.where_buy_tickets
    return 'where_buy_tickets'
  end

  def self.help_to_choose_event
    return 'help_to_choose_event'
  end

  def self.help_to_personal_page
    return 'help_to_personal_page'
  end

  def self.certificate_alias
    return 'certificate'
  end

  def self.success_internet_payment
    return 'success_internet_payment'
  end

  def self.fail_internet_payment
    return 'fail_internet_payment'
  end

end
