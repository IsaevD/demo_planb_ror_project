# == Schema Information
#
# Table name: sector_groups
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  sector_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class SectorGroup < ActiveRecord::Base

  scope :with_seat_items, -> { includes(seat_items: :seat_item_type) }

  belongs_to :sector

  has_many :seat_items, dependent: :destroy

end
