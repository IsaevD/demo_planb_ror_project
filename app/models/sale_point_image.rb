# == Schema Information
#
# Table name: sale_point_images
#
#  id                         :integer          not null, primary key
#  sale_point_item_id         :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#  image_element_file_name    :string(255)
#  image_element_content_type :string(255)
#  image_element_file_size    :integer
#  image_element_updated_at   :datetime
#

class SalePointImage < ActiveRecord::Base

  belongs_to :sale_point_item

  has_attached_file :image_element, :styles => {  :slider => "272x208", :thumb => "100x100" }, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :image_element, :content_type => /\Aimage\/.*\Z/

end
