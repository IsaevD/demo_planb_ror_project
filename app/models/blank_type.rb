# == Schema Information
#
# Table name: blank_types
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class BlankType < ActiveRecord::Base
  belongs_to :blank_counter

  NORMAL_ALIAS = 'normal'
  SPECIAL_ALIAS = 'special'

  scope :normal_type, -> { find_by(alias: NORMAL_ALIAS) }
  scope :special_type, -> { find_by(alias: SPECIAL_ALIAS) }
end
