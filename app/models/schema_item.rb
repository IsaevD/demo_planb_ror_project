# == Schema Information
#
# Table name: schema_items
#
#  id             :integer          not null, primary key
#  alias          :string(255)
#  name           :string(255)
#  schema_type_id :integer
#  hall_id        :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class SchemaItem < ActiveRecord::Base

  belongs_to :hall

  has_many :sectors
  has_many :seat_items

end
