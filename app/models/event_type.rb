# == Schema Information
#
# Table name: event_types
#
#  id              :integer          not null, primary key
#  alias           :string(255)
#  name            :string(255)
#  system_state_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class EventType < ActiveRecord::Base
  validates :alias, presence: true
  validates :name, presence: true
end
