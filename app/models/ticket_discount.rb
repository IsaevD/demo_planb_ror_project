# == Schema Information
#
# Table name: ticket_discounts
#
#  id              :integer          not null, primary key
#  alias           :string(255)
#  name            :string(255)
#  value           :integer
#  is_percent      :boolean
#  system_state_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#  event_item_id   :integer
#  date            :datetime
#  active_from     :datetime
#

class TicketDiscount < ActiveRecord::Base
  scope :not_started, -> { where('active_from > ?', DateTime.now) }

  has_many :tickets

  def self.get_price_by_discount(ticket)
    discount = ticket.ticket_discount
    price = ticket.price
    return price unless discount
    today = Time.now
      if ( discount.date >= today ) && ( discount.active_from <= today )
        if discount.is_percent
          return (price - price * (discount.value.to_f / 100)).round
        else
          return (price - discount.value).round
        end
      else
        return price
      end
  end

  def self.get_price_by_discount_without_date_link(ticket)
    discount = ticket.ticket_discount
    price = ticket.price
    if discount.is_percent
      return (price - price * (discount.value.to_f / 100)).round
    else
      return (price - discount.value).round
    end
  end
end
