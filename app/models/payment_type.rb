# == Schema Information
#
# Table name: payment_types
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class PaymentType < ActiveRecord::Base
  PURCHASE_ALIAS = 'purchase'
  RESERVATION_ALIAS = 'reservation'

  scope :purchase_state, -> { find_by(alias: PURCHASE_ALIAS) }
  scope :reservation_state, -> { find_by(alias: RESERVATION_ALIAS) }
end
