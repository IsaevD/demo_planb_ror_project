# == Schema Information
#
# Table name: sectors
#
#  id             :integer          not null, primary key
#  alias          :string(255)
#  name           :string(255)
#  schema_item_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Sector < ActiveRecord::Base

  belongs_to :schema_item

  has_many :sector_groups, dependent: :destroy

end
