# == Schema Information
#
# Table name: money_transfers
#
#  id           :integer          not null, primary key
#  counter      :integer          default(0)
#  delivered_at :datetime
#  state        :string(255)      default("initialized")
#  assignee_id  :integer
#  assignor_id  :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class MoneyTransfer < ActiveRecord::Base
  scope :in_delivery, -> { where(state: 'in_delivery') }
  scope :assigned_to, -> (user_id) { where(assignee: user_id) }
  scope :with_assignor, -> { includes(:assignor) }

  belongs_to :assignor, class_name: 'SystemMainItemsD::SystemUser', foreign_key: :assignor_id
  belongs_to :assignee, class_name: 'SystemMainItemsD::SystemUser', foreign_key: :assignee_id

  validates :assignor, :state, presence: true
  validates :counter, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def start
    return false unless initialized? && money_stock(assignor_id).withdraw(counter)

    update(state: 'in_delivery')
  end

  def finish
    return false unless !delivered? && money_stock(assignee_id).deposit(counter)

    update(state: 'delivered', delivered_at: DateTime.now)
  end

  def cancel
    return false unless in_delivery? && money_stock(assignor_id).deposit(counter)

    update(state: 'not_delivered')
  end

  def global?
    assignee_id.blank? && assignor.system_user_role.alias == 'kassir_administrator'
  end

  def initialized?
    state == 'initialized'
  end

  def in_delivery?
    state == 'in_delivery'
  end

  def delivered?
    state == 'delivered'
  end

  def not_delivered?
    state == 'not_delivered'
  end

  private

  def money_stock(system_user_id)
    if system_user_id.present?
      system_user_role = SystemMainItemsD::SystemUser.find(system_user_id).system_user_role.alias
      system_user_id = nil if system_user_role == 'kassir_administrator'
    end

    MoneyStock.where(system_user_id: system_user_id).first_or_create
  end
end
