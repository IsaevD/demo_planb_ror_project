# == Schema Information
#
# Table name: seat_items
#
#  id                :integer          not null, primary key
#  row               :integer
#  number            :integer
#  sector_group_id   :integer
#  created_at        :datetime
#  updated_at        :datetime
#  schema_item_id    :integer
#  seat_item_type_id :integer
#  seat_item_kind_id :integer
#

class SeatItem < ActiveRecord::Base

  belongs_to :seat_item_type
  belongs_to :seat_item_kind
  belongs_to :sector_group

  # I do not know what the hell row is a string(255). STRING!!! 255!!! KARL!
  # kvokka
  has_many :tickets

end
