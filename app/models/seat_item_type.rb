# == Schema Information
#
# Table name: seat_item_types
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  color      :string(255)
#  price      :integer
#  created_at :datetime
#  updated_at :datetime
#

class SeatItemType < ActiveRecord::Base
  def self.get_color_by_price(price)
    @seat_item_types ||= SeatItemType.all

    type = @seat_item_types
      .select{ |type| type.price <= price }
      .sort_by(&:price)
      .last

    type.present? ? type.color : @seat_items_type.sort_by(&:price).first.color
  end
end
