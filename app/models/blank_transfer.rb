# == Schema Information
#
# Table name: blank_transfers
#
#  id            :integer          not null, primary key
#  counter       :integer          default(0)
#  delivered_at  :datetime
#  state         :string(255)      default("initialized")
#  blank_type_id :integer
#  event_item_id :integer
#  assignee_id   :integer
#  assignor_id   :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class BlankTransfer < ActiveRecord::Base
  scope :in_delivery, -> { where(state: 'in_delivery') }
  scope :delivered, -> { where(state: 'delivered') }
  scope :except_not_delivered, -> { where.not(state: 'not_delivered') }
  scope :assigned_to, -> (user_id) { where(assignee: user_id) }
  scope :with_assignor, -> { includes(:assignor) }
  scope :with_event_item, -> { includes(:event_item) }

  belongs_to :assignor, class_name: 'SystemMainItemsD::SystemUser', foreign_key: :assignor_id
  belongs_to :assignee, class_name: 'SystemMainItemsD::SystemUser', foreign_key: :assignee_id
  belongs_to :blank_type
  belongs_to :event_item

  validates :blank_type, :assignor, :state, presence: true
  validates :counter, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def start
    return false unless initialized? && blank_stock(assignor_id).withdraw(counter)

    update(state: 'in_delivery')
  end

  def finish
    return false unless !delivered? && blank_stock(assignee_id).deposit(counter)

    update(state: 'delivered', delivered_at: DateTime.now)
  end

  def cancel
    return false unless in_delivery? && blank_stock(assignor_id).deposit(counter)

    update(state: 'not_delivered')
  end

  def global?
    assignee_id.blank? && assignor.system_user_role.alias == 'kassir_administrator'
  end

  def initialized?
    state == 'initialized'
  end

  def in_delivery?
    state == 'in_delivery'
  end

  def delivered?
    state == 'delivered'
  end

  def not_delivered?
    state == 'not_delivered'
  end

  private

  def blank_stock(system_user_id)
    if system_user_id.present?
      system_user_role = SystemMainItemsD::SystemUser.find(system_user_id).system_user_role.alias
      system_user_id = nil if system_user_role == 'kassir_administrator'
    end

    BlankStock.where(
      blank_type_id: blank_type_id, event_item_id: event_item_id, system_user_id: system_user_id
    ).first_or_create
  end
end
