# == Schema Information
#
# Table name: payments
#
#  id                 :integer          not null, primary key
#  number             :string(255)
#  user_id            :integer
#  system_user_id     :integer
#  created_at         :datetime
#  updated_at         :datetime
#  event_item_id      :integer
#  payment_type_id    :integer
#  phone              :string(255)
#  delivery_address   :string(255)
#  delivery_method_id :integer
#  fio                :string(255)
#  sale_point_item_id :integer
#  payment_kind_id    :integer
#

class Payment < ActiveRecord::Base

  scope :with_tickets, -> { includes(tickets: :blank_type) }
  scope :with_payment_kind, -> { includes(:payment_kind) }

  has_many :tickets

  belongs_to :delivery_method
  belongs_to :event_item
  belongs_to :payment_type
  belongs_to :payment_kind
  belongs_to :system_user
  belongs_to :user

  def self.generate_number
    rand(9).to_s +
    (0...1).map { ('А'..'Я').to_a[rand(26)] }.join + (0...2).map { ('А'..'Я').to_a[rand(26)] }.join +
    rand(9).to_s +
    (0...1).map { ('А'..'Я').to_a[rand(26)] }.join +
    rand(9).to_s +
    (0...1).map { ('А'..'Я').to_a[rand(26)] }.join +
    rand(9).to_s +
    (0...1).map { ('А'..'Я').to_a[rand(26)] }.join
    #SecureRandom.hex(3)
  end

  def self.create_payment(user_id, system_user_id, event_id, payment_type, delivery_method = nil, fio = nil, phone = nil, delivery_address = nil, sale_point_item_id = nil, payment_kind_id = 1  )
    payment = Payment.new({
      :number => self.generate_number,
      :user_id => user_id,
      :system_user_id => system_user_id,
      :event_item_id => event_id,
      :payment_type_id => payment_type.id,
      :delivery_method_id => !delivery_method.nil? ? delivery_method.id : (!delivery_address.nil? ? DeliveryMethod.courier_state.id : nil),
      :fio => fio,
      :phone => phone,
      :delivery_address => delivery_address,
      :payment_kind_id => payment_kind_id
    })

    payment.sale_point_item_id = sale_point_item_id

    payment.save
    return payment
  end

  def self.get_payments_by_user(user)
    return nil unless user.present?

    payments = user.payments.order(:created_at => :desc)

    payments.size == 0 ? nil : payments
  end

  def cancelable?
    return false unless payment_type.present? && delivery_method.present?

    payment_type.alias == 'reservation' && delivery_method.alias == 'himself'
  end
end
