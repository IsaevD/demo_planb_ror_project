# == Schema Information
#
# Table name: kassir_counters
#
#  id             :integer          not null, primary key
#  system_user_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#  event_item_id  :integer
#  blank_type_id  :integer
#  value          :integer
#

# class KassirCounter < ActiveRecord::Base

#   belongs_to :event_item
#   belongs_to :blank_type

#   def self.get_normal_kassir_counter(user_id)
#     counter = self.find_by(:blank_type_id => BlankType.normal_type.id, :system_user_id => user_id)
#     if counter.nil?
#       counter = self.new({
#         :value => 0,
#         :blank_type_id => BlankType.normal_type.id,
#         :system_user_id => user_id
#       })
#       counter.save
#     end
#     return counter
#   end

#   def self.get_special_kassir_counter(user_id, event_item_id)
#     counter = self.find_by(:blank_type_id => BlankType.special_type.id, :system_user_id => user_id, :event_item_id => event_item_id)
#     if counter.nil?
#       counter = self.new({
#          :value => 0,
#          :blank_type_id => BlankType.special_type.id,
#          :system_user_id => user_id,
#          :event_item_id => event_item_id
#       })
#       counter.save
#     end
#     return counter
#   end

#   def self.get_kassir_by_user(user)
#     return self.find_by(:system_user_id => user.id)
#   end

# end
