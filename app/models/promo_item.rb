# == Schema Information
#
# Table name: promo_items
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  description        :text
#  content            :text
#  published_at       :datetime
#  system_state_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  beginning_at       :datetime
#  ending_at          :datetime
#

class PromoItem < ActiveRecord::Base

  has_many :promo_images, dependent: :destroy

  has_attached_file :image, :styles => { :card => "550x300", :list => "300x140" , :preview => "128x55" , :thumb => "100x100" }, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :name,
            presence: true,
      length: { minimum: 4, maximum: 255}

  validates :description,
            presence: true

  validates :content,
            presence: true

  validates :published_at,
            presence: true

  validates :beginning_at,
            presence: true

  validates :ending_at,
            presence: true

end
