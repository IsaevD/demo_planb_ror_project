# == Schema Information
#
# Table name: banners
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  banner_type_id     :integer
#  event_item_id      :integer
#  system_state_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  link               :string(255)
#  position           :string(255)
#

class Banner < ActiveRecord::Base

  belongs_to :banner_type
  belongs_to :event_item

  has_attached_file :image, :styles => { :long => "1000x52" , :slider => "660x225" , :thumb => "100x100" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 255}

  def self.get_active_banner(type = BannerType.main_type ,count = nil)
    if !count.nil?
      return self.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id, :banner_type_id => type.id).order(:position => :asc).limit(count)
    else
      return self.where(:system_state_id => SystemMainItemsD::SystemState.active_state.id, :banner_type_id => type.id).order(:position => :asc)
    end
  end

end
