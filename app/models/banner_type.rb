# == Schema Information
#
# Table name: banner_types
#
#  id         :integer          not null, primary key
#  alias      :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class BannerType < ActiveRecord::Base

  def self.MAIN_TYPE_ALIAS
    return "main"
  end
  def self.MINOR_TYPE_ALIAS
    return "minor"
  end

  def self.main_type
    return self.find_by(:alias => self.MAIN_TYPE_ALIAS)
  end
  def self.minor_type
    return self.find_by(:alias => self.MINOR_TYPE_ALIAS)
  end

end
