class UserMailer < ActionMailer::Base
  default from: "no-reply@bilet18.ru"

  # toDO: Добавить автоматическое генерирование ссылки в письмах

  def register_mail(user)
    @user = user
    mail(to: user.email, сс: Setting.find_by(:alias => "administrator_mail").value, subject: 'Регистрация PlanB')
  end

  def restore_mail(user)
    @user = user
    mail(to: user.email, сс: Setting.find_by(:alias => "administrator_mail").value, subject: 'Восстановление пароля PlanB')
  end

end
