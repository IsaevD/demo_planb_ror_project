class TicketsMailer < ActionMailer::Base
  default from: "no-reply@bilet18.ru"

  def order_tickets_mail(payment, user)
    mail_address = Setting.find_by(:alias => "administrator_mail").value
    @payment = payment
    @user = user
    if !mail_address.empty?
      mail(to: mail_address, subject: 'Покупка билетов PlanB')
    end
  end

  def user_order_mail(payment, user)
    @payment = payment
    @user = user
    mail_address = user.email
    if !mail_address.empty?
      mail(to: mail_address, subject: 'Покупка билетов PlanB')
    end
  end

end
