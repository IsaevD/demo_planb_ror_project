class MainMailer < ActionMailer::Base
  default from: "no-reply@bilet18.ru"

  def callback_mail(request)
    mail_address = Setting.find_by(:alias => "order_call_mails").value
    @request = request
    if !mail_address.empty?
      mail(to: mail_address, subject: 'Запрос обратного звонка PlanB')
    end
  end

  def certificate_mail(request)
    mail_address = Setting.find_by(:alias => "order_cert_mails").value
    @request = request
    if !mail_address.empty?
      mail(to: mail_address, subject: 'Запрос сертификата PlanB')
    end
  end

  def collective_mail(request)
    mail_address = Setting.find_by(:alias => "administrator_mail").value
    @request = request
    if !mail_address.empty?
      mail(to: mail_address, subject: 'Коллективная заявка на PlanB')
    end
  end

end
