var app = window.app || {};

app.ClientMasterRoom = (function(){
  _config = {
    parentSelector: '.js_client-master-room_modal',
  }

  function _continue() {
    var dancingContainer = $('.dancing');
    var availableTicketIds = dancingContainer.data('available-tickets-ids')

    availableTicketIds.splice(0, _getTicketsSize());

    dancingContainer.data('available-tickets-ids', availableTicketIds);
  }

  function _initialize() {
    if ($('.js_client-master-room_modal').length > 0) {
      _bindEventListeners();
    }
  }

  function _bindEventListeners() {
    $('.js_client-master-room_open').click(function(event){
      event.preventDefault();
      event.stopPropagation();

      $('.js_client-master-room_modal').show();
    });

    $('.js_client-master-room_close').click(function(event){
      event.preventDefault();
      event.stopPropagation();

      $('.js_client-master-room_modal').hide();
    });

    $('.js_client-master-room_continue').click(function(event){
      event.preventDefault();
      event.stopPropagation();

      $('.js_client-master-room_modal').hide();

      _continue();
    });
  }

  function _getPrice() {
    if ($('.js_client-master-room_modal').length == 0){
      return 0;
    }

    var price = _getTicketsSize() * parseInt($('.dancing').data('price'));
    return isNaN(price) ? 0 : price;
  }

  function _getTicketsSize() {
    if ($('.js_client-master-room_modal').length == 0){
      return 0;
    }

    var ticketsSize = parseInt($(_config.parentSelector + ' #tickets_size').val());
    return isNaN(ticketsSize) ? 0 : ticketsSize;
  }

  function _getActiveTicketsIds() {
    if ($('.js_client-master-room_modal').length == 0){
      return [];
    }

    var availableTicketIds = $('.dancing').data('available-tickets-ids')

    var activeTicketIds = availableTicketIds.slice(0, _getTicketsSize());

    return activeTicketIds;
  }

  return {
    initialize: _initialize,
    getActiveTicketsIds: _getActiveTicketsIds,
    getPrice: _getPrice,
    getTicketsSize: _getTicketsSize
  }
})()
