/*
$(function(){
    initSlider();
});
*/

function initSlider(object, children, paginator, interval) {

    // задать значения по-умолчанию
    if (object == null)
        object = $(".slider");
    if (children == null)
        children = ".slider_children";
    if (interval == null)
        interval = 3000;
    if (paginator == null)
        paginator = ".slider_paginator";

    $object = $(object);
    $children = $object.children(children);
    $paginator = $object.children(paginator);
    $children.width($object.width()*$children.children("li").size());
    $children.children("li").first().addClass("current");
    $paginator.children("li").first().addClass("current");

    initPaginator($object,$children,$paginator);
    // автоматическое переключение слайдера
    sliderInterval = setInterval(nextSlide($object, $children, $paginator), interval);


}

function nextSlide(object, children, paginator) {
    slideAnimation(object, children, "right", paginator);
}

function previousSlide(object, children, paginator) {
    slideAnimation(object, children, "left", paginator);
}

function slideAnimation(object, children, type, index, paginator) {
    if (type == "right") {
        type = 1;
        if ($(children).children(".current").index() == ($(children).children("li").size() - 1)) {
            $(children).css('left', -(($(children).children("li").size()-2)*$(object).width()));
            $(children).append($($(children).children().first()));
        }
        nextElement = $(children).children(".current").next();
    }
    else {
        type = -1;
        if ($(children).children(".current").index() == 0) {
            $(children).css('left', -$(object).width())
            $(children).prepend($($(children).children().last()));
        }
        nextElement = $(children).children(".current").prev();
    }

    if (index == null) {
        $(children).
            animate({left: -($(children).children(".current").index() + type) * $(object).width()}, 300);
        $(children).children(".current").removeClass("current");
        nextElement.addClass("current");

        if (paginator != null) {
            $(paginator).children(".current").removeClass("current");
            $($(paginator).children("li")[parseInt($(nextElement).data("slider-id"))]).addClass("current");
        }
        

    }
    else {
        $(children).
            animate({left: -($(children).children("li[data-slider-id="+index+"]").index() * $(object).width())}, 300);
        $(children).children(".current").removeClass("current");
        $(children).children("li[data-slider-id="+index+"]").addClass("current");
    }

}

function initPaginator(object, children, paginator) {
    $(paginator).children("li").click(function(){
        if (($(this).index() - $("li[data-slider-id=" + parseInt($(this).index()) + "]").index()) < 0)
            type = "left";
        else
            type = "right";

        slideAnimation(object, children, type, $(this).index()+1);
        $(paginator).children(".current").removeClass("current");
        $(this).addClass("current");
    });
}