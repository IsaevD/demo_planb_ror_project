// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// @TODO visual_interface_d surprisingly remove hidden_field values
//= require visual_interfaces_d/application
//= require admin/interface
//= require schemas/schemas
//= require payments/payments
//= require forms/forms

//= require admin/modules/tickets_refund
//= require admin/modules/tickets_hold
//= require admin/modules/tickets_hold_refund
//= require admin/modules/tickets_print
//= require admin/modules/tickets_buy

//= require admin/modules/order_master/order_master_base
//= require admin/modules/order_master/order_master_full
//= require admin/modules/order_master/order_master_reserved
//= require admin/modules/order_master/order_master_room

//= require shared/shared

$(function(){
    // Подключение интерфейса
    openTableFilterBar();
    openMenuItem();
    openLegendBar();
    removeTicketInPayment();
    printTicketInReservedPayment();

    app.OrderMasterFullModule.initialize();
    app.OrderMasterReservedModule.initialize();
    app.OrderMasterRoomModule.initialize();

    initDiasabledFields();

    // Подключение расширенных схем залов
    if ($(".advanced_schema").length != 0) {
        initialAdvancedSchemaSeatClick();
        initialAdvancedSchemaChangeTicketStates();
        initialAdvancedSchemaChangeTicketPrices();
        initialAdvancedClearAllCheckedTickets();
        initialAdvancedSchemaChangeTicketDiscounts();
        initializeAdvancedBookTicket();
        initialDancingCountTicketsForm();
    }

    if ($(".usual_schema").length != 0) {
        initialAdvancedSchemaSeatClick(true, true);
        initializeAdvancedBookTicket();
        initialDancingCountTicketsForm();
        initialAdvancedClearAllCheckedTickets();

        app.TicketsRefundModule.initialize();
        app.TicketsHoldModule.initialize();
        app.TicketsHoldRefundModule.initialize();
        /*
        initialUsualSchemaBuyClick();
        initialUsualSchemaBookClick();
        */
    }

    if ($(".order_panel").length != 0) {
        initializePaymentSearch();
        removeTicketInReservedPayment();
    }

    initializeSystemForm();

    //initialSchema();
    openCalendarOnIconClick( $('.icon-calendar') );
});




//= require turbolinks

function openCalendarOnIconClick ( target ) { // открывает datepicker по клику на икноку календаря
    $(target).on('click', function () {
        console.log($(this).parent().children('input.datetimepicker'));
       $(this).parent().children('input.datetimepicker').focus();
    });
}
