var app = window.app || {}

app.TicketsBuyModule = (function(){
  function _request(url, data, cb) {
    $.ajax({
      type: "POST", data: data, url: url,
      success: _success.bind(null, cb)
    });
  }

  function _success(cb, response, textStatus, xhr) {
    xhr.status == 200 ? cb(response) : alert(response.error);
  }

  var buy         = _request.bind(null, '/admin/ajax_buy_ticket');
  var buyReserved = _request.bind(null, '/admin/ajax_buy_ticket_by_ids');
  var cancel      = _request.bind(null, '/admin/ajax_cancel_printed_ticket');

  return {
    buy: buy,
    buyReserved: buyReserved,
    cancel: cancel
  }
})()
