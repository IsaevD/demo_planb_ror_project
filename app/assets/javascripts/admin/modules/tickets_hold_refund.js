var app = window.app || {}

app.TicketsHoldRefundModule = (function(){
  var _ticketIds = [];

  function _success(data) {
    _clearAll();
    // @TODO rely on method from outside scoping, bad, refactor
    refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"))
    alert('Билеты были успешно возвращены в продажу')
  }

  function _isActive($this) {
    return $this.css('background-color') === 'rgb(255, 0, 0)';
  }

  function _setActive($this) {
    var _button = $('.js_hold-tickets-refund_button');

    $this.css('cssText', 'background-color: red !important');

    _ticketIds.push($this.data('ticket-id'));

    _button.attr('disabled', false)
    _button.addClass('blue_btn')
    _button.removeClass('grey_btn')
  }

  function _setInactive($this) {
    var _button = $('.js_hold-tickets-refund_button');

    $this.css('cssText', 'background-color: #555 !important; border: 1px solid red');

    _ticketIds = $.grep(_ticketIds, function(ticketId){
      return ticketId !== $this.data('ticket-id');
    })

    if (_ticketIds.length === 0) {
      _button.attr('disabled', true)
      _button.addClass('grey_btn')
      _button.removeClass('blue_btn')
    }
  }

  function _clearAll() {
    var _button = $('.js_hold-tickets-refund_button');
    _button.attr('disabled', true)
    _button.addClass('grey_btn')
    _button.removeClass('blue_btn')

    _setInactive($(".hall_schema .places .seat[data-state='hold']"));

    _ticketIds = [];
  }

  function _bindEventListeners() {
    var seats = $(".hall_schema .places .seat");

    seats.click(function() {
      if ($(this).data('state') == 'hold') {
        _isActive($(this)) ? _setInactive($(this)) : _setActive($(this));

        $(".hall_schema .places .seat.active").removeClass('active');
      } else {
        _clearAll();
      }
    });

    $('.js_hold-tickets-refund_button').click(function(event) {
      event.preventDefault();
      event.stopPropagation();

      refund(_ticketIds, _success);
    })
  }

  function initialize() {
    var _button = $('.js_hold-tickets-refund_button');

    if (_button.length > 0) {
      _button.attr('disabled', true)
      _bindEventListeners();
    }
  }

  function refund(ticketIds, success) {
    $.ajax({
      type: "PUT",
      data: { 
        ticket_ids: ticketIds,
        ticket_state_id: $('.js_hold-tickets-refund_state-field').val(),
        hold_state_expires_at: null
      },
      url: "/admin/tickets/batch_update",
      success: success
    })
  }

  return {
    refund: refund,
    initialize: initialize
  }
}())
