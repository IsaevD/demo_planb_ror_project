var app = window.app || {}

app.TicketsPrintModule = (function(){
  function _success(ticketIds, cb, response, textStatus, xhr) {
    if (xhr.status == 200) {
      window.open("/admin/print_blank?ticket_ids=" + ticketIds, '_blank');
      cb(response);
    }
  }

  function print(data, cb) {
    $.ajax({
      type: "POST",
      data: data,
      url: "/admin/ajax_print_ticket",
      success: _success.bind(null, data.ticket_ids, cb),
      error: function(response, textStatus, xhr) {
        alert(response.responseJSON.error);
      }
    });
  }

  return {
    print: print
  }
})()
