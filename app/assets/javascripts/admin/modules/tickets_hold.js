var app = window.app || {}

app.TicketsHoldModule = (function(){
  function _success(data) {
    _close();
    // @TODO rely on method from outside scoping, bad, refactor
    refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"))
    alert('Эти места будут придержаны')
  }

  function _close() {
    $(".hall_schema .places .seat.active").removeClass('active');
    $('.js_hold-tickets_modal').hide();
  }

  function _open() {
    if (_ticketIds().length > 0) {
      $('.js_hold-tickets_modal').show();
    } else {
      alert('Для начала выберите места')
    }
  }

  function _ticketIds() {
    var _ticketIds = [];

    $(".hall_schema .places .seat.active").each(function (index) {
      _ticketIds.push($(this).data("ticket-id"));
    });

    return _ticketIds;
  }

  function _bindEventListeners() {
    $('.js_hold-tickets_send').click(function(event) {
      hold(_ticketIds(), _success);
    })

    $('.js_hold-tickets_close-modal').click(_close)
    $('.js_hold-tickets_open-modal').click(_open)
  }

  function initialize() {
    _bindEventListeners();
  }

  function hold(ticketIds, success) {
    $.ajax({
      type: "PUT",
      data: { 
        ticket_ids: ticketIds,
        ticket_state_id: $('.js_hold-tickets_state-field').val(),
        hold_state_expires_at: $('.js_hold-tickets_expiresAt-field').val()
      },
      url: "/admin/tickets/batch_update",
      success: success
    })
  }

  return {
    hold: hold,
    initialize: initialize
  }
}())
