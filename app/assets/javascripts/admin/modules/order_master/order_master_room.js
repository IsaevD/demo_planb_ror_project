// stop polluting global namespace
var app = window.app || {}

app.OrderMasterRoomModule = (function(){
  var baseOrderMaster;

  var _config = {
    btnSelector: '.js_order-master-room_open',
    masterAttr: 'buy',
    masterClass: 'buy_stage',
    ticketsSelector: '.dancing',
    parentSelector: '.js_order-master-room',
    openMasterCondition: function() {
      // on order master for room tickets, it isnt necessary to check tickets before
      return true;
    },
    actionWithTicket: function() {
      app.TicketsBuyModule.buy({
        "ticket_ids": _getActiveTicketsIds().join(","),
        "payment_type_id": $(_config.parentSelector + " #payment_type").val(),
        "payment_method_id": $(" #payment_method").val(),
        "event_id": $(".hall_schema").attr("data-event-id"),
        "fio": $(_config.parentSelector + " #fio").val(),
        "phone": $(_config.parentSelector + " #phone").val(),
        "delivery_address": $(_config.parentSelector + " #delivery_address").val()
      }, function() {
        var dancingContainer = $('.dancing')

        var ticketsSize = $(_config.parentSelector + ' #tickets_size').val();
        var restTicketIds = dancingContainer.data('available-tickets-ids');
        restTicketIds.splice(0, ticketsSize);

        dancingContainer.data('available-tickets-ids', restTicketIds);

        var message = 'Танцпол: свободных билетов: ' + restTicketIds.length + ' / ' + dancingContainer.data('tickets-ids').length;
        dancingContainer.text(message);
      })
    },
    getTicketIdsForPrint: _getActiveTicketsIds
  }

  function _getActiveTicketsIds() {
    var ticketsSize = $(_config.parentSelector + ' #tickets_size').val();
    var availableTicketIds = $('.dancing').data('available-tickets-ids')

    return availableTicketIds.slice(0, ticketsSize);
  }

  function _initialize(querySelector) {
    baseOrderMaster = new app.OrderMasterBaseModule(_config);
    baseOrderMaster.initialize();
  }

  return {
    initialize: _initialize
  }
}());
