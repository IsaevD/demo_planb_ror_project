// stop polluting global namespace
var app = window.app || {}

app.OrderMasterFullModule = (function(){
  var baseOrderMaster;

  var _config = {
    btnSelector: '.js_order-master_open .btn',
    masterAttr: 'buy',
    masterClass: 'buy_stage',
    ticketsSelector: '.hall_schema .places .seat.active',
    parentSelector: '.js_order-master',
    openMasterCondition: function() {
      return _getActiveTicketsIds().length > 0;
    },
    actionWithTicket: function() {
      app.TicketsBuyModule.buy({
        "ticket_ids": _getActiveTicketsIds().join(","),
        "payment_type_id": $(_config.parentSelector + " #payment_type").val(),
        "payment_method_id": $(_config.parentSelector + " #payment_method").val(),
        "event_id": $(".hall_schema").attr("data-event-id"),
        "fio": $(_config.parentSelector + " #fio").val(),
        "phone": $(_config.parentSelector + " #phone").val(),
        "delivery_address": $(_config.parentSelector + " #delivery_address").val()
      }, function() {
        refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
        $(".seat.active").removeClass("active");
      })
    },
    getTicketIdsForPrint: function() {
      var ticketIds = [];

      $(_config.parentSelector + " #confirm_buy_stage .table .checkbox_print:checked").each(
        function() {
          ticketIds.push($(this).parent().parent().attr("data-ticket-id"));
        }
      );

      return ticketIds;
    }
  }

  function _getActiveTicketsIds() {
    var _ticketIds = []

    $(_config.ticketsSelector).each(function (index) {
      _ticketIds.push($(this).data("ticket-id"));
    });

    return _ticketIds;
  }

  function _initialize(querySelector) {
    baseOrderMaster = new app.OrderMasterBaseModule(_config);
    baseOrderMaster.initialize();
  }

  return {
    initialize: _initialize
  }
}());
