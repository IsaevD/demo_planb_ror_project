var app = window.app || {}

app.OrderMasterBaseModule = function OrderMasterBaseModule(config){
  this._config = config;
};

app.OrderMasterBaseModule.prototype.initialize = function() {
  if ($(this._config.parentSelector).length > 0) {
    this._bindOrderMasterEventListeners();
    this._bindModalEventListeners();
  }
}

app.OrderMasterBaseModule.prototype._localizeSelector = function(selector) {
  return this._config.parentSelector + ' ' + selector;
}

app.OrderMasterBaseModule.prototype._bindOrderMasterEventListeners = function() {
  var self = this;
  var orderMaster = $(self._config.parentSelector);

  $(self._config.btnSelector).click(function(event){
    event.preventDefault();
    event.stopPropagation();

    if (self._config.openMasterCondition()) {
      // Обнулить, чтобы при отмене ордер мастера браковались только выбранные в этом процессе билеты
      $(self._localizeSelector("#print_results")).attr("data-ticket-ids", []);

      orderMaster.attr("data-type", self._config.masterAttr);
      orderMaster.removeClass("book_stage").addClass(self._config.masterClass);
      orderMaster.show();

      $(self._localizeSelector(".order_stage")).first().show();

      self._renderTableWithActiveTickets(self._config.ticketsSelector);
    } else {
      alert("Билеты не выбраны");
    }
  });

  // This opens next stage dialog and switch to checked this ticket
  $(self._localizeSelector('.print_ticket')).click(function(){
    $(this).parent().parent().find(".checkbox_print").attr("checked", true);;
    $(self._localizeSelector('#choose_blank_type')).show();
  });

  // This opens next stage dialog
  $(self._localizeSelector('#print_checked_tickets')).click(function(){
    $(self._localizeSelector('#choose_blank_type')).show();
  });

  $(self._localizeSelector('#cancel_print')).click(function(){
    $(self._localizeSelector('#choose_blank_type')).hide();
  });

  $(self._localizeSelector('#confirm_print')).click(function() {
    self._confirmPrint();
  });

  $(self._localizeSelector('#yes_print')).click(function() {
    self._successPrint();
  });

  $(self._localizeSelector('#repeat_print')).click(function() {
    self._repeatPrint();
  });


  $(self._localizeSelector('#buy_on_sale_point')).click(function(){
    $(self._localizeSelector('#delivery_by')).attr('checked', !$(this).is(':checked'));
  });

  $(self._localizeSelector('#delivery_by')).click(function(){
    $(self._localizeSelector('#buy_on_sale_point')).attr('checked', !$(this).is(':checked'));
  });

  // @TODO Make sure it is up to date and still executing
  $(self._localizeSelector('#abort_tickets')).click(function(){
    $(self._localizeSelector(".accepts")).hide();
  });
}

app.OrderMasterBaseModule.prototype._bindModalEventListeners = function() {
  var self = this;
  // Мастер действия
  var orderMaster = $(this._config.parentSelector);

  // Кнопки мастера действия
  var previous_btn = $(this._localizeSelector("#previous_master_stage"));
  var next_btn = $(this._localizeSelector("#next_master_stage"));
  var cancel_btn = $(this._localizeSelector("#cancel_master_stage"));

  // Стадии мастера действия
  var current_stage_class = this._localizeSelector(".order_stage:visible");
  var order_stage_class = this._localizeSelector(".order_stage");

  // Названия стадий мастеров действий
  var choose_room_tickets_size_stage_name = "choose_room_tickets_size_stage"
  var choose_payment_method_stage_name = "choose_payment_method_stage";
  var choose_payment_type_stage_name = "choose_payment_type_stage";
  var fill_delivery_data_stage_name = "fill_delivery_data_stage";
  var fill_personal_data_stage_name = "fill_personal_data_stage";
  var confirm_buy_stage_name = "confirm_buy_stage";
  var success_stage_name = "success_buy_stage";

  // Название полей
  var is_delivered_field = $(this._localizeSelector("#is_delivered"));
  // Заголовки полей
  var next_label = "Далее >";

  // Переключение на предыдущий этап мастера действия
  function switchPrevStage(current_stage, prev_stage) {
    prev_stage.show();
    current_stage.hide();

    switch (prev_stage.attr("id")) {
      case choose_payment_type_stage_name:
        previous_btn.addClass("disabled");
        break;
    }
  }

  // Переключение на следующий этап мастера действия
  function switchNextStage(current_stage, next_stage) {
    next_stage.show();
    current_stage.hide();

    previous_btn.removeClass("disabled");
  }

  previous_btn.click(function(){
    if (!$(this).hasClass("disabled")) {

      var current_stage = $(current_stage_class);
      prev_stage = current_stage.prev(order_stage_class);

      switch (current_stage.attr("id")) {
        case confirm_buy_stage_name:
          if (!is_delivered_field.is(":checked"))
            prev_stage = current_stage.prev(order_stage_class).prev(order_stage_class);
          break;
      }

      switchPrevStage(current_stage, prev_stage);
      next_btn.text(next_label)

    }
  });

  // Действие по переключению на следующий слайд
  next_btn.click(function(){
    var current_stage = $(current_stage_class);
    next_stage = current_stage.next(order_stage_class);

    need_change = true;

    switch (current_stage.attr("id")) {

      case choose_payment_type_stage_name:
        if ($(self._localizeSelector("#payment_type option:selected")).text() == "Бронирование") {
          need_change = false;
          $(self._localizeSelector("#choose_payment_type_stage")).hide();
          $(self._localizeSelector("#success_buy_stage")).show();
          self._config.actionWithTicket();
          previous_btn.hide();
          cancel_btn.hide();
          next_btn.text("Готово");
        }
        break;

      case confirm_buy_stage_name:
        self._config.actionWithTicket();
        previous_btn.hide();
        cancel_btn.hide();
        next_btn.text("Готово");
        break;

      case success_stage_name:
        orderMaster.hide();
        previous_btn.show();
        cancel_btn.show();
        next_btn.text("Далее >");
        break;

      case fill_personal_data_stage_name:
        if (!is_delivered_field.is(":checked"))
          next_stage = current_stage.next(order_stage_class).next(order_stage_class);
        break;
      }

      if (need_change)
        switchNextStage(current_stage, next_stage);

  });
  // Действие по отмене мастера
  cancel_btn.click(function(){
    ticket_ids = $(self._localizeSelector("#print_results")).attr("data-ticket-ids");

    app.TicketsBuyModule.cancel({
      "ticket_ids": ticket_ids
    }, function(){
      var current_stage = $(current_stage_class);
      current_stage.hide();
      orderMaster.hide();

      $(".seat.active").removeClass("active");
    })
  });
}

app.OrderMasterBaseModule.prototype._renderTableWithActiveTickets = function(querySelector) {
  var self = this;

  if (!$(self._localizeSelector(".accepts")).is(":visible")) {
    $(self._localizeSelector(".accepts")).show();

    $(self._localizeSelector(".table tbody tr")).remove();

    $(querySelector).each(function () {
      $(self._localizeSelector(".table tbody")).append(
        '<tr class="ticket" data-print="0" data-ticket-id="'+ $(this).attr("data-ticket-id") + '">' +
        '<td class="checkbox-cnt"><input class="checkbox_print" type="checkbox" checked>' +
        '<td>' + $(this).attr("data-group") + '</td><td>' + $(this).attr("data-range") + '</td><td>' + $(this).attr("data-place") + '</td><td>' + $(this).attr("data-discount") + '</td><td>' + $(this).attr("data-price") + ' руб.</td><td class="print_cnt"><div class="btn blue_btn print_ticket">Печать</div></td></tr>'
      );
    });
  }
}

app.OrderMasterBaseModule.prototype._basePrint = function(type) {
  var self = this;

  var ticketIds = self._config.getTicketIdsForPrint();

  app.TicketsPrintModule.print({
    counter_type: $(self._localizeSelector('#counter_type')).val(),
    ticket_ids: ticketIds.join(","),
    type: type
  }, function success(){
    $(self._localizeSelector("#print_results")).attr("data-ticket-ids", ticketIds.join(","));
    $(self._localizeSelector("#print_results")).show();
  })
}

app.OrderMasterBaseModule.prototype._confirmPrint = function() {
  app.OrderMasterBaseModule.prototype._basePrint.call(this, 'firstly');
}

app.OrderMasterBaseModule.prototype._repeatPrint = function() {
  app.OrderMasterBaseModule.prototype._basePrint.call(this, 'repeatedly');
}


app.OrderMasterBaseModule.prototype._successPrint = function() {
  var self = this;

  var ticket_ids = $(this._localizeSelector("#print_results")).attr("data-ticket-ids");

  $.each(ticket_ids.split(','), function(index, ticket_id){
    var ticket = $(self._localizeSelector('#confirm_buy_stage .ticket[data-ticket-id=' + ticket_id + ']'))

    ticket.find('.checkbox_print').attr('checked', false)
    ticket.find('.btn').text('Распечатан').removeClass('blue_btn').addClass('grey_btn');
  })

  $(this._localizeSelector("#print_results")).hide();
  $(this._localizeSelector("#choose_blank_type")).hide();
}
