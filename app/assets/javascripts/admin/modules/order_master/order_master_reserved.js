var app = window.app || {}

app.OrderMasterReservedModule = (function(){
  var baseOrderMaster;

  var _config = {
    btnSelector: '.js_order-master-reserved_open .btn',
    masterAttr: 'buy',
    masterClass: 'buy_stage',
    ticketsSelector: '#tickets .js_ticket',
    parentSelector: '.js_order-master-reserved',
    openMasterCondition: function() {
      return _getActiveTicketsIds().length > 0;
    },
    actionWithTicket: function() {
      app.TicketsBuyModule.buyReserved({
        ticket_ids: _getActiveTicketsIds().join(","),
        payment_id: $("#tickets").attr("data-payment-id")
      }, function(){
        $("#tickets .label").text("");
        $("#tickets .table .ticket").remove();
        $("#tickets .non-record").remove();
        $(".search_result .js_buy_tickets .btn").hide();
        $("#tickets .non-record").text("Билеты успешно выкуплены");
      })
    },
    getTicketIdsForPrint: function() {
      var ticketIds = [];

      $(_config.parentSelector + " #confirm_buy_stage .table .checkbox_print:checked").each(
        function() {
          ticketIds.push($(this).parent().parent().attr("data-ticket-id"));
        }
      );

      return ticketIds;
    }
  }

  function _getActiveTicketsIds() {
    var _ticketIds = []

    $(_config.parentSelector + ' ' +_config.ticketsSelector).each(function (index) {
      _ticketIds.push($(this).data("ticket-id"));
    });

    return _ticketIds;
  }

  function _initialize(querySelector) {
    baseOrderMaster = new app.OrderMasterBaseModule(_config);
    baseOrderMaster.initialize();
  }

  return {
    initialize: _initialize
  }
}());
