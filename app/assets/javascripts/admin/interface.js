// Функция интерфейса
function openTableFilterBar() {
    $(".open_bar").unbind("click");
    $(".open_bar").click(function(){
        if ($("#filter_form").is(":visible"))
            $("#filter_form").hide();
        else
            $("#filter_form").show();
    });
}

function openMenuItem() {
    $(".has_second_menu").unbind("click");
    $(".first_menu_item.has_second_menu").parent().click(function(){
        obj = $(this).children(".second_menu_item");
        if(obj.is(":visible")) {
            obj.hide();
            $(this).children(".first_menu_item").children(".icon-arrow-up").removeClass("icon-arrow-up").addClass("icon-arrow-down");
        }
        else {
            $(obj).show();
            $(this).children(".first_menu_item").children(".icon-arrow-down").removeClass("icon-arrow-down").addClass("icon-arrow-up");
        }
    });
}

function openLegendBar() {
    $("#legend_nav_panel").click(function(){
        if($("#legend").is(":visible")) {
            $("#legend").hide();
            $("#legend_nav_panel").children(".icon-arrow-up").removeClass("icon-arrow-up").addClass("icon-arrow-down");
        }
        else {
            $("#legend").show();
            $("#legend_nav_panel").children(".icon-arrow-down").removeClass("icon-arrow-down").addClass("icon-arrow-up");
        }
    })
}

function removeTicketInPayment() {
    if ($(".remove_ticket").length != 0) {
        $(".remove_ticket").click(function () {
            $(this).parent().parent().remove();
        })
    }
}

// toDO: Isaev: Название функции неинформативно
// toDO: Isaev: Зачем первые 4 строчки кода функции написаны? Что они делают?
function initDiasabledFields() {
    // put some selectors here!
    var fields = [];

    $.each( fields , function ( index , value ) {
        $(value).attr('disabled', 'disabled');
    });

    if (($("#static_page_non_removable").length != 0) && ($("#static_page_non_removable").val() == "true")) {
        $("#static_page_alias").attr("disabled", "disabled");
    }
}
