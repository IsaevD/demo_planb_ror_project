function initMainSlider() {
    if ($("#main_banner").length != 0) {
        object = $("#main_banner");
        children = object.children(".slider_children");
        paginator = object.children(".slider_paginator");
        $(children).children("li").first().addClass("current");
        $(paginator).children("li").first().addClass("current");
        reinitDChildren();

        $($(paginator).children("li")).click(function () {
            slideAnimation($(this).index());
        });

        //sliderInterval = setInterval(nextSlide(), 1000);


        /*
         $($(object).children(".left_switcher")).click(function(){
         previousSlide();
         });

         $($(object).children(".right_switcher")).click(function(){
         nextSlide();
         });

         function nextSlide() {
         slideAnimation("right");
         }
         function previousSlide() {
         slideAnimation("left");
         }
         */

        $(paginator).children("li").click(function () {
            slideAnimation($(this).index());
        });

        function nextSlide() {
            if ($(paginator).children("li.current").index() == ($(paginator).children("li").length - 1))
                position = 0;
            else
                position = $(paginator).children("li.current").index() + 1;
            slideAnimation(position);
        }

        function slideAnimation(position) {
            length = $(children).children("li").length - 1;
            width = $(children).children("li.current").width();
            $(children).animate({left: -( position * width )}, 300);
            $($(children).children("li")).removeClass("current");
            $($(children).children("li")[position]).addClass("current");
            $($(paginator).children("li")).removeClass("current");
            $($(paginator).children("li")[position]).addClass("current");
        }

        function reinitDChildren() {
            if ($("body").hasClass("mobile")) {
                width = $(children).children("li.current").width();
                current = $($(children).children("li.current")).index();
                $(children).children("li").width($(window).width());
                $(children).css("left", -( current * width ));
            }
        }

        $(window).resize(function () {
            reinitDChildren();
        });
    }
}