var order_cookie_param = 'planb_order';

// Получить ИД всех выбранных билетов схемы
function getActiveTicketsIds() {
    var seats = $(".hall_schema .places .seat.active");
    arrayIds = [];
    seats.each(function (index) {
        arrayIds.push($(this).data("ticket-id"));
    });
    return arrayIds
}

// Снять все отметки мест
function clearAllCheckedTickets() {
    $(".hall_schema .places .seat.active").removeClass("active");
}

// Подсчитать количество выбранных билетов и итоговую сумму всех билетов
// Hard to navigate through all this staff
// Adding functionality will be impossible at some point.
function caclucateSchemeCountAndTotal() {
  var count = 0, total = 0, seats = $(".hall_schema .places .seat.active");

  seats.each(function (index) {
    total = total + parseInt($(this).data("price"));
    count = count + 1;
  });

  // @TODO would be better to move it into appropriate module app.ClientMasterRoom
  // but it would require huge refactoring of this spagetti code. flyr1q
  total = total + app.ClientMasterRoom.getPrice();
  count = count + app.ClientMasterRoom.getTicketsSize();

  return [count, total]
}

// Обновить информационное поле заказа
function updateOrderInfo(count, total) {
    var countBlock = $(".main_info .count");
    var totalBlock = $(".main_info .total");
    countBlock.text(count);
    totalBlock.text(total);
}

// Открыть информационное поле над выбранным местом
function openAdvancedTicketInfoWidget(seat) {
    seat.append("<div class='info_block'>" +
        "<div class='ticket_group'>Группа: <span class='value'>"+seat.attr('data-group')+"</span></div>" +
        "<div class='number'>Номер: <span class='value'>"+seat.attr('data-ticket-number')+"</span></div>" +
        "<div class='range'>Ряд: <span class='value'>"+seat.attr('data-range')+"</span></div>" +
        "<div class='place'>Место: <span class='value'>"+seat.attr('data-place')+"</span></div>" +
        "<div class='price'>Цена: <span class='value'>"+seat.attr('data-price')+"</span> руб.</div>" +
        "<div class='discount'>Скидка: <span class='value'>" + seat.attr('data-discount') + "</span></div>" +
        "<div class='state'>Статус: <span class='value'>"+seat.attr('data-state-name')+"</span></div>" +
        "</div>");
    if (seat.attr('data-user-id') != "")
        seat.children(".info_block").append("<div class='user'>Пользователь: <span class='value'><a target='_blank' href='/admin/users/" + seat.attr('data-user-id') + "/edit'>" + seat.attr('data-user') + "</a></span></div>");
    else
        seat.children(".info_block").append("<div class='user'>Пользователь: <span class='value'>-</span></div>");

    if (seat.attr('data-system-user-id') != "")
        seat.children(".info_block").append("<div class='system-user'>Кассир: <span class='value'><a target='_blank' href='/admin/system_users/"+seat.attr('data-system-user-id')+"/edit'>"+seat.attr('data-system-user')+"</a></span></div>");
    else
        seat.children(".info_block").append("<div class='system-user'>Кассир: <span class='value'>-</span></div>");

    if (seat.attr('data-payment-id') != "0")
        seat.children(".info_block").append("<div class='payment'>Заявка: <span class='value'><a target='_blank' href='/admin/payments/"+seat.attr('data-payment-id')+"/edit'>"+seat.attr('data-payment-number')+"</a></span></div>");
    else
        seat.children(".info_block").append("<div class='payment'>Заявка: <span class='value'>-</span></div>");

}
// Закрыть информационное поле над выбранным местом
function closeAdvancedTicketInfoWidget() {
    $(".hall_schema .places .seat .info_block").remove();
}

// Открыть информационное поле над выбранным местом (обычное представление)
function openUsualTicketInfoWidget(seat, add_active) {
    if (typeof(add_active) === "undefined") { add_active = true; }
    if (add_active) {
        seat.addClass("active");
    }
    seat.append("<div class='info_block usual_info'>" +
        "<div class='arrow'></div>" +
        "<div class='ticket_group'>Группа: <span class='value'>"+seat.attr('data-group')+"</span></div>" +
        "<div class='range'>Ряд: <span class='value'>"+seat.attr('data-range')+"</span></div>" +
        "<div class='place'>Место: <span class='value'>"+seat.attr('data-place')+"</span></div>" +
        "<div class='price'>Цена: <span class='value'>"+seat.attr('data-price')+"</span> руб.</div>");
}
// Закрыть информационное поле над выбранным местом (обычное представление)
function closeUsualTicketInfoWidget(seat, remove_active) {
    if (typeof(remove_active) === "undefined") { remove_active = true; }
    var info_blocks = $(".hall_schema .places .seat .info_block");
    info_blocks.remove();
    if (remove_active) {
        seat.removeClass("active");
    }
}
// Функционал: Установить сообщение в виджете корзины в шапке
function setHeaderCartWidgetMessage(count, total) {
    if (count != 0) {
        html = "В вашей корзине <span class='attention count'>" + count + "</span> билетов на сумму <span class='attention price'>" + total + "</span> руб.";
    }
    else {
        html = "Ваша корзина пустая";
    }
    $("#auth_form .in_cart_message").html(html);
}
// Функционал: Установить сообщение в виджете корзины в схеме
function setSchemeCartWidgetMessage(count, total) {
    var schemeCartWidget = $(".cart_panel .price_values");
    if (schemeCartWidget.length != 0) {
        schemeCartWidget.children(".count").text(count);
        schemeCartWidget.children(".total").text(total);
    }
}

// Инициализация единичного клика по месту
function initialAdvancedSchemaSeatClick(usual_info_widget, update_widget) {

    if (typeof(usual_info_widget) === "undefined") { usual_info_widget = false; }
    if (typeof(update_widget) === "undefined") { update_widget = false; }

    var seats = $(".hall_schema .places .seat");
    var first_seat = null;
    if (seats.length != 0) {
        if (usual_info_widget) {
            seats.hover(
                function(){
                    if ((!$(this).hasClass("non_active")) && ($(this).attr("data-state") == "available") && ($(this).attr("data-type") == "fixed")) {
                        openUsualTicketInfoWidget($(this), false);
                    }
                },
                function(){
                    if ((!$(this).hasClass("non_active")) && ($(this).attr("data-state") == "available") && ($(this).attr("data-type") == "fixed")) {
                        closeUsualTicketInfoWidget($(this), false);
                    }
                });
        } else {
            seats.hover(
                function(){
                    if (!$(this).hasClass("non_active") && ($(this).attr("data-type") == "fixed")) {
                        openAdvancedTicketInfoWidget($(this));
                    }
                },
                function(){
                    if (!$(this).hasClass("non_active") && ($(this).attr("data-type") == "fixed")) {
                        closeAdvancedTicketInfoWidget($(this));
                    }
                });
        }
        seats.click(function () {
            if (!$(this).hasClass("non_active")) {
                if ($(this).attr("data-type") == "fixed") {
                    if ($(this).data("state") == "available") {
                        if (!$(this).hasClass("active")) {
                            $(this).addClass("active");
                            if (first_seat == null) {
                                first_seat = $(this);
                                setTimeout(function () {
                                    first_seat = null;
                                }, 2000);
                            }
                            else {
                                var second_seat = $(this);
                                $('.seat').filter(function () {
                                    var flag = false;
                                    if (($(this).data("ticket-id") > first_seat.data("ticket-id")) && ($(this).data("ticket-id") < second_seat.data("ticket-id"))) {
                                        flag = true;
                                    }
                                    if (flag) {
                                        if (!$(this).hasClass("non_active")) {
                                            return $(this);
                                        }
                                    }

                                }).addClass('active');
                                first_seat = null;
                                second_seat = null;
                            }
                        } else {
                            $(this).removeClass("active");
                        }
                    }
                }
            }
            if (update_widget) {
                updateKassirWidgetInfo();
            }
        });
    }
}
// Инициализация формы Количество билетов
function initialDancingCountTicketsForm() {

    // var dancing_room_seats = $(".dancing_room_count").parent();

    // $(".dancing_close").click(function(event) {
    //     $(".dancing_room_count").hide();
    //     event.stopPropagation();
    // });
    // $(".dancing_cancel").click(function(event){
    //     dancing_room_seats.removeClass("active");
    //     dancing_room_seats.attr("data-state", "disable");
    //     dancing_room_seats.attr("data-price", "0");
    //     $("#dancing_count").val("");
    //     $(".dancing_room_count").hide();
    //     event.stopPropagation();
    // });
    // $(".dancing_confirm").click(function(event){
    //     dancing_room_seats.addClass("active");
    //     dancing_room_seats.attr("data-state", "available");
    //     dancing_room_seats.attr("data-price", parseInt(dancing_room_seats.attr("data-price-per-ticket")) * parseInt($("#dancing_count").val()));
    //     $(".dancing_room_count").hide();
    //     event.stopPropagation();
    // })
}

// Инициализация единичного клика по месту
function initialUsualSchemaSeatClick() {
    var seats = $(".hall_schema .places .seat");
    // hardcode vars in different places is bad practice. Better make file with init data in js.
    var ticket_count = 3;
    var initial_ticket_count = $("#auth_form .in_cart_message .count").text();
    var initial_ticket_total = $("#auth_form .in_cart_message .price").text();
    if (initial_ticket_count == "") {
        initial_ticket_count = 0;
    } else {
        initial_ticket_count = parseInt(initial_ticket_count);
    }
    if (initial_ticket_total == "") {
        initial_ticket_total = 0;
    } else {
        initial_ticket_total = parseInt(initial_ticket_total);
    }
    // Получить максимально допустимое количество билетов
    // TODO it is really bad idea to use sync ajax here. kvokka
    // If some user have veeeeery slow internet and fast fingers and want
    // to order a lot- let him do it!
    // Also, you already hard code default value
    // It gives ~-100ms on development env and MUCH more in production
    $.ajax({
        type: "POST",
        // async: false,
        async: true,
        url: "/ajax_get_max_ticket_counter",
        success: function (data) {
            ticket_count = parseInt(data);
        }
    });

    // @TODO have to do this because of current architecture
    // would be better to put it in appropriate place
    // which is app.ClientMasterRoom module. flyr1q
    $('.js_client-master-room_continue').click(function(event){
        arr = caclucateSchemeCountAndTotal();
        count = initial_ticket_count + arr[0];
        total = initial_ticket_total + arr[1];
        setSchemeCartWidgetMessage(arr[0], arr[1]);
        setHeaderCartWidgetMessage(count, total);
    })

    // TODO it looks like event listener part. kvokka
    if (seats.length != 0) {
        seats.click(function () {
            if ($(this).data("state") == "available") {
                if (!$(this).hasClass("active")) {
                    if ($(".places .seat.active").length == ticket_count) {
                        $(".collective_order").show();
                    } else {
                        closeUsualTicketInfoWidget($(this));
                        openUsualTicketInfoWidget($(this));
                        arr = caclucateSchemeCountAndTotal();
                        count = initial_ticket_count + arr[0];
                        total = initial_ticket_total + arr[1];
                        setSchemeCartWidgetMessage(arr[0], arr[1]);
                        setHeaderCartWidgetMessage(count, total);
                    }
                } else {
                    closeUsualTicketInfoWidget($(this));
                    arr = caclucateSchemeCountAndTotal();
                    if (arr == undefined) {
                        count = initial_ticket_count;
                        total = initial_ticket_total;
                        current_schema_count = 0;
                        current_schema_total = 0;
                    } else {
                        count = initial_ticket_count + arr[0];
                        total = initial_ticket_total + arr[1];
                        current_schema_count = arr[0];
                        current_schema_total = arr[1];
                    }
                    setSchemeCartWidgetMessage(current_schema_count, current_schema_total);
                    setHeaderCartWidgetMessage(count, total);
                }
            }
        });
    }
}

// Функционал: Инициализация подтверждения заказа на схеме
function initialUsualSchemaSubmit() {
    var checkoutBtn = $("#checkout");
    if(checkoutBtn.length != 0) {
        checkoutBtn.click(function () {
            if(!$(this).hasClass("non_active")) {
                var activeSeats = $(".hall_schema .places .seat.active");
                if (activeSeats.length == 0 && app.ClientMasterRoom.getActiveTicketsIds().length == 0) {
                    // toDO: Заменить выводом обычного сообщения
                    alert("Вы не выбрали билеты");
                    return false;
                } else {
                    setActiveTicketsInCookies(activeSeats);
                    return true;
                }
            }
        });
    }
}

// Функционал: установить ИД заказываемых билетов в cookies
function setActiveTicketsInCookies(seats) {
    arrayIds = [];

    ticketIds = getTicketsCookie();
    console.log(ticketIds);
    /*
    ticketIds.split(",").forEach(function(item, i, arr) {
        arrayIds.push(item);
    });
    */
    seats.each(function (index) {
        arrayIds.push($(this).data("ticket-id"));
    });

    // @TODO the same as above, this is workaround
    // need to refactor in order it to be clean .flyr1q
    arrayIds = arrayIds.concat(app.ClientMasterRoom.getActiveTicketsIds());

    setTicketsCookie(arrayIds);
}

// Функционал: установить ИД билетов в куки
function setTicketsCookie(ticketIds) {
    $.cookie(order_cookie_param, null, { expires: 3, path: '/' });
    $.cookie(order_cookie_param, ticketIds.join(","), { expires: 3, path: '/' });
}
// Функционал: получить
function getTicketsCookie() {
    return $.cookie(order_cookie_param);
}
function initializeTicketsCookie() {
    $.cookie(order_cookie_param, null, { expires: 3, path: '/' });
}

// Инициализация формы смены статусов у билетов
function initialAdvancedSchemaChangeTicketStates() {
    var changeTicketStateForm = $("#change_state form");
    if (changeTicketStateForm.length != 0) {
        changeTicketStateForm.submit(function () {
            $.ajax({
                type: "POST",
                data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").data("event-id"), "ticket_state_id": $(this).children("select").val() },
                url: "/admin/ajax_change_ticket_states",
                success: function (data) {
                    if (data == "Success") {
                        eventId = $(".hall_schema").data("event-id");
                        refreshAdvancedSchemaByEventID(eventId);
                        alert("Статусы успешно обновлены");
                    }
                }
            });
            return false;
        });
    }
}

// Инициализация формы смены цены у билетов
function initialAdvancedSchemaChangeTicketPrices() {
    var changeTicketPriceForm = $("#change_price form");
    if (changeTicketPriceForm.length != 0) {
        changeTicketPriceForm.submit(function () {
            if($("#price").val() != "") {
                $.ajax({
                    type: "POST",
                    data: {"ticket_ids": getActiveTicketsIds().join(","), "price": $(this).children("#price").val()},
                    url: "/admin/ajax_change_ticket_prices",
                    success: function (data) {
                        if (data == "Success") {
                            eventId = $(".hall_schema").data("event-id");
                            refreshAdvancedSchemaByEventID(eventId);
                            $(".seat").removeClass("active");
                            alert("Цены успешно обновлены");
                        }
                    }
                });
            }
            return false;
        });
    }
}

// Инициализация формы смены скидок у билетов
function initialAdvancedSchemaChangeTicketDiscounts() {
    var changeTicketDiscountForm = $("#change_discount form");
    if (changeTicketDiscountForm.length != 0) {
        changeTicketDiscountForm.submit(function () {
            $.ajax({
                type: "POST",
                data: {"ticket_ids": getActiveTicketsIds().join(","), "ticket_discount_id": $(this).children("select").val() },
                url: "/admin/ajax_change_ticket_discounts",
                success: function (data) {
                    if (data == "Success") {
                        eventId = $(".hall_schema").data("event-id");
                        refreshAdvancedSchemaByEventID(eventId);
                        $(".seat").removeClass("active");
                        alert("Скидки успешно обновлены");
                    }
                }
            });
            return false;
        });
    }
}

// Инициализация действия очистки схемы от выбранных мест
function initialAdvancedClearAllCheckedTickets() {
    $("#clear_checked_tickets").click(function(){
        clearAllCheckedTickets();
    })
}

// Обновление схемы по идентификатору события
function refreshAdvancedSchemaByEventID(eventId) {
    $.ajax({
        type: "POST",
        data: { "event_id": eventId },
        url: "/admin/ajax_get_tickets_info",
        success: function (data) {
            data.forEach(function(item, i, arr) {
                obj = $("li.seat[data-seat-id="+item[0]+"]").first();
                if (obj.length != 0) {
                    obj.attr("data-state", item[1]);
                    obj.attr("data-price", item[2]);
                    obj.attr("data-state-alias", item[3]);
                    obj.attr("data-discount", item[5]);
                    obj.attr("data-payment-id", item[6]);
                    obj.attr("data-payment-number", item[7]);
                    obj.attr("data-user-id", item[8]);
                    obj.attr("data-user", item[9]);
                    obj.attr("data-system-user-id", item[10]);
                    obj.attr("data-system-user", item[11]);
                    obj.attr("data-ticket-number", item[12]);
                    obj.css("background-color", item[4]);
                }
            });
        }
    });
}

// Обновление схемы по идентификатору события
function refreshUsualSchemaByEventID(eventId) {
    $.ajax({
        type: "POST",
        data: { "event_id": eventId },
        url: "/admin/ajax_usual_get_tickets_info",
        success: function (data) {
            data.forEach(function(item, i, arr) {
                obj = $("li.seat[data-seat-id="+item[0]+"]").first();
                if (obj.length != 0) {
                    // @TODO attr and data a bit inconsistent between each other
                    obj.attr("data-state", item[1]);
                    obj.attr("data-price", item[2]);
                    obj.attr("data-state-alias", item[3]);
                    obj.attr("data-discount", item[5]);

                    obj.data('state', item[1]);
                    obj.data('price', item[2]);
                    obj.data('state-alias', item[3]);
                    obj.data('discount', item[5]);

                    obj.css("background-color", item[4]);
                }
            });
        }
    });
}

// Обновление виджета с информацией о выбранных билетов
function updateKassirWidgetInfo() {
    var seats = $(".hall_schema .places .seat.active");
    var total = 0;
    var count = 0;
    seats.each(function(){
        total = total + parseInt($(this).attr("data-price"));
        count = count + 1;
    });
    $(".order_info .count .value").text(count);
    $(".order_info .total .value").text(total);
}

// Инициализация клика по кнопке "Купить"
function initialUsualSchemaBuyClick() {
    var buyBtn = $(".js_buy_tickets");
    if (buyBtn.length != 0) {
        buyBtn.click(function () {
            $.ajax({
                type: "POST",
                data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").attr("data-event-id") },
                url: "/admin/ajax_create_payment_and_buy_ticket_by_ids",
                success: function (data) {
                    if (data == "Success") {
                        refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                        $(".seat.active").removeClass("active");
                        $(".order_info .count .value").text("0");
                        $(".order_info .total .value").text("0");
                    }
                }
            });
        });
    }
}

// Инициализация клика по кнопке "Забронировать"
function initialUsualSchemaBookClick() {
    var bookBtn = $("#book_ticket");
    if (bookBtn.length != 0) {
        bookBtn.click(function () {
            $.ajax({
                type: "POST",
                data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").attr("data-event-id") },
                url: "/admin/ajax_create_payment_and_book_ticket_by_ids",
                success: function (data) {
                    if (data == "Success") {
                        refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                        $(".order_info .count .value").text("0");
                        $(".order_info .total .value").text("0");
                    }
                }
            });
        });
    }
}

// Обновление состояния всех мест для расширенной схемы
function refreshAdvancedSeatsStatesByEventID(eventId) {
    $.ajax({
        type: "POST",
        data: { "event_id": eventId },
        url: "/admin/ajax_admin_get_tickets",
        success: function (data) {
            data.forEach(function(item, i, arr) {
                obj = $("li.seat[data-seat-id="+item[0]+"]").first();
                if (obj.length != 0) {
                    obj.attr("data-state", item[1]);
                }
            });
        }
    });
}

function initializeAdvancedBookTicket() {
    /*
    $("#book_ticket .btn").click(function(){
        $.ajax({
            type: "POST",
            data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").attr("data-event-id") },
            url: "/admin/ajax_book_ticket",
            success: function (data) {
                if (data == "Success") {
                    refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                    $(".seat.active").removeClass("active");
                    alert("Билеты успешно забронированы");
                }
            }
        });
    });
    */
}

// @TODO unused
// Получить количество бланков Кассира
// function getNormalKassirCounter() {
//     $.ajax({
//         type: "POST",
//         url: "/admin/ajax_get_normal_kassir_counter",
//         success: function (data) {
//         }
//     });
// }
// function getSpecialKassirCounter() {
//     $.ajax({
//         type: "POST",
//         url: "/admin/ajax_get_special_kassir_counter",
//         success: function (data) {
//             alert(data);
//         }
//     });
// }





