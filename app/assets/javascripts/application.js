// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.cookie
//= require jquery_ujs
//= require jquery-ui
//= require number_mask.js
//= require slider
//= require new_slider
//= require main_slider
//= require schemas/schemas
//= require orders/orders

//= require desktop/modules/client_master_room

var order_cookie_param = 'planb_order';

$(function(){

    // Инициализация действия кнопок в верхнем меню
    initialMobileHeaderBtns();
    // Инициализация действия кнопки Показать фильтр на странице События
    initialMobileEventsListSwitcher();
    initSalesPointsNavigation();
    initPopup();
    initCalendar();
    initPriceSlider();
    initImagePopup();
    initPhotoSlider();
    initMainSlider();
    initMasks();
    initSalePointMap();
    initSalePointCityMap($("#maps li.map_cnt.active"));
    deleteTicketFromOrderConfirmation();
    //initSchemeRefresh();

    //refreshCartWidget();
    /*initSalePointCitiesMap();*/

    // Инициализация схемы
    if ($(".hall_schema").length != 0) {
        initializeTicketsCookie();
        initialUsualSchemaSeatClick();
        initialUsualSchemaSubmit();

        app.ClientMasterRoom.initialize();
    }

    if ($("#order").length != 0) {
        checkAllOrderTableValues();
        clearRecordInOrderTable();
        checkActionValue();
        submitConfirmStage();
    }

    if ($("#delivery_data").length != 0) {
        checkActionValue();
    }


    if ($("#payment_tickets").length != 0) {
        checkAllPaymentTableValues();
        clearRecordInPaymentTable();
    }
    
});


// toDo: При переключении в пунктах меню скрывать автоматические предыдущую открытую панель - сделать в массиве
function initialMobileHeaderBtns(){
    $(".mobile #header_menu_btn").click(function(){
        activeDeactiveDisplayElement(this,"#header_main_menu");
    });
    $(".mobile #header_auth_btn").click(function(){
        activeDeactiveDisplayElement(this,"#auth_form");
    });
}

function activeDeactiveDisplayElement(object, children, reverseFlag) {
    if (typeof(reverseFlag)==='undefined') reverseFlag = false;

    if (!reverseFlag) {
        if ($(object).hasClass("active")) {
            $(children).hide();
            $(object).removeClass("active");
            return "hide"
        } else {
            $(children).show();
            $(object).addClass("active");
            return "show"
        }
    } else {
        if ($(object).hasClass("active")) {
            $(object).removeClass("active");
            $(object).hide();
            $(children).addClass("active");
            $(children).show();
            return "hide"
        } else {
            $(children).hide("active");
            $(object).addClass("active");
            $(object).show();
            return "show"
        }
    }
}

function initialMobileEventsListSwitcher(){
    $("#events_list_switcher .hidden_list").click(function(){
        if ($("#events_filter_panel").is(":visible")) {
            $(this).text("Показать фильтр");
            $("#events_filter_panel").hide();
        }
        else {
            $(this).text("Cкрыть фильтр");
            $("#events_filter_panel").show();
        }
    });

}

function initSalesPointsNavigation() {
    $("#sale_points_cities li").click(function(){
        $("#sale_points_cities li").removeClass("active");
        $("#maps li.map_cnt").removeClass("active");
        $(this).addClass("active");
        $("#sale_points ul").removeClass("active");
        $($("#sale_points ul")[$(this).index()]).addClass("active");
        $($("#maps li.map_cnt")[$(this).index()]).addClass("active");
        initSalePointCityMap($("#maps li.active"));
        return false;
    });
    $("#sale_points_show_map").click(function(){
        if ($("#maps").is(":visible")) {
            $("#maps").hide();
            $("#sale_points_show_map").text("Показать карту");
        }
        else {
            $("#maps").show();
            $("#sale_points_show_map").text("Скрыть карту");
        }
    });
}

function initPopup() {
    $(".contacts_callback_link").click(function(){
        $(".popup.call").show();

    });
    $("body").on("click", ".popup", function(){
        $(".popup").hide();
        $("#call_form").show();
        $("#call_form").parent().children(".text").hide();

    });
    $(".popup .form_bg").click(function(e) {
        e.stopPropagation();
    });
    if ($("#collective_order_form").length != 0) {
        $("#collective_order_form").submit(function(){
            $("#collective_order_form .error").hide();
            error = false;
            if ($("#collective_order_form input[type=text]").val() == "") {
                $("#collective_order_form .error").show();
                error = true;
            }
            if (!error) {
                postData = $(this).serializeArray();
                $("#collective_order_form form input").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    data: postData,
                    url: $(this).attr("action"),
                    success: function (data) {
                        if (data == "Success") {
                            $("#collective_order_form").hide();
                            $("#collective_order_form").parent().children(".text").show();
                        }
                    }
                });
            }
            return false;
        });
    }
    $("#call_form").submit(function(){
        $("#call_form .error").hide();
        error = false;
        if ($("#call_form input[type=text]").val() == "") {
            $("#call_form .error").show();
            error = true;
        }
        if (!error) {
            postData = $(this).serializeArray();
            $("#call_form form input").attr("disabled", true);
            $.ajax({
                type: "POST",
                data: postData,
                context: $(this),
                url: $(this).attr("action"),
                success: function (data) {
                    if (data == "Success") {
                        $(this).trigger("reset");
                        $("#call_form").hide();
                        $("#call_form").parent().children(".text").show();
                    }
                }
            });
        }
        return false;
    });

    $("#fake_certificate_form").submit(function(){
        $("#certificate_form input[type=hidden]").val($("#fake_certificate_form input[type=text]").val());
        $("#certificate_form").show();
        $("#certificate_form").parent().children(".text").hide();
        $(".popup.certificate").show();
        return false;
    });

    $("#certificate_form").submit(function(){
        $("#certificate_form .error").hide();
        error = false;
        if ($("#certificate_form input[type=text]").val() == "") {
            $("#certificate_form .error").show();
            error = true;
        }
        if (!error) {
            postData = $(this).serializeArray();
            $("#certificate_form form input").attr("disabled", true);
            $.ajax({
                type: "POST",
                data: postData,
                url: '/ajax_create_certificate_request',
                success: function (data) {
                    if (data == "Success") {
                        $("#certificate_form").hide();
                        $("#certificate_form").parent().children(".text").show();
                    }
                }
            });
        }
        return false;
    });

    $("#certificate_form").submit(function(){
        $("#certificate_form .error").hide();
        error = false;
        if ($("#certificate_form input[type=text]").val() == "") {
            $("#certificate_form .error").show();
            error = true;
        }
        if (!error) {
            postData = $(this).serializeArray();
            $("#certificate_form form input").attr("disabled", true);
            $.ajax({
                type: "POST",
                data: postData,
                url: $(this).attr("action"),
                success: function (data) {
                    if (data == "Success") {
                        $("#certificate_form").hide();
                        $("#certificate_form").parent().children(".text").show();
                    }
                }
            });
        }
        return false;
    });

}

function initCalendar() {
    $("#calendar").datepicker({
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        dayNamesShort: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'],
        dayNamesMin: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс']
    });
    $("#calendar td a").unbind("click");
    $("#calendar td").unbind("click");
    $("body").on("mouseup", "#calendar td a", function(e){
        var href = $(this).attr("href");
        if ((href == "#") || (href == "")) return false;
        window.location.href = href;
        e.stopPropagation();
    });
    $(document).on('mouseup', '.ui-datepicker-prev, .ui-datepicker-next', function() {
        setCalendars();
    });
    $("#event_data").click(function(){
        $(".popup.calendar_range").show();
    });
    $("#calendar_of").datepicker({
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        dayNamesShort: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'],
        dayNamesMin: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс']
    });
    $("#calendar_to").datepicker({
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        dayNamesShort: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'],
        dayNamesMin: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс']
    });
    $(".calendar_range .ok").click(function(){
        month = parseInt($("#calendar_of .ui-state-active").parent().attr("data-month")) + 1;
        month = ((parseInt(month) < 10) ? "0" + month : month);
        day = $("#calendar_of .ui-state-active").text();
        day = ((parseInt(day) < 10) ? "0" + day : day);
        date_of = day + "." + month + "." +  $("#calendar_of .ui-state-active").parent().attr("data-year");
        month = parseInt($("#calendar_to .ui-state-active").parent().attr("data-month")) + 1;
        month = ((parseInt(month) < 10) ? "0" + month : month);
        day = $("#calendar_to .ui-state-active").text();
        day = ((parseInt(day) < 10) ? "0" + day : day);
        date_to = day + "." +  month + "." +  $("#calendar_to .ui-state-active").parent().attr("data-year");
        $("#date_of").val(date_of);
        $("#date_to").val(date_to);
        $("#event_data").val(date_of + " - " + date_to);
        $(".popup.calendar_range").hide();
    });
    $(".calendar_range .cancel").click(function(){
        $(".popup.calendar_range").hide();
    });
    setCalendars();
}

function setCalendars() {
    $.ajax({
        type: "POST",
        url: "/ajax_get_events_info",
        success: function (data) {
            var grouped = {};

            data.forEach(function(item){
              var groupKey = JSON.stringify({year: item[1], month: item[2], day: item[3]});

              grouped[groupKey] = grouped[groupKey] || [];
              grouped[groupKey].push(item);
            })

            $.each(grouped, function(key, group){
              var dateObj = JSON.parse(key);
              var date = new Date(dateObj.year + '-' + dateObj.month + '-' + dateObj.day);

              $("#calendar td[data-year='"+ date.getFullYear() +"'][data-month='"+ date.getMonth() +"'] a:contains('" + date.getDate() + "')").each(function(){
                var cell = $(this);

                if (cell.text() == date.getDate()) {
                  cell.parent('td').addClass("active");

                  if (group.length > 1) {
                    cell.attr('href', '/events?date_of=' + date.toString() + '&date_to=' + date.toString());
                  } else {
                    cell.attr("href", "/events/"+ group[0][0]);
                  }
                }
              });
            });
        }
    });
}

function initPriceSlider() {
    if ($("#price_of").val())
        price_of = parseInt($("#price_of").val())
    else
        price_of = 0;
    if ($("#price_to").val())
        price_to = parseInt($("#price_to").val())
    else
        price_to = 500;

    if ($("#current_price_of").val())
        current_price_of = parseInt($("#current_price_of").val())
    else
        current_price_of = price_of;
    if ($("#current_price_to").val())
        current_price_to = parseInt($("#current_price_to").val())
    else
        current_price_to = price_to;

    $( ".price_slider" ).slider({
        range: true,
        min: price_of,
        max: price_to,
        values: [ current_price_of, current_price_to ],
        slide: function( event, ui ) {
            $($(".price_slider .ui-slider-handle")[0]).children(".price_val").text(ui.values[0] + " руб.");
            $($(".price_slider .ui-slider-handle")[1]).children(".price_val").text(ui.values[1] + " руб.");
            $("#price_of").val(ui.values[0]);
            $("#price_to").val(ui.values[1]);
        }
    });
    $($(".price_slider .ui-slider-handle")[0]).append("<div class='price_val'>"+current_price_of+"руб.</div>");
    $($(".price_slider .ui-slider-handle")[1]).append("<div class='price_val'>"+current_price_to+" руб.</div>");
}

function initMasks() {
    $("#call_form #phone").numberMask({beforePoint:14})
    $("#certificate_form #phone").numberMask({beforePoint:14});
    $("#certificate_value").numberMask({beforePoint:14});
    $("#register_form #phone").numberMask({beforePoint:14});
    $("#register_form #discount_number").numberMask({beforePoint:14});
}

function initSalePointMap() {
    if ($("#sale_point").length != 0) {
        $.ajax({
            type: "POST",
            data: "id="+$("#sale_point").data("id"),
            url: "/ajax_get_sale_point_coords",
            success: function (data) {
                var map;
                DG.then(function () {
                    map = DG.map('map', { center: data, zoom: 15 });
                    DG.marker(data).addTo(map);
                });
            }
        });
    }
}

function initSalePointCityMap(city) {
    if ($("#sale_points").length != 0) {
        if (city.children().length == 0) {
            $.ajax({
                type: "POST",
                url: "/ajax_get_sale_points_by_city_id",
                data: {"id": city.data("id")},
                success: function (data) {
                    var map;
                    DG.then(function () {
                        map = DG.map(city.data("alias"), {center: [city.data("coorx"), city.data("coory")], zoom: 10});
                    });
                    data.forEach(function (item, i, arr) {
                        DG.then(function () {
                            DG.marker([item.coor_x, item.coor_y]).addTo(map);
                        });
                    });
                }
            });
        }
    }
}

function deleteTicketFromOrderConfirmation() {
    $(document).on('click', 'table tr td .delete-box', function ( e ) {
        var
            clicked_element = $(e.currentTarget),
            // current_order_cookie is array of strings
            current_order_cookie = getCookie('planb_order')
            row_object = clicked_element.parent().parent(),
            table_object = row_object.parent().parent(),
            ticket_id = row_object.data('ticket-id')
            ;

            // check current order
            if ( current_order_cookie != undefined ) {
                current_order_cookie = current_order_cookie.split(',');
                ticket_index_in_cookie_var = current_order_cookie.indexOf(ticket_id.toString());
            }

            // if ticket_id in current_cookie_order
            if (ticket_index_in_cookie_var >= 0) {
                current_order_cookie.splice(ticket_index_in_cookie_var, 1);
                // set new cookie value and delete clicked row
                setCookie('planb_order', current_order_cookie.join(","));
                row_object.remove();

                // calculate table results 
                total_count = table_object.find('tr.value').length
                if (total_count > 0) { // the table is not empty
                    total_price = 0;
                    $.each( table_object.find('tr.value td.price') , function ( index , value ) {
                        total_price += $(value).data('price');
                    });

                    // change the table results
                    table_object.find('tr.table_footer td.total_count').text(total_count.toString() + " шт.");
                    table_object.find('tr.table_footer td.total_price').text(total_price.toString() + " руб.");
                } else { // the table is empty
                    table_object.html("<tr><td collspan = \"4\">Ваша корзина пуста</td></tr>");
                }
            } else { // ticket is not in current_cookie_index
                return false
            }

    });

    function getCookie(name) {
      var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }
}

//= require turbolinks                                   
