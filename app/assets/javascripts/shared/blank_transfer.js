$(document).ready(function(){
  if ($("#new_blank_transfer").length != 0) {
    // Разрешен ввод только цифр
    initNumber(["#blank_transfer_counter"]);

    var normal_name = "Общий";

    var blank_type_input = $('#blank_transfer_blank_type_id');
    var event_item_input = $("#blank_transfer_event_item_id");
    var event_item_row = $(".js_events_list");

    function _turnSpecial() {
      event_item_input.val('1');
      event_item_row.show();
    }

    function _turnNormal() {
      event_item_input.val(null);
      event_item_row.hide();
    }

    function _toggle() {
      blank_type_input.children("option:selected").text() == normal_name ? _turnNormal() : _turnSpecial();
    }

    _toggle();
    blank_type_input.on('change', _toggle);
  }
})
