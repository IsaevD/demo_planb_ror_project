function initImagePopup() {
    $("#photo_slider img").click(function(){
        image_src = $(this).parent().data("img")
        $(".image_popup img").attr("src", image_src);
        $(".image_popup").show();
    });
}

function initPhotoSlider() {

    object = $("#photo_slider");
    children = object.children(".slider_children");
    $(children).children("li").first().addClass("current");
    reinitChildren();

    $($(object).children(".left_switcher")).click(function(){
        previousSlide();
    });

    $($(object).children(".right_switcher")).click(function(){
        nextSlide();
    });

    function nextSlide() {
        slideAnimation("right");
    }

    function previousSlide() {
        slideAnimation("left");
    }

    function slideAnimation(type) {
        if ($("body").hasClass("mobile")) {
            admixture = 0;
        } else {
            admixture = 2;
        }

        current = $($(children).children("li.current")).index();
        length = $(children).children("li").length - 1;
        width = $(children).children("li.current").width() + 20;
        if (length > 2) {
            switch (type) {
                case "right":
                    if (current == length - admixture)
                        current = 0;
                    else
                        current = current + 1;
                    break;
                case "left":
                    if (current == 0)
                        current = length - admixture;
                    else
                        current = current - 1;
                    break;
            }
            $(children).animate({left: -( current * width )}, 300);
            $($(children).children("li")).removeClass("current");
            $($(children).children("li")[current]).addClass("current");
        }
    }

    function reinitChildren() {
        if ($("body").hasClass("mobile")) {
            width = $(children).children("li.current").width() + 20;
            current = $($(children).children("li.current")).index();
            $(children).children("li").width($(window).width() - 158);
            $(children).css("left", -( current * width ));
        }
    }

    $(window).resize(function(){
        reinitChildren();
    });
}