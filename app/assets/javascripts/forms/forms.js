//= require number_mask
//= require jquery.cookie

function initializeSystemForm() {
    initializePaymentForm();
    initializeDiscountForm();
    // initializeBlankDeliveryForm();
    // initializeBlankRequestTypeForm();
    // initializeAcceptBlankRequest();
    initRefreshBlankDefectsForm();
    initializeCheckSalePointByKassir();
    // initializeBlankRequestForm();
}

function initializePaymentForm() {
    if ($(".edit_payment").length != 0) {
        ignore_hidden = ["payment_phone", "payment_delivery_address", "payment_delivery_method_id", "payment_fio", "payment_payment_kind_id"];
        $(".edit_payment :input").each(function () {
            if ((ignore_hidden.indexOf($(this).attr("id")) == -1) && ($(this).attr("type") != "submit")) {
                $(this).attr("disabled", true);
                $(this).addClass("hidden");
            }
        });
    }
}


// function initializeBlankRequestForm() {
//     if ($(".edit_blank_request").length != 0) {
//         console.log(1);
//         ignore_hidden = ["blank_request_value", "blank_request_delivery_date", "blank_request_blank_delivery_type_id"];
//         $(".edit_blank_request :input").each(function () {
//             if ((ignore_hidden.indexOf($(this).attr("id")) == -1) && ($(this).attr("type") != "submit")) {
//                 $(this).attr("disabled", true);
//                 $(this).addClass("hidden");
//             }
//         });
//     }
// }

function initializeDiscountForm() {
    if ($(".new_ticket_discount").length != 0) {
        initAutoComplete($("#ticket_discount_name"), $("#ticket_discount_alias"));
    }
}

// function initializeBlankDeliveryForm() {
//     if ($(".new_blank_request, .edit_blank_request").length != 0) {

//         var main_blanks_label = "Общий";

//         initNumber(["#blank_request_value"]);
//         var blank_type_input = $('#blank_request_blank_type_id'),
//             event_item_row = $("#blank_request_event_item_id").parent();

//         toggleObjectVisibility( event_item_row , (blank_type_input.children("option:selected").text() == main_blanks_label) );
//         blank_type_input.on('change', function() {
//             toggleObjectVisibility ( event_item_row , (blank_type_input.children("option:selected").text() == main_blanks_label) );
//         });
//     }
// }

// function initializeBlankRequestTypeForm() {
//     if ($(".new_blank_request, .edit_blank_request").length != 0) {

//         var blanks_label = "Денежные средства";

//         var blank_request_type_input = $('#blank_request_blank_request_type_id'),
//             blank_type_row = $("#blank_request_blank_type_id").parent(),
//             event_item_row = $("#blank_request_event_item_id").parent();

//         toggleObjectVisibility( blank_type_row , (blank_request_type_input.children("option:selected").text() == blanks_label) );
//         toggleObjectVisibility( event_item_row , (blank_request_type_input.children("option:selected").text() == blanks_label) );
//         blank_request_type_input.on('change', function() {
//             toggleObjectVisibility( blank_type_row , (blank_request_type_input.children("option:selected").text() == blanks_label) );
//             toggleObjectVisibility( event_item_row , (blank_request_type_input.children("option:selected").text() == blanks_label) );
//         });
//     }
// }

// Функция для работы с формой дефектов бланков
function initRefreshBlankDefectsForm() {
    if ($(".edit_blank_defect").length != 0) {

        var main_blanks_label = "Общий";

        var blank_type_input = $('#blank_defect_blank_type_id'),
            event_item_row = $("#blank_defect_event_item_id" ).parent();
        toggleObjectVisibility( event_item_row , (blank_type_input.children("option:selected").text() == main_blanks_label) );
        blank_type_input.on('change', function() {
            toggleObjectVisibility ( event_item_row , (blank_type_input.children("option:selected").text() == main_blanks_label) );
        });
    }
}

// Функция по изменению отображения полей
function toggleObjectVisibility ( object_for_hidding , should_be_hidden ) {
    if ( should_be_hidden ) {
        object_for_hidding.hide();
    } else {
        object_for_hidding.show();
    }
}

function initNumber(object_arr) {
    object_arr.forEach(function(element) {
        $(element).numberMask({beforePoint:8});
    });
}

function initAutoComplete(object, children) {
    $(object).change(function(){
        if($(children).val() == "") {
            if ($(object).val() != "") {
                $(children).val(transliterate($(object).val()));
            }
        }
    });
}

function transliterate(word){
    var answer = "", a = {};

    a["Ё"]="YO";a["Й"]="I";a["Ц"]="TS";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="SH";a["Щ"]="SCH";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
    a["ё"]="yo";a["й"]="i";a["ц"]="ts";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="sh";a["щ"]="sch";a["з"]="z";a["х"]="h";a["ъ"]="'";
    a["Ф"]="F";a["Ы"]="I";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="ZH";a["Э"]="E";
    a["ф"]="f";a["ы"]="i";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="zh";a["э"]="e";
    a["Я"]="Ya";a["Ч"]="CH";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a["Ь"]="'";a["Б"]="B";a["Ю"]="YU";
    a["я"]="ya";a["ч"]="ch";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["ю"]="yu";
    a[" "]="_";a["%"]="";

    for (i in word){
        if (word.hasOwnProperty(i)) {
            if (a[word[i]] === undefined){
                answer += word[i];
            } else {
                answer += a[word[i]];
            }
        }
    }
    return answer;
}

// function initializeAcceptBlankRequest() {

//     $(".accept_blank_request ").click(function() {
//         $.ajax({
//             type: "POST",
//             data: {"blank_request_id": $(this).attr("data-request")},
//             url: "/admin/ajax_accept_blanks",
//             success: function (data) {
//                 if (data == "Success") {
//                     location.reload();
//                 }
//             }
//         });
//         return false;
//     });
// }


function initializeCheckSalePointByKassir() {
    $("#kassir_sale_point_item").submit(function(){
        var sale_point_id = $("#sale_point_item option:selected").val();
        var system_user_id = $('input#system_user_id').val();

        setKassirCookie(system_user_id, sale_point_id);

        location.reload();
        return false;
    });
}

function setKassirCookie(system_user_id, sale_point_id) {
    $.cookie("kassir_sale_point_item_id_for_user_" + system_user_id, null, { expires: 3, path: '/' });
    $.cookie("kassir_sale_point_item_id_for_user_" + system_user_id, sale_point_id, { expires: 3, path: '/' });
}
