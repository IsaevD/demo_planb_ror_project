module ApplicationHelper

  def generate_header_label(title)
    render "admin/elements/headers/header_label", title: title
  end

  def generate_block_label(title, path = nil)
    render "admin/elements/headers/block_label", title: title, path: path
  end

  def generate_table(table_labels)
    render "admin/elements/tables/table", table_labels: table_labels
  end

  def generate_sidebar(menu_items)
    render "admin/elements/sidebar", menu_items: menu_items
  end

  def get_app_path(path_obj)
    return Rails.application.routes.url_helpers.public_send(path_obj)
  end

  def get_system_users_module_path(path_obj)
    return DmSystemUsers::Engine.routes.url_helpers.public_send(path_obj)
  end

  def render_no_mobile_link
    # toDo заменить выдачу root_path на гененрируемое текущее url без моб. версии
    # toDo: сделать корректную выдачу URL
    params = {
        :no_mobile => "no"
    }
    return link_to  "полная версия", "#{root_path}?#{params.to_query}"
  end

  # toDO: сделать универсальный рендер и убрать его в гем
  # toDO: спрятать переменную events в параметры
  def render_desktop_events_list
    return render "desktop/elements/events_list"
  end

  def render_desktop_events_archive_list(items)
    @items = items
    return render "desktop/elements/events/archive_list"
  end

  def render_desktop_archive_event_item(item)
    return render "desktop/elements/events/archive_item", item: item
  end

  def render_mobile_events_list
    return render "mobile/elements/events_list"
  end

  # toDO: сделать вывод данные через переменные
  def render_desktop_news_list
    return render "desktop/elements/news/news_list"
  end
  def render_desktop_one_news(one_news)
    @one_news = one_news
    return render "desktop/elements/news/one_news"
  end

  def render_desktop_promo_list
    return render "desktop/elements/promos/promo_list"
  end
  def render_desktop_promo(promo)
    @promo = promo
    return render "desktop/elements/promos/promo"
  end

  def render_mobile_news_list(items)
    @news = items
    return render "mobile/elements/news/news_list"
  end
  def render_mobile_one_news(one_news)
    @one_news = one_news
    return render "mobile/elements/news/one_news"
  end

  def render_mobile_promo_list(items)
    @items = items
    return render "mobile/elements/promo/promo_list"
  end
  def render_mobile_promo(promo)
    @promo = promo
    return render "mobile/elements/promo/promo"
  end

  # Рендер списка элементов с изображениями
  def render_desktop_items_list(items)
    @items = items
    return render "desktop/elements/list/list"
  end
  def render_desktop_list_item(item)
    return render "desktop/elements/list/item"
  end

  # Рендер списка элементов без изображений для десктопа
  def render_desktop_items_list_without_image(items, category_enabled = true)
    @items = items
    @category_enabled = category_enabled
    return render "desktop/elements/list/list_without_image"
  end
  def render_desktop_list_item_without_image(item)
    return render "desktop/elements/list/item_without_image"
  end
  # Рендер списка элементов без изображений для мобильного устройства
  def render_mobile_items_list_without_image(items, category_enabled = true)
    @items = items
    @category_enabled = category_enabled
    return render "mobile/elements/list/list_without_image"
  end
  def render_mobile_list_item_without_image(item)
    return render "mobile/elements/list/item_without_image"
  end

  # Рендер списка элементов без изображений для мобильного устройства
  def render_mobile_archive_events_list(items)
    @items = items
    return render "mobile/elements/events/archive_list"
  end
  def render_mobile_archive_event(item)
    @item = item
    return render "mobile/elements/events/archive_item"
  end

  def render_photo_slider(items)
    return render "desktop/elements/photo/photo", items: items
  end
  def render_mobile_photo_slider(items)
    return render "mobile/elements/photo/photo", items: items
  end

  # Рендер списка точек продаж
  def render_desktop_sale_points(items)
    @items = items
    return render "desktop/elements/sale_points/sale_points"
  end
  def render_desktop_sale_point(item)
    return render "desktop/elements/sale_points/sale_point", item: item
  end

  def mobile_device?
    if session[:mobile_param]
      session[:mobile_param] == "1"
    else
      request.user_agent =~ /Mobile|webOS/
    end
  end

  def get_site_title ( crumbs = nil )
    index = "PlanB"
    if crumbs.nil?
      return index
    else
      keys = crumbs.keys
      keys[keys.index(t("custom.modules.main_page.name"))] = index if keys.index != nil
      keys.join(" - ")
    end
  end

end
