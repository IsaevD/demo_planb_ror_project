module UsersHelper

  def create_new_user(params)
    user = User.new(params)
    user.user_confirmation = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
    user.confirmed = false
    user.system_state_id = SystemMainItemsD::SystemState.active_state.id
    if !user.save
      flash[:register_error] = user.errors.first[1]
      return false
    else
      #UserMailer.register_mail(user)
      UserMailer.register_mail(user).deliver
      return true
    end
  end

  def confirm_user_by_token(token)
    user = User.find_by(:user_confirmation => token)
    if !user.nil?
      user.user_confirmation = nil
      user.confirmed = true
      user.save
      return true
    else
      return false
    end
  end

  def user_log_in(login, password)
    user = User.where(:email => login, :system_state_id => SystemMainItemsD::SystemState.active_state.id).first()
    if !user.nil?
      flash[:error] = nil
      if user.confirmed
        if authorize_user(User, :user_remember_token , user, password)
          return true
        else
          flash[:error] = 'Неверное имя пользователя/пароль'
          return false
        end
      else
        flash[:error] = 'Аккаунт неподтвержден'
        return false
      end
    else
      flash[:error] = 'Неверное имя пользователя/пароль'
      return false
    end
  end

  def restore_user_password(login)
    user = User.find_by(:email => login)
    if !user.nil?
      user.user_restore_password = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
      if user.save
        UserMailer.restore_mail(user).deliver
        return true
      else
        return false
      end
    else
      flash[:restore_error] = "Пользователя с таким email не существует"
      return false
    end
  end

  def set_new_user_password_by_token(token)
    user = User.find_by(:user_restore_password => params[:change_password_token])
    if !user.nil?
      user.user_restore_password = nil
      user.password = params[:new_password]
      user.password_confirmation = params[:new_repeat_password]
      if user.save
        return true
      else
        flash[:restore_error] = user.errors.first[1]
        return false
      end
    else
      return true
    end

  end

  def current_site_user
    current_user(User, :user_remember_token)
  end

  def custom_user_signed
    if !current_site_user
      flash[:register_error] = "Для покупки билета вы должны авторизоваться"
      redirect_to autorization_path
    end
  end

  def custom_user_unsigned
    if current_site_user
      redirect_to root_path
    end
  end

end
