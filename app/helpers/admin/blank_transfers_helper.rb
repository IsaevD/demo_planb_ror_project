module Admin::BlankTransfersHelper
  def blank_transfers_to_me(system_user_id = nil)
    @blank_transfers_to_me ||= BlankTransfer.in_delivery.assigned_to(system_user_id).with_assignor
  end

  def kassirs_to_select
    current_role = current_system_user.system_user_role.alias
    select_roles = current_role == 'kassir' ? 'kassir_administrator' : ['kassir', 'kassir_major']

    SystemMainItemsD::SystemUser.where(
      system_user_role_id: SystemMainItemsD::SystemUserRole.where(alias: select_roles)
    ).all.collect { |u| ["#{ u.last_name } #{ u.first_name }", u.id] }
  end
end
