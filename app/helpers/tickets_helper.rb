module TicketsHelper

  def generate_new_tickets(event)
    arr = []
    ticket_state_id = TicketState.find_by(:alias => TicketState::AVAILABLE_ALIAS).id

    schema_item = SchemaItem.find_by(:hall_id => event.hall.id)
    seats = SeatItem.where(:schema_item_id => schema_item.id).includes(:seat_item_type).all
# toDO: Необходимо выяснить причину, почему import некорректно работает на тестовом сервере
=begin
    seats.each do |seat|
      arr << Ticket.new(:seat_item_id => seat.id, :price => seat.seat_item_type.price, :event_item_id => event.id, :ticket_state_id => ticket_state_id )
    end
    Ticket.import arr
=end
    SeatItem.where(:schema_item_id => SchemaItem.find_by(:hall_id => event.hall.id).id).each do |seat|
      arr << {:seat_item_id => seat.id, :price => seat.seat_item_type.price, :event_item_id => event.id, :ticket_state_id => ticket_state_id }
    end
    Ticket.create(arr)

  end

  def get_schema(event)
    schema = {}
    Seat.where(:hall_id => event.hall_id).select(:seat_group_id).group(:seat_group_id).each do |seat|
      ranges = {}
      Seat.where(:hall_id => event.hall_id).where(:seat_group_id => seat.seat_group_id).order(:range => :asc).select(:range).group(:range).each do |seat_range|
        ranges[seat_range.range] = Seat.where(:hall_id => event.hall_id).where(:seat_group_id => seat.seat_group_id, :range => seat_range.range).order(:position => :asc)
      end
      schema[seat.seat_group_id] = ranges
    end
    return schema
  end

  def get_legend(event)
    seat_items = event.hall.schema_item.seat_items
    seat_items.select(:seat_item_type_id).each do |item|
      item
    end
  end

  def tickets_for_room(sector_group, event_item)
    @tickets_for_room ||= Ticket.includes(:ticket_state).where(event_item_id: event_item.id,
      seat_item_id: sector_group.seat_items.ids)
  end

  def available_tickets_for_room(sector_group, event_item)
    tickets_for_room(sector_group, event_item).select do |ticket|
      ticket.ticket_state.alias == TicketState::AVAILABLE_ALIAS
    end
  end

end
