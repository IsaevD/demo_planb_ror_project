module EventsHelper
  def events_to_select
    @events_to_select ||= EventItem.select(:id, :name).collect{ |e| [e.name, e.id] }
  end

  def get_events_border_price( border , event_item_id = nil )
    if event_item_id.nil?
      events_ids = EventItem.get_actual_events.map { |k,v| k.id }
    else
      events_ids = event_item_id
    end
    case border.downcase
      when "max"
        sort = "DESC"
      when "min"
        sort = "ASC"
      else
        sort = "DESC"
    end
    ticket_price =  Ticket.where(event_item_id: events_ids).order("price #{sort}").first
    unless ticket_price.nil?
      ticket_price.price
    end
  end

  # kvokka
  # rows are string, so...
  def get_hall_rows sector
    # it was when rows were strings
    # SeatItem.select('row').distinct.where('sector_group_id = ?', sector).pluck('row').sort{|a,b| a&.to_i <=> b&.to_i}
    # # TODO optimize it, if rows always are sequences
    SeatItem.select('row').distinct.where('sector_group_id = ?', sector).order('row').pluck('row')
  end

  def get_grouped_seats (sector, event)
    SeatItem.eager_load(tickets: [:ticket_discount, :ticket_state]).
        where('sector_group_id = ?', sector).
        where('tickets.event_item_id=?',event).
        order('row').
        order('seat_items.number')
  end

  def get_seat_item_types(event_item)
    ids = SeatItem.where(schema_item_id: event_item.hall.schema_item_id)
      .pluck(:seat_item_type_id).uniq

    SeatItemType.find(ids)
  end

  def get_ticket_color ticket_colors, price
    @seat_item_types ||= SeatItemType.all

    type = @seat_item_types
      .select{ |type| type.price <= price }
      .sort_by(&:price)
      .last

    type.present? ? type.color : @seat_items_type.sort_by(&:price).first.color
  end

  def place_empty_seats num
    ("<li class='seat non_active'></li>" * num).html_safe
  end
end
