class ReportService
  attr_reader :system_user, :type, :starts_at, :ends_at, :event_item_id,
    :blank_transfer_service,
    :blank_defect_service,
    :payment_service,
    :blank_stock_service,
    :event_item_service

  delegate :blank_transfers_to_me_size,
    :blank_transfers_to_me_on_normal_blanks_size,
    :blank_transfers_to_me_on_special_blanks_size,
    :blank_transfers_from_me_size,
    :blank_transfers_from_me_on_normal_blanks_size,
    :blank_transfers_from_me_on_special_blanks_size,
  to: :blank_transfer_service

  delegate :blank_defects,
    :blank_defects_on_normal_blanks_size,
    :blank_defects_on_normal_blanks_numbers,
    :blank_defects_on_special_blanks_size,
    :blank_defects_on_special_blanks_numbers,
  to: :blank_defect_service

  delegate :payments,
    :payments_total_size,
    :payments_total_cost,
    :payments_cash_cost,
    :payments_cashless_cost,
    :payments_on_normal_blanks_size,
    :payments_on_normal_blanks_numbers,
    :payments_on_special_blanks_size,
    :payments_on_special_blanks_numbers,
  to: :payment_service

  delegate :blank_stocks,
    :blank_stocks_on_normal_blanks_size,
    :blank_stocks_on_special_blanks_size,
    :blank_stocks_size,
  to: :blank_stock_service

  delegate :event_item,
    :event_item_sector_groups,
    :event_item_paid_seat_items,
  to: :event_item_service

  def initialize(system_user, type, **options)
    @system_user = system_user
    @type = type
    @starts_at = options[:starts_at].to_datetime if options[:starts_at].present?
    @ends_at = options[:ends_at].to_datetime if options[:ends_at].present?
    @event_item_id = options[:event_item_id] if options[:event_item_id].present?

    initialize_report_log(@starts_at, @ends_at)

    @blank_transfer_service = Report::BlankTransferStatService.new(system_user, @starts_at..@ends_at)
    @blank_defect_service = Report::BlankDefectStatService.new(system_user, @starts_at..@ends_at)
    @payment_service = Report::PaymentStatService.new(system_user, @starts_at..@ends_at)
    @blank_stock_service = Report::BlankStockStatService.new(system_user, @starts_at..@ends_at)
    @event_item_service = Report::EventItemStatService.new(system_user, @starts_at..@ends_at, event_item_id)
  end

  def normal_blanks_total_size
    blank_stocks_on_normal_blanks_size +
    blank_transfers_from_me_on_normal_blanks_size +
    blank_defects_on_normal_blanks_size +
    payments_on_normal_blanks_size -
    blank_transfers_to_me_on_normal_blanks_size
  end

  def special_blanks_total_size(event_item_id)
    blank_stocks_on_special_blanks_size(event_item_id) +
    blank_transfers_from_me_on_special_blanks_size(event_item_id) +
    blank_defects_on_special_blanks_size(event_item_id) +
    payments_on_special_blanks_size(event_item_id) -
    blank_transfers_to_me_on_special_blanks_size(event_item_id)
  end

  def all_blanks_total_size
    blank_stocks_size +
    blank_transfers_from_me_size +
    blank_defects.size +
    payments_total_size -
    blank_transfers_to_me_size
  end

  def initialize_report_log(starts_at, ends_at)
    return if starts_at.present? && ends_at.present?

    last_report_log = ReportLog.where(system_user: system_user.id, alias: type).last

    @starts_at = last_report_log.present? ? last_report_log.ends_at : 1.year.ago
    @ends_at = DateTime.now

    ReportLog.create(system_user_id: system_user.id, alias: type, starts_at: @starts_at, ends_at: @ends_at)
  end
end
