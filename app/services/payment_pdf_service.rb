class PaymentPDFService
  TICKET_TEMPLATE_PATH = Rails.root.join('public', 'pdf_templates', 'ticket.pdf')
  CALIBRI_FONT_PATH = Rails.root.join('public', 'pdf_templates', 'Calibri.ttf')

  attr_reader :payment, :e_ticket

  def initialize(payment)
    @payment = payment
  end

  def to_pdf
    assemble

    e_ticket.to_pdf
  end

  def assemble
    @e_ticket = CombinePDF.new

    payment.tickets.each{ |ticket| @e_ticket >> text_layer(ticket) }
  end

  private

  def text_layer(ticket)
    prawn_doc = Prawn::Document.new

    prawn_doc.font CALIBRI_FONT_PATH
    prawn_doc.font_size 12

    prawn_doc.draw_text payment.number, at: [0, 663]
    prawn_doc.draw_text payment.tickets.map(&:price).reduce(:+), at: [273, 663]
    prawn_doc.draw_text "№#{ payment.number } от #{ payment.created_at.strftime('%Y.%m.%d %H:%M') }", at: [0, 628]
    prawn_doc.draw_text "#{ payment.user.last_name } #{ payment.user.middle_name } #{ payment.user.first_name }", at: [0, 593]
    prawn_doc.draw_text payment.user.email, at: [0, 559]
    prawn_doc.draw_text payment.user.phone, at: [273, 559]

    seat = "Сектор: #{ ticket.seat_item.sector_group.name }, Ряд: #{ ticket.seat_item.row }, Место: #{ ticket.seat_item.number }"
    prawn_doc.draw_text payment.event_item.name, at: [39, 397]
    prawn_doc.draw_text payment.event_item.date.strftime('%d %B %Y'), at: [39, 362]
    prawn_doc.draw_text payment.event_item.date.strftime('%H:%M'), at: [219, 362]
    prawn_doc.draw_text payment.event_item.hall.name, at: [39, 327]
    prawn_doc.draw_text seat, at: [39, 291]
    prawn_doc.image qrcode_image_url(ticket), at: [400, 415], :width => 125, :height => 125

    prawn_doc.font_size 8
    prawn_doc.fill_color('ffffff')

    prawn_doc.draw_text payment.event_item.name, at: [55, 55]
    prawn_doc.draw_text payment.event_item.date.strftime('%Y.%m.%d - %H:%M'), at: [55, 45]
    prawn_doc.draw_text payment.event_item.hall.name, at: [55, 35]
    prawn_doc.draw_text seat, at: [55, 25]
    prawn_doc.draw_text payment.tickets.map(&:price).reduce(:+), at: [55, 15]
    prawn_doc.draw_text payment.number, at: [55, 5]
    prawn_doc.draw_text "№#{ payment.number } от #{ payment.created_at.strftime('%Y.%m.%d %H:%M') }", at: [290, 55]
    prawn_doc.draw_text "#{ payment.user.last_name } #{ payment.user.middle_name } #{ payment.user.first_name }", at: [290, 45]
    prawn_doc.draw_text payment.user.phone, at: [290, 35]
    prawn_doc.draw_text payment.number, at: [290, 25]
    prawn_doc.image qrcode_image_url(ticket), at: [465, 60], :width => 55, :height => 55

    base_layer << CombinePDF.parse(prawn_doc.render).pages.first
  end

  def qrcode_image_url(ticket)
    # a bit of memoization
    previous_path = instance_variable_get("@qrcode_path_per_ticket_#{ ticket.id }")
    return previous_path if previous_path.present?

    secret_url = Rails.application.routes.url_helpers.register_ticket_url(ticket, reg_code: ticket.reg_code)

    qrcode_image = RQRCode::QRCode.new(secret_url).as_png(size: 200, module_px_size: 100, file: nil)
    qrcode_path = "tmp/qrcodes/#{ ticket.reg_code }.png"

    # in case this directory does not exist
    FileUtils.mkdir_p 'tmp/qrcodes'
    qrcode_image.save(qrcode_path)

    instance_variable_set("@qrcode_path_per_ticket_#{ ticket.id }", qrcode_path)
  end

  def base_layer
    CombinePDF.load(TICKET_TEMPLATE_PATH).pages.first
  end
end
