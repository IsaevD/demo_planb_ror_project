class MoneyTransferCreationService
  attr_reader :current_system_user, :params, :money_transfer, :error

  def initialize(current_system_user, params)
    @current_system_user = current_system_user
    @params = params

    @money_transfer = MoneyTransfer.new(params)
  end

  def create
    if new_money_into_system?
      return false unless money_transfer_saved?
      money_transfer.finish
    else
      return false unless enough_money?
      return false unless money_transfer_saved?
      money_transfer.start
    end
  end

  private

  def new_money_into_system?
    return false unless current_system_user.system_user_role.alias == 'kassir_administrator'

    params[:assignee_id].blank?
  end

  def enough_money?
    system_user_id = current_system_user.system_user_role.alias == 'kassir_administrator' ? nil : current_system_user.id

    stock = MoneyStock.where(system_user_id: system_user_id).first

    return true if stock.present? && stock.counter >= params[:counter].to_i

    I18n.t('custom.modules.money_stocks.not_enough_blanks')

    false
  end

  def money_transfer_saved?
    return true if money_transfer.save

    @error = I18n.t('custom.modules.money_transfers.is_not_valid')

    false
  end
end
