class BlankStockCheckService
  attr_reader :event_item_id

  def initialize(system_user_id, event_item_id)
    @system_user_id = system_user_id
    @event_item_id = event_item_id
  end

  def has_enough_blanks?(counter)
    system_user_id = is_kassir_administrator?(@system_user_id) ? nil : @system_user_id

    stock = BlankStock.where(event_item_id: event_item_id, system_user_id: system_user_id).first

    return false unless stock.present?

    stock.counter >= counter.to_i
  end

  def error
    I18n.t('custom.modules.blank_stocks.not_enough_blanks')
  end

  def is_kassir_administrator?(system_user_id)
    system_user(system_user_id).system_user_role.alias == 'kassir_administrator'
  end

  private

  def system_user(system_user_id)
    @system_user ||= SystemMainItemsD::SystemUser.find(system_user_id)
  end
end
