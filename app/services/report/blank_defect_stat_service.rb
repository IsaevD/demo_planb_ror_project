class Report::BlankDefectStatService
  attr_reader :system_user, :date_range

  def initialize(system_user, date_range)
    @system_user = system_user
    @date_range = date_range
  end

  [:normal, :special].each do |type|
    define_method("blank_defects_on_#{ type }_blanks_size") do |event_item_id = nil|
      blank_defects.select do |bt|
        result = bt.blank_type.try(:alias) == BlankType.const_get("#{ type }_alias".upcase)
        result = result && bt.event_item_id == event_item_id if event_item_id.present?
        result
      end.size
    end

    define_method("blank_defects_on_#{ type }_blanks_numbers") do |event_item_id = nil|
      defects = blank_defects.select do |bt|
        result = bt.blank_type.try(:alias) == BlankType.const_get("#{ type }_alias".upcase)
        result = result && bt.event_item_id == event_item_id if event_item_id.present?
        result
      end

      map_join defects
    end
  end

  def blank_defects
    @blank_defects ||= BlankDefect.with_blank_type.where(created_at: date_range, system_user_id: system_user.id)
  end

  private

  def map_join(resource)
    numbers = resource.map(&:number).join("\n")

    numbers.present? ? numbers : '--'
  end
end
