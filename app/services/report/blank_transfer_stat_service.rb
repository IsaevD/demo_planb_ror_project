class Report::BlankTransferStatService
  attr_reader :system_user, :date_range

  def initialize(system_user, date_range)
    @system_user = system_user
    @date_range = date_range
  end

  [:from, :to].each do |type|
    define_method("blank_transfers_#{ type }_me_size") do
      map_reduce send("#{ type }_me")
    end

    define_method("blank_transfers_#{ type }_me_on_normal_blanks_size") do
      map_reduce send("#{ type }_me").select{ |bt| bt.event_item_id == nil }
    end

    define_method("blank_transfers_#{ type }_me_on_special_blanks_size") do |event_item_id|
      map_reduce send("#{ type }_me").select{ |bt| bt.event_item_id == event_item_id }
    end
  end

  def to_me
    @to_me ||= BlankTransfer.delivered.where(created_at: date_range, assignee_id: system_user.id)
  end

  def from_me
    @from_me ||= BlankTransfer.except_not_delivered.where(created_at: date_range, assignor_id: system_user.id)
  end

  private

  def map_reduce(resource)
    resource.map(&:counter).reduce(0, :+)
  end
end
