class Report::PaymentStatService
  attr_reader :system_user, :date_range

  def initialize(system_user, date_range)
    @system_user = system_user
    @date_range = date_range
  end

  [:cash, :cashless].each do |payment_kind|
    define_method("payments_#{ payment_kind }_cost") do
      cost payments.select{ |p| p.payment_kind.try(:alias) == PaymentKind.const_get("#{ payment_kind }_alias".upcase) }
    end
  end

  [:normal, :special].each do |type|
    define_method("payments_on_#{ type }_blanks_size") do |event_item_id = nil|
      ps = payments.map do |payment|
        tickets = payment.tickets.select do |ticket|
          result = ticket.blank_type.try(:alias) == BlankType.const_get("#{ type }_alias".upcase)
          result = result && ticket.event_item_id == event_item_id if event_item_id.present?
          result
        end

        tickets.size
      end

      ps.reduce(0, :+)
    end

    define_method("payments_on_#{ type }_blanks_numbers") do |event_item_id = nil|
      ps = payments.map do |payment|
        tickets = payment.tickets.select do |ticket|
          result = ticket.blank_type.try(:alias) == BlankType.const_get("#{ type }_alias".upcase)
          result = result && ticket.event_item_id == event_item_id if event_item_id.present?
          result
        end

        tickets.map(&:number)
      end

      numbers = ps.flatten.join("\n")
      numbers.present? ? numbers : '--'
    end
  end

  def payments
    # @TODO Find out should we omit "reserved tickets payments"?
    @payments ||= Payment.with_tickets.with_payment_kind
      .where(created_at: date_range, system_user_id: system_user.id)
  end

  def payments_total_size
    payments.map{ |payment| payment.tickets.size }.reduce(0, :+)
  end

  def payments_total_cost
    cost(payments)
  end

  private

  def cost(payments)
    payments.map{ |payment| payment.tickets.map(&:price) }.flatten.reduce(0, :+)
  end
end
