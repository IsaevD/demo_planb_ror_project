class Report::BlankStockStatService
  attr_reader :system_user, :date_range

  def initialize(system_user, date_range)
    @system_user = system_user
    @date_range = date_range
  end

  [:normal, :special].each do |type|
    define_method("blank_stocks_on_#{ type }_blanks_size") do |event_item_id = nil|
      map_reduce blank_stocks.select{ |bs| bs.event_item_id == event_item_id }
    end
  end

  def blank_stocks_size
    map_reduce blank_stocks
  end

  def blank_stocks
    @blank_stocks ||= BlankStock.where(system_user_id: system_user.id)
  end

  private

  def map_reduce(resource)
    resource.map(&:counter).reduce(0, :+)
  end
end
