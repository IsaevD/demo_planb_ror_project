class Report::EventItemStatService
  attr_reader :event_item, :system_user, :date_range

  def initialize(system_user, date_range, event_item_id)
    @system_user = system_user
    @date_range = date_range
    @event_item = EventItem.includes(:hall).find(event_item_id) if event_item_id.present?
  end

  def event_item_sector_groups
    @sector_groups ||= SectorGroup.with_seat_items.where(alias: sector_groups_assoc[event_item.hall.alias.to_sym])
  end

  def event_item_paid_seat_items
    @payment_ids ||= event_item.payments.where(created_at: date_range).ids

    @paid_seat_items ||= event_item.tickets
      .includes(seat_item: [:sector_group, :seat_item_type])
      .where(payment_id: @payment_ids)
      .map(&:seat_item).compact
  end

  private

  def sector_groups_assoc
    # @TODO izhstal, cyrk, test_schema is not covered
    # @TODO ambigious match, ask Denis what do they mean
    {
      filarmonia: ['filarmonia_parter', 'filarmonia_balkon'],
      aksion: ['parter', 'amfiteatr', 'balkon', 'vip_1', 'vip_2'].map{ |s| s.prepend('aksion') },
      metallurg_without_dancing: [
        'parter', 'central_parter', 'amfiteatr', 'balkon', 'loga_1', 'loga_2'
      ].map{ |s| s.prepend('metallurg_without_dancing') },
      metallurg_with_dancing: [
        'dancing_room', 'central_parter', 'amfiteatr', 'balkon', 'loga_1', 'loga_2'
      ].map{ |s| s.prepend('metallurg_with_dancing') },
      test_schema_with_dancing: ['parter', 'dancing_room'].map{ |s| s.prepend('test_schema_with_dancing') }
    }
  end
end
