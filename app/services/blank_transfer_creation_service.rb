class BlankTransferCreationService
  attr_reader :current_system_user, :params, :blank_transfer, :error

  def initialize(current_system_user, params)
    @current_system_user = current_system_user
    @params = params

    @blank_transfer = BlankTransfer.new(params)
  end

  def create
    if new_blanks_into_system?
      return false unless blank_transfer_saved?
      blank_transfer.finish
    else
      return false unless enough_blanks?
      return false unless blank_transfer_saved?
      blank_transfer.start
    end
  end

  private

  def new_blanks_into_system?
    return false unless current_system_user.system_user_role.alias == 'kassir_administrator'

    params[:assignee_id].blank?
  end

  def enough_blanks?
    blank_stock_service = BlankStockCheckService.new(current_system_user.id, params[:event_item_id])

    return true if blank_stock_service.has_enough_blanks?(params[:counter])

    @error = blank_stock_service.error

    false
  end

  def blank_transfer_saved?
    return true if blank_transfer.save

    @error = I18n.t('custom.modules.blank_transfers.is_not_valid')

    false
  end
end
