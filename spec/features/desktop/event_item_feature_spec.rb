require 'spec_helper'

feature 'User index EventItems' do
  let(:hall) { create :hall, alias: 'aksion', name: 'Aksion' }
  let!(:events) { create_list :event_item, 2, hall: hall }

  scenario 'events lists sorted by date, asc order' do
    visit root_path

    expect(page).to have_selector("#events_list li:nth-child(1) .event_info .date", text: events[0].date.day)
    expect(page).to have_selector("#events_list li:nth-child(2) .event_info .date", text: events[1].date.day)
  end
end

feature 'User open passed event item in archive' do
  let(:hall) { create :hall, alias: 'aksion', name: 'Aksion' }
  let!(:expired_event) { create :event_item, hall: hall, date: 1.day.ago }

  scenario 'has been redirected to archive page' do
    visit event_path(expired_event.url)

    expect(current_url).to eq(archive_item_url('events', expired_event))
  end
end

feature 'User sees 404 page when trying to access non-existing event item' do
  scenario 'has been redirected to 404 page' do
    visit event_path('none')

    expect(current_path).to eq('/404')
  end
end
