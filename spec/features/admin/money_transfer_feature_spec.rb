require 'spec_helper'

feature 'Kassir administrator adding money into system' do
  let(:kassir_admin) { create :kassir_administrator }

  before(:each) { sign_in_as(kassir_admin) }

  scenario 'show success notification' do
    visit main_page_path

    create_money_transfer_with(counter: 10_000)

    expect(page).to have_selector('.flash--notice')
    expect(MoneyTransfer.count).to eq(1)
  end

  scenario 'with counter < 0 show error notification' do
    visit main_page_path

    create_money_transfer_with(counter: -10_000)

    expect(page).to have_selector('.flash--error')
    expect(MoneyTransfer.count).to eq(0)
  end
end

feature 'Kassir administrator adding money to kassir' do
  let(:kassir) { create :kassir }
  let(:kassir_admin) { create :kassir_administrator }
  let!(:kassir_admin_money_stock) { create :money_stock, system_user: kassir_admin }

  before(:each) { sign_in_as(kassir_admin) }

  scenario 'shows success notification' do
    visit new_money_transfer_path

    create_money_transfer_with(counter: 10_000, assignee: kassir)

    expect(page).to have_selector('.flash--notice')
    expect(MoneyTransfer.count).to eq(1)
  end

  scenario 'with counter < 0 show error notification' do
    visit new_money_transfer_path

    create_money_transfer_with(counter: -10_000, assignee: kassir)

    expect(page).to have_selector('.flash--error')
    expect(MoneyTransfer.count).to eq(0)
  end
end

feature 'Kassir adding money to kassir administrator' do
  let(:kassir) { create :kassir }
  let(:kassir_admin) { create :kassir_administrator }
  let!(:kassir_money_stock) { create :money_stock, system_user: kassir, counter: 20_000 }

  before(:each) { sign_in_as(kassir) }

  scenario 'shows success notification' do
    visit new_money_transfer_path

    create_money_transfer_with(counter: 10_000)

    expect(page).to have_selector('.flash--notice')
    expect(MoneyTransfer.count).to eq(1)
  end

  scenario 'with counter < 0 show error notification' do
    visit new_money_transfer_path

    create_money_transfer_with(counter: -10_000)

    expect(page).to have_selector('.flash--error')
    expect(MoneyTransfer.count).to eq(0)
  end

  scenario 'with counter > than money in stock show error notification' do
    visit new_money_transfer_path

    create_money_transfer_with(counter: 30_000)

    expect(page).to have_selector('.flash--error')
    expect(MoneyTransfer.count).to eq(0)
  end
end

feature 'kassir_administrator accepts money_transfer' do
  let(:kassir) { create :kassir }
  let(:kassir_admin) { create :kassir_administrator }

  let!(:kassir_admin_money_stock) { create :money_stock, system_user: nil, counter: 0 }
  let!(:kassir_money_transfer) do
    create :money_transfer, counter: 100, state: 'in_delivery',
      assignor: kassir, assignee: nil
  end

  before(:each) { sign_in_as(kassir_admin) }

  scenario 'shows success notification' do
    visit main_page_path

    finish_money_transfer(kassir_money_transfer)

    expect(page).to have_selector('.flash--notice')
    expect(kassir_admin_money_stock.reload.counter).to eq(100)
  end
end

feature 'kassir accepts money transfer' do
  let(:kassir) { create :kassir }
  let(:kassir_admin) { create :kassir_administrator }

  let!(:kassir_money_stock) { create :money_stock, system_user: kassir, counter: 0 }
  let!(:kassir_admin_money_transfer) do
    create :money_transfer, counter: 100, state: 'in_delivery',
      assignor: kassir_admin, assignee: kassir
  end

  before(:each) { sign_in_as(kassir) }

  scenario 'shows success notification' do
    page.driver.browser.set_cookie("kassir_sale_point_item_id_for_user_#{ kassir.id }=1")
    visit main_page_path

    finish_money_transfer(kassir_admin_money_transfer)

    expect(page).to have_selector('.flash--notice')
    expect(kassir_money_stock.reload.counter).to eq(100)
  end
end
