require 'spec_helper'

feature 'Administrator create EventItem' do
  let(:administrator) { create :administrator }
  let(:hall) { create :hall, alias: 'aksion', name: 'Aksion' }
  let!(:schema_item) { create :schema_item, hall: hall }

  let(:event_item_attrs) { attributes_for :event_item }

  before(:each) { sign_in_as(administrator) }

  scenario 'with valid attributes' do
    event_item_attrs[:hall_name] = hall.name

    create_event_item_with(event_item_attrs)

    expect(page).to have_content(event_item_attrs[:url])
  end

  scenario 'with invalid attributes' do
    event_item_attrs[:name] = ''
    event_item_attrs[:hall_name] = hall.name

    create_event_item_with(event_item_attrs)

    expect(page).to_not have_content(event_item_attrs[:url])
  end

  scenario 'with images' do
    event_item_attrs[:hall_name] = hall.name
    event_item_attrs[:image_list] = File.join(fixture_path, 'test_img.jpg')

    create_event_item_with(event_item_attrs)

    expect(page).to have_content(event_item_attrs[:url])
    expect(EventItem.last.event_images.size).to eq(1)
  end
end
