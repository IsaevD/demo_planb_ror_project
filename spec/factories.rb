FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{ n }@gmail.com" }
    first_name 'Ivan'
    last_name 'Ivanov'
    password 'password'
    password_confirmation 'password'
    confirmed true
  end

  factory :system_user, class: SystemMainItemsD::SystemUser do
    sequence(:email) { |n| "system_user#{ n }@gmail.com" }
    sequence(:login) { |n| "login#{ n }" }
    password 'password'
    password_confirmation 'password'

    association :system_user_role, factory: :system_user_role

    factory :kassir do
      association :system_user_role, factory: :kassir_role
    end

    factory :kassir_administrator do
      association :system_user_role, factory: :kassir_administrator_role
    end

    factory :administrator do
      association :system_user_role, factory: :administrator_role
    end
  end

  factory :system_user_role, class: SystemMainItemsD::SystemUserRole do
    self.alias 'default'
    name { self.alias.capitalize }

    initialize_with do 
      SystemMainItemsD::SystemUserRole.where(alias: self.alias).first_or_create do |role|
        role.name = name
      end 
    end

    factory :kassir_role do
      self.alias 'kassir'
    end

    factory :kassir_administrator_role do
      self.alias 'kassir_administrator'
    end

    factory :administrator_role do
      self.alias 'administrator'
    end
  end

  factory :ticket do
    number 'ASD123DAK'
    price 1500

    seat_item
    blank_type
  end

  factory :ticket_state do
    self.alias 'available'
    name 'available'

    factory :hold_ticket_state do
      self.alias 'hold'
      name 'hold'
    end
  end

  # Try to use Faker gem
  # I is a good practice to test wih random data, rather static
  factory :payment do
    number { Payment.generate_number }
    phone '8 800 123 123 23'
    # fio 'Ivanov Ivan'
    fio Faker::Name.name
    user
    payment_kind
    payment_type
    delivery_method

    factory :cash_payment do
      association :payment_kind, factory: :payment_kind
    end

    factory :cashless_payment do
      association :payment_kind, factory: :cashless_payment_kind
    end
  end

  factory :event_item do
    sequence(:url) { |n| "#{ n }_unique_url" }
    sequence(:date) { |n| n.year.from_now }

    name 'Tinashe LIVE'
    description 'Tinashe LIVE'
    content 'Tinashe LIVE'
    published_at { 1.week.ago }

    system_state_id 1
  end

  factory :hall do
    name 'Filarmonia'
    self.alias 'filarmonia'

    initialize_with { Hall.where(alias: self.alias).first_or_create{ |hall| hall.name = name } }
  end

  factory :sector_group do
    name 'Filarmonia parter'
    self.alias 'filarmonia_parter'
  end

  factory :seat_item do
    sequence(:row) { |n| n }
    sequence(:number) { |n| n }
  end

  factory :payment_kind do
    name 'Наличный'
    self.alias 'cash'

    factory :cashless_payment_kind do
      name 'Безналичный'
      self.alias 'cashless'
    end
  end

  factory :payment_type do
    name 'Бронирование'
    self.alias 'reservation'
  end

  factory :delivery_method do
    name 'Самостоятельно через точку продаж'
    self.alias 'himself'
  end

  factory :blank_type do
    name 'Normal type'
    self.alias 'normal'

    factory :special_blank_type do
      name 'Special type'
      self.alias 'special'
    end
  end

  factory :blank_stock do
    counter 10_000

    event_item
    blank_type
  end

  factory :blank_transfer do
    counter { 10_000 }

    association :assignee, factory: :system_user
    association :assignor, factory: :system_user

    event_item
    blank_type
  end

  factory :money_stock do
    counter 10_000
  end

  factory :money_transfer do
   counter 10_000

   association :assignee, factory: :system_user
   association :assignor, factory: :system_user
  end

  factory :blank_defect do
    number 'ASDAS1239'

    blank_type
    system_user
  end

  factory :report_log do
    self.alias 'cashbox_release'

    starts_at { 1.year.ago }
    ends_at { DateTime.now }
  end

  factory :schema_item do
    name 'default'
    self.alias 'default'
  end
end
