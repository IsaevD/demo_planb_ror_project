require 'rails_helper'

RSpec.describe Report::BlankStockStatService do
  let(:kassir) { create :kassir }

  let(:event_item) { create :event_item }
  let(:normal_blank_type) { create :blank_type }
  let(:special_blank_type) { create :special_blank_type }

  let!(:normal_blank_stock) do
    create :blank_stock, counter: 333, event_item: nil, system_user_id: kassir.id,
      blank_type: normal_blank_type
  end

  let!(:special_blank_stock) do
    create :blank_stock, counter: 666, event_item: event_item, system_user_id: kassir.id,
      blank_type: special_blank_type
  end

  let(:service) { Report::BlankStockStatService.new(kassir, (1.year.ago)..DateTime.now) }

  describe '#blank_stocks_on_normal_blanks_size' do
    it 'returns 333 ' do
      expect(service.blank_stocks_on_normal_blanks_size).to eq(333)
    end
  end

  describe '#blank_stocks_on_special_blanks_size' do
    it 'returns 666 ' do
      expect(service.blank_stocks_on_special_blanks_size(event_item.id)).to eq(666)
    end
  end

  describe '#blank_stocks_size' do
    it 'returns 999' do
      expect(service.blank_stocks_size).to eq(999)
    end
  end

  describe '#blank_stocks' do
    it 'returns 2 ' do
      expect(service.blank_stocks.size).to eq(2)
    end
  end
end
