require 'rails_helper'

RSpec.describe Report::BlankDefectStatService do
  let(:kassir) { create :kassir }

  let(:event_item) { create :event_item }
  let(:normal_blank_type) { create :blank_type }
  let(:special_blank_type) { create :special_blank_type }

  let!(:normal_blank_defect) do
    create_list :blank_defect, 2, number: 'GH12', system_user_id: kassir.id,
      blank_type: normal_blank_type
  end

  let!(:special_blank_defect) do
    create_list :blank_defect, 2, number: 'OP55', system_user_id: kassir.id,
      blank_type: special_blank_type, event_item: event_item
  end

  let(:service) do
    Report::BlankDefectStatService.new(kassir, (1.year.ago)..DateTime.now)
  end

  describe '#blank_defects_on_normal_blanks_size' do
    it 'returns 1' do
      expect(service.blank_defects_on_normal_blanks_size).to eq(2)
    end
  end

  describe '#blank_defects_on_special_blanks_size' do
    it 'returns 1' do
      expect(service.blank_defects_on_special_blanks_size(event_item.id)).to eq(2)
    end
  end

  describe '#blank_defects_on_normal_blanks_numbers' do
    it 'returns appropriate number' do
      expect(service.blank_defects_on_normal_blanks_numbers).to eq("GH12\nGH12")
    end
  end

  describe '#blank_defects_on_special_blanks_numbers' do
    it 'returns appropriate number' do
      expect(service.blank_defects_on_special_blanks_numbers(event_item.id)).to eq("OP55\nOP55")
    end
  end

  describe '#blank_defects' do
    it 'returns 2' do
      expect(service.blank_defects.size).to eq(4)
    end
  end
end
