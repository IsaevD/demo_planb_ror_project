require 'rails_helper'

RSpec.describe Report::EventItemStatService do
  let(:kassir) { create :kassir }
  let(:hall) { create :hall, alias: 'filarmonia' }
  let(:event_item) { create :event_item, hall: hall }

  let(:sector_groups) do
    create_list :sector_group, 3, alias: 'filarmonia_parter'
  end

  let(:payment) { create :payment, event_item: event_item }
  let!(:tickets) { create_list :ticket, 2, event_item: event_item, payment: payment }

  let(:service) do 
    Report::EventItemStatService.new(kassir, (1.year.ago)..DateTime.now, event_item)
  end

  describe '#event_item_sector_groups' do
    it 'returns 3 sector_groups' do
      expect(service.event_item_sector_groups).to eq(sector_groups)
    end
  end

  describe '#event_item_paid_seat_items' do
    it 'returns 3 seat_items' do
      expect(service.event_item_paid_seat_items.size).to eq(tickets.size)
    end
  end
end
