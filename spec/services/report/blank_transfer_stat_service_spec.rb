require 'rails_helper'

RSpec.describe Report::BlankTransferStatService do
  let(:kassir) { create :kassir }
  let(:kassir_administrator) { create :kassir_administrator }

  let(:event_item) { create :event_item }

  let(:service) { Report::BlankTransferStatService.new(kassir, (1.year.ago)..DateTime.now) }

  before(:each) do
    %w(delivered in_delivery).each do |state|
      create :blank_transfer, counter: 10, state: state, assignee: kassir_administrator,
        assignor: kassir, event_item: nil
      create :blank_transfer, counter: 10, state: state, assignee: kassir_administrator,
        assignor: kassir, event_item: event_item
      create :blank_transfer, counter: 10, state: state, assignee: kassir,
        assignor: kassir_administrator, event_item: nil
      create :blank_transfer, counter: 10, state: state, assignee: kassir,
        assignor: kassir_administrator, event_item: event_item
    end
  end

  describe '#blank_transfers_to_me_on_special_blanks_size' do
    it 'returns 10' do
      expect(service.blank_transfers_to_me_on_special_blanks_size(event_item.id)).to eq(10)
    end
  end

  describe '#blank_transfers_from_me_on_special_blanks_size' do
    it 'returns 10' do
      expect(service.blank_transfers_from_me_on_special_blanks_size(event_item.id)).to eq(20)
    end
  end

  describe '#blank_transfers_to_me_on_normal_blanks_size' do
    it 'returns 10' do
      expect(service.blank_transfers_to_me_on_normal_blanks_size).to eq(10)
    end
  end

  describe '#blank_transfers_from_me_on_normal_blanks_size' do
    it 'returns 10' do
      expect(service.blank_transfers_from_me_on_normal_blanks_size).to eq(20)
    end
  end

  describe '#blank_transfers_to_me_size' do
    it 'returns 20' do
      expect(service.blank_transfers_to_me_size).to eq(20)
    end
  end

  describe '#blank_transfers_from_me_size' do
    it 'returns 40' do
      expect(service.blank_transfers_from_me_size).to eq(40)
    end
  end

  describe '#to_me' do
    it 'returns 2' do
      expect(service.to_me.size).to eq(2)
    end
  end

  describe '#from_me' do
    it 'returns 4' do
      expect(service.from_me.size).to eq(4)
    end
  end
end
