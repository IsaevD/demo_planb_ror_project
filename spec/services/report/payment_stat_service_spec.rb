require 'rails_helper'

RSpec.describe Report::PaymentStatService do
  let(:kassir) { create :kassir }
  let(:event_item) { create :event_item }

  let(:normal_blank_type) { create :blank_type }
  let(:special_blank_type) { create :special_blank_type }

  let(:cash_payment) { create :cash_payment, system_user_id: kassir.id }
  let(:cashless_payment) { create :cashless_payment, system_user_id: kassir.id }

  let!(:tickets_on_normal_blanks) do
    create_list :ticket, 2, number: 'GH12', price: 100, payment: cash_payment,
      blank_type: normal_blank_type, event_item: event_item
  end

  let!(:tickets_on_special_blanks) do
    create_list :ticket, 2, number: 'RS65', price: 200, payment: cashless_payment,
      blank_type: special_blank_type, event_item: event_item
  end

  let(:service) { Report::PaymentStatService.new(kassir, (1.year.ago)..DateTime.now) }

  describe '#payments' do
    it 'returns 2' do
      expect(service.payments.size).to eq(2)
    end
  end

  describe '#payments_cash_cost' do
    it 'returns 200' do
      expect(service.payments_cash_cost).to eq(200)
    end
  end

  describe '#payments_cashless_cost' do
    it 'returns 400' do
      expect(service.payments_cashless_cost).to eq(400)
    end
  end

  describe '#payments_on_normal_blanks_size' do
    it 'returns 2' do
      expect(service.payments_on_normal_blanks_size).to eq(2)
    end
  end

  describe '#payments_on_special_blanks_size' do
    it 'returns 2' do
      expect(service.payments_on_special_blanks_size(event_item.id)).to eq(2)
    end
  end

  describe '#payments_on_normal_blanks_numbers' do
    it 'returns appropriate numbers' do
      expect(service.payments_on_normal_blanks_numbers).to eq("GH12\nGH12")
    end
  end

  describe '#payments_on_special_blanks_numbers' do
    it 'returns appropriate numbers' do
      expect(service.payments_on_special_blanks_numbers(event_item.id)).to eq("RS65\nRS65")
    end
  end

  describe '#payments_total_cost' do
    it 'returns 600' do
      expect(service.payments_total_cost).to eq(600)
    end
  end

  describe '#payments_total_size' do
    it 'returns 4 tickets size' do
      expect(service.payments_total_size).to eq(4)
    end
  end
end
