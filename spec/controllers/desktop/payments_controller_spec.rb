require 'rails_helper'

RSpec.describe Desktop::PaymentsController, type: :controller do
  let(:user) { create :user }
  let!(:payment) { create :payment, user: user }
  let!(:ticket) { create :ticket, payment: payment }
  let!(:ticket_state) { create :ticket_state }
  let(:courier_delivery_method) { create :delivery_method, alias: 'courier' }

  it { should route(:delete, '/payments/1').to(action: :destroy, id: 1) }

  before(:each) do
    controller.class.skip_before_filter :init_desktop_variables
    sign_in(User, user, :user_remember_token)
  end

  describe 'DELETE destroy' do
    context 'when payment is cancelable' do
     # before(:each) { payment && ticket }   # look to string 5-6

      it 'destroys payment, releases tickets, create TicketRevert instance' do
        ticket_revert_count = TicketRevert.count
        ticket_ids = Ticket.where(payment_id: payment.id).ids

        delete :destroy, id: payment.id

        expect(Ticket.find(ticket_ids).map(&:payment_id)).to eq([nil])
        expect(TicketRevert.count).to eq(ticket_revert_count + 1)
        expect(Payment.find_by_id(payment.id)).to be_nil
      end
    end

    context 'when payment is not cancelable' do
      let(:payment) { create :payment, user: user, delivery_method: courier_delivery_method }
     # before(:each) { payment && ticket }

      it 'do not destroys payment, releases tickets, create TicketRevert instance' do
        ticket_revert_count = TicketRevert.count
        ticket_ids = Ticket.where(payment_id: payment.id).ids

        delete :destroy, id: payment.id

        expect(Ticket.find(ticket_ids).map(&:payment_id)).to eq([payment.id])
        expect(TicketRevert.count).to eq(ticket_revert_count)
        expect(Payment.find_by_id(payment.id)).to_not be_nil
      end
    end
  end
end
