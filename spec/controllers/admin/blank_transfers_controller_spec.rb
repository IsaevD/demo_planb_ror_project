RSpec.describe Admin::BlankTransfersController, type: :controller do
  let(:kassir) { create :kassir }
  let(:kassir_administrator) { create :kassir_administrator }

  let(:blank_type) { create :blank_type }
  let(:blank_transfer_attrs) { attributes_for :blank_transfer }

  let!(:global_blank_stock) { create :blank_stock, counter: 10000, blank_type: blank_type, event_item: nil }

  let(:blank_transfer_from_admin_to_kassir) do
    create :blank_transfer, counter: 100, assignee: kassir, assignor: kassir_administrator
  end

  it { should route(:post, '/admin/blank_transfers').to(action: :create) }
  it { should route(:get, '/admin/blank_transfers/new').to(action: :new) }
  it { should route(:put, '/admin/blank_transfers/1/cancel').to(action: :cancel, id: 1) }
  it { should route(:put, '/admin/blank_transfers/1/finish').to(action: :finish, id: 1) }

  before(:each) do
    controller.class.skip_before_filter :init_app_variables
    controller.class.skip_before_filter :check_access_rights
    sign_in(SystemMainItemsD::SystemUser, kassir_administrator, :remember_token)
  end

  describe 'GET #new' do
    it 'renders new page' do
      get :new

      expect(response).to render_template(:new)
    end
  end

  describe 'POST #create' do
    context 'when move blanks from admin to kassir' do
      it 'redirects to main page' do
        blank_transfer_attrs[:assignee_id] = kassir.id
        blank_transfer_attrs[:blank_type_id] = blank_type.id

        post :create, blank_transfer: blank_transfer_attrs

        expect(response).to redirect_to(main_page_path)
      end
    end

    context 'when bring new blanks into system' do
      it 'redirects to main page' do
        blank_transfer_attrs[:assignee_id] = kassir_administrator.id
        blank_transfer_attrs[:blank_type_id] = blank_type.id

        post :create, blank_transfer: blank_transfer_attrs

        expect(response).to redirect_to(main_page_path)
      end
    end

    it 'renders new page' do
      post :create, blank_transfer: { assignee_id: kassir.id }

      expect(response).to render_template('new')
    end
  end

  describe 'PUT #cancel' do
    it 'redirects to main page' do
      put :cancel, id: blank_transfer_from_admin_to_kassir.id

      expect(response).to redirect_to(main_page_path)
    end
  end

  describe 'PUT #finish' do
    it 'redirects to main page' do
      put :finish, id: blank_transfer_from_admin_to_kassir.id

      expect(response).to redirect_to(main_page_path)
    end
  end
end
