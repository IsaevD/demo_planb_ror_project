RSpec.describe Admin::TicketsController, type: :controller do
  let(:kassir_administrator) { create :kassir_administrator }
  let(:tickets) { create_list :ticket, 5, seat_item: nil, blank_type: nil }
  let!(:available_ticket_state) { create :ticket_state }
  let(:hold_ticket_state) { create :hold_ticket_state }

  it { should route(:put, '/admin/tickets/batch_update').to(action: :batch_update) }

  before(:each) do
    controller.class.skip_before_filter :init_app_variables
    controller.class.skip_before_filter :check_access_rights
    sign_in(SystemMainItemsD::SystemUser, kassir_administrator, :remember_token)
  end

  describe 'PUT #batch_update' do
    it 'to set hold state' do
      put :batch_update, ticket_ids: tickets.map(&:id),
        ticket_state: hold_ticket_state, hold_state_expires_at: 2.days.from_now

      expect(response.status).to eq(200)
    end

    it 'to set hold state' do
      put :batch_update, ticket_ids: tickets.map(&:id),
        ticket_state: available_ticket_state, hold_state_expires_at: nil

      expect(response.status).to eq(200)
    end
  end
end
