RSpec.describe Admin::MoneyTransfersController, type: :controller do
  let(:kassir) { create :kassir }
  let(:kassir_administrator) { create :kassir_administrator }

  let(:money_type) { create :money_type }
  let(:money_transfer_attrs) { attributes_for :money_transfer }

  let!(:global_money_stock) { create :money_stock, counter: 10000 }

  let(:money_transfer_from_admin_to_kassir) do
    create :money_transfer, counter: 100, assignee: kassir, assignor: kassir_administrator
  end

  it { should route(:post, '/admin/money_transfers').to(action: :create) }
  it { should route(:get, '/admin/money_transfers/new').to(action: :new) }
  it { should route(:put, '/admin/money_transfers/1/cancel').to(action: :cancel, id: 1) }
  it { should route(:put, '/admin/money_transfers/1/finish').to(action: :finish, id: 1) }

  before(:each) do
    controller.class.skip_before_filter :init_app_variables
    controller.class.skip_before_filter :check_access_rights
    sign_in(SystemMainItemsD::SystemUser, kassir_administrator, :remember_token)
  end

  describe 'GET #new' do
    it 'renders new page' do
      get :new

      expect(response).to render_template(:new)
    end
  end

  describe 'POST #create' do
    context 'when move money from admin to kassir' do
      it 'redirects to main page' do
        money_transfer_attrs[:assignee_id] = kassir.id

        post :create, money_transfer: money_transfer_attrs

        expect(response).to redirect_to(main_page_path)
      end
    end

    context 'when bring new money into system' do
      it 'redirects to main page' do
        money_transfer_attrs[:assignee_id] = kassir_administrator.id

        post :create, money_transfer: money_transfer_attrs

        expect(response).to redirect_to(main_page_path)
      end
    end

    it 'renders new page' do
      post :create, money_transfer: { assignee_id: kassir.id, counter: -100 }

      expect(response).to render_template('new')
    end
  end

  describe 'PUT #cancel' do
    it 'redirects to main page' do
      put :cancel, id: money_transfer_from_admin_to_kassir.id

      expect(response).to redirect_to(main_page_path)
    end
  end

  describe 'PUT #finish' do
    it 'redirects to main page' do
      put :finish, id: money_transfer_from_admin_to_kassir.id

      expect(response).to redirect_to(main_page_path)
    end
  end
end
