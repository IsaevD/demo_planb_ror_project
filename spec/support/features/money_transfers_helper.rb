module Features
  module MoneyTransfersHelpers
    def create_money_transfer_with(attrs)
      fill_in 'money_transfer_counter', with: attrs[:counter]

      select attrs[:kassir].name, :from => 'asignee_id' if attrs[:kassir].present?

      find('#new_money_transfer .btn').click
    end

    def finish_money_transfer(money_transfer)
      find("a[href^='#{ finish_money_transfer_path(money_transfer) }']").click
    end
  end
end
