module Features
  module EventItemsHelpers
    def create_event_item_with(attrs)
      visit new_event_item_path

      fill_in 'event_item_url', with: attrs[:url]
      fill_in 'event_item_name', with: attrs[:name]
      fill_in 'event_item_description', with: attrs[:description]
      fill_in 'event_item_content', with: attrs[:content]

      fill_in 'event_item_date', with: attrs[:date]
      fill_in 'event_item_published_at', with: attrs[:published_at]

      select attrs[:hall_name], :from => 'event_item_hall_id'
      attach_file 'event_item_image_list', attrs[:image_list] if attrs[:image_list].present?

      click_button 'Сохранить'
    end
  end
end
