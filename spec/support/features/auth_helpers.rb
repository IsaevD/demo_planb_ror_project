module Features
  module AuthHelpers
    def sign_in_as(user)
      visit system_users_authorize_d.sign_in_path

      fill_in 'system_user_auth_login', with: user.login
      fill_in 'system_user_auth_password', with: 'password'

      click_button 'Войти'
    end
  end
end
