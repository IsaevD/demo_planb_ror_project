require 'rails_helper'

RSpec.describe BlankStock, type: :model do
  let(:blank_stock) { create :blank_stock, counter: 200 }

  describe 'Associations' do
    it { should belong_to :blank_type }
    it { should belong_to :event_item }
  end

  describe 'Validations' do
    it { should validate_presence_of :blank_type }

    it { should validate_presence_of :counter }

    it { should validate_numericality_of(:counter).is_greater_than_or_equal_to(0) }
  end

  describe '#withdraw' do
    it 'decreases counter by certain value' do
      blank_stock.withdraw(100)

      expect(blank_stock.reload.counter).to eq(100)
    end

    it 'cannot decrease counter to less than zero' do
      blank_stock.withdraw(300)

      expect(blank_stock.reload.counter).to eq(200)
    end
  end

  describe '#deposit' do
    it 'increases counter by certain value' do
      blank_stock.deposit(100)

      expect(blank_stock.reload.counter).to eq(300)
    end
  end
end
