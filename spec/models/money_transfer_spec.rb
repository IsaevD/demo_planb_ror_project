require 'rails_helper'

RSpec.describe MoneyTransfer, type: :model do
  let(:kassir) { create :kassir }
  let(:kassir_admin) { create :kassir_administrator }

  let!(:global_money_stock) { create :money_stock, counter: 100 }
  let!(:kassir_money_stock) { create :money_stock, counter: 100, system_user_id: kassir.id }

  let(:money_transfer_from_admin_to_kassir) do
    create :money_transfer, counter: 100, assignee: kassir, assignor: kassir_admin
  end

  let(:money_transfer_from_kassir_to_admin) do
    create :money_transfer, counter: 100, assignee: nil, assignor: kassir
  end

  describe 'Associations' do
    it { should belong_to :assignor }
    it { should belong_to :assignee }
  end

  describe 'Validations' do
    it { should validate_presence_of :assignor }

    it { should validate_presence_of :counter }
    it { should validate_presence_of :state }

    it { should validate_numericality_of(:counter).is_greater_than_or_equal_to(0) }
  end

  describe '#start' do
    context 'when it is assigned from kassir to kassir_admin' do
      it 'decreases global money stock by 100' do
        money_transfer_from_kassir_to_admin.start

        expect(kassir_money_stock.reload.counter).to eq(0)
      end
    end

    context 'when is it assigned from admin to kassir' do
      it 'decreases kassir money stock by 100' do
        money_transfer_from_admin_to_kassir.start

        expect(global_money_stock.reload.counter).to eq(0)
      end
    end
  end

  describe '#finish' do
    context 'when it is assigned to kassir_admin' do
      it 'increases event global money stock by 100' do
        money_transfer_from_kassir_to_admin.start
        money_transfer_from_kassir_to_admin.finish

        expect(kassir_money_stock.reload.counter).to eq(0)
        expect(global_money_stock.reload.counter).to eq(200)
      end
    end

    context 'when is it assigned to kassir' do
      it 'increases kassir money stock by 100' do
        money_transfer_from_admin_to_kassir.start
        money_transfer_from_admin_to_kassir.finish

        expect(global_money_stock.reload.counter).to eq(0)
        expect(kassir_money_stock.reload.counter).to eq(200)
      end
    end
  end

  describe '#cancel' do
    context 'when it is assigned to kassir_admin' do
      it 'increases kassir global money stock by 100' do
        money_transfer_from_kassir_to_admin.start
        money_transfer_from_kassir_to_admin.cancel

        expect(kassir_money_stock.reload.counter).to eq(100)
        expect(global_money_stock.reload.counter).to eq(100)
      end
    end

    context 'when is it assigned to kassir' do
      it 'increases kassir money stock by 100' do
        money_transfer_from_admin_to_kassir.start
        money_transfer_from_admin_to_kassir.cancel

        expect(kassir_money_stock.reload.counter).to eq(100)
        expect(global_money_stock.reload.counter).to eq(100)
      end
    end
  end
end
