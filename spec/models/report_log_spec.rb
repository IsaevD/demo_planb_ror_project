require 'rails_helper'

RSpec.describe ReportLog, type: :model do
  describe 'Associations' do
    it { should belong_to :system_user }
  end

  describe 'Validations' do
    it { should validate_presence_of(:system_user) }

    it { should validate_presence_of(:starts_at) }
    it { should validate_presence_of(:ends_at) }

    it { should validate_presence_of(:alias) }
    it { should validate_inclusion_of(:alias).in_array(ReportLog::ALIASES) }
  end
end
