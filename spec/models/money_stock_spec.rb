require 'rails_helper'

RSpec.describe MoneyStock, type: :model do
  let(:money_stock) { create :money_stock, counter: 200 }

  describe 'Associations' do
    it { should belong_to :system_user }
  end

  describe 'Validations' do
    it { should validate_presence_of :counter }

    it { should validate_numericality_of(:counter).is_greater_than_or_equal_to(0) }
  end

  describe '#withdraw' do
    it 'decreases counter by certain value' do
      money_stock.withdraw(100)

      expect(money_stock.reload.counter).to eq(100)
    end

    it 'cannot decrease counter to less than zero' do
      money_stock.withdraw(300)

      expect(money_stock.reload.counter).to eq(200)
    end
  end

  describe '#deposit' do
    it 'increases counter by certain value' do
      money_stock.deposit(100)

      expect(money_stock.reload.counter).to eq(300)
    end
  end
end
