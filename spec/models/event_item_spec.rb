require 'rails_helper'

RSpec.describe EventItem, type: :model do
  describe 'Associations' do
    it { should belong_to :hall }

    it { should have_many :banners }
    it { should have_many :blank_stocks }
    it { should have_many :blank_transfers }
    it { should have_many :event_images }
    it { should have_many :event_tags }
    it { should have_many :payments }
    it { should have_many :tickets }
    it { should have_many :ticket_discounts }
  end

  describe 'Validations' do
    it { validate_presence_of :name }
    it { validate_length_of(:name).is_at_least(4).is_at_most(255) }

    it { validate_presence_of :description }
    it { validate_presence_of :date }
    it { validate_presence_of :content }
    it { validate_presence_of :published_at }

    it { validate_presence_of :url }
    it { validate_length_of(:url).is_at_least(4).is_at_most(255) }
    it { validate_uniqueness_of(:url) }
  end
end
