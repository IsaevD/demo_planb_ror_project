require 'rails_helper'

RSpec.describe BlankTransfer, type: :model do
  let(:event_item) { create :event_item }
  let(:blank_type) { create :blank_type }

  let(:kassir) { create :kassir }
  let(:kassir_admin) { create :kassir_administrator }

  let!(:global_blank_stock) do
    create :blank_stock, counter: 100, event_item: event_item, blank_type: blank_type
  end

  let!(:kassir_blank_stock) do
    create :blank_stock, counter: 100, event_item: event_item, system_user_id: kassir.id,
    blank_type: blank_type
  end

  let(:blank_transfer_from_admin_to_kassir) do
    create :blank_transfer, counter: 100, assignee: kassir, assignor: kassir_admin,
      event_item: event_item, blank_type: blank_type
  end

  let(:blank_transfer_from_kassir_to_admin) do
    create :blank_transfer, counter: 100, assignee: nil, assignor: kassir,
      event_item: event_item, blank_type: blank_type
  end

  describe 'Associations' do
    it { should belong_to :assignor }
    it { should belong_to :assignee }
    it { should belong_to :blank_type }
    it { should belong_to :event_item }
  end

  describe 'Validations' do
    it { should validate_presence_of :assignor }
    it { should validate_presence_of :blank_type }

    it { should validate_presence_of :counter }
    it { should validate_presence_of :state }

    it { should validate_numericality_of(:counter).is_greater_than_or_equal_to(0) }
  end

  describe '#start' do
    context 'when it is assigned from kassir to kassir_admin' do
      it 'decreases event global blank stock by 100' do
        blank_transfer_from_kassir_to_admin.start

        expect(kassir_blank_stock.reload.counter).to eq(0)
      end
    end

    context 'when is it assigned from admin to kassir' do
      it 'decreases kassir blank stock by 100' do
        blank_transfer_from_admin_to_kassir.start

        expect(global_blank_stock.reload.counter).to eq(0)
      end
    end
  end

  describe '#finish' do
    context 'when it is assigned to kassir_admin' do
      it 'increases event global blank stock by 100' do
        blank_transfer_from_kassir_to_admin.start
        blank_transfer_from_kassir_to_admin.finish

        expect(kassir_blank_stock.reload.counter).to eq(0)
        expect(global_blank_stock.reload.counter).to eq(200)
      end
    end

    context 'when is it assigned to kassir' do
      it 'increases kassir blank stock by 100' do
        blank_transfer_from_admin_to_kassir.start
        blank_transfer_from_admin_to_kassir.finish

        expect(global_blank_stock.reload.counter).to eq(0)
        expect(kassir_blank_stock.reload.counter).to eq(200)
      end
    end
  end

  describe '#cancel' do
    context 'when it is assigned to kassir_admin' do
      it 'increases kassir global blank stock by 100' do
        blank_transfer_from_kassir_to_admin.start
        blank_transfer_from_kassir_to_admin.cancel

        expect(kassir_blank_stock.reload.counter).to eq(100)
        expect(global_blank_stock.reload.counter).to eq(100)
      end
    end

    context 'when is it assigned to kassir' do
      it 'increases kassir blank stock by 100' do
        blank_transfer_from_admin_to_kassir.start
        blank_transfer_from_admin_to_kassir.cancel

        expect(kassir_blank_stock.reload.counter).to eq(100)
        expect(global_blank_stock.reload.counter).to eq(100)
      end
    end
  end
end
