# -*- encoding : utf-8 -*-
Rubykassa.configure do |config|
  config.login = Rails.application.secrets.robokassa['login']
  config.first_password = Rails.application.secrets.robokassa['first_password']
  config.second_password = Rails.application.secrets.robokassa['second_password']
  config.mode = :test # or :production
  config.http_method = :get # or :post
  config.xml_http_method = :get # or :post


  # Result callback is called in RobokassaController#paid action if valid signature
  # was generated. It should always return "OK#{ invoice_id }" string, so implement
  # your custom logic above `render text: notification.success` line

  config.result_callback = -> (notification) do
    render text: notification.success 
  end

  # Define success or failure callbacks here like:

  config.success_callback = -> (notification) do 
    transaction = PaymentTransaction.where(id: params['InvId']).first
    transaction.update(status: PaymentTransaction::SUCCESS_STATUS)

    tickets = Ticket.where(id: cookies[:planb_order].split(','))
    event_item_id = tickets.first.event_item_id

    payment = Payment.create_payment(current_site_user.id, nil, event_item_id, PaymentType.purchase_state, nil, nil, nil)

    tickets.each do |ticket|
      ticket.update(ticket_state: TicketState.bought_state, payment_id: payment.id)
      ticket.generate_reg_code
    end

    cookies[:planb_order] = nil

    redirect_to user_history_path, notice: 'Оплата произведена успешно'
  end

  config.fail_callback = -> (notification) do
    transaction = PaymentTransaction.where(id: params['InvId']).first
    transaction.update(status: PaymentTransaction::FAILURE_STATUS)

    redirect_to internet_payment_path, alert: 'Произошла ошибка, попробуйте еще раз'
  end
end
