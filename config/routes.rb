Rails.application.routes.draw do

  namespace :mobile do
    match "/", to: "index#index", via: "get"
    resources :news, only: [:index, :show]
    resources :promos, only: [:index, :show]
    resources :events, only: [:index]
    match "/events/:url", to: "events#show", via: "get"
    get '/events/:url', to: 'events#show', as: :event
    resources :sale_points, only: [:index, :show]
    match "/search", to: "search#index", via: "get"

    match '/cart', to: "index#cart", via: "get"
    match '/order', to: "index#order", via: "get"

    match '/archive', to: "index#archive", via: "get"
    get '/archive/(:category)/(:id)/', to: 'index#archive_item', as: :archive_item

    match "/profile",  to: "users#profile", via: "get"
    match "/save_profile",  to: "users#save_profile", via: "post"
    match "/change_password", to: "users#change_password", via: "post"
    # Регистрация пользователя
    match "/register", to: "users#register", via: "get"
    match "/register", to: "users#register", via: "post"
    match "/register_complete", to: "users#register_complete", via: "get"
    match "/confirm", to: "users#confirm", via: "get"
    match "/confirm_complete", to: "users#confirm_complete", via: "get"
    # Восстановление пароля
    match "/restore", to: "users#restore", via: "get"
    match "/restore", to: "users#restore", via: "post"
    match "/restore_complete", to: "users#restore_complete", via: "get"
    match "/set_new_password", to: "users#set_new_password", via: "get"
    match "/set_new_password", to: "users#set_new_password", via: "post"
    match "/set_new_password_complete", to: "users#set_new_password_complete", via: "get"
  end


  scope module: "desktop" do
    root "index#index"
    match '/e404', to: "index#e404", via: "get"
    resources :news, only: [:index, :show]
    resources :promos, only: [:index, :show]
    resources :events, only: [:index]
    resources :payments, only: :destroy do
      get :print, on: :member
    end
    match '/ajax_get_tickets', to: "events#ajax_get_tickets" , via: "post"
    match '/ajax_get_tickets_by_ids', to: "events#ajax_get_tickets_by_ids" , via: "post"
    match '/ajax_get_ticket_price', to: "events#ajax_get_ticket_price" , via: "post"
    match '/buy_ticket/:id', to: "events#buy_ticket", via: "get"
    match '/confirm_order', to: "events#confirm_order", via: "get"
    match '/confirm_order', to: "events#confirm_order", via: "post"
    match '/success_book_tickets', to: "events#success_book_tickets", via: "get"

    match '/choose_delivery_data', to: "events#choose_delivery_data", via: "get"
    match '/choose_delivery_data', to: "events#choose_delivery_data", via: "post"

    match '/confirm_book', to: "events#confirm_book", via: "get"
    match '/confirm_book', to: "events#confirm_book", via: "post"

    match '/internet_payment', to: "events#internet_payment", via: "get"
    match '/choose_delivery_methods', to: "events#choose_delivery_methods", via: "get"
    match '/choose_delivery_methods', to: "events#choose_delivery_methods", via: "post"
    match '/order_result', to: "events#order_result", via: "get"


    resources :sale_points, only: [:index, :show]
    
    match '/archive', to: "index#archive", via: "get"
    get '/archive/(:category)/(:id)/', to: 'index#archive_item', as: :archive_item

    match '/ajax_create_call_request', to: "index#ajax_create_call_request", via: "post"
    match '/ajax_create_certificate_request', to: "index#ajax_create_certificate_request", via: "post"
    match '/ajax_get_max_ticket_counter', to: "index#ajax_get_max_ticket_counter", via: "post"
    match '/ajax_create_collective_order', to: "events#ajax_create_collective_order", via: "post"

    match '/ajax_get_events_info', to: "index#ajax_get_events_info" , via: "post"
    match '/ajax_get_sale_point_coords', to: "sale_points#ajax_get_sale_point_coords", via: "post"
    match '/ajax_get_cities', to: "sale_points#ajax_get_cities", via: "post"
    match '/ajax_get_sale_points_by_city_id', to: "sale_points#ajax_get_sale_points_by_city_id", via: "post"
    match '/ajax_get_city', to: "sale_points#ajax_get_city", via: "post"
    # match '/certificate', to: "index#certificate", via: "get"
    match "/search", to: "index#search", via: "get"



    match "/profile",  to: "users#profile", via: "get"
    match "/save_profile",  to: "users#save_profile", via: "post"
    match "/change_password", to: "users#change_password", via: "post"
    # Регистрация пользователя
    match "/register", to: "users#register", via: "get"
    match "/register", to: "users#register", via: "post"
    match "/autorization", to: "users#autorization", via: "get"
    match "/register_complete", to: "users#register_complete", via: "get"
    match "/confirm", to: "users#confirm", via: "get"
    match "/confirm_complete", to: "users#confirm_complete", via: "get"
    # Авторизация пользователя
    match "/log_in",  to: "users#log_in",  via: "post"
    match "/log_out",  to: "users#log_out", via: "delete"
    # Восстановление пароля
    match "/restore", to: "users#restore", via: "get"
    match "/restore", to: "users#restore", via: "post"
    match "/restore_complete", to: "users#restore_complete", via: "get"
    match "/set_new_password", to: "users#set_new_password", via: "get"
    match "/set_new_password", to: "users#set_new_password", via: "post"
    match "/set_new_password_complete", to: "users#set_new_password_complete", via: "get"
    # История заказов
    match "/user_history", to: "users#history", via: "get"
    match "/payments/:id", to: "users#payments", via: "get"
    match "/payment_save", to: "users#payment_save", via: "post"

    match 'payments/result', to: "payments#result", via: "get"
    match 'payments/success', to: "payments#success", via: "get"
    match 'payments/fail', to: "payments#fail", via: "get"
    match 'payments/pay', to: "payments#pay", via: "get"

    match 'schemas', to: "schemas#index", via: "get"

    match "/:alias", to: "index#static_page", via: "get", :as => :desctop_static_pages
    get '/events/:url', to: 'events#show', as: :event

    # toDO: Убрать при внедрении модуля Пользователи
    match '/cart', to: "index#cart", via: "get"
    # match '/certificate', to: "index#certificate", via: "get"
    match '/order', to: "index#order", via: "get"
    match '/profile', to: "index#profile", via: "get"
    resources :events, only: [:index]

    resources :promos, only: [:index, :show]
    resources :search, only: [:index]

    match '/t', to: "index#index", via: "get"
  end

  scope "/admin" do
    mount ModulesD::Engine => "/"
    mount SystemMainItemsD::Engine => "/"
    mount SystemUsersAuthorizeD::Engine => "/"
    mount AccessRightsD::Engine => "/"
    mount SystemUserActionsLogD::Engine => "/"
    scope module: "admin" do
      match '/main_page', to: "index#index", via: "get"
      resources :news_items
      resources :promo_items
      resources :halls
      resources :event_types
      resources :event_items
      resources :banners
      resources :sale_point_items
      resources :cities
      resources :partner_items
      resources :settings
      resources :static_pages
      resources :navigation_menus
      resources :requests, only: [:index]
      resources :users
      resources :tickets do
        put :batch_update, on: :collection
        get :register, on: :member
      end
      resources :ticket_reverts
      resources :ticket_discounts
      resources :ticket_states
      resources :seat_item_types
      resources :payments
      # resources :blank_requests
      resources :blank_defects
      resources :blank_transfers, except: [:destroy, :show] do
        member do
          put :cancel
          put :finish
        end
      end

      resources :money_transfers, except: [:destroy, :show] do
        member do
          put :cancel
          put :finish
        end
      end

      match '/blank_requests/:id', to: "blank_requests#update", via: "post"
      match '/schemas/:event_id', to: "schemas#index", via: "get"
      match '/only_schema/:event_id', to: "schemas#only_schema", via: "get"

      match '/ajax_book_ticket_by_ids', to: "schemas#ajax_book_ticket_by_ids", via: "post"
      match '/ajax_get_ticket_by_id', to: "schemas#ajax_get_ticket_by_id", via: "post"
      match '/ajax_admin_get_tickets', to: "schemas#ajax_get_tickets", via: "post"
      match '/ajax_release_tickets', to: "schemas#ajax_release_tickets", via: "post"

      # Расширенные схемы
      match '/advanced_schemas/:event_id', to: "advanced_schemas#index", via: "get"
      match '/ajax_change_ticket_states', to: "advanced_schemas#ajax_change_ticket_states", via: "post"
      match '/ajax_change_ticket_prices', to: "advanced_schemas#ajax_change_ticket_prices", via: "post"
      match '/ajax_get_tickets_info', to: "advanced_schemas#ajax_get_tickets_info", via: "post"
      match '/ajax_change_ticket_discounts', to: "advanced_schemas#ajax_change_ticket_discounts", via: "post"
      match '/ajax_buy_ticket', to: "advanced_schemas#ajax_buy_ticket", via: "post"
      match '/ajax_cancel_printed_ticket', to: "advanced_schemas#ajax_cancel_printed_ticket", via: "post"
      match '/ajax_refund_ticket', to: "advanced_schemas#ajax_refund_ticket", via: "post"
      match '/ajax_book_ticket', to: "advanced_schemas#ajax_book_ticket", via: "post"
      match '/ajax_print_ticket', to: "advanced_schemas#ajax_print_ticket", via: "post"
      # match '/ajax_get_normal_kassir_counter', to: "advanced_schemas#ajax_get_normal_kassir_counter", via: "post"
      # match '/ajax_get_special_kassir_counter', to: "advanced_schemas#ajax_get_special_kassir_counter", via: "post"

      # Кассир
      match '/ajax_get_reservation_payment_by_number', to: "schemas#ajax_get_reservation_payment_by_number", via: "post"
      match '/ajax_buy_ticket_by_ids', to: "schemas#ajax_buy_ticket_by_ids", via: "post"
      match '/ajax_create_payment_and_buy_ticket_by_ids', to: "schemas#ajax_create_payment_and_buy_ticket_by_ids", via: "post"
      match '/ajax_create_payment_and_book_ticket_by_ids', to: "schemas#ajax_create_payment_and_book_ticket_by_ids", via: "post"
      match '/ajax_usual_get_tickets_info', to: "schemas#ajax_usual_get_tickets_info", via: "post"
      # match '/accept_blank_request', to: "schemas#accept_blank_request", via: "get"
      # match '/accept_blank_request', to: "schemas#accept_blank_request", via: "post"
      match '/available_blank_counters_by_kassir', to: "schemas#available_blank_counters_by_kassir", via: "get"
      # match '/delivery_blank_counters_by_kassir', to: "schemas#delivery_blank_counters_by_kassir", via: "get"
      match '/print_blank', to: "schemas#print_blank", via: "get"
      # match '/ajax_accept_blanks', to: "schemas#ajax_accept_blanks", via: "post"

      # Администратор кассиров
      resources :reports, only: :index do
        collection do
          get :current_blanks_state
          get :note_per_blanks_report
          get :cashbox_release
          get :kassir_productivity
          get :event_items
        end
      end
      # match "/reports/blank_kassir_report", to: "reports#blank_kassir_report" , via: "get"
      # match "/reports/kassir_blanks_state", to: "reports#kassir_blanks_state" , via: "get"
      # Администратор кассиров
      # match '/refresh_blank_counters', to: "blank_counters#refresh_blank_counters", via: "post"
    end
  end

end
